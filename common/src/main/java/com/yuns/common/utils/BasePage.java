package com.yuns.common.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 用来接收页面传过来的查询字段   对象
 */
@Data
@ApiModel("分页模型")
public class BasePage<T> implements Serializable {


    @ApiModelProperty(value = "是否是分页")
    private boolean _search;

    //时间戳（毫秒）
    @ApiModelProperty(value = "删选参数")
    private T data;

    //	每页显示条数
    @ApiModelProperty(value = "每页显示条数")
    private int pageSize;

    //当前页数
    @ApiModelProperty(value = "当前页数")
    private int pageNumber;

    @ApiModelProperty(value = "结束时间", required = false)
    private String endTm;

    @ApiModelProperty(value = "开始时间", required = false)
    private String bgTm;

}
