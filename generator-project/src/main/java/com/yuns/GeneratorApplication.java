package com.yuns;

import com.yuns.util.MpGenerator;

/**
 * @Author: Clarck
 * @Date: 2018/7/4 10:03
 */
public class GeneratorApplication {
    public static void main(String[] args) {
        final String SAVE_PATH = "G:\\nanjingBoot\\nanjing\\src\\main\\java\\";
        final String JS_PATH = "";
        final String VUE_PATH = "";
        final String SAVE_MAPPERXML_PATH = "G:\\nanjingBoot\\nanjing\\src\\main\\resources\\mapper\\";
        MpGenerator.generator("com/yuns", SAVE_PATH,SAVE_MAPPERXML_PATH,JS_PATH,VUE_PATH);
    }
}
