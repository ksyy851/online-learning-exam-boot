package com.yuns.util;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Mr.Chen
 * @Date: 2018/7/4 10:09
 */
public class MpGenerator {

    /**
     * @param packagePath 包路径
     * @param savePath    保存路径
     * @param JS_PATH
     * @param VUE_PATH
     */
    public static void generator(String packagePath, String savePath, String SAVE_MAPPERXML_PATH, String JS_PATH, String VUE_PATH) {
        String FULL_PATH = savePath + "/" + packagePath + "/";
        final String CONTROLLER_PATH = FULL_PATH + "com.yuns.web/controller/";
        final String ENTITY_PATH = FULL_PATH + "model/entity/";
        String DTO_PATH = FULL_PATH + "com.yuns.web/dto/";
        String MAPPER_PATH = FULL_PATH + "model/mapper/";
        String MAPPER_XML_PATH = savePath + "resources/mapper/";
        String SERVICE_PATH = FULL_PATH + "model/service/";
        String SERVICE_IMPL_PATH = FULL_PATH + "model/service/impl/";
        AutoGenerator mpg = new AutoGenerator();
        //全局配置
        GlobalConfig gc = new GlobalConfig();
        gc.setOutputDir(savePath);
        gc.setFileOverride(true);
        gc.setActiveRecord(true);
        gc.setEnableCache(false);
        gc.setBaseResultMap(true);
        gc.setAuthor("luwenchao");
        gc.setServiceName("%sService");
        mpg.setGlobalConfig(gc);

        //数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL);
        dsc.setTypeConvert(new MySqlTypeConvert() {
            @Override
            public DbColumnType processTypeConvert(String fieldType) {
                return super.processTypeConvert(fieldType);
            }
        });
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("nanjing");
        dsc.setPassword("nanjing");
//        dsc.setPassword("root");
        dsc.setUrl("jdbc:mysql://132.232.80.207:3306/nanjing?characterEncoding=UTF8");
        mpg.setDataSource(dsc);
        List<String> list=new ArrayList<String>();

        list.add("operation_log");
        list.add("sys_login");
        list.add("sys_user_role");
        list.add("sys_user_group");
        list.add("sys_user");
        list.add("sys_supp_info");
        list.add("sys_rule_record");
        list.add("sys_roleinfo");
        list.add("sys_role_permission");
        list.add("sys_role");
        list.add("sys_permission");
        list.add("sys_msg_info");
        list.add("sys_loginfo");
        list.add("sys_group_role");
        list.add("sys_group_permission");
        list.add("sys_group");
        list.add("sys_dict");
        list.add("sys_dept");

        list.add("vip_note_record");
        list.add("vip_member_info");
        list.add("vip_favorite_record");
        list.add("vip_enterpries_info");

        list.add("info_type_e");
        list.add("info_rdxw_e");
        list.add("info_doc_i");
        list.add("info_content_e");


        list.add("course_base");
        list.add("course_type_rel");
        list.add("course_type");
        list.add("course_info");
        list.add("course_doc_rel");
        list.add("course_chapter");
        list.add("course_chapter_rel");

        list.add("live_video_file");
        list.add("gt_badword");
        list.add("gt_exam");
        list.add("gt_exam_answer");
        list.add("gt_exam_answer_record");
        list.add("gt_exam_infor");
        list.add("gt_exam_period_log");
        list.add("gt_exam_question");
        list.add("gt_exam_question_option");
        list.add("gt_exam_record");
        list.add("gt_exam_title_err");
        list.add("gt_exam_title_info");
        list.add("gt_exam_title_list");
        list.add("gt_exam_titleinfo");
        list.add("gt_exam_student");
        list.add("gt_question");
        list.add("gt_question_option");
        list.add("gt_testing");
        list.add("gt_testing_wrong");

        list.add("live_base");
        list.add("live_info");

        list.add("study_video_info");
        list.add("study_video_base");
        list.add("study_char_info");
        list.add("study_char_base");
        list.add("study_char_doc_rel");

        list.add("company_user_rel");
        list.add("company_user_base");
        list.add("company_info_base");
        list.add("company_doc_rel");

        list.add("info_sms_delivery");
        list.add("info_chat_manag");

        list.add("course_note");
        list.add("course_watch_record");
        list.add("live_answer");
        list.add("live_comment");

        list.add("company_user_integral");
        list.add("exam_title_note");
        list.add("study_watch_record");
        list.add("live_user_rel");

        //策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        String[] d=new String[list.size()];
        String[] strings = list.toArray(d);
        strategy.setExclude(strings);
        strategy.setRestControllerStyle(true);
        mpg.setStrategy(strategy);

        //1. 全局配置
        GlobalConfig config = new GlobalConfig();
        config.setFileOverride(false).setEnableCache(false).setIdType(IdType.AUTO).setBaseResultMap(true); // 文件不覆盖

        mpg.setGlobalConfig(config);
        //包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent("com.yuns");
        pc.setEntity("model.entity");
        pc.setController("com.yuns.web.sys.controller");
        pc.setService("model.service");
        pc.setServiceImpl("model.service.impl");
        pc.setMapper("model.mapper");
        pc.setXml("model.mapper");
        mpg.setPackageInfo(pc);

        //注入自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
            }
        };

        //关闭自身模板生成
        TemplateConfig tc = new TemplateConfig();
        tc.setController(null);
        tc.setService(null);
        tc.setServiceImpl(null);
        tc.setEntity(null);
        tc.setMapper(null);
        tc.setXml(null);
        mpg.setTemplate(tc);

        //生成文件
        List<FileOutConfig> focList = new ArrayList<FileOutConfig>();

        focList.add(new FileOutConfig("/template/controller.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {

                    return CONTROLLER_PATH + tableInfo.getControllerName() + ".java";

            }
        });

        focList.add(new FileOutConfig("/template/entity.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {

                return ENTITY_PATH + tableInfo.getEntityName() + ".java";

            }
        });

//        focList.add(new FileOutConfig("/template/dto.java.vm") {
//
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//
//                    return DTO_PATH + tableInfo.getEntityName() + "Dto" + ".java";
//
//
//            }
//        });

        focList.add(new FileOutConfig("/template/mapper.java.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {

                    return MAPPER_PATH+ tableInfo.getMapperName() + ".java";

            }
        });

        focList.add(new FileOutConfig("/template/mapper.xml.vm") {
            @Override
            public String outputFile(TableInfo tableInfo) {

                    return SAVE_MAPPERXML_PATH  + tableInfo.getXmlName() + ".xml";

            }
        });

        focList.add(new FileOutConfig("/template/service.java.vm") {

            @Override
            public String outputFile(TableInfo tableInfo) {

                    return SERVICE_PATH + tableInfo.getServiceName() + ".java";

            }
        });

        focList.add(new FileOutConfig("/template/serviceImpl.java.vm") {

            @Override
            public String outputFile(TableInfo tableInfo) {

                    return SERVICE_IMPL_PATH + tableInfo.getServiceImplName() + ".java";

            }
        });
//        focList.add(new FileOutConfig("/template/index.js.vm") {
//
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//
//                    return JS_PATH + tableInfo.getEntityName().toLowerCase() + ".js";
//
//            }
//
//        });
//        focList.add(new FileOutConfig("/template/index.vue.vm") {
//            @Override
//            public String outputFile(TableInfo tableInfo) {
//                    return VUE_PATH+ tableInfo.getEntityName().toLowerCase() + ".vue";
//            }
//        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        //执行生成操作
        mpg.execute();
    }

}
