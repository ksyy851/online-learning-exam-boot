# 在线学习考试

#### 介绍
在线学习考试系统-后端(包含门户和后台管理)

#### 软件架构

1. 基于Springcloud(Finchley.SR2版本)开发。
2. 开发环境：JDK1.8
3. 基础框架用到的组件:Springboot、MybatisPlus、Eureka、Zuul、Ribbon、Fegin、Hystrix、
4. 第三方组件：Redis、FastDfs、ES、Shior、Jwt
5. 数据库：Mysql
6. 持久层：Mybatis
7. 数据交换格式：Json

#### 安装教程

1.  mvn clean
2.  mvn install

#### 演示地址

1.  门户：http://47.99.194.179/#/    账号：钢铁公司  密码：123456
2.  后台管理：http://47.99.194.179:8090/  账号：wxj   密码：123456


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
