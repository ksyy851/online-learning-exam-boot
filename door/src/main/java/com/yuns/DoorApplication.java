package com.yuns;

import com.yuns.common.utils.SpringBeanUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@EnableFeignClients
@ServletComponentScan
public class DoorApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(DoorApplication.class, args);
        SpringBeanUtil.setApplicationContext(applicationContext);
    }

    /**
     * 文件上传配置
     *
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //文件最大
        factory.setMaxFileSize("30240KB"); //KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize("202400KB");
        return factory.createMultipartConfig();
    }

    @Override
    protected WebApplicationContext run(SpringApplication application) {
        WebApplicationContext run = super.run(application);
        SpringBeanUtil.setApplicationContext(run);
        return run;
    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(DoorApplication.class);
    }

}
