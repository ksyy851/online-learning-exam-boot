package com.yuns.web.study.controller;

import com.yuns.web.info.vo.InfoDocI;
import com.yuns.web.study.api.StudyApi;
import com.yuns.web.study.vo.StudyCharVO;
import com.yuns.web.study.vo.StudyVideoVo;
import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.study.vo.StudyWatchRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 学习资料
 * @author liuchengwen
 * @since 2020-11-10
 */
@Api(tags = "学习资料 - Controller")
@RestController
@RequestMapping("/door/study")
public class StudyController {

    @Autowired
    StudyApi studyApi;

    @ApiOperation(value = "左侧查询 - queryInfoDocI")
    @PostMapping(value = "queryInfoDocI")
    public ResultJson queryInfoDocI(@RequestBody BasePage<InfoDocI> frontPage) {
        return studyApi.queryInfoDocI(frontPage);
    }

    @ApiOperation(value = "文字资料查询 - queryChars")
    @PostMapping(value = "queryChars")
    public ResultJson queryChars(@RequestBody BasePage<StudyCharVO> frontPage) {
        return studyApi.queryChars(frontPage);
    }

    @ApiOperation(value = "mp4资料查询 - queryVideos")
    @PostMapping(value = "queryVideos")
    public ResultJson queryVideos(@RequestBody BasePage<StudyVideoVo> frontPage) {
        return studyApi.queryVideos(frontPage);
    }

    @ApiOperation(value = "学习记录插入(直播/资料)  - insertWatchRecord")
    @PostMapping(value = "insertWatchRecord")
    public ResultJson insertWatchRecord(@RequestBody StudyWatchRecord frontPage) {
        return studyApi.insertWatchRecord(frontPage);
    }

    @ApiOperation(value = "学习记录查询(直播/资料) - queryWatchRecord")
    @PostMapping(value = "queryWatchRecord")
    public ResultJson queryWatchRecord(@RequestBody StudyWatchRecord frontPage) {
        return studyApi.queryWatchRecord(frontPage);
    }
}
