package com.yuns.web.study.api;

import com.yuns.web.info.vo.InfoDocI;
import com.yuns.web.study.vo.StudyCharVO;
import com.yuns.web.study.vo.StudyVideoVo;
import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.study.vo.StudyWatchRecord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 学习资料服务
 */
@FeignClient(name = "manage-service")
public interface StudyApi {

    @PostMapping("infoDocI/queryForDoor")
    ResultJson queryForDoor();

    @PostMapping("infoDocI/query")
    ResultJson queryInfoDocI(@RequestBody BasePage<InfoDocI> frontPage);

    @PostMapping("study/video/query")
    ResultJson queryVideos(@RequestBody BasePage<StudyVideoVo> frontPage);

    @PostMapping("study/char/query")
    ResultJson queryChars(@RequestBody BasePage<StudyCharVO> frontPage);

    @PostMapping("studyWatchRecord/insert")
    ResultJson insertWatchRecord(@RequestBody StudyWatchRecord model);

    @PostMapping("studyWatchRecord/query")
    ResultJson queryWatchRecord(@RequestBody StudyWatchRecord model);
}
