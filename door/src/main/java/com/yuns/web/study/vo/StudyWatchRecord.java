package com.yuns.web.study.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 直播-资料观看记录表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-20
 */
@Data
@TableName("study_watch_record")
public class StudyWatchRecord extends Model<StudyWatchRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    @ApiModelProperty(value = "用户ID")
    @TableField("company_user_id")
    private Integer companyUserId;
    /**
     * 观看类ID
     */
    @ApiParam(value = "观看类ID")
    @ApiModelProperty(value = "观看类ID")
    @TableField("watch_id")
    private Integer watchId;
    /**
     * 0:直播;1:视频资料;2:文字资料
     */
    @ApiParam(value = "0:直播;1:视频资料;2:文字资料")
    @ApiModelProperty(value = "0:直播;1:视频资料;2:文字资料")
    private String type;
    /**
     * 观看时间
     */
    @ApiParam(value = "观看时间")
    @ApiModelProperty(value = "观看时间")
    private String time;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
