package com.yuns.web.center.api;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.param.CerterUserParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "manage-service")
@RequestMapping("centerExamRecord")
public interface CenterExamRecordApi {

    @PostMapping("queryExamRecord")
    ResultJson queryExamRecord(@RequestBody BasePage<CerterUserParam> frontPage);

}
