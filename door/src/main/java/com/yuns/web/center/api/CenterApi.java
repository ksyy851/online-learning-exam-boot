package com.yuns.web.center.api;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.param.CerterUserParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 个人中心服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("center")
public interface CenterApi {

    @PostMapping("queryCerterUserInfo")
    ResultJson queryCerterUserInfo(@RequestBody CerterUserParam certerUserParam);

    @PostMapping(value = "queryUserStatisticsInfo" )
    public ResultJson queryUserStatisticsInfo(@RequestBody CerterUserParam certerUserParam);

    @PostMapping(value = "queryCourseHoursStatistics" )
    public ResultJson queryCourseHoursStatistics(@RequestBody CerterUserParam certerUserParam);

    @PostMapping(value = "queryIntegralStatistics" )
    public ResultJson queryIntegralStatistics(@RequestBody CerterUserParam certerUserParam);

}
