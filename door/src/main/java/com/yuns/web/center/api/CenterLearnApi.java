package com.yuns.web.center.api;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.param.CerterUserParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 个人中心 - 学习记录
 */
@FeignClient(name = "manage-service")
@RequestMapping("centerLearn")
public interface CenterLearnApi {

    @PostMapping(value = "queryLearnCourse" )
    public ResultJson queryLearnCourse(@RequestBody BasePage<CerterUserParam> frontPage);

    @PostMapping(value = "queryLearnLive" )
    public ResultJson queryLearnLive(@RequestBody BasePage<CerterUserParam> frontPage);

    @PostMapping(value = "queryLearnVideo" )
    public ResultJson queryLearnVideo(@RequestBody BasePage<CerterUserParam> frontPage);

    @PostMapping(value = "queryLearnText" )
    public ResultJson queryLearnText(@RequestBody BasePage<CerterUserParam> frontPage);

}
