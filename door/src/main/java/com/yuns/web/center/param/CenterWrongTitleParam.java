package com.yuns.web.center.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description:个人中心-我的笔记 入参
 * User: zhangzhan
 * Date: 2020-11-14
 */

@Data
public class CenterWrongTitleParam implements Serializable {

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 笔记类型
     */
    @ApiModelProperty(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）", required = true)
    private Integer wrongTitleType;
}
