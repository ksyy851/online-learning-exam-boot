package com.yuns.web.center.api;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.param.CerterUserParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 个人中心 - 我的笔记
 */
@FeignClient(name = "manage-service")
@RequestMapping("centerNote")
public interface CenterNoteApi {

    @PostMapping(value = "queryCourseNote" )
    public ResultJson queryCourseNote(@RequestBody BasePage<CerterUserParam> frontPage);

    @PostMapping(value = "queryExamTitleNote" )
    public ResultJson queryExamTitleNote(@RequestBody BasePage<CerterUserParam> frontPage);

}
