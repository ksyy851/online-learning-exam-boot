package com.yuns.web.center.controller;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.api.CenterExamRecordApi;
import com.yuns.web.center.api.CenterIntegralApi;
import com.yuns.web.center.param.CerterUserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 我的积分 - Controller" )
@RestController
@RequestMapping("/door/centerIntegral" )
public class CenterIntegralController {

    @Autowired
    private CenterIntegralApi centerIntegralApi;

    @ApiOperation(value = "积分记录 - queryIntegralRecord" )
    @PostMapping(value = "queryIntegralRecord" )
    public ResultJson queryIntegralRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerIntegralApi.queryIntegralRecord(frontPage);
    }

    @ApiOperation(value = "积分排名 - queryIntegralRank" )
    @PostMapping(value = "queryIntegralRank" )
    public ResultJson queryIntegralRank(@RequestBody BasePage frontPage){
        return centerIntegralApi.queryIntegralRank(frontPage);
    }

}
