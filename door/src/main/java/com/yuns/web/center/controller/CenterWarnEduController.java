package com.yuns.web.center.controller;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.api.CenterWarnEduApi;
import com.yuns.web.center.param.CerterUserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 警示教育 - Controller" )
@RestController
@RequestMapping("/door/centerWarnEdu" )
public class CenterWarnEduController {
    @Autowired
    private CenterWarnEduApi centerWarnEduApi;

    @ApiOperation(value = "警示教育记录 - queryWarnEduRecord" )
    @PostMapping(value = "queryWarnEduRecord" )
    public ResultJson queryWarnEduRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return  centerWarnEduApi.queryWarnEduRecord(frontPage);
    }

    @ApiOperation(value = "学时记录 - queryHoursRecord" )
    @PostMapping(value = "queryHoursRecord" )
    public ResultJson queryHoursRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return  centerWarnEduApi.queryHoursRecord(frontPage);
    }
}
