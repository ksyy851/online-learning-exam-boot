package com.yuns.web.center.api;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.param.CenterWrongTitleParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 个人中心服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("centerWrongTitle")
public interface CenterWrongTitleApi {

    @PostMapping(value = "queryWrongTitle" )
    public ResultJson queryWrongTitle(@RequestBody BasePage<CenterWrongTitleParam> frontPage);

}
