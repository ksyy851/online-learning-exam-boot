package com.yuns.web.center.api;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.param.CerterUserParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 个人中心 警示教育
 */
@FeignClient(name = "manage-service")
@RequestMapping("centerWarnEdu")
public interface CenterWarnEduApi {

    @PostMapping(value = "queryWarnEduRecord" )
    public ResultJson queryWarnEduRecord(@RequestBody BasePage<CerterUserParam> frontPage);

    @PostMapping(value = "queryHoursRecord" )
    public ResultJson queryHoursRecord(@RequestBody BasePage<CerterUserParam> frontPage);

}
