package com.yuns.web.center.controller;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.api.CenterApi;
import com.yuns.web.center.api.CenterNoteApi;
import com.yuns.web.center.param.CerterUserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 - 我的笔记 Controller" )
@RestController
@RequestMapping("/door/centerNote" )
public class CenterNoteController {

    @Autowired
    private CenterNoteApi centerNoteApi;

    @ApiOperation(value = "课堂笔记 - queryCourseNote" )
    @PostMapping(value = "queryCourseNote" )
    public ResultJson queryCourseNote(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerNoteApi.queryCourseNote(frontPage);
    }

    @ApiOperation(value = "题目笔记 - queryExamTitleNote" )
    @PostMapping(value = "queryExamTitleNote" )
    public ResultJson queryExamTitleNote(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerNoteApi.queryExamTitleNote(frontPage);
    }
}
