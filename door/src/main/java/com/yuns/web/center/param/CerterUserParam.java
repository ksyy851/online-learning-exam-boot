package com.yuns.web.center.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description:个人中心-用户信息查询入参
 * User: zhangzhan
 * Date: 2020-11-14
 */
@Data
public class CerterUserParam implements Serializable {

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

}
