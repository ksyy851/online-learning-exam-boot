package com.yuns.web.center.controller;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.api.CenterApi;
import com.yuns.web.center.api.CenterLearnApi;
import com.yuns.web.center.param.CerterUserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 - 学习记录 Controller" )
@RestController
@RequestMapping("/door/centerLearn" )
public class CenterLearnController {

    @Autowired
    private CenterLearnApi centerLearnApi;

    @ApiOperation(value = "课程 - queryLearnCourse" )
    @PostMapping(value = "queryLearnCourse" )
    public ResultJson queryLearnCourse(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerLearnApi.queryLearnCourse(frontPage);
    }

    @ApiOperation(value = "直播 - queryLearnLive" )
    @PostMapping(value = "queryLearnLive" )
    public ResultJson queryLearnLive(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerLearnApi.queryLearnLive(frontPage);
    }

    @ApiOperation(value = "视频资料 - queryLearnVideo" )
    @PostMapping(value = "queryLearnVideo" )
    public ResultJson queryLearnVideo(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerLearnApi.queryLearnVideo(frontPage);
    }

    @ApiOperation(value = "文字资料 - queryLearnText" )
    @PostMapping(value = "queryLearnText" )
    public ResultJson queryLearnText(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerLearnApi.queryLearnText(frontPage);
    }

}
