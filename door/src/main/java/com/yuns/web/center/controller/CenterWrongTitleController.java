package com.yuns.web.center.controller;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.api.CenterWrongTitleApi;
import com.yuns.web.center.param.CenterWrongTitleParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 错题报告 - Controller" )
@RestController
@RequestMapping("/door/centerWrongTitle" )
public class CenterWrongTitleController {

    @Autowired
    private CenterWrongTitleApi centerWrongTitleApi;

    @ApiOperation(value = "错题报告 - queryWrongTitle" )
    @PostMapping(value = "queryWrongTitle" )
    public ResultJson queryWrongTitle(@RequestBody BasePage<CenterWrongTitleParam> frontPage){
        return centerWrongTitleApi.queryWrongTitle(frontPage);
    }


}
