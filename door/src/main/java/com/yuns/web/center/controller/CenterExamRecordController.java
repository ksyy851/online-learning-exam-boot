package com.yuns.web.center.controller;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.api.CenterApi;
import com.yuns.web.center.api.CenterExamRecordApi;
import com.yuns.web.center.param.CerterUserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 考试记录 - Controller" )
@RestController
@RequestMapping("/door/centerExamRecord" )
public class CenterExamRecordController {

    @Autowired
    private CenterExamRecordApi centerExamRecordApi;

    @ApiOperation(value = "考试记录 - queryExamRecord" )
    @PostMapping(value = "queryExamRecord" )
    public ResultJson queryExamRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return centerExamRecordApi.queryExamRecord(frontPage);
    }

}
