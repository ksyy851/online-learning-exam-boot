package com.yuns.web.center.controller;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.api.CenterApi;
import com.yuns.web.center.param.CerterUserParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 - Controller" )
@RestController
@RequestMapping("/door/center" )
public class CenterController {

    @Autowired
    private CenterApi centerApi;

    @ApiOperation(value = "个人中心-用户信息查询 - queryCerterUserInfo" )
    @PostMapping(value = "queryCerterUserInfo" )
    public ResultJson queryCerterUserInfo(@RequestBody CerterUserParam certerUserParam){
        return centerApi.queryCerterUserInfo(certerUserParam);
    }

    @ApiOperation(value = "个人中心-积分、考试记录、学习记录等 - queryUserStatisticsInfo" )
    @PostMapping(value = "queryUserStatisticsInfo" )
    public ResultJson queryUserStatisticsInfo(@RequestBody CerterUserParam certerUserParam){
        return centerApi.queryUserStatisticsInfo(certerUserParam);
    }

    @ApiOperation(value = "个人中心-我的各类别课程学时统计 - queryCourseHoursStatistics" )
    @PostMapping(value = "queryCourseHoursStatistics" )
    public ResultJson queryCourseHoursStatistics(@RequestBody CerterUserParam certerUserParam){
        return centerApi.queryCourseHoursStatistics(certerUserParam);
    }

    @ApiOperation(value = "个人中心-我的各类积分占比 - queryIntegralStatistics" )
    @PostMapping(value = "queryIntegralStatistics" )
    public ResultJson queryIntegralStatistics(@RequestBody CerterUserParam certerUserParam){
        return centerApi.queryIntegralStatistics(certerUserParam);
    }

}
