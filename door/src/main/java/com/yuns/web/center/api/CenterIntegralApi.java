package com.yuns.web.center.api;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.center.param.CerterUserParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 个人中心 我的积分
 */
@FeignClient(name = "manage-service")
@RequestMapping("centerIntegral")
public interface CenterIntegralApi {

    @PostMapping(value = "queryIntegralRecord" )
    public ResultJson queryIntegralRecord(@RequestBody BasePage<CerterUserParam> frontPage);

    @PostMapping(value = "queryIntegralRank" )
    public ResultJson queryIntegralRank(@RequestBody BasePage frontPage);
}


