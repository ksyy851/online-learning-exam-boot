package com.yuns.web.info.controller;

import com.yuns.web.info.api.InfoApi;
import com.yuns.web.info.param.InfoContentEParam;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 信息咨询
 * @author liuChengWen
 * @since 2020-10-28
 */
@Api(tags = "信息咨询 - Controller")
@RestController
@RequestMapping("/door/infoContentE")
public class InfoController {

    @Autowired
    private InfoApi infoApi;

    @ApiOperation(value = "咨询查询 - selectInfoList")
    @PostMapping(value = "selectInfoList")
    public ResultJson selectInfoList(@RequestBody InfoContentEParam frontPage) {
        return infoApi.selectInfoList(frontPage);
    }

}
