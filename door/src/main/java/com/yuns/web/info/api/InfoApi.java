package com.yuns.web.info.api;

import com.yuns.web.info.param.InfoContentEParam;
import com.yuns.common.utils.ResultJson;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 信息咨询相关服务
 */
@FeignClient(name = "manage-service")
public interface InfoApi {
    @PostMapping("infoContentE/query")
    ResultJson selectInfoList(@RequestBody InfoContentEParam frontPage);
}
