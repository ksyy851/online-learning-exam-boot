package com.yuns.web.course.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.Date;

/**
 * <p>
 * 课程列表VO
 * </p>
 *
 * @author liuChengWen
 * @since 2020-10-26
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CourseVO {

    @TableId(value = "id")
    private Integer id;
    /**
     * 课程名称
     */
    @ApiParam(value = "课程名称")
    @ApiModelProperty(value = "课程名称")
    @TableField("course_name")
    private String courseName;
    /**
     * 积分
     */
    @ApiParam(value = "积分")
    @ApiModelProperty(value = "积分")
    private Integer integral;

    /**
     * 图片文件ID
     */
    @ApiParam(value = "图片文件ID")
    @ApiModelProperty(value = "图片文件ID")
    private String fileNo;
    /**
     * 课程详情
     */
    @ApiParam(value = "课程详情")
    @ApiModelProperty(value = "课程详情")
    @TableField("course_info")
    private String courseInfo;
    /**
     * 课程详情id
     */
    @ApiParam(value = "课程详情id")
    @ApiModelProperty(value = "课程详情id")
    @TableField("course_info_id")
    private Integer courseInfoId;
    /**
     * 上传时间
     */
    @ApiParam(value = "上传时间")
    @ApiModelProperty(value = "上传时间")
    @TableField("upload_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date uploadTime;
    /**
     * 课时
     */
    @ApiParam(value = "课时")
    @ApiModelProperty(value = "课时")
    private Integer period;

    /**
     * 课程类型ID
     */
    @ApiParam(value = "课程类型ID")
    @ApiModelProperty(value = "课程类型ID")
    @TableField("course_type_id")
    private String courseTypeId;

    /**
     * 课程类型
     */
    @ApiParam(value = "课程类型")
    @ApiModelProperty(value = "课程类型")
    @TableField("course_type")
    private String courseType;

    /**
     * 浏览量
     */
    @ApiParam(value = "浏览量")
    @ApiModelProperty(value = "浏览量")
    @TableField("page_views")
    private Integer pageViews;
    /**
     * 完成率
     */
    @ApiParam(value = "完成率")
    @ApiModelProperty(value = "完成率")
    @TableField("comple_rate")
    private String compleRate;
    /**
     * 上下架(0:上架;1:下架)
     */
    @ApiParam(value = "上下架(0:上架;1:下架)")
    @ApiModelProperty(value = "上下架(0:上架;1:下架)")
    private Integer type;

    @ApiParam("法律法规ID")
    @ApiModelProperty(value = "法律法规ID" ,required = false)
    private String docId;

    @ApiParam("法律法规名称")
    @ApiModelProperty(value = "法律法规名称" ,required = false)
    private String docName;

    @ApiParam("课程章节ID")
    @ApiModelProperty(value = "课程章节ID" ,required = false)
    private String chapterId;

    @ApiParam("课程章节")
    @ApiModelProperty(value = "课程章节" ,required = false)
    private String chapterName;

    @ApiParam("课程老师")
    @ApiModelProperty(value = "课程老师" ,required = false)
    private String chapterTeacherName;

    @ApiParam("章节视频名称")
    @ApiModelProperty(value = "章节视频名称" ,required = false)
    private String chapterFileName;

    @ApiParam("视频文件ID")
    @ApiModelProperty(value = "视频文件ID" ,required = false)
    private String chapterFileId;

    @ApiParam("视频文件URL")
    @ApiModelProperty(value = "视频文件URL" ,required = false)
    private String chapterFileUrl;

    @ApiParam("章节学时")
    @ApiModelProperty(value = "章节学时" ,required = false)
    private String chapterPeriod;

    @ApiParam("封面图URL")
    @ApiModelProperty(value = "封面图URL" ,required = false)
    private String fileUrl;
}
