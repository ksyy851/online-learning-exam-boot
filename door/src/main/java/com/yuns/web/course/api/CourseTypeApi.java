package com.yuns.web.course.api;

import com.yuns.web.course.param.CourseType;
import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "manage-service")
@RequestMapping("courseType")
public interface CourseTypeApi {

    @PostMapping(value = "query")
    ResultJson queryCourseType(@RequestBody BasePage<CourseType> frontPage);
}
