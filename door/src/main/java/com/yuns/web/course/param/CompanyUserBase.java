package com.yuns.web.course.param;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户基础表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
@Data
@TableName("company_user_base")
public class CompanyUserBase extends Model<CompanyUserBase> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户名
     */
    @ApiParam(value = "用户名")
    @ApiModelProperty(value = "用户名")
    @TableField("user_name")
    private String userName;
    /**
     * 昵称
     */
    @ApiParam(value = "昵称")
    @ApiModelProperty(value = "昵称")
    @TableField("nickname")
    private String nickname;
    /**
     * 身份证
     */
    @ApiParam(value = "身份证")
    @ApiModelProperty(value = "身份证")
    @TableField("id_card")
    private String idCard;
    /**
     * 工作岗位
     */
    @ApiParam(value = "工作岗位")
    @ApiModelProperty(value = "工作岗位")
    @TableField("job")
    private String job;
    /**
     * 所属学校
     */
    @ApiParam(value = "所属学校")
    @ApiModelProperty(value = "所属学校")
    @TableField("school_name")
    private String schoolName;
    /**
     * 用户类型(0:企业用户;1:学校学生用户;2:学校老师用户;3:个人用户;4:环保局用户)
     */
    @ApiParam(value = "用户类型(0:企业用户;1:学校学生用户;2:学校老师用户;3:个人用户;4:环保局用户)")
    @ApiModelProperty(value = "用户类型(0:企业用户;1:学校学生用户;2:学校老师用户;3:个人用户;4:环保局用户)",required = true)
    @TableField("user_type")
    private Integer userType;
    /**
     * 微信号
     */
    @ApiParam(value = "微信号")
    @ApiModelProperty(value = "微信号")
    private String wechat;
    /**
     * 电话号码
     */
    @ApiParam(value = "电话号码")
    @ApiModelProperty(value = "电话号码")
    private String tel;
    /**
     * 用户状态(0:启用;1:禁用)
     */
    @ApiParam(value = "用户状态(0:启用;1:禁用)")
    @ApiModelProperty(value = "用户状态(0:启用;1:禁用)")
    @TableField("user_status")
    private Integer userStatus;
    /**
     * 邮箱
     */
    @ApiParam(value = "邮箱")
    @ApiModelProperty(value = "邮箱")
    private String email;

    /**
     * 已学课时
     */
    @ApiParam(value = "已学课时")
    @ApiModelProperty(value = "已学课时")
    private Integer period;

    /**
     * 已获积分
     */
    @ApiParam(value = "已获积分")
    @ApiModelProperty(value = "已获积分")
    private Integer integral;
    /**
     * 录入时间
     */
    @ApiParam(value = "录入时间")
    @ApiModelProperty(value = "录入时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private Date createTime;
    /**
     * 是否删除(0:未删除;1:已删除)
     */
    @ApiParam(value = "是否删除(0:未删除;1:已删除)")
    @ApiModelProperty(value = "是否删除(0:未删除;1:已删除)")
    private Integer del;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
