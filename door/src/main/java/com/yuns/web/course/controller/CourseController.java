package com.yuns.web.course.controller;

import com.yuns.web.course.api.*;
import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.course.param.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author liuChengWen
 * @since 2020-10-26
 */
@Api(tags = "热点专题 - Controller" )
@RestController
@RequestMapping("/door/course" )
public class CourseController {

    @Autowired
    private CourseApi courseApi;

    @Autowired
    private CourseInfoApi courseInfoApi;

    @Autowired
    private CourseTypeApi courseTypeApi;

    @Autowired
    private CompanyUserBaseApi companyUserBaseApi;

    @Autowired
    private CourseWatchRecordApi courseWatchRecordApi;

    @Autowired
    private CompanyUserIntegralApi companyUserIntegralApi;

    @ApiOperation(value = "课程列表查询 - queryCourseList" )
    @PostMapping(value = "queryCourseList" )
    public ResultJson queryCourseList(@RequestBody CourseParam courseParam){
        return courseApi.queryCourseList(courseParam);
    }

    @ApiOperation(value = "课程类型查询 - queryCourseType" )
    @PostMapping(value = "queryCourseType" )
    public ResultJson queryCourseType(@RequestBody BasePage<CourseType> frontPage){
        return courseTypeApi.queryCourseType(frontPage);
    }

    @ApiOperation(value = "单个课程查询 - queryOne" )
    @PostMapping(value = "queryOne/{id}" )
    public ResultJson queryOne(@PathVariable("id") Integer id){
        return courseApi.queryOne(id);
    }

    @ApiOperation(value = "个人用户查询 - queryUser" )
    @PostMapping(value = "queryUser/{id}" )
    public ResultJson queryUser(@PathVariable("id") Integer id){
        return companyUserBaseApi.queryUser(id);
    }

    @ApiOperation(value = "更新个人课时/学时 - updateEachPeriod")
    @PostMapping(value = "updateEachPeriod" )
    public ResultJson updateEachPeriod(@RequestBody CompanyUserBase model){
        return companyUserBaseApi.updateEachPeriod(model);
    }

    @ApiOperation(value = "课程浏览量更新 - updatePageViews")
    @PostMapping(value = "updatePageViews")
    public ResultJson updatePageViews(@RequestBody CourseInfo model){
        return courseInfoApi.updatePageViews(model);
    }

    @ApiOperation(value = "用户课程-章节观看记录插入 - insertWatch")
    @PostMapping(value = "insertWatch")
    public ResultJson insertWatch(@RequestBody CourseWatchRecord model){
        return courseWatchRecordApi.insertWatch(model);
    }

    @ApiOperation(value = "用户课程-章节观看记录更新 - updateWatch")
    @PostMapping(value = "updateWatch")
    public ResultJson updateWatch(@RequestBody CourseWatchRecord model){
        return courseWatchRecordApi.updateWatch(model);
    }

    @ApiOperation(value = "用户课程-章节观看记录查询 - queryWatch")
    @PostMapping(value = "queryWatch")
    public ResultJson queryWatch(@RequestBody CourseWatchRecord model){
        return courseWatchRecordApi.queryWatch(model);
    }

    @ApiOperation(value = "用户课程(直播)-积分记录插入 - insertIntegral")
    @PostMapping(value = "insertIntegral")
    public ResultJson insertIntegral(@RequestBody CompanyUserIntegral model){
        return companyUserIntegralApi.insertIntegral(model);
    }
}
