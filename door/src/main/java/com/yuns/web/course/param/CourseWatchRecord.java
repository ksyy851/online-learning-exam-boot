package com.yuns.web.course.param;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 课程-章节观看记录表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-13
 */
@Data
@TableName("course_watch_record")
public class CourseWatchRecord extends Model<CourseWatchRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 课程ID
     */
    @ApiParam(value = "课程ID")
   @ApiModelProperty(value = "课程ID")
    @TableField("course_id")
    private Integer courseId;
    /**
     * 章节ID
     */
    @ApiParam(value = "章节ID")
   @ApiModelProperty(value = "章节ID")
    @TableField("chapter_id")
    private Integer chapterId;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
   @ApiModelProperty(value = "用户ID")
    @TableField("company_user_id")
    private Integer companyUserId;
    /**
     * 时间段
     */
    @ApiParam(value = "时间段")
   @ApiModelProperty(value = "时间段")
    private String section;
    /**
     * 是否看完(0:未看完;1:已看完)
     */
    @ApiParam(value = "是否看完(0:未看完;1:已看完)")
   @ApiModelProperty(value = "是否看完(0:未看完;1:已看完)")
    private Integer type;

    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField("watch_time")
    private Date watchTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
