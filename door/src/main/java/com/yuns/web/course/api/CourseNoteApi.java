package com.yuns.web.course.api;

import com.yuns.web.course.param.CourseNote;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 课堂笔记服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("courseNote")
public interface CourseNoteApi {

    @ApiOperation(value = "添加 - course_note")
    @PostMapping(value = "insert")
    ResultJson insert(@RequestBody CourseNote model);

    @ApiOperation(value = "修改笔记 - course_note")
    @PostMapping(value = "update")
    ResultJson update(@RequestBody CourseNote model);

    @ApiOperation(value = "删除 - course_note")
    @PostMapping(value = "delete/{id}")
    ResultJson delete(@PathVariable("id") Integer id);

    @ApiOperation(value = "笔记列表查询 - query")
    @PostMapping(value = "query")
    ResultJson queryCourseNote(@RequestBody CourseNote model);
}
