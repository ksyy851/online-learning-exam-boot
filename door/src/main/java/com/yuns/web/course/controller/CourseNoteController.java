package com.yuns.web.course.controller;

import com.yuns.web.course.api.CourseNoteApi;
import com.yuns.web.course.param.CourseNote;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "热点专题-课堂笔记 - Controller" )
@RestController
@RequestMapping("/door/courseNote" )
public class CourseNoteController {
    @Autowired
    private CourseNoteApi courseNoteApi;

    @ApiOperation(value = "添加 - course_note" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CourseNote model){
        return courseNoteApi.insert(model);
    }

    @ApiOperation(value = "修改笔记 - update" )
    @PostMapping(value = "update" )
    public ResultJson update(@RequestBody CourseNote model){
        return courseNoteApi.update(model);
    }

    @ApiOperation(value = "删除 - course_note")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id){
        return courseNoteApi.delete(id);
    }

    @ApiOperation(value = "笔记列表查询 - queryCourseNote")
    @PostMapping(value = "query")
    public ResultJson queryCourseNote(@RequestBody CourseNote model){
        return courseNoteApi.queryCourseNote(model);
    }
}
