package com.yuns.web.course.api;

import com.yuns.web.course.param.CourseInfo;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 课程详情服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("courseInfo")
public interface CourseInfoApi {

    @ApiOperation(value = "课程浏览量更新 - update")
    @PostMapping(value = "update")
    ResultJson updatePageViews(@RequestBody CourseInfo model);
}
