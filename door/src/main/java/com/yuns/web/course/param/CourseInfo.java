package com.yuns.web.course.param;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

import java.io.Serializable;

/**
 * <p>
 * 课程详情表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@TableName("course_info")
public class CourseInfo extends Model<CourseInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 课程ID
     */
    @ApiParam(value = "课程ID")
   @ApiModelProperty(value = "课程ID")
    private Integer pid;
    /**
     * 浏览量
     */
    @ApiParam(value = "浏览量")
   @ApiModelProperty(value = "浏览量")
    @TableField("page_views")
    private Integer pageViews;
    /**
     * 完成率
     */
    @ApiParam(value = "完成率")
   @ApiModelProperty(value = "完成率")
    @TableField("comple_rate")
    private String compleRate;
    /**
     * 上下架(0:上架;1:下架)
     */
    @ApiParam(value = "上下架(0:上架;1:下架)")
   @ApiModelProperty(value = "上下架(0:上架;1:下架)")
    private Integer type;
    /**
     * 操作时间
     */
    @ApiParam(value = "操作时间")
    @ApiModelProperty(value = "操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("create_time")
    private String createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getPageViews() {
        return pageViews;
    }

    public void setPageViews(Integer pageViews) {
        this.pageViews = pageViews;
    }

    public String getCompleRate() {
        return compleRate;
    }

    public void setCompleRate(String compleRate) {
        this.compleRate = compleRate;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "CourseInfo{" +
        ", id=" + id +
        ", pid=" + pid +
        ", pageViews=" + pageViews +
        ", compleRate=" + compleRate +
        ", type=" + type +
        ", createTime=" + createTime +
        "}";
    }
}
