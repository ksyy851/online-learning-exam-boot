package com.yuns.web.course.api;

import com.yuns.web.course.param.CourseWatchRecord;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 课程-章节观看记录
 */
@FeignClient(name = "manage-service")
@RequestMapping("courseWatchRecord")
public interface CourseWatchRecordApi {

    @ApiOperation(value = "用户课程-章节观看记录插入 - insert")
    @PostMapping(value = "insert")
    ResultJson insertWatch(@RequestBody CourseWatchRecord model);

    @ApiOperation(value = "用户课程-章节观看记录更新 - update")
    @PostMapping(value = "up")
    ResultJson updateWatch(@RequestBody CourseWatchRecord model);

    @ApiOperation(value = "用户课程-章节观看记录查询 - query")
    @PostMapping(value = "query")
    ResultJson queryWatch(@RequestBody CourseWatchRecord frontPage);
}
