package com.yuns.web.course.api;

import com.yuns.web.course.param.CourseParam;
import com.yuns.common.utils.ResultJson;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 热点专题服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("course")
public interface CourseApi {

    @PostMapping("query")
    ResultJson queryCourseList(@RequestBody CourseParam courseParam);

    @PostMapping("edit/{id}")
    ResultJson queryOne(@PathVariable("id") Integer id);
}
