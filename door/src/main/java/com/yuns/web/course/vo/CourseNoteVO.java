package com.yuns.web.course.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * <p>
 * 课程笔记VO
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
@Data
public class CourseNoteVO {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    @ApiModelProperty(value = "用户ID")
    @TableField("company_user_id")
    private String companyUserId;
    /**
     * 章节ID
     */
    @ApiParam(value = "章节ID")
    @ApiModelProperty(value = "章节ID")
    @TableField("chapter_id")
    private String chapterId;
    /**
     * 笔记内容
     */
    @ApiParam(value = "笔记内容")
    @ApiModelProperty(value = "笔记内容")
    private String note;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private String createTime;
    /**
     * 章节名称
     */
    @ApiParam(value = "章节名称")
    @ApiModelProperty(value = "章节名称")
    private String chapterName;
    /**
     * 用户名
     */
    @ApiParam(value = "用户名")
    @ApiModelProperty(value = "用户名")
    private String userName;
}
