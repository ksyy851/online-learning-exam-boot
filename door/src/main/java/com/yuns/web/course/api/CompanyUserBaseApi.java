package com.yuns.web.course.api;

import com.yuns.common.utils.BasePage;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.course.param.CompanyUserBase;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 用户服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("companyUserBase")
public interface CompanyUserBaseApi {

    @ApiOperation(value = "个人用户查询 - queryUser")
    @PostMapping(value = "edit/{id}")
    ResultJson queryUser(@PathVariable("id") Integer id);

    @ApiOperation(value = "更新个人课时/学时 - updateEachPeriod")
    @PostMapping(value = "updateEachPeriod")
    ResultJson updateEachPeriod(@RequestBody CompanyUserBase model);

    @ApiOperation(value = "用户查询 - query")
    @PostMapping(value = "query")
    ResultJson query(@RequestBody BasePage<CompanyUserBase> frontPage);

}
