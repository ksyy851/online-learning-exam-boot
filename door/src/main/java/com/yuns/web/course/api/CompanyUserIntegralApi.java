package com.yuns.web.course.api;

import com.yuns.web.course.param.CompanyUserIntegral;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 用户服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("companyUserIntegral")
public interface CompanyUserIntegralApi {

    @ApiOperation(value = "用户课程(直播)-积分记录插入 - insertIntegral")
    @PostMapping(value = "insert")
    ResultJson insertIntegral(@RequestBody CompanyUserIntegral model);
}
