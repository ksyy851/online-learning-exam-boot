package com.yuns.web.live.api;

import com.yuns.web.live.param.LiveInfo;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 直播课程详情服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("/liveInfo")
public interface LiveInfoApi {

    @ApiOperation(value = "直播浏览量更新 - update")
    @PostMapping(value = "update")
    ResultJson updatePageViews(@RequestBody LiveInfo model);
}
