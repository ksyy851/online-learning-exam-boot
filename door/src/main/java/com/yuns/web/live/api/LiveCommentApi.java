package com.yuns.web.live.api;

import com.yuns.web.live.param.LiveComment;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 直播课程-互动区服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("liveComment")
public interface LiveCommentApi {

    @ApiOperation(value = "添加 - live_comment")
    @PostMapping(value = "insert")
    ResultJson insert(@RequestBody LiveComment model);

    @ApiOperation(value = "删除 - live_comment")
    @PostMapping(value = "delete/{id}")
    ResultJson delete(@PathVariable("id") Integer id);

    @ApiOperation(value = "评论区查询 - live_comment")
    @PostMapping(value = "query")
    ResultJson query(@RequestBody LiveComment model);
}
