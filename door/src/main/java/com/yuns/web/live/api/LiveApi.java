package com.yuns.web.live.api;

import com.yuns.web.live.param.LiveParam;
import com.yuns.common.utils.ResultJson;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 直播课程服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("/live")
public interface LiveApi {

    @PostMapping("query")
    ResultJson queryCourseList(@RequestBody LiveParam liveParam);

    @PostMapping("edit/{id}")
    ResultJson queryOne(@PathVariable("id") Integer id);
}
