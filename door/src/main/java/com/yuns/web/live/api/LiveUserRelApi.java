package com.yuns.web.live.api;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.exam.param.QueryParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 直播课程详情服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("/liveUserRel")
public interface LiveUserRelApi {

    @ApiOperation(value = "判断用户是否有权限看直播 - judgeLive")
    @PostMapping(value = "judgeLive")
    ResultJson judgeLive(@RequestBody QueryParam frontPage);
}
