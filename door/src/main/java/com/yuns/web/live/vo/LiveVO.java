package com.yuns.web.live.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * <p>
 * 直播列表VO
 * </p>
 *
 * @author liuChengWen
 * @since 2020-10-26
 */
@Data
public class LiveVO {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 直播名称
     */
    @ApiParam(value = "直播名称")
    @ApiModelProperty(value = "直播名称")
    @TableField("live_name")
    private String liveName;
    /**
     * 直播老师名称
     */
    @ApiParam(value = "直播老师名称")
    @ApiModelProperty(value = "直播老师名称")
    @TableField("live_teacher_name")
    private String liveTeacherName;
    /**
     * 直播老师简介
     */
    @ApiParam(value = "直播老师简介")
    @ApiModelProperty(value = "直播老师简介")
    @TableField("live_teacher_info")
    private String liveTeacherInfo;
    /**
     * 总积分
     */
    @ApiParam(value = "总积分")
    @ApiModelProperty(value = "总积分")
    private Integer integral;
    /**
     * 学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private Integer period;
    /**
     * 直播介绍
     */
    @ApiParam(value = "直播介绍")
    @ApiModelProperty(value = "直播介绍")
    @TableField("live_info")
    private String liveInfo;
    /**
     * 直播时间
     */
    @ApiParam(value = "直播时间")
    @ApiModelProperty(value = "直播时间")
    @TableField("live_time")
    private String liveTime;
    /**
     * 直播时长
     */
    @ApiParam(value = "直播时长")
    @ApiModelProperty(value = "直播时长")
    @TableField("live_period")
    private Integer livePeriod;
    /**
     * 视频文件ID
     */
    @ApiParam(value = "视频文件ID")
    @ApiModelProperty(value = "视频文件ID")
    @TableField("live_file_no")
    private String liveFileNo;
    /**
     * 直播视频名
     */
    @ApiParam(value = "直播视频名")
    @ApiModelProperty(value = "直播视频名")
    @TableField("live_file_name")
    private String liveFileName;
    /**
     * 直播视频路径
     */
    @ApiParam(value = "直播视频路径")
    @ApiModelProperty(value = "直播视频路径")
    @TableField("live_file_path")
    private String liveFilePath;
    /**
     * 封面图文件ID
     */
    @ApiParam(value = "封面图文件ID")
    @ApiModelProperty(value = "封面图文件ID")
    @TableField("pic_file_no")
    private String picFileNo;
    /**
     * 操作时间
     */
    @ApiParam(value = "操作时间")
    @ApiModelProperty(value = "操作时间")
    @TableField("create_time")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String createTime;

    /**
     * 直播创建者
     */
    @ApiParam(value = "直播创建者")
    @ApiModelProperty(value = "直播创建者")
    @TableField("creater")
    private Integer creater;

    /**
     * 直播创建者名称
     */
    @ApiParam(value = "直播创建者名称")
    @ApiModelProperty(value = "直播创建者名称")
    @TableField("creater_name")
    private String createrName;

    /**
     * 是否公开(0:已公开;1:未公开)
     */
    @ApiParam(value = "是否公开(0:已公开;1:未公开)")
    @ApiModelProperty(value = "是否公开(0:已公开;1:未公开)")
    @TableField("show_type")
    private Integer showType;

    /**
     * 直播详情id
     */
    @ApiParam(value = "直播详情id")
    @ApiModelProperty(value = "直播详情id")
    @TableField("live_info_id")
    private Integer liveInfoId;

    /**
     * 浏览量
     */
    @ApiParam(value = "浏览量")
    @ApiModelProperty(value = "浏览量")
    @TableField("page_views")
    private Integer pageViews;
    /**
     * 上下架(0:上架;1:下架)
     */
    @ApiParam(value = "上下架(0:上架;1:下架)")
    @ApiModelProperty(value = "上下架(0:上架;1:下架)")
    private Integer type;

    /**
     * 直播状态
     */
    @ApiParam(value = "直播状态")
    @ApiModelProperty(value = "直播状态")
    private String liveType;

    /**
     * 封面图名称
     */
    @ApiParam(value = "封面图名称")
    @ApiModelProperty(value = "封面图名称")
    private String picFileName;

    /**
     * 直播视频名称
     */
    @ApiParam(value = "直播视频名称")
    @ApiModelProperty(value = "直播视频名称")
    private String videoFileName;
}
