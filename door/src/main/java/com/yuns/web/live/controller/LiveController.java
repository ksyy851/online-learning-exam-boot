package com.yuns.web.live.controller;

import com.yuns.web.exam.param.QueryParam;
import com.yuns.web.live.api.LiveApi;
import com.yuns.web.live.api.LiveInfoApi;
import com.yuns.web.live.api.LiveUserRelApi;
import com.yuns.web.live.param.LiveInfo;
import com.yuns.web.live.param.LiveParam;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author liuChengWen
 * @since 2020-10-28
 */
@Api(tags = "直播课程 - Controller")
@RestController
@RequestMapping("/door/live")
public class LiveController {

    @Autowired
    private LiveApi liveApi;

    @Autowired
    private LiveInfoApi liveInfoApi;

    @Autowired
    private LiveUserRelApi liveUserRelApi;

    @ApiOperation(value = "直播列表查询 - selectLiveList")
    @PostMapping(value = "selectLiveList")
    public ResultJson selectLiveList(@RequestBody LiveParam liveParam) {
        return liveApi.queryCourseList(liveParam);
    }

    @ApiOperation(value = "单个直播查询 - queryOne")
    @PostMapping(value = "queryOne/{id}")
    public ResultJson queryOne(@PathVariable("id") Integer id) {
        return liveApi.queryOne(id);
    }

    @ApiOperation(value = "直播浏览量更新 - update")
    @PostMapping(value = "updatePageViews")
    public ResultJson updatePageViews(@RequestBody LiveInfo model){
        return liveInfoApi.updatePageViews(model);
    }

    @ApiOperation(value = "判断用户是否有权限看直播 - judgeLive")
    @PostMapping(value = "judgeLive")
    public ResultJson judgeLive(@RequestBody QueryParam frontPage){
        return liveUserRelApi.judgeLive(frontPage);
    }
}
