package com.yuns.web.live.controller;

import com.yuns.web.live.param.LiveComment;
import com.yuns.web.live.api.LiveCommentApi;
import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "直播课程-互动区 - Controller")
@RestController
@RequestMapping("/door/liveComment")
public class LiveCommentController {
    @Autowired
    private LiveCommentApi liveCommentApi;

    @ApiOperation(value = "添加 - live_comment" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody LiveComment model){
        return liveCommentApi.insert(model);
    }

    @ApiOperation(value = "删除 - live_comment")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id){
        return liveCommentApi.delete(id);
    }

    @ApiOperation(value = "评论区查询 - live_comment")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody LiveComment model){
        return liveCommentApi.query(model);
    }
}
