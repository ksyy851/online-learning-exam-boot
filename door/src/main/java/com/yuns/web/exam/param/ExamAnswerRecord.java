package com.yuns.web.exam.param;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 试卷试题答题答案表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_answer_record")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ExamAnswerRecord extends Model<ExamAnswerRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;
    /**
     * 答题时间
     */
    @ApiParam(value = "答题时间")
   @ApiModelProperty(value = "答题时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date aTime;
    /**
     * 试卷试题对应关系表ID
     */
    @ApiParam(value = "试卷试题对应关系表ID")
   @ApiModelProperty(value = "试卷试题对应关系表ID")
    private Long examTitleId;


    @ApiParam(value = "答题的题目")
    @ApiModelProperty(value = "答题的题目")
    private String questionId;
    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
   @ApiModelProperty(value = "会员ID")
    private Integer memberId;
    /**
     * 答题结果
     */
    @ApiParam(value = "答题结果")
   @ApiModelProperty(value = "答题结果")
    private String Answer;
    /**
     * 是否答错
     */
    @ApiParam(value = "是否答错")
   @ApiModelProperty(value = "是否答错")
    private String isOk;
    /**
     * 得分
     */
    @ApiParam(value = "得分")
   @ApiModelProperty(value = "得分")
    private BigDecimal Score;
    /**
     * 答题时长
     */
    @ApiParam(value = "答题时长")
   @ApiModelProperty(value = "答题时长")
    private Integer userTime;
    /**
     * 标识
     */
    @ApiParam(value = "标识")
   @ApiModelProperty(value = "标识")
    private Integer Sign;

    @ApiParam(value = "题目类型")
    @ApiModelProperty(value = "题目类型")
    private Integer questionType;

    @ApiParam(value = "简答题答题结果")
    @ApiModelProperty(value = "简答题答题结果")
    private String answerReply;

    public String getExamRecordId() {
        return examRecordId;
    }

    public void setExamRecordId(String examRecordId) {
        this.examRecordId = examRecordId;
    }

    @ApiParam(value = "考试记录id")
    @ApiModelProperty(value = "考试记录id")
    private String examRecordId;


    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField("update_user")
    private Long updateUser;

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getATime() {
        return aTime;
    }

    public void setATime(Date ATime) {
        this.aTime = ATime;
    }

    public Long getExamTitleId() {
        return examTitleId;
    }

    public void setExamTitleId(Long ExamTitleID) {
        this.examTitleId = ExamTitleID;
    }

    public Integer getMemberID() {
        return memberId;
    }

    public void setMemberID(Integer MemberID) {
        this.memberId = MemberID;
    }

    public String getAnswer() {
        return Answer;
    }

    public void setAnswer(String Answer) {
        this.Answer = Answer;
    }

    public String getIsOK() {
        return isOk;
    }

    public void setIsOK(String IsOK) {
        this.isOk = IsOK;
    }

    public BigDecimal getScore() {
        return Score;
    }

    public void setScore(BigDecimal Score) {
        this.Score = Score;
    }

    public Integer getUserTime() {
        return userTime;
    }

    public void setUserTime(Integer UserTime) {
        this.userTime = UserTime;
    }

    public Integer getSign() {
        return Sign;
    }

    public void setSign(Integer Sign) {
        this.Sign = Sign;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamAnswerRecord{" +
        ", id=" + id +
        ", ATime=" + aTime +
        ", ExamTitleID=" + examTitleId +
        ", MemberID=" + memberId +
        ", Answer=" + Answer +
        ", IsOK=" + isOk +
        ", Score=" + Score +
        ", UserTime=" + userTime +
        ", Sign=" + Sign +
        "}";
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public String getAnswerReply() {
        return answerReply;
    }

    public void setAnswerReply(String answerReply) {
        this.answerReply = answerReply;
    }
}
