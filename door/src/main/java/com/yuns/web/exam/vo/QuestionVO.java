package com.yuns.web.exam.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description: TODO: 题目管理题目列表
 * @author: zhansang
 * @create: 2020-11-10 14:30
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionVO {

    @ApiModelProperty(value = "题目主键")
    private String id;

    @ApiModelProperty(value = "题库主键值")
    private String examIds;

    /**
     * 题目类型（0：单选题 1多选题 2 判断题 3 简答题）
     */
    @ApiParam(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
    @ApiModelProperty(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
    private Integer questionType;

    /**
     * 题目描述
     */
    @ApiParam(value = "题目描述")
    @ApiModelProperty(value = "题目描述")
    private String questionTitle;

    @ApiParam(value = "所属题库id")
    @ApiModelProperty(value = "所属题库id")
    private String examId;

    @ApiParam(value = "所属题库名称")
    @ApiModelProperty(value = "所属题库名称")
    private String examName;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

}
