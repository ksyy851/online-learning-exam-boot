package com.yuns.web.exam.api;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.exam.param.ExamDoorParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * @description: TODO: 考试服务
 * @author: zhansang
 * @create: 2020-11-22 10:21
 * @version: 1.0
 **/
@FeignClient(name = "manage-service")
@RequestMapping("exam")
public interface ExamApi {
    @PostMapping(value = "getExamListForDoor")
    public ResultJson getExamListForDoor(@RequestBody ExamDoorParam frontPage);

   /* @GetMapping(value = "/push/{toUserId}")
    public ResultJson pushToWeb(String message, @PathVariable String toUserId);*/
}
