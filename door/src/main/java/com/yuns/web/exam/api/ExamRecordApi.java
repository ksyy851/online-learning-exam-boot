package com.yuns.web.exam.api;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.exam.param.*;
import com.yuns.web.exam.vo.ExamRecordVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @description: TODO: 考试记录服务
 * @author: zhansang
 * @create: 2020-11-22 10:21
 * @version: 1.0
 **/
@FeignClient(name = "manage-service")
@RequestMapping("examRecord")
public interface ExamRecordApi {

    @PostMapping(value = "beginExam" )
    public ResultJson beginExam(@RequestBody ExamRecordVO examRecordVO);

    @PostMapping(value = "updateExamAnswerRecord" )
    public ResultJson updateExamAnswerRecord(@RequestBody ExamAnswerRecord examAnswerRecord);

    @PostMapping(value = "submitExam" )
    public ResultJson submitExam(@RequestBody ExamRecordVO examRecordVO);

    @PostMapping(value = "selectErrorQuestionList" )
    public  ResultJson selectErrorQuestionList(@RequestBody ExamAnswerRecordReq examAnswerRecordReq);


    @PostMapping(value = "selectErrorQuestionListByQuestionType" )
    public  ResultJson selectErrorQuestionListByQuestionType(@RequestBody QuestionParam frontPage);

    @PostMapping(value = "testExam" )
    public ResultJson testExam();



    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody ExamRecordParm frontPage);

    @RequestMapping(value = "/qrcode")
    @ResponseBody
    public void getQrcode(@RequestParam("content") String content , HttpServletResponse resp) ;

    @PostMapping(value = "sendCertificate" )
    public ResultJson sendCertificate(@RequestBody ExamRecordVO examRecordVO);

    @PostMapping(value = "queryCertificate" )
    public ResultJson queryCertificate(@RequestBody ExamRecordVO examRecordVO) throws Exception;

}
