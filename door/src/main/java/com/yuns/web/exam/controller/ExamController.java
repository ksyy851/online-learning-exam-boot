package com.yuns.web.exam.controller;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.exam.api.ExamApi;
import com.yuns.web.exam.param.ExamDoorParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description: TODO: 考试相关门户
 * @author: zhansang
 * @create: 2020-11-22 10:31
 * @version: 1.0
 **/
@Api(tags = "门户考试中心 - Controller" )
@RestController
@RequestMapping("/door/exam" )
public class ExamController {

    @Autowired
    private ExamApi examApi;

    @ApiOperation(value = "门户考试中心 - gt_exam")
    @PostMapping(value = "getExamListForDoor")
    public ResultJson getExamListForDoor(@RequestBody ExamDoorParam frontPage){
        return examApi.getExamListForDoor(frontPage);
    };

    /*@ApiOperation(value = "发送考试倒计时消息的接口")
    @GetMapping(value = "/push/{toUserId}")
    public ResultJson pushToWeb(String message, @PathVariable String toUserId){
        return examApi.pushToWeb(message,toUserId);
    };*/
}
