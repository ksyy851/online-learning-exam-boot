package com.yuns.web.exam.param;

import com.yuns.web.exam.vo.QuestionVO;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: TODO: 题目查询入参
 * @author: zhansang
 * @create: 2020-11-10 15:06
 * @version: 1.0
 **/
@Data
@Builder
public class QuestionParam extends FrontPage<QuestionVO> implements Serializable {
    private static final long serialVersionUID = 112321L;


}
