package com.yuns.web.exam.api;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.exam.param.ExamAnswerRecord;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description: TODO: 试卷试题记录
 * @author: zhansang
 * @create: 2020-12-02 21:38
 * @version: 1.0
 **/
@FeignClient(name = "manage-service")
@RequestMapping("examAnswerRecord")
public interface ExamAnswerRecordApi {


    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody ExamAnswerRecord model) ;
}
