package com.yuns.web.exam.api;

import com.yuns.common.utils.ResultJson;
import com.yuns.web.exam.param.FrontPage;
import com.yuns.web.exam.param.QueryParam;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description: TODO: 题目选项
 * @author: zhansang
 * @create: 2020-11-22 10:43
 * @version: 1.0
 **/
@FeignClient(name = "manage-service")
@RequestMapping("questionOption")
public interface QuestionOptionApi {
    /**
     * 查询题目选项
     * @param frontPage
     * @return
     */
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage);
}
