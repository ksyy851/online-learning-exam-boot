package com.yuns.web.exam.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 用来接收页面传过来的查询字段   对象
 */
@ApiModel("分页模型")
public class FrontPage<T> implements Serializable {


    @ApiModelProperty(value = "是否是分页")
    private boolean _search;

    @ApiModelProperty(value = "模糊查询参数", required = false)
    private String selected;

    //搜索条件
    @ApiModelProperty(value = "搜索条件")
    private String keywords;

    @ApiModelProperty(value = "类型查询参数", required = false)
    private String selectedType;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "删选参数")
    private T data;

    //	每页显示条数
    @ApiModelProperty(value = "每页显示条数")
    private int pageSize;

    //当前页数
    @ApiModelProperty(value = "当前页数")
    private int pageNumber;

    //排序的字段
    @ApiModelProperty(value = "排序的字段")
    private String sortField;

    //排序方式 asc升序  desc降序
    @ApiModelProperty(value = "排序方式 asc升序  desc降序")
    private String sortOrder;

    //创建人删选
    @ApiModelProperty(value = "创建人删选")
    private Integer createBy;

    @ApiModelProperty(value = "结束时间", required = false)
    private String endTm;

    @ApiModelProperty(value = "开始时间", required = false)
    private String bgTm;

    public boolean is_search() {
        return _search;
    }

    public void set_search(boolean _search) {
        this._search = _search;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public String getSortField() {
        return sortField;
    }

    public void setSortField(String sortField) {
        this.sortField = sortField;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getBgTm() {
        return bgTm;
    }

    public void setBgTm(String bgTm) {
        this.bgTm = bgTm;
    }

    public String getEndTm() {
        return endTm;
    }

    public void setEndTm(String endTm) {
        this.endTm = endTm;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public String getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(String selectedType) {
        this.selectedType = selectedType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
