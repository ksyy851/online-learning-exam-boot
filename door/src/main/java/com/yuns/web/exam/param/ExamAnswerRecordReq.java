package com.yuns.web.exam.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 考试答题记录传参
 * @author: zhansang
 * @create: 2020-11-18 17:15
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExamAnswerRecordReq {

    @ApiModelProperty(value = "用户id")
    private Integer userId;
}
