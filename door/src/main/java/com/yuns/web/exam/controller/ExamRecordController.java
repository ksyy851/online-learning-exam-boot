package com.yuns.web.exam.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.yuns.common.utils.ResultJson;
import com.yuns.web.exam.api.ExamAnswerRecordApi;
import com.yuns.web.exam.api.ExamRecordApi;
import com.yuns.web.exam.api.QuestionOptionApi;
import com.yuns.web.exam.param.*;
import com.yuns.web.exam.vo.ExamRecordVO;
import io.netty.util.internal.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: TODO: 考试记录控制
 * @author: zhansang
 * @create: 2020-11-22 10:34
 * @version: 1.0
 **/
@Api(tags = "考试记录 - Controller" )
@RestController
@RequestMapping("/door/examRecord" )
public class ExamRecordController {

    @Autowired
    private ExamRecordApi examRecordApi;

    @Autowired
    private QuestionOptionApi questionOptionApi;

    @Autowired
    private ExamAnswerRecordApi examAnswerRecordApi;



    @ApiOperation(value = "开始考试" )
    @PostMapping(value = "beginExam" )
    public ResultJson beginExam(@RequestBody ExamRecordVO examRecordVO){
        return  examRecordApi.beginExam(examRecordVO);
    };

    @ApiOperation(value = "做题接口" )
    @PostMapping(value = "updateExamAnswerRecord" )
    public ResultJson updateExamAnswerRecord(@RequestBody ExamAnswerRecord examAnswerRecord){
       return  examRecordApi.updateExamAnswerRecord(examAnswerRecord);
    };

    @ApiOperation(value = "提交试卷" )
    @PostMapping(value = "submitExam" )
    public ResultJson submitExam(@RequestBody ExamRecordVO examRecordVO){
        return  examRecordApi.submitExam(examRecordVO);
    };


    @ApiOperation(value = "错题集统计" )
    @PostMapping(value = "selectErrorQuestionList" )
    public  ResultJson selectErrorQuestionList(@RequestBody ExamAnswerRecordReq examAnswerRecordReq){
        return examRecordApi.selectErrorQuestionList(examAnswerRecordReq);
    };

    @ApiOperation(value = "根据错题集类型查找错题列表" )
    @PostMapping(value = "selectErrorQuestionListByQuestionType" )
    public  ResultJson selectErrorQuestionListByQuestionType(@RequestBody QuestionParam frontPage){
        return examRecordApi.selectErrorQuestionListByQuestionType(frontPage);
    };

    @ApiOperation(value = "模拟考试" )
    @PostMapping(value = "testExam" )
    public ResultJson testExam(){
        return examRecordApi.testExam();
    };

    @ApiOperation(value = "模拟考试获取选项" )
    @PostMapping(value = "queryQuestionOption" )
    public ResultJson queryQuestionOption(@RequestBody FrontPage<QueryParam> frontPage){
        return  questionOptionApi.query(frontPage);
    }

    @ApiOperation(value = "模拟考试做题 - gt_exam_answer_record")
    @PostMapping(value = "insertQuestionAnswerRecord")
    public ResultJson insert(@RequestBody ExamAnswerRecord model) {
        if(model.getExamTitleId()==null || StringUtil.isNullOrEmpty(String.valueOf(model.getExamTitleId()))){
            return ResultJson.error("试卷id不能为空");
        }
        if(StringUtil.isNullOrEmpty(String.valueOf(model.getMemberID()))){
            return ResultJson.error("用户id不能为空");
        }
        if(StringUtil.isNullOrEmpty(model.getQuestionId())){
            return ResultJson.error("题目id不能为空");
        }
       return examAnswerRecordApi.insert(model);
    }

    @ApiOperation(value = "查询gt_question_option" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
        return questionOptionApi.query(frontPage);
    }

    @ApiOperation(value = "考试记录 - gt_exam_record" )
    @PostMapping(value = "queryExamRecord" )
    public ResultJson queryExamRecord(@RequestBody ExamRecordParm frontPage){
       return examRecordApi.query(frontPage);
    }

    @ApiOperation(value = "生成二维码" )
    @RequestMapping(value = "/qrcode")
    @ResponseBody
    public void getQrcode(@RequestParam("content") String content,HttpServletResponse resp) throws Exception {
        ExamRecordVO examRecordVO = ExamRecordVO.builder().build();
        examRecordVO.setMemberId(Integer.valueOf(content));
        Object t =  examRecordApi.queryCertificate(examRecordVO);
        ServletOutputStream stream = null;
        try {
            stream = resp.getOutputStream();
            Map<EncodeHintType, Object> hints = new HashMap<>();
            //编码
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            //边框距
            hints.put(EncodeHintType.MARGIN, 0);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            String dateStr = DateUtil.format(LocalDateTime.now(),"yyyy-MM-dd HH:mm:ss");

            BitMatrix bm = qrCodeWriter.encode(JSONUtil.toJsonStr(t), BarcodeFormat.QR_CODE, 200, 200, hints);
            MatrixToImageWriter.writeToStream(bm, "png", stream);
        } catch (WriterException e) {
            e.getStackTrace();

        } finally {
            if (stream != null) {
                stream.flush();
                stream.close();
            }
        }


    }

    @ApiOperation(value = "发放证书 - gt_exam_record" )
    @PostMapping(value = "sendCertificate" )
    public ResultJson sendCertificate(@RequestBody ExamRecordVO examRecordVO){
        return examRecordApi.sendCertificate(examRecordVO);
    }

    @ApiOperation(value = "查询证书 - gt_exam_record" )
    @PostMapping(value = "queryCertificate" )
    public ResultJson queryCertificate(@RequestBody ExamRecordVO examRecordVO) throws Exception {
        return  examRecordApi.queryCertificate(examRecordVO);
    }
}
