package com.yuns.web.exam.param;

import com.yuns.web.exam.vo.ExamRecordVO;
import lombok.Builder;
import lombok.Data;

/**
 * @description: TODO: 考试记录入参
 * @author: zhansang
 * @create: 2020-11-10 17:00
 * @version: 1.0
 **/
@Data
@Builder
public class ExamRecordParm extends FrontPage<ExamRecordVO> {
}
