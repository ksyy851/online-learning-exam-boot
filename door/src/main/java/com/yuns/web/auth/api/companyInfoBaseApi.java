package com.yuns.web.auth.api;

import com.yuns.common.utils.ResultJson;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "manage-service")
@RequestMapping("companyInfoBase")
public interface companyInfoBaseApi {

    @ApiOperation(value = "企业列表查询 - queryList")
    @PostMapping(value = "queryList")
    ResultJson queryList();
}
