package com.yuns.web.auth.api;

import com.yuns.common.utils.ResultJson;
import com.yuns.common.utils.SysUser;
import com.yuns.web.auth.param.CompanyUserRegParam;
import com.yuns.web.center.param.CerterUserParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

/**
 * 个人中心服务
 */
@FeignClient(name = "manage-service")
@RequestMapping("sysUser")
public interface AuthApi {

    @ApiOperation(value = "账号登录")
    @GetMapping("accountLogin")
    ResultJson accountLogin(@RequestParam("username") String username, @RequestParam("password") String password);

    @ApiOperation(value = "手机号登录")
    @GetMapping("telLogin")
    ResultJson telLogin(@RequestParam("tel") String tel, @RequestParam("code") String code);

    /**
     * 发送短信
     *
     * @param phone
     * @return
     */
    @ApiOperation(value = "发送短信")
    @GetMapping("code")
    ResultJson sendCode(@RequestParam("phone") String phone);


    /**
     * 手机号注册
     * @return
     */
    @ApiOperation(value = "手机号注册")
    @PostMapping("registerByTel")
    Object registerByTel(@RequestBody SysUser userDto);

    /**
     * 企业员工注册
     * @param companyUserRegParam
     * @return
     */
    @ApiOperation(value = "企业员工注册")
    @PostMapping("register")
    Object register(@RequestBody CompanyUserRegParam companyUserRegParam);

}
