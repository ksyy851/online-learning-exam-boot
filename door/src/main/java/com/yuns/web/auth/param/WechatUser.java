package com.yuns.web.auth.param;

import com.yuns.web.course.param.CompanyUserBase;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
@Data
public class WechatUser implements Serializable {

    @ApiModelProperty(value = "授权用户唯一标识" )
    private String openid;

    @ApiModelProperty(value = "微信用户呢称" )
    private String nickname;

    @ApiModelProperty(value = "性别" )
    private String sex;

    @ApiModelProperty(value = "省份" )
    private String province;

    @ApiModelProperty(value = "城市" )
    private String city;

    @ApiModelProperty(value = "国家" )
    private String country;

    @ApiModelProperty(value = "微信头像地址" )
    private String headimgurl;

    @ApiModelProperty(value = "用户特权信息，json数组" )
    private String privilege;

    @ApiModelProperty(value = "用户统一标识" )
    private String unionid;

    @ApiModelProperty(value = "用户在本系统是否存在" )
    private boolean flag;

    @ApiModelProperty(value = "用户在本系统的企业信息" )
    private Object companyUserBase;
}
