package com.yuns.company.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 企业-法律法规关联表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@TableName("company_doc_rel")
public class CompanyDocRel extends Model<CompanyDocRel> {

    private static final long serialVersionUID = 1L;

    /**
     * 企业ID
     */
    @ApiParam(value = "企业ID")
    @ApiModelProperty(value = "企业ID")
    @TableField("company_id")
    @TableId(value = "company_id")
    private Integer companyId;
    /**
     * 法律法规ID
     */
    @ApiParam(value = "法律法规ID")
    @ApiModelProperty(value = "法律法规ID")
    @TableField("doc_id")
    private Integer docId;


    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    @Override
    protected Serializable pkVal() {
        return this.companyId;
    }

    @Override
    public String toString() {
        return "CompanyDocRel{" +
                ", companyId=" + companyId +
                ", docId=" + docId +
                "}";
    }
}
