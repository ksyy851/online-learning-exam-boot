package com.yuns.company.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 企业用户关联表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
@TableName("company_user_rel")
public class CompanyUserRel extends Model<CompanyUserRel> {

    private static final long serialVersionUID = 1L;

    /**
     * 企业ID
     */
    @ApiParam(value = "企业ID")
   @ApiModelProperty(value = "企业ID")
    @TableField("company_id")
    private Integer companyId;
    /**
     * 企业用户ID
     */
    @ApiParam(value = "企业用户ID")
   @ApiModelProperty(value = "企业用户ID")
    @TableField("company_user_id")
    private Integer companyUserId;


    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getCompanyUserId() {
        return companyUserId;
    }

    public void setCompanyUserId(Integer companyUserId) {
        this.companyUserId = companyUserId;
    }

    @Override
    protected Serializable pkVal() {
        return this.companyId;
    }

    @Override
    public String toString() {
        return "CompanyUserRel{" +
        ", companyId=" + companyId +
        ", companyUserId=" + companyUserId +
        "}";
    }
}
