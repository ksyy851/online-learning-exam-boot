package com.yuns.company.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 用户学习积分表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-14
 */
@Data
@TableName("company_user_integral")
public class CompanyUserIntegral extends Model<CompanyUserIntegral> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 积分
     */
    @ApiParam(value = "积分")
    @ApiModelProperty(value = "积分")
    private Integer integral;
    /**
     * 课程ID(直播ID)
     */
    @ApiParam(value = "课程ID(直播ID)")
    @ApiModelProperty(value = "课程ID(直播ID)")
    private Integer pid;
    /**
     * 课程名称(直播名称)
     */
    @ApiParam(value = "课程名称(直播名称)")
    @ApiModelProperty(value = "课程名称(直播名称)")
    private String name;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    @ApiModelProperty(value = "用户ID")
    @TableField("company_user_id")
    private Integer companyUserId;
    /**
     * 观看时间
     */
    @ApiParam(value = "观看时间")
    @ApiModelProperty(value = "观看时间")
    private String time;
    /**
     * 学习渠道:0:课程学习;1:直播学习
     */
    @ApiParam(value = "学习渠道:0:课程学习;1:直播学习")
    @ApiModelProperty(value = "学习渠道:0:课程学习;1:直播学习")
    private Integer type;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
