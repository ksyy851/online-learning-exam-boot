package com.yuns.company.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 企业信息基础表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
@Data
@TableName("company_info_base")
public class CompanyInfoBase extends Model<CompanyInfoBase> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 企业名称
     */
    @ApiParam(value = "企业名称")
   @ApiModelProperty(value = "企业名称")
    @TableField("company_name")
    private String companyName;
    /**
     * 处罚日期
     */
    @ApiParam(value = "处罚日期")
   @ApiModelProperty(value = "处罚日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("pulish_time")
    private String pulishTime;
    /**
     * 处理日期
     */
    @ApiParam(value = "处理日期")
   @ApiModelProperty(value = "处理日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("deal_time")
    private String dealTime;
    /**
     * 处罚文件号
     */
    @ApiParam(value = "处罚文件号")
   @ApiModelProperty(value = "处罚文件号")
    @TableField("pulish_no")
    private String pulishNo;
    /**
     * 地址
     */
    @ApiParam(value = "地址")
   @ApiModelProperty(value = "地址")
    private String address;
    /**
     * 法人
     */
    @ApiParam(value = "法人")
   @ApiModelProperty(value = "法人")
    @TableField("person_name")
    private String personName;
    /**
     * 企业类型(0:重点排污企业;1:其他企业;2:无企业)
     */
    @ApiParam(value = "企业类型(0:重点排污企业;1:其他企业;2:无企业)")
   @ApiModelProperty(value = "企业类型(0:重点排污企业;1:其他企业;2:无企业)")
    @TableField("company_type")
    private Integer companyType;
    /**
     * 证书编号
     */
    @ApiParam(value = "证书编号")
    @ApiModelProperty(value = "证书编号")
    @TableField("cert_no")
    private String certNo;
    /**
     * 录入日期
     */
    @ApiParam(value = "录入日期")
   @ApiModelProperty(value = "录入日期")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String createTime;
    /**
     * 是否删除(0:未删除;1:已删除)
     */
    @ApiParam(value = "是否删除(0:未删除;1:已删除)")
   @ApiModelProperty(value = "是否删除(0:未删除;1:已删除)")
    private Integer del;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
