package com.yuns.company.entity;

import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class CompanyInfoBean {
    /**
     * 企业名称
     */
    @ApiParam(value = "企业名称")
    private String companyName;
    /**
     * 处罚日期
     */
    @ApiParam(value = "处罚日期")
    private String pulishTime;
    /**
     * 处理日期
     */
    @ApiParam(value = "处理日期")
    private String dealTime;
    /**
     * 处罚文件号
     */
    @ApiParam(value = "处罚文件号")
    private String pulishNo;

    /**
     * 地址
     */
    @ApiParam(value = "地址")
    private String address;
    /**
     * 法人
     */
    @ApiParam(value = "法人")
    private String personName;
}
