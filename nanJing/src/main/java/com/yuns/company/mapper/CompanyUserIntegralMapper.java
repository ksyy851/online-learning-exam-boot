package com.yuns.company.mapper;

import com.yuns.company.entity.CompanyUserIntegral;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 用户学习积分表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-14
 */
public interface CompanyUserIntegralMapper extends BaseMapper<CompanyUserIntegral> {
        List<CompanyUserIntegral> selectCompanyUserIntegralList(CompanyUserIntegral frontPage);
}
