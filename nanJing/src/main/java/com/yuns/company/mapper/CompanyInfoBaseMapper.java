package com.yuns.company.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.entity.CompanyInfoBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.vo.CompanyInfoVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

/**
 * <p>
 * 企业信息基础表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
public interface CompanyInfoBaseMapper extends BaseMapper<CompanyInfoBase> {

    List<CompanyInfoVo> queryForFocus(BasePage<CompanyInfoBase> frontPage);

    List<CompanyInfoVo> queryForFocus(Page<CompanyInfoVo> page,BasePage<CompanyInfoBase> frontPage);

    List<CompanyInfoVo> queryForOther(BasePage<CompanyInfoBase> frontPage);

    List<CompanyInfoVo> queryForOther(Page<CompanyInfoVo> page,BasePage<CompanyInfoBase> frontPage);

    CompanyInfoVo editForFocus(Integer id);

    CompanyInfoVo editForOther(Integer id);

    List<CompanyUserBase> queryUsers(Integer id);

}
