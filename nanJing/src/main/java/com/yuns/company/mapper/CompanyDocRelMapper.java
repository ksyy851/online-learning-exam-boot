package com.yuns.company.mapper;

import com.yuns.company.entity.CompanyDocRel;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

import com.yuns.web.param.FrontPage;

/**
 * <p>
 * 企业-法律法规关联表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface CompanyDocRelMapper extends BaseMapper<CompanyDocRel> {
    List<CompanyDocRel> selectCompanyDocRelList(FrontPage frontPage);

    Integer updateByCompanyId(CompanyDocRel rel);
}
