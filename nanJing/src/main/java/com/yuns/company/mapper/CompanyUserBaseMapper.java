package com.yuns.company.mapper;

import com.yuns.company.entity.CompanyUserBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

/**
 * <p>
 * 用户基础表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
public interface CompanyUserBaseMapper extends BaseMapper<CompanyUserBase> {
    List<CompanyUserBase> selectCompanyUserBaseList(BasePage frontPage);

    /**
     *
     * @param id 企业ID
     * @return
     */
    Integer updateUserType(Integer id);

    Integer selectCompanyTypeById(Integer id);
}
