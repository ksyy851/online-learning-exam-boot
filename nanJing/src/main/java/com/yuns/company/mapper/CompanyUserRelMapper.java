package com.yuns.company.mapper;

import com.yuns.company.entity.CompanyUserRel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.company.vo.ReceiverVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

/**
 * <p>
 * 企业用户关联表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
public interface CompanyUserRelMapper extends BaseMapper<CompanyUserRel> {
        List<CompanyUserRel> selectCompanyUserRelList(BasePage frontPage);
        List<ReceiverVo> getReceiverList();
}
