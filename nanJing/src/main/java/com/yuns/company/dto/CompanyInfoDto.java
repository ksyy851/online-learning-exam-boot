package com.yuns.company.dto;

import com.yuns.company.entity.CompanyInfoBase;
import com.yuns.study.entity.InfoDocI;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class CompanyInfoDto extends CompanyInfoBase {

    @ApiModelProperty(value = "法律法规集合" ,required = true)
    List<InfoDocI> infoDocIList;
}
