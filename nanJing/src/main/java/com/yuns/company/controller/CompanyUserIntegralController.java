package com.yuns.company.controller;


import com.yuns.company.service.ICompanyUserIntegralService;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.entity.CompanyUserIntegral;
import org.springframework.web.bind.annotation.*;

//import com.yuns.com.yuns.web.sys.dto.CompanyUserIntegralDto;


/**
 * @author ${author}
 * @since 2020-11-14
 */
@Api(tags = "用户学习积分表 - Controller")
@RestController
@RequestMapping("/companyUserIntegral")
public class CompanyUserIntegralController {

    @Autowired
    private ICompanyUserIntegralService iCompanyUserIntegralService;

    @ApiOperation(value = "添加 - company_user_integral")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CompanyUserIntegral model) {
        //  CompanyUserIntegral entity=DtoUtil.convertObject(model,CompanyUserIntegral.class);
        boolean flag = iCompanyUserIntegralService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - company_user_integral")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody CompanyUserIntegral model) {
        //CompanyUserIntegral entity=DtoUtil.convertObject(model,CompanyUserIntegral.class);
        boolean flag = iCompanyUserIntegralService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - company_user_integral")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iCompanyUserIntegralService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - company_user_integral")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody CompanyUserIntegral frontPage) {
        return ResultJson.ok(iCompanyUserIntegralService.selectCompanyUserIntegralList(frontPage));
    }
}

