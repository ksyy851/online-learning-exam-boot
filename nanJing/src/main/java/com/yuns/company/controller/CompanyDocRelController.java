package com.yuns.company.controller;

import com.yuns.company.service.ICompanyDocRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.entity.CompanyDocRel;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CompanyDocRelDto;


/**
 * @author ${author}
 * @since 2020-11-07
 */
@Api(tags = "企业-法律法规关联表 - Controller" )
    @RestController
@RequestMapping("/companyDocRel" )
    public class CompanyDocRelController {

    @Autowired
    private ICompanyDocRelService iCompanyDocRelService;

    @ApiOperation(value = "添加 - company_doc_rel" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CompanyDocRel model){
            //  CompanyDocRel entity=DtoUtil.convertObject(model,CompanyDocRel.class);
            boolean flag=iCompanyDocRelService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - company_doc_rel" )
    @PostMapping(value = "up/{id}" )
    public ResultJson update(@RequestBody CompanyDocRel model){
            //CompanyDocRel entity=DtoUtil.convertObject(model,CompanyDocRel.class);
            boolean flag=iCompanyDocRelService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - company_doc_rel" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iCompanyDocRelService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - company_doc_rel" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
            if(!frontPage.is_search()){
            Page<CompanyDocRel> page=new Page<CompanyDocRel>();
            Page<CompanyDocRel> list=page.setRecords(iCompanyDocRelService.selectCompanyDocRelList(frontPage));
            return ResultJson.ok(list);
            }
            Page<CompanyDocRel> page=new Page<CompanyDocRel>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<CompanyDocRel> list=page.setRecords(iCompanyDocRelService.selectCompanyDocRelList(frontPage));
            return ResultJson.ok(list);
            }
        }

