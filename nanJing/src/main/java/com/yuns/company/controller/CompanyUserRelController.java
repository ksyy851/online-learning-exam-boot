package com.yuns.company.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.entity.CompanyUserRel;
import com.yuns.company.service.ICompanyUserRelService;
import com.yuns.company.vo.ReceiverVo;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//import com.yuns.web.dto.CompanyUserRelDto;


/**
 * @author ${author}
 * @since 2020-11-06
 */
@Api(tags = "企业用户关联表 - Controller" )
    @RestController
@RequestMapping("/companyUserRel" )
    public class CompanyUserRelController {

    @Autowired
    private ICompanyUserRelService iCompanyUserRelService;

    @ApiOperation(value = "添加 - company_user_rel" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CompanyUserRel model){
            //  CompanyUserRel entity=DtoUtil.convertObject(model,CompanyUserRel.class);
            boolean flag=iCompanyUserRelService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - company_user_rel" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody CompanyUserRel model){
            //CompanyUserRel entity=DtoUtil.convertObject(model,CompanyUserRel.class);
            boolean flag=iCompanyUserRelService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - company_user_rel" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iCompanyUserRelService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - company_user_rel" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody BasePage<CompanyUserRel> frontPage){
            if(!frontPage.is_search()){
            Page<CompanyUserRel> page=new Page<CompanyUserRel>();
            Page<CompanyUserRel> list=page.setRecords(iCompanyUserRelService.selectCompanyUserRelList(frontPage));
            return ResultJson.ok(list);
            }
            Page<CompanyUserRel> page=new Page<CompanyUserRel>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<CompanyUserRel> list=page.setRecords(iCompanyUserRelService.selectCompanyUserRelList(frontPage));
            return ResultJson.ok(list);
            }

    @ApiOperation(value = "短息下发查询接收人" )
    @PostMapping(value = "queryReceiverList")
    public ResultJson queryReceiverList(){
        List<ReceiverVo> receiverVoList =  iCompanyUserRelService.getReceiverList();
        return ResultJson.ok(receiverVoList);
    }

}


