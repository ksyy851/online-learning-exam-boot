package com.yuns.company.controller;

import com.yuns.company.service.ICompanyUserBaseService;
import com.yuns.course.entity.CourseChapter;
import com.yuns.web.sys.param.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CompanyUserBaseDto;


/**
 * @author ${author}
 * @since 2020-11-06
 */
@Api(tags = "用户管理 - Controller")
@RestController
@RequestMapping("/companyUserBase")
public class CompanyUserBaseController {

    @Autowired
    private ICompanyUserBaseService iCompanyUserBaseService;

    @ApiOperation(value = "添加 - company_user_base")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CompanyUserBase model) {
        //  CompanyUserBase entity=DtoUtil.convertObject(model,CompanyUserBase.class);
        boolean flag = iCompanyUserBaseService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - up")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody CompanyUserBase model) {
        //CompanyUserBase entity=DtoUtil.convertObject(model,CompanyUserBase.class);
        boolean flag = iCompanyUserBaseService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - delete")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iCompanyUserBaseService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "用户查询(个人用户/学校用户) - query")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody BasePage<CompanyUserBase> frontPage) {
        if (!frontPage.is_search()) {
            Page<CompanyUserBase> page = new Page<CompanyUserBase>();
            Page<CompanyUserBase> list = page.setRecords(iCompanyUserBaseService.selectCompanyUserBaseList(frontPage));
            return ResultJson.ok(list);
        }
        Page<CompanyUserBase> page = new Page<CompanyUserBase>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<CompanyUserBase> list = page.setRecords(iCompanyUserBaseService.selectCompanyUserBaseList(frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "编辑查询 - edit")
    @PostMapping(value = "edit/{id}")
    public ResultJson edit(@PathVariable("id") Integer id) {
        return ResultJson.ok(iCompanyUserBaseService.edit(id));
    }

    @ApiOperation(value = "禁用操作 - disable")
    @PostMapping(value = "disable")
    public ResultJson disable(@RequestBody CompanyUserBase model) {
        return ResultJson.ok(iCompanyUserBaseService.disable(model));
    }

    @ApiOperation(value = "更新个人课时/学时 - updateEachPeriod")
    @PostMapping(value = "updateEachPeriod")
    public ResultJson updateEachPeriod(@RequestBody CompanyUserBase model) {
        boolean flag = iCompanyUserBaseService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }
}

