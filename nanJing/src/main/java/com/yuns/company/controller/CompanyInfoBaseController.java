package com.yuns.company.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yuns.company.dto.CompanyInfoDto;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.service.ICompanyInfoBaseService;
import com.yuns.company.vo.CompanyInfoVo;
import com.yuns.study.vo.StudyCharVO;
import com.yuns.web.sys.param.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.entity.CompanyInfoBase;
import com.yuns.web.param.QueryParam;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.stream.Collectors;

//import com.yuns.web.dto.CompanyInfoBaseDto;


/**
 * @author ${author}
 * @since 2020-11-06
 */
@Api(tags = "企业管理 - Controller")
@RestController
@RequestMapping("/companyInfoBase")
public class CompanyInfoBaseController {

    @Autowired
    private ICompanyInfoBaseService iCompanyInfoBaseService;

    @ApiOperation(value = "添加 - insert")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CompanyInfoDto model) {
        //  CompanyInfoBase entity=DtoUtil.convertObject(model,CompanyInfoBase.class);
        boolean flag = iCompanyInfoBaseService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - company_info_base")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody CompanyInfoDto model) {
        //CompanyInfoBase entity=DtoUtil.convertObject(model,CompanyInfoBase.class);
        boolean flag = iCompanyInfoBaseService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - delete")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iCompanyInfoBaseService.delete(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "重点排污企业列表查询 - queryForFocus")
    @PostMapping(value = "queryForFocus")
    public ResultJson queryForFocus(@RequestBody BasePage<CompanyInfoBase> frontPage) {
        if (!frontPage.is_search()) {
            Page<CompanyInfoVo> page = new Page<CompanyInfoVo>();
            Page<CompanyInfoVo> list = page.setRecords(iCompanyInfoBaseService.queryForFocus(frontPage));
            return ResultJson.ok(list);
        }
        Page<CompanyInfoVo> page = new Page<CompanyInfoVo>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<CompanyInfoVo> list = page.setRecords(iCompanyInfoBaseService.queryForFocus(page,frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "其他企业列表查询 - queryForOther")
    @PostMapping(value = "queryForOther")
    public ResultJson queryForOther(@RequestBody BasePage<CompanyInfoBase> frontPage) {
        if (!frontPage.is_search()) {
            Page<CompanyInfoVo> page = new Page<CompanyInfoVo>();
            Page<CompanyInfoVo> list = page.setRecords(iCompanyInfoBaseService.queryForOther(frontPage));
            return ResultJson.ok(list);
        }
        Page<CompanyInfoVo> page = new Page<CompanyInfoVo>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<CompanyInfoVo> list = page.setRecords(iCompanyInfoBaseService.queryForOther(page,frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "重点排污企业编辑查询 - editForFocus")
    @PostMapping(value = "editForFocus/{id}")
    public ResultJson editForFocus(@PathVariable("id") Integer id) {
        return ResultJson.ok(iCompanyInfoBaseService.editForFocus(id));
    }

    @ApiOperation(value = "其他企业编辑查询 - editForOther")
    @PostMapping(value = "editForOther/{id}")
    public ResultJson editForOther(@PathVariable("id") Integer id) {
        return ResultJson.ok(iCompanyInfoBaseService.editForOther(id));
    }

    @ApiOperation(value = "员工查询 - queryUsers")
    @PostMapping(value = "queryUsers/{id}")
    public ResultJson queryUsers(@PathVariable("id") Integer id) {
        return ResultJson.ok(iCompanyInfoBaseService.queryUsers(id));
    }

    @ApiOperation(value = "企业列表查询 - queryList")
    @PostMapping(value = "queryList")
    public ResultJson queryList() {
        EntityWrapper wrapper=new EntityWrapper();
        CompanyInfoBase base=new CompanyInfoBase();
        base.setDel(0);
        wrapper.setEntity(base);
        return ResultJson.ok(iCompanyInfoBaseService.selectList(wrapper));
    }

    @ApiOperation(value = "导出重点排污企业列表 - exportListForFocus" )
    @PostMapping(value = "exportListForFocus" )
    public ResultJson exportList(@RequestBody BasePage<CompanyInfoBase> frontPage, HttpServletResponse response) {
        iCompanyInfoBaseService.exportListForFocus(frontPage,response);
        return ResultJson.ok();
    }

    @ApiOperation(value = "导出企业列表 - exportListForOther" )
    @PostMapping(value = "exportListForOther" )
    public ResultJson exportListForOther(@RequestBody BasePage<CompanyInfoBase> frontPage, HttpServletResponse response) {
        iCompanyInfoBaseService.exportListForOther(frontPage,response);
        return ResultJson.ok();
    }

}

