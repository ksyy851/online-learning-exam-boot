package com.yuns.company.service.impl;

import com.yuns.company.entity.CompanyDocRel;
import com.yuns.company.mapper.CompanyDocRelMapper;
import com.yuns.company.service.ICompanyDocRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 企业-法律法规关联表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@Service
public class CompanyDocRelServiceImpl extends ServiceImpl<CompanyDocRelMapper, CompanyDocRel> implements ICompanyDocRelService {
        @Override
        public List<CompanyDocRel> selectCompanyDocRelList(FrontPage frontPage) {
        return   baseMapper.selectCompanyDocRelList(frontPage);
     }
}
