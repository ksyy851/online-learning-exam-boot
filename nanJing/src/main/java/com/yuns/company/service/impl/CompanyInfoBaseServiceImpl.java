package com.yuns.company.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.dto.CompanyInfoDto;
import com.yuns.company.entity.CompanyDocRel;
import com.yuns.company.entity.CompanyInfoBase;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.entity.CompanyUserRel;
import com.yuns.company.mapper.CompanyDocRelMapper;
import com.yuns.company.mapper.CompanyInfoBaseMapper;
import com.yuns.company.mapper.CompanyUserBaseMapper;
import com.yuns.company.mapper.CompanyUserRelMapper;
import com.yuns.company.service.ICompanyInfoBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.company.vo.CompanyInfoVo;
import com.yuns.study.entity.InfoDocI;
import com.yuns.study.vo.StudyCharVO;
import com.yuns.util.CommonExcel;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 企业信息基础表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
@Service
public class CompanyInfoBaseServiceImpl extends ServiceImpl<CompanyInfoBaseMapper, CompanyInfoBase> implements ICompanyInfoBaseService {

    @Resource
    CompanyDocRelMapper companyDocRelMapper;

    @Resource
    CompanyUserRelMapper companyUserRelMapper;

    @Resource
    CompanyUserBaseMapper companyUserBaseMapper;


    /**
     * 新增
     *
     * @param model
     * @return
     */
    @Transactional
    @Override
    public boolean insert(CompanyInfoDto model) {
        Integer flag = baseMapper.insert(model);
        if (flag != -1) {
            List<InfoDocI> infoDocIList = model.getInfoDocIList();
            infoDocIList.stream().forEach(item -> {
                Integer docId = item.getId();
                CompanyDocRel rel = new CompanyDocRel();
                rel.setCompanyId(model.getId());
                rel.setDocId(docId);
                companyDocRelMapper.insert(rel);
            });
            return true;
        }
        return false;
    }

    @Override
    public List<CompanyInfoVo> queryForFocus(BasePage<CompanyInfoBase> frontPage) {
        return baseMapper.queryForFocus(frontPage);
    }

    @Override
    public List<CompanyInfoVo> queryForFocus(Page<CompanyInfoVo> page,BasePage<CompanyInfoBase> frontPage) {
        return baseMapper.queryForFocus(page,frontPage);
    }

    @Override
    public List<CompanyInfoVo> queryForOther(BasePage<CompanyInfoBase> frontPage) {
        return baseMapper.queryForOther(frontPage);
    }

    @Override
    public List<CompanyInfoVo> queryForOther(Page<CompanyInfoVo> page,BasePage<CompanyInfoBase> frontPage) {
        return baseMapper.queryForOther(page,frontPage);
    }

    @Transactional
    @Override
    public boolean updateById(CompanyInfoDto model) {
        Integer flag = baseMapper.updateById(model);
        if (flag != -1) {
            List<InfoDocI> infoDocIList = model.getInfoDocIList();
            //法律法规修改
            EntityWrapper<CompanyDocRel> wrapper = new EntityWrapper<>();
            CompanyDocRel companyDocRel = new CompanyDocRel();
            companyDocRel.setCompanyId(model.getId());
            wrapper.setEntity(companyDocRel);
            companyDocRelMapper.delete(wrapper);
            infoDocIList.stream().forEach(item -> {
                Integer docId = item.getId();
                CompanyDocRel rel = new CompanyDocRel();
                rel.setCompanyId(model.getId());
                rel.setDocId(docId);
                companyDocRelMapper.insert(rel);
            });
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public boolean delete(Integer id) {
        CompanyInfoBase base = new CompanyInfoBase();
        base.setId(id);
        base.setDel(1);
        Integer flag = baseMapper.updateById(base);
        if (flag != -1) {
            //把企业下面的员工转成无企业下面
            EntityWrapper<CompanyUserRel> wrapper = new EntityWrapper<>();
            CompanyUserRel rel = new CompanyUserRel();
            rel.setCompanyId(id);
            wrapper.setEntity(rel);
            companyUserRelMapper.delete(wrapper);
            //修改用户企业类型
            companyUserBaseMapper.updateUserType(id);
            return true;
        }
        return false;
    }

    @Override
    public CompanyInfoVo editForFocus(Integer id) {
        return baseMapper.editForFocus(id);
    }

    @Override
    public CompanyInfoVo editForOther(Integer id) {
        return baseMapper.editForOther(id);
    }

    @Override
    public List<CompanyUserBase> queryUsers(Integer id) {
        return baseMapper.queryUsers(id);
    }

    /**
     * 重点排污企业导出
     * @param frontPage
     * @param response
     */
    @Override
    public void exportListForFocus(BasePage<CompanyInfoBase> frontPage, HttpServletResponse response) {
        try {
            List<CompanyInfoVo> list = baseMapper.queryForFocus(frontPage);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String fileName = "重点排污企业列表-" + df.format(new Date()) + ".xls";

            String[] rowsName = new String[]{"序号", "企业名称", "处罚日期", "录入日期", "处理期限","处罚文书号","地址","法人","已得积分","证书编号"};
            List<Object[]> dataList = new ArrayList<Object[]>();
            Object[] objs = null;
            for (int i = 0; i < list.size(); i++) {
                objs = new Object[rowsName.length];
                objs[0] = i;
                objs[1] = list.get(i).getCompanyName();
                if(list.get(i).getPulishTime()!=null)
                    objs[2] = df.format(list.get(i).getPulishTime());
                if(list.get(i).getCreateTime()!=null)
                    objs[3] = df.format(list.get(i).getCreateTime());
                if(list.get(i).getDealTime()!=null)
                    objs[4] = df.format(list.get(i).getDealTime());
                objs[5] = list.get(i).getPulishNo();
                objs[6] = list.get(i).getAddress();
                objs[7] = list.get(i).getPersonName();
                objs[8] = list.get(i).getIntegral();
                objs[9] = list.get(i).getCertNo();
                dataList.add(objs);
            }
            CommonExcel ex = new CommonExcel("重点排污企业列表情况", rowsName, dataList, response, fileName);
            ex.downloadExcel();
        } catch (Exception e) {
            throw new RuntimeException("重点排污企业列表导出失败");
        }
    }

    /**
     * 其他企业导出
     * @param frontPage
     * @param response
     */
    @Override
    public void exportListForOther(BasePage<CompanyInfoBase> frontPage, HttpServletResponse response) {
        try {
            List<CompanyInfoVo> list = baseMapper.queryForFocus(frontPage);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String fileName = "其他企业列表-" + df.format(new Date()) + ".xls";

            String[] rowsName = new String[]{"序号", "企业名称", "处罚日期", "录入日期", "处理期限","处罚文书号","地址","法人","已得积分","证书编号"};
            List<Object[]> dataList = new ArrayList<Object[]>();
            Object[] objs = null;
            for (int i = 0; i < list.size(); i++) {
                objs = new Object[rowsName.length];
                objs[0] = i;
                objs[1] = list.get(i).getCompanyName();
                if(list.get(i).getPulishTime()!=null)
                    objs[2] = df.format(list.get(i).getPulishTime());
                if(list.get(i).getCreateTime()!=null)
                    objs[3] = df.format(list.get(i).getCreateTime());
                if(list.get(i).getDealTime()!=null)
                    objs[4] = df.format(list.get(i).getDealTime());
                objs[5] = list.get(i).getPulishNo();
                objs[6] = list.get(i).getAddress();
                objs[7] = list.get(i).getPersonName();
                objs[8] = list.get(i).getIntegral();
                objs[9] = list.get(i).getCertNo();
                dataList.add(objs);
            }
            CommonExcel ex = new CommonExcel("其他企业列表情况", rowsName, dataList, response, fileName);
            ex.downloadExcel();
        } catch (Exception e) {
            throw new RuntimeException("其他企业列表导出失败");
        }
    }
}
