package com.yuns.company.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.dto.CompanyInfoDto;
import com.yuns.company.entity.CompanyInfoBase;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.vo.CompanyInfoVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 企业信息基础表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
public interface ICompanyInfoBaseService extends IService<CompanyInfoBase> {

    boolean insert(CompanyInfoDto model);

    List<CompanyInfoVo> queryForFocus(BasePage<CompanyInfoBase> frontPage);

    List<CompanyInfoVo> queryForFocus(Page<CompanyInfoVo> page,BasePage<CompanyInfoBase> frontPage);

    List<CompanyInfoVo> queryForOther(BasePage<CompanyInfoBase> frontPage);

    List<CompanyInfoVo> queryForOther(Page<CompanyInfoVo> page,BasePage<CompanyInfoBase> frontPage);

    boolean updateById(CompanyInfoDto model);

    boolean delete(Integer id);

    CompanyInfoVo editForFocus(Integer id);

    CompanyInfoVo editForOther(Integer id);

    List<CompanyUserBase> queryUsers(Integer id);

    void exportListForFocus(BasePage<CompanyInfoBase> frontPage, HttpServletResponse response);

    void exportListForOther(BasePage<CompanyInfoBase> frontPage, HttpServletResponse response);
}
