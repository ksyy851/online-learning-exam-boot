package com.yuns.company.service.impl;

import com.yuns.company.entity.CompanyUserRel;
import com.yuns.company.mapper.CompanyUserRelMapper;
import com.yuns.company.service.ICompanyUserRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.company.vo.ReceiverVo;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 企业用户关联表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
@Service
public class CompanyUserRelServiceImpl extends ServiceImpl<CompanyUserRelMapper, CompanyUserRel> implements ICompanyUserRelService {
        @Override
        public List<CompanyUserRel> selectCompanyUserRelList(BasePage frontPage) {
        return   baseMapper.selectCompanyUserRelList(frontPage);
     }

    @Override
    public List<ReceiverVo> getReceiverList() {
        return baseMapper.getReceiverList();
    }
}
