package com.yuns.company.service.impl;

import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.entity.CompanyUserIntegral;
import com.yuns.company.mapper.CompanyUserIntegralMapper;
import com.yuns.company.service.ICompanyUserBaseService;
import com.yuns.company.service.ICompanyUserIntegralService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.model.entity.SysUser;
import com.yuns.model.service.SysUserService;
import com.yuns.web.param.FrontPage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 用户学习积分表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-14
 */
@Service
public class CompanyUserIntegralServiceImpl extends ServiceImpl<CompanyUserIntegralMapper, CompanyUserIntegral> implements ICompanyUserIntegralService {

    @Resource
    ICompanyUserBaseService companyUserBaseService;

    @Override
    public List<CompanyUserIntegral> selectCompanyUserIntegralList(CompanyUserIntegral frontPage) {
        return baseMapper.selectCompanyUserIntegralList(frontPage);
    }

    @Override
    @Transactional
    public boolean insert(CompanyUserIntegral companyUserIntegral){
        Integer flag=baseMapper.insert(companyUserIntegral);
        if(flag!=-1){
            //同步用户积分
            CompanyUserBase companyUserBase=companyUserBaseService.selectById(companyUserIntegral.getCompanyUserId());
            companyUserBase.setIntegral(companyUserBase.getIntegral()+companyUserIntegral.getIntegral());
            return companyUserBaseService.updateById(companyUserBase);
        }
        return false;
    }
}
