package com.yuns.company.service;

import com.yuns.company.entity.CompanyUserBase;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户基础表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
public interface ICompanyUserBaseService extends IService<CompanyUserBase> {
    boolean deleteById(Integer id);

    List<CompanyUserBase> selectCompanyUserBaseList(BasePage frontPage);

    Map edit(Integer id);

    boolean disable(CompanyUserBase model);

    boolean updateById(CompanyUserBase model);
}
