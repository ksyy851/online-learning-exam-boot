package com.yuns.company.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yuns.company.entity.CompanyInfoBase;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.entity.CompanyUserRel;
import com.yuns.company.mapper.CompanyInfoBaseMapper;
import com.yuns.company.mapper.CompanyUserBaseMapper;
import com.yuns.company.mapper.CompanyUserRelMapper;
import com.yuns.company.service.ICompanyUserBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.exam.entity.Exam;
import com.yuns.exam.mapper.ExamMapper;
import com.yuns.exam.service.IExamService;
import com.yuns.exam.vo.ExamLibVo;
import com.yuns.model.entity.SysUser;
import com.yuns.model.service.SysUserService;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户基础表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
@Service
public class CompanyUserBaseServiceImpl extends ServiceImpl<CompanyUserBaseMapper, CompanyUserBase> implements ICompanyUserBaseService {

    @Resource
    CompanyUserRelMapper companyUserRelMapper;
    @Resource
    CompanyInfoBaseMapper companyInfoBaseMapper;

    @Resource
    SysUserService sysUserService;

    @Resource
    ExamMapper examMapper;

    @Transactional
    @Override
    public boolean deleteById(Integer id) {
        Integer flag=baseMapper.deleteById(id);
        if(flag!=-1){
            EntityWrapper<CompanyUserRel> wrapper=new EntityWrapper<>();
            CompanyUserRel rel=new CompanyUserRel();
            rel.setCompanyUserId(id);
            wrapper.setEntity(rel);
            companyUserRelMapper.delete(wrapper);
            return true;
        }
        return false;
    }

    @Override
        public List<CompanyUserBase> selectCompanyUserBaseList(BasePage frontPage) {
        return   baseMapper.selectCompanyUserBaseList(frontPage);
     }

    @Override
    public Map edit(Integer id) {
        Map res=new HashMap<>();
        CompanyUserBase user=baseMapper.selectById(id);
        //如果是企业用户则返回企业信息
        CompanyInfoBase companyInfoBase=new CompanyInfoBase();
        if(user.getUserType()==0){
            HashMap param=new HashMap();
            param.put("company_user_id",id);
            List<CompanyUserRel> rel=companyUserRelMapper.selectByMap(param);
            if(rel.size()>0){
                Integer companyId=rel.get(0).getCompanyId();
                companyInfoBase=companyInfoBaseMapper.selectById(companyId);
            }
        }
        res.put("user",user);
        res.put("companyInfo",companyInfoBase);
        return res;
    }

    @Override
    public boolean disable(CompanyUserBase model) {
        Integer flag=baseMapper.updateById(model);
        if(flag!=-1){
            return true;
        }
        return false;
    }

    /**
     * 更新个人课时/学时
     * @param model
     * @return
     */
    @Transactional
    @Override
    public boolean updateById(CompanyUserBase model){
        Integer flag=baseMapper.updateById(model);
        if(flag!=-1){
            //判断学时是否满足,满足则发送短信
            Integer companyType=baseMapper.selectCompanyTypeById(model.getId());
            if(companyType!=null && model.getTel()!=null){
                FrontPage frontPage=new FrontPage();
                List<ExamLibVo> examList=examMapper.selectExamList(frontPage);
                examList.stream()
                        .filter(c->c.getExamObjCompanyType()!=null && c.getExamObjCompanyType().equals(companyType.toString()))
                        .filter(c->c.getNeedStudyTime()<=model.getPeriod())
                        .forEach(item->{
                    //发送短信
                    String message="恭喜你,【"+item.getExamName()+"】已满足学时,可进入考试中心-开始考试了!";
                    sysUserService.sendSms(model.getTel(),message);
                });
            }
            return true;
        }
        return false;
    }
}
