package com.yuns.company.service;

import com.yuns.company.entity.CompanyUserIntegral;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 用户学习积分表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-14
 */
public interface ICompanyUserIntegralService extends IService<CompanyUserIntegral> {
    List<CompanyUserIntegral> selectCompanyUserIntegralList(CompanyUserIntegral frontPage);

    boolean insert(CompanyUserIntegral companyUserIntegral);
}
