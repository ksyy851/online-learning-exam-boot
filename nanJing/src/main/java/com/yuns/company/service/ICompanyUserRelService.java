package com.yuns.company.service;

import com.yuns.company.entity.CompanyUserRel;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.company.vo.ReceiverVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

import java.util.List;
/**
 * <p>
 * 企业用户关联表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-06
 */
public interface ICompanyUserRelService extends IService<CompanyUserRel> {
        List<CompanyUserRel> selectCompanyUserRelList(BasePage frontPage);
        List<ReceiverVo> getReceiverList();
}
