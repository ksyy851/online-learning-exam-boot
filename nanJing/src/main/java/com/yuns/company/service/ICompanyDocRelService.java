package com.yuns.company.service;

import com.yuns.company.entity.CompanyDocRel;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 企业-法律法规关联表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface ICompanyDocRelService extends IService<CompanyDocRel> {
        List<CompanyDocRel> selectCompanyDocRelList(FrontPage frontPage);
}
