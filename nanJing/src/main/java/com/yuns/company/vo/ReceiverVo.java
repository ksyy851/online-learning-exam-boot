package com.yuns.company.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description: TODO: 接收人
 * @author: zhansang
 * @create: 2020-11-08 17:11
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReceiverVo implements Serializable {
    @ApiModelProperty(value = "企业类型(0:重点排污企业;1:其他企业;2:无企业)")
    private  Integer companyType;
    @ApiModelProperty(value = "用户类型")
    private Integer userType;
    @ApiModelProperty(value = "企业名称")
    private String companyName;
    @ApiModelProperty(value = "用户名称")
    private String userName;
}
