package com.yuns.company.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yuns.company.entity.CompanyInfoBase;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.Date;

/**
 * 企业管理Vo
 */
@Data
public class CompanyInfoVo  {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 企业名称
     */
    @ApiParam(value = "企业名称")
    @ApiModelProperty(value = "企业名称")
    @TableField("company_name")
    private String companyName;
    /**
     * 处罚日期
     */
    @ApiParam(value = "处罚日期")
    @ApiModelProperty(value = "处罚日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("pulish_time")
    private Date pulishTime;
    /**
     * 处理日期
     */
    @ApiParam(value = "处理日期")
    @ApiModelProperty(value = "处理日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField("deal_time")
    private Date dealTime;
    /**
     * 处罚文件号
     */
    @ApiParam(value = "处罚文件号")
    @ApiModelProperty(value = "处罚文件号")
    @TableField("pulish_no")
    private String pulishNo;
    /**
     * 地址
     */
    @ApiParam(value = "地址")
    @ApiModelProperty(value = "地址")
    private String address;
    /**
     * 法人
     */
    @ApiParam(value = "法人")
    @ApiModelProperty(value = "法人")
    @TableField("person_name")
    private String personName;
    /**
     * 企业类型(0:重点排污企业;1:其他企业;2:无企业)
     */
    @ApiParam(value = "企业类型(0:重点排污企业;1:其他企业;2:无企业)")
    @ApiModelProperty(value = "企业类型(0:重点排污企业;1:其他企业;2:无企业)")
    @TableField("company_type")
    private Integer companyType;
    /**
     * 证书编号
     */
    @ApiParam(value = "证书编号")
    @ApiModelProperty(value = "证书编号")
    @TableField("cert_no")
    private String certNo;
    /**
     * 录入日期
     */
    @ApiParam(value = "录入日期")
    @ApiModelProperty(value = "录入日期")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 是否删除(0:未删除;1:已删除)
     */
    @ApiParam(value = "是否删除(0:未删除;1:已删除)")
    @ApiModelProperty(value = "是否删除(0:未删除;1:已删除)")
    private Integer del;

    /**
     * 所属法律法规ID
     */
    @ApiParam(value = "所属法律法规ID")
    @ApiModelProperty(value = "所属法律法规ID")
    @TableField("doc_id")
    private String docId;
    /**
     * 所属法律法规
     */
    @ApiParam(value = "所属法律法规")
    @ApiModelProperty(value = "所属法律法规")
    @TableField("doc_name")
    private String docName;

    /**
     * 已学课时
     */
    @ApiParam(value = "已学课时")
    @ApiModelProperty(value = "已学课时")
    private Integer period;

    /**
     * 已获积分
     */
    @ApiParam(value = "已获积分")
    @ApiModelProperty(value = "已获积分")
    private Integer integral;
}
