package com.yuns.config;

import feign.Client;
import feign.Feign;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import javax.net.ssl.SSLContext;

/**
 * Created by luwenchao on 2020-06-10.
 */
public class SSLFeignClientConfig {


    @Bean
    @Scope("prototype")
    @ConditionalOnMissingBean
    public Feign.Builder feignBuilder() {
        Client client;
        try {
            SSLContext context =
                    new SSLContextBuilder()
                            .loadTrustMaterial(null, (chain, authType) -> true)
                            .build();
            client = new Client.Default(context.getSocketFactory(), new NoopHostnameVerifier());
        } catch (Exception e) {
//            logger.error("Create feign client with SSL config failed", e);
            client = new Client.Default(null, null);
        }
        return Feign.builder().client(client);
    }
}
