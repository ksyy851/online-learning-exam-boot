package com.yuns.config;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yuns.exam.entity.Exam;
import com.yuns.exam.service.IExamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: TODO: 定时任务
 * @author: zhansang
 * @create: 2020-11-17 20:13
 * @version: 1.0
 **/
@Slf4j
@Component
@EnableAutoConfiguration
@RestController
public class SchedulingTask {

    @Autowired
    IExamService examService;

    /**
     * 倒计时统计
     */
    @Scheduled(fixedRate = 60000)//每隔一分钟查一次
    public void CheckExam() {

        HashMap map = new HashMap();
        map.put("selectedType","id");
        map.put("type","1252601196249673730");
        Long midTime = 60*60L;
        List<HashMap<String,Object>> examList = examService.selectList(map);
        if (examList != null && examList.size() > 0) {
            Map exam1 = examList.get(0);
            Date bt = (Date) exam1.get("start_time");
            Date et = (Date) exam1.get("end_time");
            Long endTime = et.getTime();
            Long startTime = bt.getTime();
//            midTime = Math.abs(endTime-startTime)/1000;
        }


        String msg="";
        while (midTime > 0) {
            midTime--;
            long hh = midTime / 60 / 60 % 60;
            long mm = midTime / 60 % 60;
            long ss = midTime % 60;
//            log.info("还剩" + hh + "小时" + mm + "分钟" + ss + "秒");
            msg = "还剩" + hh + "小时" + mm + "分钟" + ss + "秒";
            if(hh == 0 && mm==0 && ss==0){
                break;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        try {
            WebsocketServer.sendInfo(msg, "999");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
