package com.yuns.model.mapper;

import com.yuns.model.entity.SysUserGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户组表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysUserGroupMapper extends BaseMapper<SysUserGroup> {

}
