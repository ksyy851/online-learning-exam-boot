package com.yuns.model.mapper;

import com.yuns.model.entity.SysDept;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 组织表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    List<SysDept> queryListParentId(@Param("id") Long id);
}
