package com.yuns.model.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.model.entity.SysRole;
import org.springframework.stereotype.Repository;

import javax.management.relation.Role;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-25
 */
@Repository
public interface SysRoleMapper extends BaseMapper<SysRole> {

    public List<SysRole> selectRoleByName(Map<String,String> map);

    List<SysRole> queryRoleByUserId(int id);

}
