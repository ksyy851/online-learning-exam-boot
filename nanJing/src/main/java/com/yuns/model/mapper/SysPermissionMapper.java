package com.yuns.model.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.model.entity.SysPermission;
import com.yuns.model.entity.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-25
 */
@Repository
public interface SysPermissionMapper extends BaseMapper<SysPermission> {

    public List<SysPermission> selectPermissionByName(Map<String,String> map);

    List<SysPermission> selectPermissionByUser(SysUser user);

    List<SysPermission> queryPermissionByRole(@Param("id") Long id);
}
