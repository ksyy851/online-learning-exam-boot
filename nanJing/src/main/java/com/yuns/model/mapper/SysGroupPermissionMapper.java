package com.yuns.model.mapper;

import com.yuns.model.entity.SysGroupPermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 组权限表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysGroupPermissionMapper extends BaseMapper<SysGroupPermission> {

}
