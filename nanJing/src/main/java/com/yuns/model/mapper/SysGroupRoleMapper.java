package com.yuns.model.mapper;

import com.yuns.model.entity.SysGroupRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 组角色表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysGroupRoleMapper extends BaseMapper<SysGroupRole> {

}
