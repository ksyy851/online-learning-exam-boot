package com.yuns.model.mapper;

import com.yuns.model.entity.SysLogin;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysLoginMapper extends BaseMapper<SysLogin> {

    List<SysLogin> selectSysLoginList(@Param("frontPage") FrontPage<QueryParam> frontPage);
}
