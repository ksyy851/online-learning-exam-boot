package com.yuns.model.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-08-01
 */
@Repository
public interface SysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> queryUserBySysDeptId(@Param("id") Integer id);

    List<SysUser> selectUserList(Page<SysUser> page , FrontPage<SysUser> frontPage);

}
