package com.yuns.model.mapper;

import com.yuns.model.entity.SysRolePermission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysRolePermissionMapper extends BaseMapper<SysRolePermission> {

}
