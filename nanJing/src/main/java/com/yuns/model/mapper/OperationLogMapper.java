package com.yuns.model.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.OperationLog;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 操作日志 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2019-10-28
 */
public interface OperationLogMapper extends BaseMapper<OperationLog> {

    List<OperationLog> selectOperationLogList(FrontPage frontPage, Page<OperationLog> page);
}
