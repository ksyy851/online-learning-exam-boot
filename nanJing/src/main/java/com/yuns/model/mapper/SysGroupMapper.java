package com.yuns.model.mapper;

import com.yuns.model.entity.SysGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 组表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysGroupMapper extends BaseMapper<SysGroup> {

}
