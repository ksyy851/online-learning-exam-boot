package com.yuns.model.mapper;

import com.yuns.model.entity.SysDict;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author luwenchao
 * @since 2019-11-14
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    List<SysDict> selectListByCode(@Param("code") String code);
}
