package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@TableName("sys_user_role")
public class SysUserRole extends Model<SysUserRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 记录标识
     */
    @ApiParam(value = "记录标识")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 角色ID
     */
    @ApiParam(value = "角色ID")
    @TableField("role_id")
    private Long roleId;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    @TableField("user_id")
    private Long userId;

    /**
     * 角色名称
     */
    @ApiParam(value = "角色名称")
    @TableField(exist = false)
    private String roleName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysUserRole{" +
        ", id=" + id +
        ", roleId=" + roleId +
        ", userId=" + userId +
        "}";
    }
}
