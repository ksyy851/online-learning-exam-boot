package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 组权限表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@TableName("sys_group_permission")
public class SysGroupPermission extends Model<SysGroupPermission> {

    private static final long serialVersionUID = 1L;

    /**
     * 记录标识
     */
    @ApiParam(value = "记录标识")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 权限ID
     */
    @ApiParam(value = "权限ID")
    @TableField("permission_id")
    private Long permissionId;
    /**
     * 组ID
     */
    @ApiParam(value = "组ID")
    @TableField("group_id")
    private Long groupId;
    /**
     * 权限类型（0:可访问，1:可授权）
     */
    @ApiParam(value = "权限类型（0:可访问，1:可授权）")
    @TableField("permission_type")
    private Integer permissionType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Integer getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(Integer permissionType) {
        this.permissionType = permissionType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysGroupPermission{" +
        ", id=" + id +
        ", permissionId=" + permissionId +
        ", groupId=" + groupId +
        ", permissionType=" + permissionType +
        "}";
    }
}
