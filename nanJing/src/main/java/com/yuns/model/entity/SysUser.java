package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.yuns.util.ExcelAttribute;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author luwenchao
 * @since 2018-08-01
 */
@Data
@TableName("sys_user")
public class SysUser extends Model<SysUser> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")

    @TableId(value = "id", type = IdType.AUTO)
    @ExcelAttribute(sort = 0)
    private Integer id;
    /**
     * 用户名称
     */
    @ApiParam(value = "用户名称")
    @TableField("user_name")
    @ApiModelProperty(value = "用户名称")
    @ExcelAttribute(sort = 1)
    private String userName;
    /**
     * 登录帐号
     */
    @ApiParam(value = "登录帐号")
    @ApiModelProperty(value = "登录帐号")
    @TableField("account")
    @ExcelAttribute(sort = 2)
    private String account;



    /**
     * 部门名称
     */
    @ApiParam(value = "用户编号")
    @ApiModelProperty(value = "部门名称")
    @TableField(exist = false)
    @ExcelAttribute(sort = 4)
    private String deptName;


    /**
     * 部门名称
     */
    @ApiParam(value = "部门名称")
    @TableField(exist = false)
    private List<Integer> serviceIds;


    /**
     * 部门id
     */
    @ApiParam(value = "部门id")
    @TableField("dept_id")
    @ApiModelProperty(value = "部门id")
    private Integer deptId;


    /**
     * 行政区域
     */
    @ApiParam(value = "行政区域")
    @TableField(exist = false)
    private String xareaid;

    /**
     * 地址
     */
    @ApiParam(value = "地址")
    @TableField("address")
    @ExcelAttribute(sort = 6)
    private String address;
    /**
     * 用户密码
     */
    @ApiParam(value = "用户密码")
    @ExcelAttribute(sort = 7)
    private String password;
    /**
     * 简称
     */
    @ApiParam(value = "简称")
    @TableField("short_name")

    @ExcelAttribute(sort = 8)
    private String shortName;
    /**
     * 全称
     */
    @ApiParam(value = "全称")
    @TableField("full_name")
    private String fullName;
    /**
     * 手机号
     */
    @ApiParam(value = "手机号")
    @ExcelAttribute(sort = 9)
    @ApiModelProperty(value = "手机号")
    private String phone;
    /**
     * 电子邮箱
     */
    @ApiParam(value = "电子邮箱")
    private String email;
    /**
     * 用户身份(0:用水用户 1:平台用户)
     */
    @ApiParam(value = "用户身份(1:平台用户)")
    private Integer type;

    /**
     * 维修类型
     */
    @ApiParam(value = "维修类型")
    @TableField(exist = false)
    private Integer repairType;


    /**
     * 登录时间
     */
    @ApiParam(value = "登录时间")
    @TableField("login_date")
    private Date loginDate;

    /**
     * 上次登录时间
     */
    @ApiParam(value = "上次登录时间")
    @TableField("last_login_date")
    private Date lastLoginDate;

    /**
     * 身份证
     */
    @ApiParam(value = "身份证")
    @TableField("brith_id")
    @ApiModelProperty(value = "身份证")
    @ExcelAttribute(sort = 10)

    private String brithId;

    /**
     * 用户头像
     */
    @ApiParam(value = "用户头像")
    private String pic;
    /**
     * 设备类型
     */
    @ApiParam(value = "设备类型")
    @TableField("device_type")
    private String deviceType;

    /**
     * 所在区域
     */
    @ApiParam(value = "所在区域")
    @TableField("area_id")
    private Integer areaId;
    /**
     * 用户状态
     */
    @ApiParam(value = "用户状态")
    private String status;
    /**
     * 创建人
     */
    @ApiParam(value = "创建人")
    @TableField("created_by")
    private String createdBy;
    /**
     * 创建日期
     */
    @ApiParam(value = "创建日期")
    @TableField("created_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdDate;
    /**
     * 最后更新人
     */
    @ApiParam(value = "最后更新人")
    @TableField("last_modified_by")
    private String lastModifiedBy;
    /**
     * 更新日期
     */
    @ApiParam(value = "更新日期")
    @TableField("last_modified_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastModifiedDate;

    /**
     * 描述
     */
    @ApiParam(value = "描述")
    private String description;
    /**
     * 盐值
     */
    @ApiParam(value = "盐值")
    private String salt;
    /**
     * 经度
     */
    @ApiParam(value = "经度")
    private String longitude;
    /**
     * 纬度
     */
    @ApiParam(value = "纬度")
    private String latitude;
    /**
     * 手机绑定时间
     */
    @ApiParam(value = "手机绑定时间")
    @TableField("phone_bindtime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date phoneBindtime;
    /**
     * 简介
     */
    @ApiParam(value = "简介")
    private String profile;
    /**
     * 设备
     */
    @ApiParam(value = "设备")
    private String instrument;
    /**
     * 微信号
     */
    @ApiParam(value = "微信号")
    @ApiModelProperty(value = "微信号")
    private String wechat;

    /**
     * 权限列表
     */
    @ApiParam(value = "权限列表")
    @TableField(exist = false)
    private List<SysPermission> permissions;

    /**
     * 角色列表
     */
    @ApiParam(value = "角色列表")
    @TableField(exist = false)
    private List<SysUserRole> sysUserRoles;


    /**
     * 角色列表
     */
    @ApiParam(value = "角色列表")
    @TableField(exist = false)
    private List<Long> roleIds;

    /**
     * 验证码
     */
    @ApiParam(value = "验证码")
    @ApiModelProperty(value = "验证码")
    @TableField(exist = false)
    private String code;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
