package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 操作日志表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@TableName("sys_login")
public class SysLogin extends Model<SysLogin> {

    private static final long serialVersionUID = 1L;

    /**
     * 日志ID
     */
    @ApiParam(value = "日志ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户名
     */
    @ApiParam(value = "用户名")
    @TableField("created_by")
    private String createdBy;

    /**
     * 用户操作
     */
    @ApiParam(value = "用户操作")
    private String operation;

    /**
     * 参数
     */
    @ApiParam(value = "参数")
    private String params;
    /**
     * IP地址
     */
    @ApiParam(value = "IP地址")
    @TableField("ip_addr")
    private String ipAddr;

    /**
     * 任务状态    0：成功    1：失败
     */
    @ApiParam(value = "任务状态    0：成功    1：失败")
    private Integer status;
    /**
     * 失败信息
     */
    @ApiParam(value = "失败信息")
    private String error;

    /**
     * 耗时(单位：毫秒)
     */
    @ApiParam(value = "耗时(单位：毫秒)")
    private Long times;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private String payload;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Long getTimes() {
        return times;
    }

    public void setTimes(Long times) {
        this.times = times;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysLogin{" +
                ", id=" + id +
                ", createdBy=" + createdBy +
                ", operation=" + operation +
                ", params=" + params +
                ", ipAddr=" + ipAddr +
                ", status=" + status +
                ", error=" + error +
                ", times=" + times +
                ", createTime=" + createTime +
                ", payload=" + payload +
                "}";
    }
}
