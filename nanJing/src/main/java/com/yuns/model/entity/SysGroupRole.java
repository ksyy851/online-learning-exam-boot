package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 组角色表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@TableName("sys_group_role")
public class SysGroupRole extends Model<SysGroupRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 记录标识
     */
    @ApiParam(value = "记录标识")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 角色ID
     */
    @ApiParam(value = "角色ID")
    @TableField("role_id")
    private Long roleId;
    /**
     * 组ID
     */
    @ApiParam(value = "组ID")
    @TableField("group_id")
    private Long groupId;
    /**
     * 权限类型（0:可访问，1:可授权）
     */
    @ApiParam(value = "权限类型（0:可访问，1:可授权）")
    @TableField("permission_type")
    private Integer permissionType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Integer getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(Integer permissionType) {
        this.permissionType = permissionType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysGroupRole{" +
        ", id=" + id +
        ", roleId=" + roleId +
        ", groupId=" + groupId +
        ", permissionType=" + permissionType +
        "}";
    }
}
