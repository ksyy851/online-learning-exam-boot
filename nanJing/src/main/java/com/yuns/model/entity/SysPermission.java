package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.yuns.web.sys.dto.Action;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 菜单管理
 * </p>
 *
 * @author ${author}
 * @since 2019-12-13
 */
@TableName("sys_permission")
public class SysPermission extends Model<SysPermission> {
    private static final long serialVersionUID = 1L;

    /**
     * 编号
     */
    @ApiParam(value = "编号")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 菜单名称
     */
    @ApiParam(value = "菜单名称")
    private String title;


    @ApiParam(value = "父菜单名称")
    @TableField(exist = false)
    private String parentTitle;


    /**
     * 路径
     */
    @ApiParam(value = "路径")
    private String path;

    /**
     * 传参
     */
    @ApiParam(value = "传参")
    private String param;
    /**
     * 父菜单ID，一级菜单为0
     */
    @ApiParam(value = "父菜单ID，一级菜单为0")
    @TableField("parent_id")
    private Long parentId;

    /**
     * 父菜单名称
     */
    @ApiParam(value = "父菜单名称")
    @TableField(exist = false)
    private String parentName;
    /**
     * 菜单URL,类型：1.普通页面（如用户管理， /sys/com.yuns.user） 2.嵌套完整外部页面，以http(s)开头的链接 3.嵌套服务器页面，使用iframe:前缀+目标URL(如SQL监控， iframe:/druid/login.html, iframe:前缀会替换成服务器地址)
     */
    @ApiParam(value = "菜单URL,类型：1.普通页面（如用户管理， /sys/com.yuns.user） 2.嵌套完整外部页面，以http(s)开头的链接 3.嵌套服务器页面，使用iframe:前缀+目标URL(如SQL监控， iframe:/druid/login.html, iframe:前缀会替换成服务器地址)")
    private String url;
    /**
     * 授权(多个用逗号分隔，如：sys:com.yuns.user:add,sys:com.yuns.user:edit)
     */
    @ApiParam(value = "授权(多个用逗号分隔，如：sys:com.yuns.user:add,sys:com.yuns.user:edit)")
    private String perms;
    /**
     * 类型   0：目录   1：菜单   2：按钮
     */
    @ApiParam(value = "类型   0：目录   1：菜单   2：按钮")
    private Integer type;
    /**
     * 菜单图标
     */
    @ApiParam(value = "菜单图标")
    private String icon;
    /**
     * 层级
     */
    @ApiParam(value = "层级")
    @TableField(exist = false)
    private Integer level;
    /**
     * 排序
     */
    @ApiParam(value = "排序")
    @TableField("order_num")
    private Integer orderNum;
    /**
     * 创建人
     */
    @ApiParam(value = "创建人")
    @TableField("create_by")
    private Integer createBy;

    /**
     * 权限子集
     */
    @ApiParam(value = "权限子集")
    @TableField(exist = false)
    private List<SysPermission> children;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    /**
     * 是否删除  -1：已删除  0：正常
     */
    @ApiParam(value = "是否删除  -1：已删除  0：正常")
    @TableField("del_flag")
    private Integer delFlag;

    private String name;

    private String redirect;

    private String component;



    private Integer hidden;
    /**
     * 是否删除  -1：已删除  0：正常
     */
    @ApiParam("所有权限1")
    @TableField(exist = false)
    private  String actions;



   @ApiParam("所有权限")
   @TableField(exist = false)
   private Set<Action> actionEntitySet;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public Set<Action> getActionEntitySet() {
        return actionEntitySet;
    }

    public void setActionEntitySet(Set<Action> actionEntitySet) {
        this.actionEntitySet = actionEntitySet;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }



    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public Integer getHidden() {
        return hidden;
    }

    public void setHidden(Integer hidden) {
        this.hidden = hidden;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public List<SysPermission> getChildren() {
        return children;
    }

    public void setChildren(List<SysPermission> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "SysMenu{" +
        ", id=" + id +
        ", title=" + title +
        ", path=" + path +
        ", parentId=" + parentId +
        ", url=" + url +
        ", perms=" + perms +
        ", type=" + type +
        ", icon=" + icon +
        ", orderNum=" + orderNum +
        ", createBy=" + createBy +
        ", createTime=" + createTime +
        ", delFlag=" + delFlag +
        ", name=" + name +
        ", redirect=" + redirect +
        ", component=" + component +
        ", hidden=" + hidden +
        "}";
    }
}
