package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 角色权限表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@TableName("sys_role_permission")
public class SysRolePermission extends Model<SysRolePermission> {

    private static final long serialVersionUID = 1L;

    /**
     * 记录标识
     */
    @ApiParam(value = "记录标识")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 权限ID
     */
    @ApiParam(value = "权限ID")
    @TableField("permission_id")
    private Long permissionId;
    /**
     * 角色ID
     */
    @ApiParam(value = "角色ID")
    @TableField("role_id")
    private Long roleId;
    /**
     * 权限类型（0:可访问，1:可授权）
     */
    @ApiParam(value = "权限类型（0:可访问，1:可授权）")
    @TableField("permission_type")
    private Integer permissionType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Long permissionId) {
        this.permissionId = permissionId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Integer getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(Integer permissionType) {
        this.permissionType = permissionType;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysRolePermission{" +
        ", id=" + id +
        ", permissionId=" + permissionId +
        ", roleId=" + roleId +
        ", permissionType=" + permissionType +
        "}";
    }
}
