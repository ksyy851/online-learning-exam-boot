package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@TableName("sys_role")
public class SysRole extends Model<SysRole> {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
    @ApiParam(value = "角色ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 父级角色ID
     */
    @ApiParam(value = "父级角色ID")
    @TableField("parent_tr_id")
    private Long parentTrId;
    /**
     * 角色名称
     */
    @ApiParam(value = "角色名称")
    @TableField("role_name")
    private String roleName;
    /**
     * 创建人
     */
    @ApiParam(value = "创建人")
    @TableField("created_by")
    private String createdBy;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @TableField("created_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createdDate;
    /**
     * 最后修改人
     */
    @ApiParam(value = "最后修改人")
    @TableField("last_modified_by")
    private String lastModifiedBy;
    /**
     * 最后修改时间
     */
    @ApiParam(value = "最后修改时间")
    @TableField("last_modified_date")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date lastModifiedDate;
    /**
     * 备注
     */
    @ApiParam(value = "备注")
    private String description;

    @ApiParam(value = "备注")
    @TableField(exist = false)
    private  List<SysPermission> permissions;

    @ApiParam(value = "备注")
    @TableField(exist = false)
    private  List<Long> permissionIds;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentTrId() {
        return parentTrId;
    }

    public void setParentTrId(Long parentTrId) {
        this.parentTrId = parentTrId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public List<SysPermission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<SysPermission> permissions) {
        this.permissions = permissions;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                "id=" + id +
                ", parentTrId=" + parentTrId +
                ", roleName='" + roleName + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdDate=" + createdDate +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedDate=" + lastModifiedDate +
                ", description='" + description + '\'' +
                ", permissions=" + permissions +
                ", permissionIds=" + permissionIds +
                '}';
    }

    public List<Long> getPermissionIds() {
        return permissionIds;
    }

    public void setPermissionIds(List<Long> permissionIds) {
        this.permissionIds = permissionIds;
    }
}
