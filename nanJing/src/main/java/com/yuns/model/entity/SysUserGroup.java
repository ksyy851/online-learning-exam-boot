package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 用户组表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@TableName("sys_user_group")
public class SysUserGroup extends Model<SysUserGroup> {

    private static final long serialVersionUID = 1L;

    /**
     * 记录标识
     */
    @ApiParam(value = "记录标识")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 组ID
     */
    @ApiParam(value = "组ID")
    @TableField("group_id")
    private Long groupId;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    @TableField("user_id")
    private Long userId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysUserGroup{" +
        ", id=" + id +
        ", groupId=" + groupId +
        ", userId=" + userId +
        "}";
    }
}
