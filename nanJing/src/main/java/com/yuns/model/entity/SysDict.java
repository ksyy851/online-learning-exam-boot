package com.yuns.model.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 字典表
 * </p>
 *
 * @author luwenchao
 * @since 2019-11-14
 */
@TableName("sys_dict")
public class SysDict extends Model<SysDict> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiParam(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 父id
     */
    @ApiParam(value = "父id")
    private Integer pid;
    /**
     * 字典名称
     */
    @ApiParam(value = "字典名称")
    private String name;
    /**
     * 字典码
     */
    @ApiParam(value = "字典码")
    private String code;
    /**
     * 字典值
     */
    @ApiParam(value = "字典值")
    private String value;
    /**
     * 排序
     */
    @ApiParam(value = "排序")
    private Integer ordered;
    /**
     * 权重(可以用来排序，或者表示子父关系,备用)
     */
    @ApiParam(value = "权重(可以用来排序，或者表示子父关系,备用)")
    private Integer weight;
    /**
     * 备注
     */
    @ApiParam(value = "备注")
    private String remark;
    /**
     * pid
     */
    @ApiParam(value = "parentName")
    @TableField(exist = false)
    private String parentName;
    /**
     * 创建人
     */
    @ApiParam(value = "子区域")
    @TableField(exist = false)
    private List<SysDict> children;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 删除标记（0删除，1未删除）
     */
    @ApiParam(value = "删除标记（0删除，1未删除）")
    @TableField("delete_flag")
    private Integer deleteFlag;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getOrdered() {
        return ordered;
    }

    public void setOrdered(Integer ordered) {
        this.ordered = ordered;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Integer deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public List<SysDict> getChildren() {
        return children;
    }

    public void setChildren(List<SysDict> children) {
        this.children = children;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }


    @Override
    public String toString() {
        return "SysDict{" +
                "id=" + id +
                ", pid=" + pid +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", value='" + value + '\'' +
                ", ordered=" + ordered +
                ", weight=" + weight +
                ", remark='" + remark + '\'' +
                ", parentName='" + parentName + '\'' +
                ", children=" + children +
                ", createTime=" + createTime +
                ", deleteFlag=" + deleteFlag +
                '}';
    }
}
