package com.yuns.model.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysLogin;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;

/**
 * <p>
 * 操作日志表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysLoginService extends IService<SysLogin> {

    Page<SysLogin> selectSysLoginList(FrontPage<QueryParam> data);
}
