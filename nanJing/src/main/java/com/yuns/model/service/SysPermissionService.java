package com.yuns.model.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.model.entity.SysPermission;
import com.yuns.web.sys.param.MetaParam;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-25
 */
public interface SysPermissionService extends IService<SysPermission> {
    public List<SysPermission> selectPermissionByName(Map<String,String> map);

    List<SysPermission> findTree(Integer menuType);

    List<SysPermission> queryPermissionByRole(Long id);

    Set<MetaParam> findPermission() throws UnsupportedEncodingException;


    List<SysPermission> findTree();

}
