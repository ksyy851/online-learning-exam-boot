package com.yuns.model.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.model.entity.SysRole;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-25
 */
public interface SysRoleService extends IService<SysRole> {

    public List<SysRole> selectRoleByName(Map<String,String> map);

    List<SysRole> queryRoleByUserId(int i);

    List<SysRole> selectSysRoleList();

    List<SysRole> queryPermissionIdS();


}
