package com.yuns.model.service;

import com.yuns.model.entity.SysRolePermission;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.dto.SysRolePermissionDto;

import java.util.List;

/**
 * <p>
 * 角色权限表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysRolePermissionService extends IService<SysRolePermission> {

    boolean insertRolePermission(List<SysRolePermission> model);
}
