package com.yuns.model.service.impl;

import com.yuns.model.entity.SysGroupRole;
import com.yuns.model.mapper.SysGroupRoleMapper;
import com.yuns.model.service.SysGroupRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 组角色表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysGroupRoleServiceImpl extends ServiceImpl<SysGroupRoleMapper, SysGroupRole> implements SysGroupRoleService {

}
