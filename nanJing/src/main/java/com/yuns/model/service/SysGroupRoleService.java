package com.yuns.model.service;

import com.yuns.model.entity.SysGroupRole;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 组角色表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysGroupRoleService extends IService<SysGroupRole> {

}
