package com.yuns.model.service.impl;

import com.yuns.model.entity.SysGroupPermission;
import com.yuns.model.mapper.SysGroupPermissionMapper;
import com.yuns.model.service.SysGroupPermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 组权限表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysGroupPermissionServiceImpl extends ServiceImpl<SysGroupPermissionMapper, SysGroupPermission> implements SysGroupPermissionService {

}
