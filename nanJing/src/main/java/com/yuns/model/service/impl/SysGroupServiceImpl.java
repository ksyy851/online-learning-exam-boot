package com.yuns.model.service.impl;

import com.yuns.model.entity.SysGroup;
import com.yuns.model.mapper.SysGroupMapper;
import com.yuns.model.service.SysGroupService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 组表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysGroupServiceImpl extends ServiceImpl<SysGroupMapper, SysGroup> implements SysGroupService {

}
