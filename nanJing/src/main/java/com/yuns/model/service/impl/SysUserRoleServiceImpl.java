package com.yuns.model.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.yuns.exception.MyException;
import com.yuns.model.entity.SysUserRole;
import com.yuns.model.mapper.SysUserRoleMapper;
import com.yuns.model.service.SysUserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.sys.param.UserRoles;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements SysUserRoleService {

    @Override
    public boolean insertSysUserRole(UserRoles sysUserRoles) {
        String[] ids = sysUserRoles.getIds();
        if(ids.length == 0){
            throw  new MyException("用户没有绑定角色",500);
        }
        Integer userId = sysUserRoles.getUserId();
        Wrapper<SysUserRole> tWrapper = new EntityWrapper<>();
        tWrapper.eq("user_id",userId);
        baseMapper.delete(tWrapper);
        for (String id : ids) {
            SysUserRole sysUserRole=new SysUserRole();
            sysUserRole.setUserId(Long.valueOf(userId));
            sysUserRole.setRoleId(Long.valueOf(id));
            baseMapper.insert(sysUserRole);
        }
        return true;
    }

    @Override
    public List<SysUserRole> selectUserRoleList(Integer userId) {
        return baseMapper.selectUserRoleList(userId);
    }

}
