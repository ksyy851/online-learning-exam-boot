package com.yuns.model.service;

import com.yuns.model.entity.SysGroupPermission;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 组权限表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysGroupPermissionService extends IService<SysGroupPermission> {

}
