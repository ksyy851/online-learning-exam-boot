package com.yuns.model.service.impl;

import com.yuns.model.entity.SysUserGroup;
import com.yuns.model.mapper.SysUserGroupMapper;
import com.yuns.model.service.SysUserGroupService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户组表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysUserGroupServiceImpl extends ServiceImpl<SysUserGroupMapper, SysUserGroup> implements SysUserGroupService {

}
