package com.yuns.model.service.impl;

import com.yuns.exception.MyException;
import com.yuns.model.entity.SysDept;
import com.yuns.model.mapper.SysDeptMapper;
import com.yuns.model.service.SysDeptService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.util.StringUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 组织表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Override
    public List<SysDept> selectSysDeptList() {
        List<SysDept> sysDepts = baseMapper.selectList(null);

        List<SysDept> list = new ArrayList<>();
        sysDepts.stream().forEach(x -> {
                    if (x.getParentId() == 0) {
                        list.add(x);
                    }
                }
        );
        findChildren(list, sysDepts);
        return list;
    }
    private void findChildren(List<SysDept> sysDeptList, List<SysDept> sysDepts) {
        for (SysDept sysDept : sysDeptList) {
            List<SysDept> children = new ArrayList<>();
            for (SysDept sysDept1 : sysDepts) {
                if (sysDept.getId() != null && sysDept.getId().equals(sysDept1.getParentId())) {
                    sysDept.setParentName(sysDept.getName());
                    children.add(sysDept1);
                }
            }
            sysDept.setChildren(children);
            findChildren(children, sysDepts);
        }

    }
    @Override
    public boolean deleteSysDeptById(Long id) {
        List<SysDept> sysDeptList = baseMapper.queryListParentId(id);
        if (sysDeptList.size() > 0) {
            throw new MyException("请先删除子部门", 500);
        }
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    public SysDept querySysDeptByChildren(String deptId) {
        if (!StringUtil.notEmptyString(deptId)) {
            throw new MyException("部门id为空", 500);
        }
        SysDept sysDept = baseMapper.selectById(deptId);
        if (sysDept.getParentId() == 0) {
            return sysDept;
        }
        List<Long> result1 = new ArrayList<Long>();
        selectFather(Long.valueOf(deptId), result1);
        Collections.sort(result1);
        System.out.println("部门测试" + result1);
        if (result1.size() > 0) {
            List<SysDept> sysDepts = selectSysDeptList();
            SysDept sysDept1 = sysDepts.stream().filter(y ->
                    y.getId().equals(result1.get(0))
            ).findFirst().get();
            return sysDept1;
        } else {
            return null;
        }
    }

    @Override
    public List<Long> querySysDeptByChildrenLong(Long deptId) {

        List<Long> result1 = new ArrayList<Long>();
        SysDept sysDept = baseMapper.selectById(deptId);
        if (sysDept==null){
            throw new  MyException("当前部门不存在");
        }
        if (sysDept.getParentId() == 0) {
            result1.add(deptId);
            return result1;
        }
        selectFather(Long.valueOf(deptId), result1);
        result1.add(deptId);
        Collections.sort(result1);
        return result1;
    }


    public void selectFather(Long id, List<Long> result1) {
//查询数据库中对应id的实体类
        SysDept sysDept = baseMapper.selectById(id);
        //查询父级架构
        //此处使用mybaatisPlus的条件构造器，查询id等于pid的对象
        SysDept w = new SysDept();
        w.setId(sysDept.getParentId());
        //查询出符合条件的对象
        SysDept sysDept1 = baseMapper.selectOne(w);
        if (sysDept1 != null) {
            //如果查出的对象不为空，则将此对象的id存到全局变量中，并且继续调用自己，即递归，一直到查询不到为止
            result1.add(sysDept1.getId());
            selectFather(sysDept1.getId(), result1);
        }
    }


}
