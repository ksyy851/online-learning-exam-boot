package com.yuns.model.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysLogin;
import com.yuns.model.mapper.SysLoginMapper;
import com.yuns.model.service.SysLoginService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysLoginServiceImpl extends ServiceImpl<SysLoginMapper, SysLogin> implements SysLoginService {

    @Override
    public Page<SysLogin> selectSysLoginList(FrontPage<QueryParam> frontPage) {
        Page<SysLogin> page = new Page<SysLogin>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<SysLogin> list = page.setRecords(baseMapper.selectSysLoginList(frontPage));
        return list;
    }
}
