package com.yuns.model.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.exception.MyException;
import com.yuns.model.entity.SysRolePermission;
import com.yuns.model.mapper.SysRolePermissionMapper;
import com.yuns.model.service.SysRolePermissionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色权限表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
@Service
public class SysRolePermissionServiceImpl extends ServiceImpl<SysRolePermissionMapper, SysRolePermission> implements SysRolePermissionService {

    @Override
    @Transactional
    public boolean insertRolePermission(List<SysRolePermission> model) {
        Long roleId = model.get(0).getRoleId();
        Wrapper<SysRolePermission> tWrapper = new EntityWrapper<>();
        tWrapper.eq("role_id",roleId);
        baseMapper.delete(tWrapper);
        Map<String, String> map = new HashMap<>();
        new Thread(){
            @Override
            public void run() {
                model.forEach(x->{
                    if (!map.containsKey(x.getPermissionId().toString())){
                        baseMapper.insert(x);
                        map.put(x.getPermissionId().toString(),x.getPermissionId().toString());
                    }
                });
            }
        }.run();
        return true;
    }
}
