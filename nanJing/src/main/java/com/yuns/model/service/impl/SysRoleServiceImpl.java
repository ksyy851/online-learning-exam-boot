package com.yuns.model.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.model.entity.SysPermission;
import com.yuns.model.entity.SysRole;
import com.yuns.model.entity.SysRolePermission;
import com.yuns.model.mapper.SysRoleMapper;
import com.yuns.model.service.SysPermissionService;
import com.yuns.model.service.SysRolePermissionService;
import com.yuns.model.service.SysRoleService;
import com.yuns.web.sys.dto.Action;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-25
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Autowired
    private SysRoleMapper roleMapper;

    @Override
    public List<SysRole> selectRoleByName(Map<String, String> map) {
        return roleMapper.selectRoleByName(map);
    }

    @Autowired
    private SysPermissionService sysPermissionService;

    @Override
    public List<SysRole> queryRoleByUserId(int id) {
        List<SysRole> roleList = baseMapper.queryRoleByUserId(id);
        roleList.forEach(x -> {
            List<SysPermission> list = sysPermissionService.queryPermissionByRole(x.getId());
            List<SysPermission> sysPermissionList = new ArrayList<>();
            list.forEach(y -> {
                if (y.getParentId() == null || y.getParentId() == 0) {
                    y.setLevel(0);
                    sysPermissionList.add(y);
                }
                sysPermissionList.sort((o1, o2) -> o1.getOrderNum().compareTo(o2.getOrderNum()));
                findChildren(sysPermissionList, list);
            });
            x.setPermissions(sysPermissionList);
        });
        return roleList;
    }

    @Override
    public List<SysRole> selectSysRoleList() {
        List<SysRole> sysRoles = baseMapper.selectList(null);
        sysRoles.forEach(x -> {
            List<SysPermission> list = sysPermissionService.queryPermissionByRole(x.getId());
            List<SysPermission> sysPermissionList = new ArrayList<>();
            list.forEach(y -> {
                if (y.getParentId() == null || y.getParentId() == 0) {
                    y.setLevel(0);
                    sysPermissionList.add(y);
                }
                sysPermissionList.sort((o1, o2) -> o1.getOrderNum().compareTo(o2.getOrderNum()));
                findChildren(sysPermissionList, list);
            });
            x.setPermissions(sysPermissionList);
        });
        return sysRoles;
    }

    @Autowired
    private SysRolePermissionService  sysRolePermissionService;
    @Override
    public List<SysRole> queryPermissionIdS() {
        List<SysRole> sysRoles = baseMapper.selectList(null);
        sysRoles.forEach(x -> {
            Wrapper<SysRolePermission> wrapper=new EntityWrapper<SysRolePermission>();
            wrapper.eq("role_id",x.getId());
            List<SysRolePermission> sysRolePermissions = sysRolePermissionService.selectList(wrapper);
            List<Long>    permissionIds=new ArrayList<>();
            sysRolePermissions.forEach(y->{
                if (x.getId().equals(y.getRoleId())){
                        permissionIds.add(y.getPermissionId());
                    }else {
                        permissionIds.add(y.getPermissionId());
                }
            });
            x.setPermissionIds(permissionIds);
        });
        return sysRoles;
    }

    private void findChildren(List<SysPermission> sysPermissionList, List<SysPermission> sysPermissions) {
        for (SysPermission sysPermission : sysPermissions) {
            List<SysPermission> children = new ArrayList<>();
            for (SysPermission permission : sysPermissionList) {
                if (sysPermission.getId() != null && sysPermission.getId().equals(permission.getParentId())) {
                    if (permission.getType() == 2) {
                        Action action = new Action();
                        action.setAction(permission.getActions());
                        action.setDefaultCheck(false);
                        action.setDescribe(permission.getName());
                        Set<Action> actionEntitySet = permission.getActionEntitySet();
                        actionEntitySet.add(action);
                    } else {
                        permission.setParentName(sysPermission.getName());
                        permission.setParentTitle(sysPermission.getTitle());
                        permission.setLevel(sysPermission.getLevel() + 1);
                        Action action = new Action();
                        action.setAction(permission.getActions());
                        Set<Action> actionEntitySet = permission.getActionEntitySet();
                        actionEntitySet.add(action);
                        children.add(permission);
                    }
                }
            }
            sysPermission.setChildren(children);
            children.sort((o1, o2) -> o1.getOrderNum().compareTo(o2.getOrderNum()));
            findChildren(children, sysPermissionList);
        }
    }
}
