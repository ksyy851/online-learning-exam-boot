package com.yuns.model.service;

import com.yuns.model.entity.SysDict;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2019-11-14
 */
public interface SysDictService extends IService<SysDict> {

    List<SysDict> selectDictList();

    List<SysDict> selectListByCode(String payType);
}
