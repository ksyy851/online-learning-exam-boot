package com.yuns.model.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.model.entity.OperationLog;
import com.yuns.model.mapper.OperationLogMapper;
import com.yuns.model.service.OperationLogService;
import com.yuns.web.param.FrontPage;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2019-10-28
 */
@Service
public class OperationLogServiceImpl extends ServiceImpl<OperationLogMapper, OperationLog> implements OperationLogService {

    @Override
    public Page<OperationLog> selectOperationLogList(FrontPage frontPage) {
        Page<OperationLog> page=new Page<>(frontPage.getPageNumber(),frontPage.getPageSize());
        return page.setRecords(baseMapper.selectOperationLogList(frontPage,page));
    }
}
