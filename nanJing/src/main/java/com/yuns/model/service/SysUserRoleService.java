package com.yuns.model.service;

import com.yuns.model.entity.SysUserRole;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.sys.param.UserRoles;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    boolean insertSysUserRole(UserRoles sysUserRoles);

    List<SysUserRole> selectUserRoleList(Integer userId);
}
