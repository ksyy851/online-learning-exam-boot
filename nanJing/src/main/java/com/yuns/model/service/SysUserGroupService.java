package com.yuns.model.service;

import com.yuns.model.entity.SysUserGroup;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 用户组表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysUserGroupService extends IService<SysUserGroup> {

}
