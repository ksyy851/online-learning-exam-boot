package com.yuns.model.service;

import com.yuns.model.entity.SysGroup;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 组表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysGroupService extends IService<SysGroup> {

}
