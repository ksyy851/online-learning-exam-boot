package com.yuns.model.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.OperationLog;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;

/**
 * <p>
 * 操作日志 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2019-10-28
 */
public interface OperationLogService extends IService<OperationLog> {

    Page<OperationLog> selectOperationLogList(FrontPage data);
}
