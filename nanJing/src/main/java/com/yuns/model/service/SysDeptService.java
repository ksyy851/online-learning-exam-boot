package com.yuns.model.service;

import com.yuns.model.entity.SysDept;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 组织表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public interface SysDeptService extends IService<SysDept> {

    List<SysDept> selectSysDeptList();

    boolean deleteSysDeptById(Long id);


    SysDept querySysDeptByChildren(String deptId);

    List<Long> querySysDeptByChildrenLong(Long deptId);

}
