package com.yuns.model.service.impl;

import com.yuns.jwt.JWTUtil;
import com.yuns.model.entity.SysPermission;
import com.yuns.model.entity.SysUser;
import com.yuns.model.mapper.SysPermissionMapper;
import com.yuns.model.service.SysPermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.sys.param.Meta;
import com.yuns.web.sys.param.MetaParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * <p>
 * 权限表 服务实现类
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-25
 */
@Service
public class SysPermissionServiceImpl extends ServiceImpl<SysPermissionMapper, SysPermission> implements SysPermissionService {

    @Autowired
    SysPermissionMapper permissionMapper;

    @Override
    public List<SysPermission> selectPermissionByName(Map<String, String> map) {
        return permissionMapper.selectPermissionByName(map);
    }

    @Override
    public List<SysPermission> findTree(Integer menuType) {
        List<SysPermission> sysPermissionList = new ArrayList<>();
        List<SysPermission> sysPermissions = null;
        /**
         * 用户登录后调用
         */
        if (menuType == 0) {
            SysUser user = JWTUtil.getUser();
            sysPermissions = permissionMapper.selectPermissionByUser(user);
        } else {
            //菜单列表
            sysPermissions = permissionMapper.selectList(null);
        }

        for (SysPermission permission : sysPermissions) {
            if (permission.getParentId() == null || permission.getParentId() == 0) {
                permission.setLevel(0);
                sysPermissionList.add(permission);
            }
        }
        sysPermissionList.sort((o1, o2) -> o1.getOrderNum().compareTo(o2.getOrderNum()));
        findChildren(sysPermissionList, sysPermissions, menuType);
        return sysPermissionList;

    }
    @Override
    public List<SysPermission> queryPermissionByRole(Long id) {
        return baseMapper.queryPermissionByRole(id);
    }

    @Override
    public Set<MetaParam> findPermission() throws UnsupportedEncodingException {
        SysUser user = JWTUtil.getUser();
        String account = user.getAccount() + "";
        List<SysPermission> sysPermissions;
//        account = new String(account.getBytes("ISO-8859-1"),"UTF-8");
        if ("luwenchao".trim().equalsIgnoreCase(account.trim())) {
            sysPermissions = permissionMapper.selectList(null);
        } else {
            sysPermissions = permissionMapper.selectPermissionByUser(user);
        }
        sysPermissions.sort((o1, o2) -> o1.getOrderNum().compareTo(o2.getOrderNum()));
        Set<MetaParam> metaParams = new LinkedHashSet<>();
        Map<String, MetaParam> map = new HashMap<>();
        for (SysPermission sysPermission : sysPermissions) {
            if (!map.containsKey(sysPermission.getId().toString())) {
                MetaParam metaParam = new MetaParam();
                metaParam.setId(sysPermission.getId());
                metaParam.setOrderNum(sysPermission.getOrderNum());
                metaParam.setKey(sysPermission.getId());
                metaParam.setParentId(sysPermission.getParentId());
                metaParam.setName(sysPermission.getName());
                metaParam.setComponent(sysPermission.getComponent());
                metaParam.setPath(sysPermission.getPath());
                metaParam.setRedirect(sysPermission.getRedirect());
                Meta metas = new Meta();
                metas.setTitle(sysPermission.getTitle());
                metas.setTarget("");
                metas.setIcon(sysPermission.getIcon());
                if (sysPermission.getHidden() == 0) {
                    metas.setShow(true);
                } else {
                    metas.setShow(false);
                }
                metaParam.setMeta(metas);
                metaParams.add(metaParam);
                map.put(sysPermission.getId().toString(), metaParam);
            }
        }
        return metaParams;
    }

    @Override
    public List<SysPermission> findTree() {
        List<SysPermission> sysPermissionList = new ArrayList<>();
        List<SysPermission> sysPermissions = null;
        //菜单列表
        sysPermissions = permissionMapper.selectList(null);

        for (SysPermission permission : sysPermissions) {
            if (permission.getParentId() == null || permission.getParentId() == 0) {
                permission.setLevel(0);
                sysPermissionList.add(permission);
            }
        }
        sysPermissionList.sort((o1, o2) -> o1.getOrderNum().compareTo(o2.getOrderNum()));
        findChildren(sysPermissionList, sysPermissions, 0);

        return sysPermissionList;
    }


    private void findChildren(List<SysPermission> sysPermissions, List<SysPermission> permissionList, int menuType) {
        for (SysPermission sysPermission : sysPermissions) {
            List<SysPermission> children = new ArrayList<>();
            for (SysPermission permission : permissionList) {
                if (menuType == 1 && permission.getType() == 2) {
                    // 如果是获取类型不需要按钮，且菜单类型是按钮的，直接过滤掉
                    continue;
                }
                if (sysPermission.getId() != null && (sysPermission.getId() + "").equals(permission.getParentId() + "")) {
                    permission.setParentName(sysPermission.getName());
                    permission.setParentTitle(sysPermission.getTitle());
                    permission.setLevel(sysPermission.getLevel() + 1);
                    children.add(permission);
                }
            }
            sysPermission.setChildren(children);
            children.sort((o1, o2) -> o1.getOrderNum().compareTo(o2.getOrderNum()));
            findChildren(children, permissionList, menuType);
        }
    }

}
