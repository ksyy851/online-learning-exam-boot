package com.yuns.model.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.model.entity.SysDept;
import com.yuns.model.entity.SysUser;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.dto.CompanyUserRegParam;
import com.yuns.web.param.*;
import com.yuns.web.sys.param.LoginParam;


import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author luwenchao
 * @since 2018-08-01
 */
public interface SysUserService extends IService<SysUser> {

    void createSysUser(LoginParam sysUser);

    List<SysUser> queryUserBySysDeptId(Integer id);

    List<SysUser> selectUserList(Page<SysUser> page , FrontPage<SysUser> frontPage);

    List<SysDept> selectSysDeptUserList();

    boolean insertSysUser(SysUser entity);

    Boolean checkData(String data, Integer type);

    void sendCode(String phone);

    String sendSms(String phone,String Message);

    Object registerByTel(SysUser userDto);

    Object register(CompanyUserRegParam companyUserRegParam);

    Object accountLogin(String username, String password);

    Object telLogin(String tel, String code);
}
