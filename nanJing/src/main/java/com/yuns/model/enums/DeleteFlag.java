package com.yuns.model.enums;

/**
 * @author zhenghe
 * @date 2018/7/29  12:46
 */
public enum DeleteFlag {
    DELETE(0), EXSIST(1);

    int val;

    DeleteFlag(int val) {
        this.val = val;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }

    public static DeleteFlag valueOf(Integer val) {
        for (DeleteFlag flag : DeleteFlag.values()) {
            if (flag.getVal() == val) {
                return flag;
            }
        }
        throw new RuntimeException("删除标志类型【" + val + "】不是有效类型！");
    }

    //判断是否是删除标志
    public static boolean isDeleteFlag(Integer val) {
        for (DeleteFlag flag : DeleteFlag.values()) {
            if (flag.getVal() == val) {
                return true;
            }
        }
        return false;
    }
}
