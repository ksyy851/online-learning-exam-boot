package com.yuns.model.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 验证码类型
 */
public enum CodeType {
    注册("register"), 登录("login"), 邮箱校验("email"), 重置密码("resetPassword"), 绑定手机号("bindPhone"), 重置支付密码("resetPaymentPassword"), 设置支付密码("setPaymentPassword"), 转账("transfer"), 绑定第三方("bindThirdUser");
    String code;

    CodeType(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static CodeType parse(String code) {
        for (CodeType ms : CodeType.values()) {
            if (StringUtils.equalsIgnoreCase(code, ms.getCode())) {
                return ms;
            }
        }
        throw new RuntimeException("验证码类型【" + code + "】不是有效类型！");
    }

}
