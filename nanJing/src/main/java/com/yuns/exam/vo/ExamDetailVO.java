package com.yuns.exam.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 试卷管理查询试卷详情
 * @author: zhansang
 * @create: 2020-11-15 10:29
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExamDetailVO {

    @ApiModelProperty(value = "试卷名称")
    private String examName;
    @ApiModelProperty(value = "试卷备注")
    private String examRemark;
    @ApiModelProperty(value = "试卷id")
    private String examId;
    @ApiModelProperty(value = "单选题分数")
    private Integer singleChoiceGrade;
    @ApiModelProperty(value = "单选题总数")
    private Integer singleChoiceCount;
    @ApiModelProperty(value = "多选题分数")
    private Integer multiChoiceCount;
    @ApiModelProperty(value = "多选题总数")
    private Integer multiChoiceGrade;
    @ApiModelProperty(value = "判断题总数")
    private Integer judgeCount;
    @ApiModelProperty(value = "判断题分数")
    private Integer judgeGrade;
    @ApiModelProperty(value = "简答题总数")
    private Integer shortAnswerCount;
    @ApiModelProperty(value = "简答题分数")
    private Integer shortAnswerGrade;
    @ApiModelProperty(value = "总分数")
    private Integer totalGrade;
    @ApiModelProperty(value = "及格分数")
    private Integer passGrade;
    @ApiModelProperty(value = "题库")
    private String questionLibs;

    @ApiModelProperty(value = "考试对象")
    private String examObjCompanyType;
    @ApiModelProperty(value = "满足学时")
    private Integer needStudyTime;
}
