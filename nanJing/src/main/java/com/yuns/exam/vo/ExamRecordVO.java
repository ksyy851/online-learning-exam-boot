package com.yuns.exam.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * @description: TODO: 阅卷管理返回参数
 * @author: zhansang
 * @create: 2020-11-10 16:39
 * @version: 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExamRecordVO implements Serializable {
    private static final long serialVersionUID = 11334243L;


    @ApiModelProperty(value = "记录id")
    private String id;
    /**
     * 答题开始
     */
    @ApiParam(value = "答题开始")
    @ApiModelProperty(value = "答题开始")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String BTime;
    /**
     * 答题结束
     */
    @ApiParam(value = "答题结束")
    @ApiModelProperty(value = "答题结束")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String ETime;

    /**
     * 试卷ID
     */
    @ApiParam(value = "试卷ID")
    @ApiModelProperty(value = "试卷ID")
    private String ExaminationInfoID;

    @ApiParam(value = "试卷名称")
    @ApiModelProperty(value = "试卷名称")
    private String examName;
    /**

    /**
     * 成绩
     */
    @ApiParam(value = "成绩")
    @ApiModelProperty(value = "成绩")
    private BigDecimal AScroe;

    /**
     * 成绩
     */
    @ApiParam(value = "总分")
    @ApiModelProperty(value = "总分")
    private BigDecimal TScroe;

    /**
     * 所属企业
     */
    @ApiParam(value = "所属企业id")
    @ApiModelProperty(value = "所属企业id")
    private String eid;

    /**
     * 所属企业
     */
    @ApiParam(value = "所属企业")
    @ApiModelProperty(value = "所属企业")
    private String eName;

    /**
     * 所属企业
     */
    @ApiParam(value = "用户id")
    @ApiModelProperty(value = "用户id")
    private Integer memberId;

    /**
     * 得分
     */
    @ApiParam(value = "得分")
    @ApiModelProperty(value = "得分")
    private BigDecimal score;

    /**
     * 及格分
     */
    @ApiParam(value = "及格分")
    @ApiModelProperty(value = "及格分")
    private String passGrade;


    @ApiParam(value = "是否已阅卷")
    @ApiModelProperty(value = "是否已阅卷")
    private Integer isCheck;
    /**
     * 答题人名称
     */
    @ApiParam(value = "答题人名称")
    @ApiModelProperty(value = "答题人名称")
    private String userName;

    @ApiParam(value = "所属公司")
    @ApiModelProperty(value = "所属公司")
    private String companName;
}
