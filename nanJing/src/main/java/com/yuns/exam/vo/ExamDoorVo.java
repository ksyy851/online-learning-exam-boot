package com.yuns.exam.vo;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description: TODO: 考试中心vo
 * @author: zhansang
 * @create: 2020-11-14 15:34
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExamDoorVo {

    @ApiModelProperty(value = "id")
    @JSONField(serializeUsing = ToStringSerializer.class)
    private String id;
    @ApiModelProperty(value = "考试名称")
    private String examName;
    @ApiModelProperty(value = "考试分数")
    private Integer grade;
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;
    @ApiModelProperty(value = "考试时长")
    private Integer examTime;
    @ApiModelProperty(value = "总题数")
    private Integer totalCount;
    @ApiModelProperty(value = "总分")
    private Integer Ascroe;

    @ApiModelProperty(value = "得分")
    private Integer score;

    @ApiModelProperty(value = "所需学时")
    private Integer needStudyTime;
    @ApiModelProperty(value = "考试对象")
    private String examObjCompanyType;

    @ApiModelProperty(value = "考试记录ID")
    private Integer recordId;
    @ApiModelProperty(value = "是否阅卷")
    private Integer isCheck;

}
