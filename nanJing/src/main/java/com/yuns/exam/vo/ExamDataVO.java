package com.yuns.exam.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @description: TODO: 题目VO
 * @author: zhansang
 * @create: 2020-11-14 11:43
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExamDataVO implements Serializable {

  @ApiModelProperty(value = "题库id")
  private String examId;
  @ApiModelProperty(value = "题库名称")
  private String examName;
}
