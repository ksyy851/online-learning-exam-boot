package com.yuns.exam.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "题库", description = "题库")
public class ExamLibVo implements Serializable {

    @ApiModelProperty(value = "题库id")
    private String examId;
    @ApiModelProperty(value = "题库名称")
    private String examName;

    @ApiModelProperty(value = "考试对象")
    private String examObjCompanyType;
    @ApiModelProperty(value = "满足学时")
    private Integer needStudyTime;
}
