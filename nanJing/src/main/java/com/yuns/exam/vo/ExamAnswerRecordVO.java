package com.yuns.exam.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @description: TODO: 考试答题记录
 * @author: zhansang
 * @create: 2020-11-28 18:50
 * @version: 1.0
 **/
@Data
@Builder
public class ExamAnswerRecordVO {


    private Long id;
    /**
     * 答题时间
     */
    @ApiParam(value = "答题时间")
    @ApiModelProperty(value = "答题时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date aTime;
    /**
     * 试卷试题对应关系表ID
     */
    @ApiParam(value = "试卷试题对应关系表ID")
    @ApiModelProperty(value = "试卷试题对应关系表ID")
    private String examTitleId;


    @ApiParam(value = "答题的题目")
    @ApiModelProperty(value = "答题的题目")
    private String questionId;
    @ApiParam(value = "题目名称")
    @ApiModelProperty(value = "题目名称")
    private String questionTitle;

    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
    @ApiModelProperty(value = "会员ID")
    private Integer memberId;
    /**
     * 答题结果
     */
    @ApiParam(value = "答题结果")
    @ApiModelProperty(value = "答题结果")
    private String Answer;
    /**
     * 是否答错
     */
    @ApiParam(value = "是否答错")
    @ApiModelProperty(value = "是否答错")
    private String isOk;
    /**
     * 得分
     */
    @ApiParam(value = "得分")
    @ApiModelProperty(value = "得分")
    private BigDecimal Score;

    @ApiParam(value = "题目分数")
    @ApiModelProperty(value = "题目分数")
    private Integer qScore;
    /**
     * 答题时长
     */
    @ApiParam(value = "答题时长")
    @ApiModelProperty(value = "答题时长")
    private Integer userTime;
    /**
     * 标识
     */
    @ApiParam(value = "标识")
    @ApiModelProperty(value = "标识")
    private Integer Sign;

    @ApiParam(value = "题目类型")
    @ApiModelProperty(value = "题目类型")
    private Integer questionType;

    @ApiParam(value = "简答题答题结果")
    @ApiModelProperty(value = "简答题答题结果")
    private String answerReply;

    public String getExamRecordId() {
        return examRecordId;
    }

    public void setExamRecordId(String examRecordId) {
        this.examRecordId = examRecordId;
    }

    @ApiParam(value = "考试记录id")
    @ApiModelProperty(value = "考试记录id")
    private String examRecordId;


    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField("update_user")
    private Long updateUser;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

}
