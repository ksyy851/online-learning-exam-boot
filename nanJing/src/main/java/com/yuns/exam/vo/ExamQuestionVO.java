package com.yuns.exam.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yuns.exam.entity.Exam;
import com.yuns.exam.entity.ExamQuestion;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @description: TODO: 题目vo
 * @author: zhansang
 * @create: 2020-11-24 22:39
 * @version: 1.0
 **/
@Data
@Builder
public class ExamQuestionVO  {

    /**
     * 题目来源 0：试题创建 1（题库） 通过题目生成
     */
    @ApiParam(value = "题目来源 0：试题创建 1（题库） 通过题目生成")
    @ApiModelProperty(value = "题目来源 0：试题创建 1（题库） 通过题目生成")
    @TableField("question_source")
    private Integer questionSource;
    @TableId(value = "id", type = IdType.AUTO)
    private String id;
    /**
     * 考试id
     */
    @ApiParam(value = "考试id")
    @ApiModelProperty(value = "考试id")
    @TableField("exam_id")
    private String examId;
    @TableField("question_id")
    private String questionId;
    /**
     * 题目分数
     */
    @ApiParam(value = "题目分数")
    @ApiModelProperty(value = "题目分数")
    private Integer grade;
    /**
     * 题目描述
     */
    @ApiParam(value = "题目描述")
    @ApiModelProperty(value = "题目描述")
    @TableField("question_title")
    private String questionTitle;
    /**
     * 题目类型  0：类型一   1：类型二
     */
    @ApiParam(value = "题目类型  0：类型一   1：类型二")
    @ApiModelProperty(value = "题目类型  0：类型一   1：类型二")
    @TableField("question_type")
    private Integer questionType;
    /**
     * 正确答案选项 （A、B、C、D）
     */
    @ApiParam(value = "正确答案选项 （A、B、C、D）")
    @ApiModelProperty(value = "正确答案选项 （A、B、C、D）")
    @TableField("question_answer")
    private String questionAnswer;
    /**
     * 正确答案（简答题）
     */
    @ApiParam(value = "正确答案（简答题）")
    @ApiModelProperty(value = "正确答案（简答题）")
    @TableField("question_answer_reply")
    private String questionAnswerReply;
    /**
     * 题目解析
     */
    @ApiParam(value = "题目解析")
    @ApiModelProperty(value = "题目解析")
    private String analysis;
    /**
     * 图片url
     */
    @ApiParam(value = "图片url")
    @ApiModelProperty(value = "图片url")
    @TableField("image_url")
    private String imageUrl;
    /**
     * 图片原名称
     */
    @ApiParam(value = "图片原名称")
    @ApiModelProperty(value = "图片原名称")
    @TableField("image_name")
    private String imageName;
    /**
     * 解析图片url
     */
    @ApiParam(value = "解析图片url")
    @ApiModelProperty(value = "解析图片url")
    @TableField("analysis_img_url")
    private String analysisImgUrl;
    /**
     * 解析图片原名称
     */
    @ApiParam(value = "解析图片原名称")
    @ApiModelProperty(value = "解析图片原名称")
    @TableField("analysis_img_name")
    private String analysisImgName;

    @TableField("create_user")
    private Long createUser;

    @TableField("create_dept")
    private Long createDept;

    @TableField("create_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @TableField("update_user")
    private Long updateUser;

    @TableField("update_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @TableField("status")
    private Integer status;

    @TableField("is_deleted")
    private Integer isDeleted;

    @ApiModelProperty(value = "序号")
    private Integer index;

}
