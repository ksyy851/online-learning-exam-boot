package com.yuns.exam.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 证书信息
 * @author: zhansang
 * @create: 2020-11-22 20:15
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CertificateVO {

    private String companyCode;
    private String time;
    private String index;
    private String code;
}
