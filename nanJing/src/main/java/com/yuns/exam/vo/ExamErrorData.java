package com.yuns.exam.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 错题集
 * @author: zhansang
 * @create: 2020-11-18 17:18
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExamErrorData {

    @ApiModelProperty(value = "错题数")
    private  Integer totalId;

    @ApiModelProperty(value = "题目类型")
    private Integer questionType;
}
