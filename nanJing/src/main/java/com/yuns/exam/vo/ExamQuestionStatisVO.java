package com.yuns.exam.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description: TODO: 题库列表查询实体
 * @author: zhansang
 * @create: 2020-11-01 17:25
 * @version: 1.0
 **/
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ExamQuestionStatisVO {

    @ApiModelProperty(value = "题库Id")
    private String examId;
    @ApiModelProperty(value = "题库名称")
    private String examName;
    @ApiModelProperty(value = "单选题数量")
    private Integer singleChoiceCount;
    @ApiModelProperty(value = "多选题数量")
    private Integer multiChoiceCount;
    @ApiModelProperty(value = "判断题数量")
    private Integer judgeCount;
    @ApiModelProperty(value = "简答题数量")
    private Integer shortAnswerCount;
    @ApiModelProperty(value = "题目备注")
    private String examRemark;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
}
