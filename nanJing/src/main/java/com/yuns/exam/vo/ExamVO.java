package com.yuns.exam.vo;

import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description: TODO: 试卷列表
 * @author: zhansang
 * @create: 2020-10-28 13:48
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "试卷信息", description = "试卷信息")
public class ExamVO implements Serializable {

    @ApiModelProperty(value = "试卷名")
    private String examName;
    @ApiModelProperty(value = "考试时间")
    private String examTime;
    @ApiModelProperty(value = "考试开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private String startTime;
    @ApiModelProperty(value = "考试结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss", timezone = "GMT+8")
    private String endTime;
    @ApiModelProperty(value = "总分")
    private Integer grade;
    @ApiModelProperty(value = "考试人数")
    private Integer examStudentCount;
    @ApiModelProperty(value = "最高分")
    private Integer maxGrade;
    @ApiModelProperty(value = "最低分")
    private Integer minGrade;
    @ApiModelProperty(value = "平均分")
    private Integer avgGrade;
    @ApiModelProperty(value = "错误率")
    private Float errorRate;


    @ApiModelProperty(value = "试卷id")
//    @JsonSerialize(using= ToStringSerializer.class)
    private String examId;
    @ApiModelProperty(value = "考试对象")
    private Integer examObjCompanyType;
    @ApiModelProperty(value = "满足学时")
    private Integer needStudyTime;
    @ApiModelProperty(value = "题库")
    private String  questionLibs;

}
