package com.yuns.exam.service.impl;

import com.yuns.exam.entity.VipFavoriteRecord;
import com.yuns.exam.mapper.VipFavoriteRecordMapper;
import com.yuns.exam.service.IVipFavoriteRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 会员收藏信息 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class VipFavoriteRecordServiceImpl extends ServiceImpl<VipFavoriteRecordMapper, VipFavoriteRecord> implements IVipFavoriteRecordService {
        @Override
        public List<VipFavoriteRecord> selectVipFavoriteRecordList(FrontPage frontPage) {
        return   baseMapper.selectVipFavoriteRecordList(frontPage);
     }
}
