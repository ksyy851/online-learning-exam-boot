package com.yuns.exam.service;

import com.yuns.exam.entity.SysMsgInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 消息信息表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ISysMsgInfoService extends IService<SysMsgInfo> {
        List<SysMsgInfo> selectSysMsgInfoList(FrontPage frontPage);
}
