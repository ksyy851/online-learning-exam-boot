package com.yuns.exam.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.InfoContentE;
import com.yuns.exam.mapper.InfoContentEMapper;
import com.yuns.exam.req.InfoContentEParam;
import com.yuns.exam.service.IInfoContentEService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 资讯内容 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class InfoContentEServiceImpl extends ServiceImpl<InfoContentEMapper, InfoContentE> implements IInfoContentEService {

        @Override
        public Object selectInfoContentEList(InfoContentEParam frontPage) {
            if(!frontPage.is_search()){
                return   baseMapper.selectInfoContentEList(frontPage);
            }
            Page<InfoContentE> page = new Page<>(frontPage.getPageNumber(),frontPage.getPageSize());
            List<InfoContentE> list = baseMapper.selectInfoContentEList(page,frontPage);
        return   page.setRecords(list);
     }
}
