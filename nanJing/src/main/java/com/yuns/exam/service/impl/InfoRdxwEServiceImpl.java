package com.yuns.exam.service.impl;

import com.yuns.exam.entity.InfoRdxwE;
import com.yuns.exam.mapper.InfoRdxwEMapper;
import com.yuns.exam.service.IInfoRdxwEService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class InfoRdxwEServiceImpl extends ServiceImpl<InfoRdxwEMapper, InfoRdxwE> implements IInfoRdxwEService {
        @Override
        public List<InfoRdxwE> selectInfoRdxwEList(FrontPage frontPage) {
        return   baseMapper.selectInfoRdxwEList(frontPage);
     }
}
