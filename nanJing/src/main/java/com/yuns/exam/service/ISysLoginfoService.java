package com.yuns.exam.service;

import com.yuns.exam.entity.SysLoginfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 系统日志信息表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ISysLoginfoService extends IService<SysLoginfo> {
        List<SysLoginfo> selectSysLoginfoList(FrontPage frontPage);
}
