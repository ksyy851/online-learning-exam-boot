package com.yuns.exam.service;

import com.yuns.exam.entity.QuestionOption;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IQuestionOptionService extends IService<QuestionOption> {
        List<QuestionOption> selectQuestionOptionList(FrontPage frontPage);
}
