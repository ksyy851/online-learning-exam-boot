package com.yuns.exam.service;

import com.yuns.exam.entity.ExamPeriodLog;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 学时日志表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamPeriodLogService extends IService<ExamPeriodLog> {
        List<ExamPeriodLog> selectExamPeriodLogList(FrontPage frontPage);
}
