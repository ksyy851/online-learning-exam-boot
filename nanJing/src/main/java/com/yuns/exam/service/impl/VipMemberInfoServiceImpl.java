package com.yuns.exam.service.impl;

import com.yuns.exam.entity.VipMemberInfo;
import com.yuns.exam.mapper.VipMemberInfoMapper;
import com.yuns.exam.service.IVipMemberInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 会员信息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class VipMemberInfoServiceImpl extends ServiceImpl<VipMemberInfoMapper, VipMemberInfo> implements IVipMemberInfoService {
        @Override
        public List<VipMemberInfo> selectVipMemberInfoList(FrontPage frontPage) {
        return   baseMapper.selectVipMemberInfoList(frontPage);
     }
}
