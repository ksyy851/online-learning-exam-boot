package com.yuns.exam.service.impl;

import com.google.common.collect.Lists;
import com.yuns.exam.entity.ExamAnswerRecord;
import com.yuns.exam.entity.ExamQuestion;
import com.yuns.exam.entity.Question;
import com.yuns.exam.mapper.ExamAnswerRecordMapper;
import com.yuns.exam.mapper.QuestionMapper;
import com.yuns.exam.req.ExamAnswerRecordReq;
import com.yuns.exam.service.IExamAnswerRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.exam.vo.ExamAnswerRecordVO;
import com.yuns.exam.vo.ExamErrorData;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 试卷试题答题答案表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamAnswerRecordServiceImpl extends ServiceImpl<ExamAnswerRecordMapper, ExamAnswerRecord> implements IExamAnswerRecordService {

    @Autowired
    private QuestionMapper questionMapper;

        @Override
        public List<ExamAnswerRecordVO> selectExamAnswerRecordList(FrontPage frontPage) {
            List<ExamAnswerRecord> examAnswerRecords = baseMapper.selectExamAnswerRecordList(frontPage);

         List<ExamAnswerRecordVO> examAnswerRecordVOS = Lists.newArrayList();
            examAnswerRecords.forEach(item->{
                ExamAnswerRecordVO examAnswerRecordVO = ExamAnswerRecordVO.builder().build();
                BeanUtils.copyProperties(item,examAnswerRecordVO);

                examAnswerRecordVOS.add(examAnswerRecordVO);
            });
         for(ExamAnswerRecordVO examAnswerRecord:examAnswerRecordVOS){
             FrontPage<Question> frontPage1 = new FrontPage<>();
             frontPage1.set_search(false);
             Question t1 = Question.builder().id(Long.valueOf(examAnswerRecord.getQuestionId())).build();
             frontPage1.setData(t1);

             Question examQuestions =    questionMapper.selectByIdSelf(String.valueOf(examAnswerRecord.getQuestionId()));
             Question question =Question.builder().build();
             if(examQuestions!=null ){
                 question = examQuestions;
             }
             examAnswerRecord.setQuestionTitle(question.getQuestionTitle());
             examAnswerRecord.setQScore(question.getQuestionScore());
             examAnswerRecord.setAnswer(question.getAnalysis());

          };

            return   examAnswerRecordVOS;
     }

    @Override
    public List<ExamErrorData> selectErrorQuestionList(ExamAnswerRecordReq m) {
        return baseMapper.selectErrorQuestionList(m);
    }

    @Override
    public Boolean updateSign(ExamAnswerRecord examAnswerRecord) {
        return baseMapper.updateSign(examAnswerRecord);
    }
}
