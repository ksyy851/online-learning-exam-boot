package com.yuns.exam.service.impl;

import com.yuns.exam.entity.SysLoginfo;
import com.yuns.exam.mapper.SysLoginfoMapper;
import com.yuns.exam.service.ISysLoginfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 系统日志信息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class SysLoginfoServiceImpl extends ServiceImpl<SysLoginfoMapper, SysLoginfo> implements ISysLoginfoService {
        @Override
        public List<SysLoginfo> selectSysLoginfoList(FrontPage frontPage) {
        return   baseMapper.selectSysLoginfoList(frontPage);
     }
}
