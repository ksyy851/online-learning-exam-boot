package com.yuns.exam.service;

import com.yuns.exam.entity.ExamTitleInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 习题试题信息表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamTitleInfoService extends IService<ExamTitleInfo> {
        List<ExamTitleInfo> selectExamTitleinfoList(FrontPage frontPage);
}
