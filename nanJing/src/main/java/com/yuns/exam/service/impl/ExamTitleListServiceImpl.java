package com.yuns.exam.service.impl;

import com.yuns.exam.entity.ExamTitleList;
import com.yuns.exam.mapper.ExamTitleListMapper;
import com.yuns.exam.service.IExamTitleListService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 试题选项表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamTitleListServiceImpl extends ServiceImpl<ExamTitleListMapper, ExamTitleList> implements IExamTitleListService {
        @Override
        public List<ExamTitleList> selectExamTitleListList(FrontPage frontPage) {
        return   baseMapper.selectExamTitleListList(frontPage);
     }
}
