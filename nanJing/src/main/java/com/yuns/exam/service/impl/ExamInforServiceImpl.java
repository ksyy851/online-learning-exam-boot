package com.yuns.exam.service.impl;

import com.yuns.exam.entity.ExamInfor;
import com.yuns.exam.mapper.ExamInforMapper;
import com.yuns.exam.service.IExamInforService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 试卷定义表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamInforServiceImpl extends ServiceImpl<ExamInforMapper, ExamInfor> implements IExamInforService {
        @Override
        public List<ExamInfor> selectExamInforList(FrontPage frontPage) {
        return   baseMapper.selectExamInforList(frontPage);
     }
}
