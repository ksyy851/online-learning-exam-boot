package com.yuns.exam.service;

import com.yuns.exam.entity.ExamTitleErr;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 试题纠错表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamTitleErrService extends IService<ExamTitleErr> {
        List<ExamTitleErr> selectExamTitleErrList(FrontPage frontPage);
}
