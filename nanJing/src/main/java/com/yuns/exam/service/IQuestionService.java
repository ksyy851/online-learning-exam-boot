package com.yuns.exam.service;

import com.yuns.exam.entity.Question;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.exam.req.QuestionParam;
import com.yuns.web.param.FrontPage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IQuestionService extends IService<Question> {
        Object selectQuestionList(FrontPage frontPage);
        Object selectQuestionListByCondition(QuestionParam frontPage);
        Object selectQuestionDetail(QuestionParam questionParam);
        void exportList(QuestionParam frontPage, HttpServletResponse response);

}
