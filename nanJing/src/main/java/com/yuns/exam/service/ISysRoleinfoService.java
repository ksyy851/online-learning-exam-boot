package com.yuns.exam.service;

import com.yuns.exam.entity.SysRoleinfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 后台角色表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ISysRoleinfoService extends IService<SysRoleinfo> {
        List<SysRoleinfo> selectSysRoleinfoList(FrontPage frontPage);
}
