package com.yuns.exam.service;

import com.yuns.exam.entity.VipEnterpriesInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 企业信息表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IVipEnterpriesInfoService extends IService<VipEnterpriesInfo> {
        List<VipEnterpriesInfo> selectVipEnterpriesInfoList(FrontPage frontPage);
}
