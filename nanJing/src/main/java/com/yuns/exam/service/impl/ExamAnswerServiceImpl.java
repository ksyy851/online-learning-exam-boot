package com.yuns.exam.service.impl;

import com.yuns.exam.entity.ExamAnswer;
import com.yuns.exam.mapper.ExamAnswerMapper;
import com.yuns.exam.service.IExamAnswerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamAnswerServiceImpl extends ServiceImpl<ExamAnswerMapper, ExamAnswer> implements IExamAnswerService {
        @Override
        public List<ExamAnswer> selectExamAnswerList(FrontPage frontPage) {
        return   baseMapper.selectExamAnswerList(frontPage);
     }
}
