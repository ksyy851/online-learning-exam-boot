package com.yuns.exam.service;

import com.yuns.exam.entity.VipFavoriteRecord;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 会员收藏信息 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IVipFavoriteRecordService extends IService<VipFavoriteRecord> {
        List<VipFavoriteRecord> selectVipFavoriteRecordList(FrontPage frontPage);
}
