package com.yuns.exam.service.impl;

import com.yuns.exam.entity.SysMsgInfo;
import com.yuns.exam.mapper.SysMsgInfoMapper;
import com.yuns.exam.service.ISysMsgInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 消息信息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class SysMsgInfoServiceImpl extends ServiceImpl<SysMsgInfoMapper, SysMsgInfo> implements ISysMsgInfoService {
        @Override
        public List<SysMsgInfo> selectSysMsgInfoList(FrontPage frontPage) {
        return   baseMapper.selectSysMsgInfoList(frontPage);
     }
}
