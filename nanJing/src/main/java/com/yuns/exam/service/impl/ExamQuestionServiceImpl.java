package com.yuns.exam.service.impl;

import com.yuns.exam.entity.ExamQuestion;
import com.yuns.exam.mapper.ExamQuestionMapper;
import com.yuns.exam.service.IExamQuestionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamQuestionServiceImpl extends ServiceImpl<ExamQuestionMapper, ExamQuestion> implements IExamQuestionService {
        @Override
        public List<ExamQuestion> selectExamQuestionList(FrontPage frontPage) {
        return   baseMapper.selectExamQuestionList(frontPage);
     }
}
