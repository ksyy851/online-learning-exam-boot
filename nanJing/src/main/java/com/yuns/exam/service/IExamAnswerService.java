package com.yuns.exam.service;

import com.yuns.exam.entity.ExamAnswer;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamAnswerService extends IService<ExamAnswer> {
        List<ExamAnswer> selectExamAnswerList(FrontPage frontPage);
}
