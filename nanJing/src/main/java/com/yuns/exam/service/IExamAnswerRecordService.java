package com.yuns.exam.service;

import com.yuns.exam.entity.ExamAnswerRecord;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.exam.req.ExamAnswerRecordReq;
import com.yuns.exam.vo.ExamAnswerRecordVO;
import com.yuns.exam.vo.ExamErrorData;
import com.yuns.web.param.FrontPage;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 试卷试题答题答案表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamAnswerRecordService extends IService<ExamAnswerRecord> {
        List<ExamAnswerRecordVO> selectExamAnswerRecordList(FrontPage frontPage);
        List<ExamErrorData>  selectErrorQuestionList(ExamAnswerRecordReq examAnswerRecordReq);
        Boolean updateSign(ExamAnswerRecord examAnswerRecord);
}
