package com.yuns.exam.service;

import com.yuns.exam.entity.InfoTypeE;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 资讯类型 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IInfoTypeEService extends IService<InfoTypeE> {
        List<InfoTypeE> selectInfoTypeEList(FrontPage frontPage);
}
