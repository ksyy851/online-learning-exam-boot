package com.yuns.exam.service.impl;

import com.yuns.exam.entity.SysRoleinfo;
import com.yuns.exam.mapper.SysRoleinfoMapper;
import com.yuns.exam.service.ISysRoleinfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 后台角色表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class SysRoleinfoServiceImpl extends ServiceImpl<SysRoleinfoMapper, SysRoleinfo> implements ISysRoleinfoService {
        @Override
        public List<SysRoleinfo> selectSysRoleinfoList(FrontPage frontPage) {
        return   baseMapper.selectSysRoleinfoList(frontPage);
     }
}
