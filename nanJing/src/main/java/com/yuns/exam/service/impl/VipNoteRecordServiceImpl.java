package com.yuns.exam.service.impl;

import com.yuns.exam.entity.VipNoteRecord;
import com.yuns.exam.mapper.VipNoteRecordMapper;
import com.yuns.exam.service.IVipNoteRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 会员笔记信息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class VipNoteRecordServiceImpl extends ServiceImpl<VipNoteRecordMapper, VipNoteRecord> implements IVipNoteRecordService {
        @Override
        public List<VipNoteRecord> selectVipNoteRecordList(FrontPage frontPage) {
        return   baseMapper.selectVipNoteRecordList(frontPage);
     }
}
