package com.yuns.exam.service.impl;

import com.yuns.exam.entity.SysRuleRecord;
import com.yuns.exam.mapper.SysRuleRecordMapper;
import com.yuns.exam.service.ISysRuleRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 违章/要求信息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class SysRuleRecordServiceImpl extends ServiceImpl<SysRuleRecordMapper, SysRuleRecord> implements ISysRuleRecordService {
        @Override
        public List<SysRuleRecord> selectSysRuleRecordList(FrontPage frontPage) {
        return   baseMapper.selectSysRuleRecordList(frontPage);
     }
}
