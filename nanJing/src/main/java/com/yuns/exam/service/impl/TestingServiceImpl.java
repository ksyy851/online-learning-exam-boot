package com.yuns.exam.service.impl;

import com.yuns.exam.entity.Testing;
import com.yuns.exam.mapper.TestingMapper;
import com.yuns.exam.service.ITestingService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class TestingServiceImpl extends ServiceImpl<TestingMapper, Testing> implements ITestingService {
        @Override
        public List<Testing> selectTestingList(FrontPage frontPage) {
        return   baseMapper.selectTestingList(frontPage);
     }
}
