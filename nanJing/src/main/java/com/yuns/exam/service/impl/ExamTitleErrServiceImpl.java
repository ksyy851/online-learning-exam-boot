package com.yuns.exam.service.impl;

import com.yuns.exam.entity.ExamTitleErr;
import com.yuns.exam.mapper.ExamTitleErrMapper;
import com.yuns.exam.service.IExamTitleErrService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 试题纠错表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamTitleErrServiceImpl extends ServiceImpl<ExamTitleErrMapper, ExamTitleErr> implements IExamTitleErrService {
        @Override
        public List<ExamTitleErr> selectExamTitleErrList(FrontPage frontPage) {
        return   baseMapper.selectExamTitleErrList(frontPage);
     }
}
