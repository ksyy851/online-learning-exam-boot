package com.yuns.exam.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.exam.entity.ExamTitleInfo;
import com.yuns.exam.mapper.ExamTitleInfoMapper;
import com.yuns.exam.service.IExamTitleInfoService;
import com.yuns.web.param.FrontPage;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * <p>
 * 习题试题信息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamTitleInfoServiceImpl extends ServiceImpl<ExamTitleInfoMapper, ExamTitleInfo> implements IExamTitleInfoService {
        @Override
        public List<ExamTitleInfo> selectExamTitleinfoList(FrontPage frontPage) {
        return   baseMapper.selectExamTitleinfoList(frontPage);
     }
}
