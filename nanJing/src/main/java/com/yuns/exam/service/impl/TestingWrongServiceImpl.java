package com.yuns.exam.service.impl;

import com.yuns.exam.entity.TestingWrong;
import com.yuns.exam.mapper.TestingWrongMapper;
import com.yuns.exam.service.ITestingWrongService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class TestingWrongServiceImpl extends ServiceImpl<TestingWrongMapper, TestingWrong> implements ITestingWrongService {
        @Override
        public List<TestingWrong> selectTestingWrongList(FrontPage frontPage) {
        return   baseMapper.selectTestingWrongList(frontPage);
     }
}
