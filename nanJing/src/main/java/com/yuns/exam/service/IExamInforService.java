package com.yuns.exam.service;

import com.yuns.exam.entity.ExamInfor;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 试卷定义表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamInforService extends IService<ExamInfor> {
        List<ExamInfor> selectExamInforList(FrontPage frontPage);
}
