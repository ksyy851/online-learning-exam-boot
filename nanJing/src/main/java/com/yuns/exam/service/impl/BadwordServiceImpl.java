package com.yuns.exam.service.impl;

import com.yuns.exam.entity.Badword;
import com.yuns.exam.mapper.BadwordMapper;
import com.yuns.exam.service.IBadwordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 敏感词表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class BadwordServiceImpl extends ServiceImpl<BadwordMapper, Badword> implements IBadwordService {
        @Override
        public List<Badword> selectBadwordList(FrontPage frontPage) {
        return   baseMapper.selectBadwordList(frontPage);
     }
}
