package com.yuns.exam.service.impl;

import com.yuns.exam.entity.VipEnterpriesInfo;
import com.yuns.exam.mapper.VipEnterpriesInfoMapper;
import com.yuns.exam.service.IVipEnterpriesInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 企业信息表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class VipEnterpriesInfoServiceImpl extends ServiceImpl<VipEnterpriesInfoMapper, VipEnterpriesInfo> implements IVipEnterpriesInfoService {
        @Override
        public List<VipEnterpriesInfo> selectVipEnterpriesInfoList(FrontPage frontPage) {
        return   baseMapper.selectVipEnterpriesInfoList(frontPage);
     }
}
