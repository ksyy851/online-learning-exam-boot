package com.yuns.exam.service;

import com.yuns.exam.entity.ExamQuestion;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamQuestionService extends IService<ExamQuestion> {
        List<ExamQuestion> selectExamQuestionList(FrontPage frontPage);
}
