package com.yuns.exam.service;

import com.yuns.exam.entity.SysSuppInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 后台信息表(环保信息中心、宣教中心、市局主要处室人员、 各区环保局管理人员、题库管理人员、技术公司平台维护人员) 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ISysSuppInfoService extends IService<SysSuppInfo> {
        List<SysSuppInfo> selectSysSuppInfoList(FrontPage frontPage);
}
