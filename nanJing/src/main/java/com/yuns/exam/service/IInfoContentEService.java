package com.yuns.exam.service;

import com.yuns.exam.entity.InfoContentE;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.exam.req.InfoContentEParam;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 资讯内容 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IInfoContentEService extends IService<InfoContentE> {
        Object selectInfoContentEList(InfoContentEParam frontPage);
}
