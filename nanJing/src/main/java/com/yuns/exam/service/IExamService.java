package com.yuns.exam.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.Exam;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.exam.req.ExamDoorParam;
import com.yuns.exam.req.ExamDto;
import com.yuns.exam.req.ExamQueryReq;
import com.yuns.exam.vo.ExamDataVO;
import com.yuns.exam.vo.ExamDetailVO;
import com.yuns.exam.vo.ExamQuestionStatisVO;
import com.yuns.exam.vo.ExamVO;
import com.yuns.web.param.FrontPage;

import java.util.HashMap;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamService extends IService<Exam> {
        Object selectExamList(FrontPage frontPage);
        List<HashMap<String,Object>> selectList(HashMap map);

        boolean insertExamData(ExamDto examDto);
        Object selectExamDataList(ExamQueryReq req);

        List<ExamDataVO> selectQuestionLibList();
        Page<ExamQuestionStatisVO> selectAllExamList(ExamQueryReq req);

        Object getExamListForDoor(ExamDoorParam req);
        List<ExamDetailVO> selectExamDetailList(ExamVO exam);


}
