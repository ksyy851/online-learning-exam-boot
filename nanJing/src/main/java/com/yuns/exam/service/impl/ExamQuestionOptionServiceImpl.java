package com.yuns.exam.service.impl;

import com.yuns.exam.entity.ExamQuestionOption;
import com.yuns.exam.mapper.ExamQuestionOptionMapper;
import com.yuns.exam.service.IExamQuestionOptionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class ExamQuestionOptionServiceImpl extends ServiceImpl<ExamQuestionOptionMapper, ExamQuestionOption> implements IExamQuestionOptionService {
        @Override
        public List<ExamQuestionOption> selectExamQuestionOptionList(FrontPage frontPage) {
        return   baseMapper.selectExamQuestionOptionList(frontPage);
     }
}
