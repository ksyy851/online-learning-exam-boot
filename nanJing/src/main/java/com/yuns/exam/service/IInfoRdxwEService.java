package com.yuns.exam.service;

import com.yuns.exam.entity.InfoRdxwE;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IInfoRdxwEService extends IService<InfoRdxwE> {
        List<InfoRdxwE> selectInfoRdxwEList(FrontPage frontPage);
}
