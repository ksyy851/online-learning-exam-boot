package com.yuns.exam.service.impl;

import com.yuns.exam.entity.QuestionOption;
import com.yuns.exam.mapper.QuestionOptionMapper;
import com.yuns.exam.service.IQuestionOptionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class QuestionOptionServiceImpl extends ServiceImpl<QuestionOptionMapper, QuestionOption> implements IQuestionOptionService {
        @Override
        public List<QuestionOption> selectQuestionOptionList(FrontPage frontPage) {
        return   baseMapper.selectQuestionOptionList(frontPage);
     }
}
