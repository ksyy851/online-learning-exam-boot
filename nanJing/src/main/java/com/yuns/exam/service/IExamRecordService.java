package com.yuns.exam.service;

import com.yuns.exam.dto.ExamQuestionDto;
import com.yuns.exam.dto.ExamQuestionStatisDto;
import com.yuns.exam.entity.ExamAnswerRecord;
import com.yuns.exam.entity.ExamQuestion;
import com.yuns.exam.entity.ExamRecord;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.exam.req.ExamRecordParm;
import com.yuns.exam.vo.ExamRecordVO;
import com.yuns.exam.vo.QuestionVO;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 考试记录表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IExamRecordService extends IService<ExamRecord> {
        Object selectExamRecordList(ExamRecordParm frontPage);

        /**
         * 开始考试
         * 生成考试记录
         * 更新考试答题记录并计算题目得分
         * @return
         */
        Object beginExam(ExamRecordVO examRecordVO);

        Object updateExamAnswerRecord(ExamAnswerRecord examAnswerRecord);

        /**
         * 提交试卷
         * @param examRecordVO
         * @return
         */
        Object submitExam(ExamRecordVO examRecordVO);

        /**
         * 根据错题类型查询错题列表
         */
        Object selectErrorQuestionListByQuestionType(FrontPage<QuestionVO> frontPage);

        void updateExamRecord(ExamRecord examRecord);

        /**
         * 模拟考试
         * @return
         */
        ExamQuestionStatisDto testExam();

        /**
         * 生成证书编码
         * 时间，单位名称，证书编号
         */
        String genCertificateEncoding(String companyCode,String time,String index);

        Object sendCertificate(ExamRecordVO examRecordVO);

        Object queryCertificate(ExamRecordVO examRecordVO);
}
