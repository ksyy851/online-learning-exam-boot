package com.yuns.exam.service.impl;

import com.yuns.exam.entity.SysSuppInfo;
import com.yuns.exam.mapper.SysSuppInfoMapper;
import com.yuns.exam.service.ISysSuppInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 后台信息表(环保信息中心、宣教中心、市局主要处室人员、 各区环保局管理人员、题库管理人员、技术公司平台维护人员) 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class SysSuppInfoServiceImpl extends ServiceImpl<SysSuppInfoMapper, SysSuppInfo> implements ISysSuppInfoService {
        @Override
        public List<SysSuppInfo> selectSysSuppInfoList(FrontPage frontPage) {
        return   baseMapper.selectSysSuppInfoList(frontPage);
     }
}
