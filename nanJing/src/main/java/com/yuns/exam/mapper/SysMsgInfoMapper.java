package com.yuns.exam.mapper;

import com.yuns.exam.entity.SysMsgInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 消息信息表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface SysMsgInfoMapper extends BaseMapper<SysMsgInfo> {
        List<SysMsgInfo> selectSysMsgInfoList(FrontPage frontPage);
}
