package com.yuns.exam.mapper;

import com.yuns.exam.entity.ExamTitleErr;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 试题纠错表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamTitleErrMapper extends BaseMapper<ExamTitleErr> {
        List<ExamTitleErr> selectExamTitleErrList(FrontPage frontPage);
}
