package com.yuns.exam.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.ExamAnswerRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.exam.entity.ExamQuestion;
import com.yuns.exam.req.ExamAnswerRecordReq;
import com.yuns.exam.vo.ExamErrorData;
import com.yuns.exam.vo.QuestionVO;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 试卷试题答题答案表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamAnswerRecordMapper extends BaseMapper<ExamAnswerRecord> {
        List<ExamAnswerRecord> selectExamAnswerRecordList(@Param("frontPage") FrontPage frontPage);
        List<ExamAnswerRecord> selectExamAnswerRecordList(@Param("page") Page<ExamAnswerRecord> page,@Param("frontPage") FrontPage frontPage);
        Boolean updateSign(ExamAnswerRecord examAnswerRecord);

        List<ExamErrorData> selectErrorQuestionList(ExamAnswerRecordReq examAnswerRecordReq);
//        List<ExamQuestion> selectErrorQuestionListByQuestionType(QuestionVO questionVO);
        List<ExamQuestion> selectErrorQuestionListByQuestionType(@Param("frontPage")FrontPage frontPage);
        List<ExamQuestion> selectErrorQuestionListByQuestionType(@Param("page") Page<QuestionVO> page,
                                                                 @Param("frontPage")FrontPage frontPage);
}
