package com.yuns.exam.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.dto.QuesDto;
import com.yuns.exam.entity.Question;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.exam.req.QuestionParam;
import com.yuns.exam.vo.QuestionVO;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface QuestionMapper extends BaseMapper<Question> {
        List<Question> selectQuestionList(@Param("frontPage") FrontPage frontPage);
        Question selectByIdSelf(@Param("id") String id);
        List<QuesDto> selectQuestionDtoList(@Param("frontPage") FrontPage frontPage);
        List<Question> selectQuestionList(@Param("page") Page<Question> page, @Param("frontPage") FrontPage frontPage);
        List<Question> selectQuestionListByRandom(Question question);

        List<QuestionVO> selectQuestionListByCondition(@Param("frontPage")FrontPage frontPage);
        List<QuestionVO> selectQuestionListByCondition(@Param("page") Page<QuestionVO> page,
                                                       @Param("frontPage")QuestionParam frontPage);
}
