package com.yuns.exam.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.ExamRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;

import com.yuns.exam.vo.ExamRecordVO;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 考试记录表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamRecordMapper extends BaseMapper<ExamRecord> {

        Integer insertExamRecord(ExamRecord examRecord);
        List<ExamRecord> selectExamRecordList(FrontPage frontPage);
        List<ExamRecord> selectListSelf(Map m);
        List<ExamRecord> selectExamRecordData();
        List<ExamRecordVO> selectExamRecordData(@Param("frontPage")FrontPage frontPage);
        List<ExamRecordVO> selectExamRecordData(@Param("page") Page<ExamRecordVO> page, @Param("frontPage")FrontPage frontPage);
        void updateExamRecord(ExamRecord examRecord);
}
