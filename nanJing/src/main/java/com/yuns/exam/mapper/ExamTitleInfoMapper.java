package com.yuns.exam.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.exam.entity.ExamTitleInfo;
import com.yuns.web.param.FrontPage;

import java.util.List;
/**
 * <p>
 * 习题试题信息表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamTitleInfoMapper extends BaseMapper<ExamTitleInfo> {

        List<ExamTitleInfo> selectExamTitleinfoList(FrontPage frontPage);
}
