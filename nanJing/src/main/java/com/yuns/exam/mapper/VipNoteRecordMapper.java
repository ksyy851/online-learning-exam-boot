package com.yuns.exam.mapper;

import com.yuns.exam.entity.VipNoteRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 会员笔记信息表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface VipNoteRecordMapper extends BaseMapper<VipNoteRecord> {
        List<VipNoteRecord> selectVipNoteRecordList(FrontPage frontPage);
}
