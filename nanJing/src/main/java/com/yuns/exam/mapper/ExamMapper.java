package com.yuns.exam.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.Exam;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.HashMap;
import java.util.List;

import com.yuns.exam.req.ExamQueryReq;
import com.yuns.exam.vo.*;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamMapper extends BaseMapper<Exam> {
        List<ExamLibVo> selectExamList(@Param("frontPage")FrontPage frontPage);
        List<HashMap<String,Object>> selectList(HashMap map);
        List<ExamLibVo> selectExamList(@Param("page") Page<ExamLibVo>page, @Param("frontPage")FrontPage frontPage);

        List<ExamDoorVo> getHasExamListForDoor(@Param("frontPage")FrontPage frontPage);
        List<ExamDoorVo> getHasExamListForDoor(@Param("page") Page<ExamDoorVo>page, @Param("frontPage")FrontPage frontPage);

        List<ExamDoorVo> getUnExamListForDoor(@Param("frontPage")FrontPage frontPage);
        List<ExamDoorVo> getUnExamListForDoor(@Param("page") Page<ExamDoorVo>page, @Param("frontPage")FrontPage frontPage);

        List<ExamVO> selectExamDataList();
        List<ExamVO> selectExamDataListWithPage(@Param("frontPage")FrontPage frontPage);
        List<ExamVO> selectExamDataListWithPage(@Param("page") Page<ExamVO>page, @Param("frontPage")FrontPage frontPage);
        List<ExamDataVO> selectQuestionLibList();
        List<ExamQuestionStatisVO> selectAllExamList(@Param("page") Page<ExamQuestionStatisVO> page, ExamQueryReq req);

        /**
         * 考试管理列表详情
         * @param exam
         * @return
         */
        List<ExamDetailVO> selectExamDetailList(ExamVO exam);
}
