package com.yuns.exam.mapper;

import com.yuns.exam.entity.ExamQuestionOption;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamQuestionOptionMapper extends BaseMapper<ExamQuestionOption> {
        List<ExamQuestionOption> selectExamQuestionOptionList(FrontPage frontPage);
}
