package com.yuns.exam.mapper;

import com.yuns.exam.entity.ExamPeriodLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 学时日志表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamPeriodLogMapper extends BaseMapper<ExamPeriodLog> {
        List<ExamPeriodLog> selectExamPeriodLogList(FrontPage frontPage);
}
