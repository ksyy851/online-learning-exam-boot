package com.yuns.exam.mapper;

import com.yuns.exam.entity.ExamTitleList;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 试题选项表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamTitleListMapper extends BaseMapper<ExamTitleList> {
        List<ExamTitleList> selectExamTitleListList(FrontPage frontPage);
}
