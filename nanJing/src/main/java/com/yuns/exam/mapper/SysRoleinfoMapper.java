package com.yuns.exam.mapper;

import com.yuns.exam.entity.SysRoleinfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 后台角色表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface SysRoleinfoMapper extends BaseMapper<SysRoleinfo> {
        List<SysRoleinfo> selectSysRoleinfoList(FrontPage frontPage);
}
