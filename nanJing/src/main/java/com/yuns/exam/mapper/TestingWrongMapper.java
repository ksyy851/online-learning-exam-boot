package com.yuns.exam.mapper;

import com.yuns.exam.entity.TestingWrong;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface TestingWrongMapper extends BaseMapper<TestingWrong> {
        List<TestingWrong> selectTestingWrongList(FrontPage frontPage);
}
