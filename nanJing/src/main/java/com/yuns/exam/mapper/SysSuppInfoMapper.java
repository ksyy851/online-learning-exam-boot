package com.yuns.exam.mapper;

import com.yuns.exam.entity.SysSuppInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 后台信息表(环保信息中心、宣教中心、市局主要处室人员、 各区环保局管理人员、题库管理人员、技术公司平台维护人员) Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface SysSuppInfoMapper extends BaseMapper<SysSuppInfo> {
        List<SysSuppInfo> selectSysSuppInfoList(FrontPage frontPage);
}
