package com.yuns.exam.mapper;

import com.yuns.exam.entity.SysRuleRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 违章/要求信息表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface SysRuleRecordMapper extends BaseMapper<SysRuleRecord> {
        List<SysRuleRecord> selectSysRuleRecordList(FrontPage frontPage);
}
