package com.yuns.exam.mapper;

import com.yuns.exam.entity.InfoTypeE;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 资讯类型 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface InfoTypeEMapper extends BaseMapper<InfoTypeE> {
        List<InfoTypeE> selectInfoTypeEList(FrontPage frontPage);
}
