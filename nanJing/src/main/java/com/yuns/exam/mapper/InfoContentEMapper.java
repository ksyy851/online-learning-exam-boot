package com.yuns.exam.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.InfoContentE;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.exam.req.InfoContentEParam;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 资讯内容 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface InfoContentEMapper extends BaseMapper<InfoContentE> {
        List<InfoContentE> selectInfoContentEList(@Param("page") Page<InfoContentE> page, @Param("frontPage") InfoContentEParam frontPage);
        List<InfoContentE> selectInfoContentEList(@Param("frontPage") InfoContentEParam frontPage);
}
