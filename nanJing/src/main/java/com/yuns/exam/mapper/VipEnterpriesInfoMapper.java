package com.yuns.exam.mapper;

import com.yuns.exam.entity.VipEnterpriesInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 企业信息表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface VipEnterpriesInfoMapper extends BaseMapper<VipEnterpriesInfo> {
        List<VipEnterpriesInfo> selectVipEnterpriesInfoList(FrontPage frontPage);
}
