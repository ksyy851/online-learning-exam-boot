package com.yuns.exam.mapper;

import com.yuns.exam.entity.ExamInfor;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 试卷定义表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamInforMapper extends BaseMapper<ExamInfor> {
        List<ExamInfor> selectExamInforList(FrontPage frontPage);
}
