package com.yuns.exam.mapper;

import com.yuns.exam.entity.VipFavoriteRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 会员收藏信息 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface VipFavoriteRecordMapper extends BaseMapper<VipFavoriteRecord> {
        List<VipFavoriteRecord> selectVipFavoriteRecordList(FrontPage frontPage);
}
