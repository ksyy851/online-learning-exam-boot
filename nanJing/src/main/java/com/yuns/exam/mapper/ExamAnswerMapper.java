package com.yuns.exam.mapper;

import com.yuns.exam.entity.ExamAnswer;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamAnswerMapper extends BaseMapper<ExamAnswer> {
        List<ExamAnswer> selectExamAnswerList(FrontPage frontPage);
}
