package com.yuns.exam.mapper;

import com.yuns.exam.entity.SysLoginfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 系统日志信息表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface SysLoginfoMapper extends BaseMapper<SysLoginfo> {
        List<SysLoginfo> selectSysLoginfoList(FrontPage frontPage);
}
