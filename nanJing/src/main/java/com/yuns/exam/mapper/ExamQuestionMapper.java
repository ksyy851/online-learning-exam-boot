package com.yuns.exam.mapper;

import com.yuns.exam.dto.QuestionRandomDto;
import com.yuns.exam.entity.ExamQuestion;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.HashMap;
import java.util.List;

import com.yuns.exam.entity.Question;
import com.yuns.exam.vo.ExamVO;
import com.yuns.web.param.FrontPage;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ExamQuestionMapper extends BaseMapper<ExamQuestion> {
        List<ExamQuestion> selectExamQuestionList(FrontPage frontPage);
        List<ExamVO>  selectQuestionWrongRate();
        List<QuestionRandomDto>  selectQuestionListByRandom(Question question);

        List<ExamQuestion> selectExamQuestionByMap(HashMap map);
}
