package com.yuns.exam.req;

import com.yuns.exam.vo.ExamVO;
import com.yuns.web.param.FrontPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @description: TODO: 试卷查询入参
 * @author: zhansang
 * @create: 2020-10-28 18:44
 * @version: 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ApiModel(value = "试卷查询入参", description = "试卷查询入参")
public class ExamQueryReq extends FrontPage implements Serializable {
    @ApiModelProperty(value = "错误率从高到底0 从低到高1", required = true)
    private Integer orderBy;
    @ApiModelProperty(value = "试卷名")
    private String examName;
    @ApiModelProperty(value = "考试时间")
    private String examTime;
    @ApiModelProperty(value = "考试开始时间")
    private String startTime;
    @ApiModelProperty(value = "考试结束时间")
    private String endTime;
    @ApiModelProperty(value = "总分")
    private Integer grade;
    @ApiModelProperty(value = "考试人数")
    private Integer examStudentCount;
    @ApiModelProperty(value = "最高分")
    private Integer maxGrade;
    @ApiModelProperty(value = "最低分")
    private Integer minGrade;
    @ApiModelProperty(value = "平均分")
    private Integer avgGrade;
    @ApiModelProperty(value = "错误率")
    private Integer errorRate;
    @ApiModelProperty(value = "试卷id")
    private BigInteger examId;
}
