package com.yuns.exam.req;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @description: TODO: 考试入参
 * @author: zhansang
 * @create: 2020-10-29 18:06
 * @version: 1.0
 **/
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class ExamReq {

    private Integer id;
}
