package com.yuns.exam.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 考试中心查询入参
 * @author: zhansang
 * @create: 2020-11-14 15:55
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExamDoorReq {
    @ApiModelProperty(value = "考试试卷状态（0 已考 1未考）")
    private Integer examStatus;

    @ApiModelProperty(value = "考试开始时间")
    private String startTime;
    @ApiModelProperty(value = "考试结束时间")
    private String endTime;

    @ApiModelProperty(value = "用户id")
    private Integer userId;
    @ApiModelProperty(value = "企业用户，学校用户，个人用户,环保局用户")
    private Integer examObjCompanyType;
}
