package com.yuns.exam.req;

import com.yuns.exam.vo.ExamRecordVO;
import com.yuns.web.param.FrontPage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 考试记录入参
 * @author: zhansang
 * @create: 2020-11-10 17:00
 * @version: 1.0
 **/
@Data
@Builder
public class ExamRecordParm extends FrontPage<ExamRecordVO> {
}
