package com.yuns.exam.req;

import com.yuns.exam.entity.Exam;
import com.yuns.exam.entity.Question;
import com.yuns.exam.vo.ExamVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

import java.util.List;

/**
 * @description: TODO: 试卷新增入参
 * @author: zhansang
 * @create: 2020-10-28 12:49
 * @version: 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ExamDto", description = "试卷新增入参")
public class ExamDto {
    @ApiModelProperty(value = "试卷", required = true)
    private Exam exam;
//    @ApiModelProperty(value = "题目", required = true)
//    private List<Question> questionList;
    // （0：单选题 1多选题 2 判断题 3 简答题）
    @ApiModelProperty(value = "题库列表", required = true)
    List<Exam> questionLibs;
    @ApiModelProperty(value = "单选题题目数量", required = true)
    Integer questionNum0;
    @ApiModelProperty(value = "单选题题目分数", required = true)
    Integer questionScore0;
    @ApiModelProperty(value = "多选题 题目数量", required = true)
    Integer questionNum1;
    @ApiModelProperty(value = "多选题 题目分数", required = true)
    Integer questionScore1;
    @ApiModelProperty(value = "判断题题目数量", required = true)
    Integer questionNum2;
    @ApiModelProperty(value = "判断题 题目分数", required = true)
    Integer questionScore2;
    @ApiModelProperty(value = "简答题题目数量", required = true)
    Integer questionNum3;
    @ApiModelProperty(value = "简答题题目分数", required = true)
    Integer questionScore3;
}
