package com.yuns.exam.req;

import com.yuns.exam.entity.InfoContentE;
import com.yuns.web.param.FrontPage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @description: TODO: 资讯类请求参数
 * @author: zhansang
 * @create: 2020-11-08 12:08
 * @version: 1.0
 **/
@Data
public class InfoContentEParam implements Serializable {

    @ApiModelProperty(value = "上传开始时间", required = false)
    private String bgTm;

    @ApiModelProperty(value = "上传结束时间", required = false)
    private String endTm;


    @ApiModelProperty(value = "是否是分页")
    private boolean _search;


    //	每页显示条数
    @ApiModelProperty(value = "每页显示条数")
    private int pageSize;

    //当前页数
    @ApiModelProperty(value = "当前页数")
    private int pageNumber;

    //排序的字段
    @ApiModelProperty(value = "排序的字段")
    private String sortField;

    //排序方式 asc升序  desc降序
    @ApiModelProperty(value = "排序方式 asc升序  desc降序")
    private String sortOrder;

    //搜索条件
    @ApiModelProperty(value = "搜索条件")
    private String keywords;

    @ApiModelProperty(value = "模糊查询参数", required = false)
    private String selected;


    @ApiModelProperty(value = "类型查询参数", required = false)
    private String selectedType;

    //类型（毫秒）
    @ApiModelProperty(value = "类型")
    private String type;

}
