package com.yuns.exam.controller;

import com.yuns.exam.service.IInfoRdxwEService;
import com.yuns.exam.entity.InfoRdxwE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.InfoRdxwEDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = " - Controller" )
    @RestController
@RequestMapping("/infoRdxwE" )
    public class InfoRdxwEController {

    @Autowired
    private IInfoRdxwEService iInfoRdxwEService;

    @ApiOperation(value = "添加 - info_rdxw_e" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody InfoRdxwE model){
            //  InfoRdxwE entity=DtoUtil.convertObject(model,InfoRdxwE.class);
            boolean flag=iInfoRdxwEService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - info_rdxw_e" )
    @PostMapping(value = "up/{id}" )
    public ResultJson update(@RequestBody InfoRdxwE model){
            //InfoRdxwE entity=DtoUtil.convertObject(model,InfoRdxwE.class);
            boolean flag=iInfoRdxwEService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - info_rdxw_e" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iInfoRdxwEService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - info_rdxw_e" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
            if(!frontPage.is_search()){
            Page<InfoRdxwE> page=new Page<InfoRdxwE>();
            Page<InfoRdxwE> list=page.setRecords(iInfoRdxwEService.selectInfoRdxwEList(frontPage));
            return ResultJson.ok(list);
            }
            Page<InfoRdxwE> page=new Page<InfoRdxwE>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<InfoRdxwE> list=page.setRecords(iInfoRdxwEService.selectInfoRdxwEList(frontPage));
            return ResultJson.ok(list);
            }
        }

