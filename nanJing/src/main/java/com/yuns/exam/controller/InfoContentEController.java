package com.yuns.exam.controller;

import com.yuns.exam.req.InfoContentEParam;
import com.yuns.exam.service.IInfoContentEService;
import com.yuns.exam.entity.InfoContentE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.InfoContentEDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = "资讯内容 - Controller")
@RestController
@RequestMapping("/infoContentE")
public class InfoContentEController {

    @Autowired
    private IInfoContentEService iInfoContentEService;

    @ApiOperation(value = "添加 - info_content_e")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody InfoContentE model) {
        //  InfoContentE entity=DtoUtil.convertObject(model,InfoContentE.class);
        boolean flag = iInfoContentEService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - info_content_e")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody InfoContentE model) {
        //InfoContentE entity=DtoUtil.convertObject(model,InfoContentE.class);
        boolean flag = iInfoContentEService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - info_content_e")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iInfoContentEService.deleteById(String.valueOf(id));
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - info_content_e")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody InfoContentEParam frontPage) {
        Object list = iInfoContentEService.selectInfoContentEList(frontPage);
        return ResultJson.ok(list);

    }
}

