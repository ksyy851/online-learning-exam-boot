package com.yuns.exam.controller;

import com.yuns.exam.service.IExamQuestionOptionService;
import com.yuns.exam.entity.ExamQuestionOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.ExamQuestionOptionDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = " - Controller" )
    @RestController
@RequestMapping("/examQuestionOption" )
    public class ExamQuestionOptionController {

    @Autowired
    private IExamQuestionOptionService iExamQuestionOptionService;

    @ApiOperation(value = "添加 - gt_exam_question_option" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody ExamQuestionOption model){
            //  ExamQuestionOption entity=DtoUtil.convertObject(model,ExamQuestionOption.class);
            boolean flag=iExamQuestionOptionService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - gt_exam_question_option" )
    @PostMapping(value = "up/{id}" )
    public ResultJson update(@RequestBody ExamQuestionOption model){
            //ExamQuestionOption entity=DtoUtil.convertObject(model,ExamQuestionOption.class);
            boolean flag=iExamQuestionOptionService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - gt_exam_question_option" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iExamQuestionOptionService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - gt_exam_question_option" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
            if(!frontPage.is_search()){
            Page<ExamQuestionOption> page=new Page<ExamQuestionOption>();
            Page<ExamQuestionOption> list=page.setRecords(iExamQuestionOptionService.selectExamQuestionOptionList(frontPage));
            return ResultJson.ok(list);
            }
            Page<ExamQuestionOption> page=new Page<ExamQuestionOption>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<ExamQuestionOption> list=page.setRecords(iExamQuestionOptionService.selectExamQuestionOptionList(frontPage));
            return ResultJson.ok(list);
            }
        }

