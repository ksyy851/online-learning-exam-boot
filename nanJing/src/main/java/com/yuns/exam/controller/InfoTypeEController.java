package com.yuns.exam.controller;

import com.yuns.exam.service.IInfoTypeEService;
import com.yuns.exam.entity.InfoTypeE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.InfoTypeEDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = "资讯类型 - Controller" )
    @RestController
@RequestMapping("/infoTypeE" )
    public class InfoTypeEController {

    @Autowired
    private IInfoTypeEService iInfoTypeEService;

    @ApiOperation(value = "添加 - info_type_e" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody InfoTypeE model){
            //  InfoTypeE entity=DtoUtil.convertObject(model,InfoTypeE.class);
            boolean flag=iInfoTypeEService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - info_type_e" )
    @PostMapping(value = "up/{id}" )
    public ResultJson update(@RequestBody InfoTypeE model){
            //InfoTypeE entity=DtoUtil.convertObject(model,InfoTypeE.class);
            boolean flag=iInfoTypeEService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - info_type_e" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iInfoTypeEService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - info_type_e" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
            if(!frontPage.is_search()){
            Page<InfoTypeE> page=new Page<InfoTypeE>();
            Page<InfoTypeE> list=page.setRecords(iInfoTypeEService.selectInfoTypeEList(frontPage));
            return ResultJson.ok(list);
            }
            Page<InfoTypeE> page=new Page<InfoTypeE>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<InfoTypeE> list=page.setRecords(iInfoTypeEService.selectInfoTypeEList(frontPage));
            return ResultJson.ok(list);
            }
        }

