package com.yuns.exam.controller;

import com.yuns.exam.service.IQuestionOptionService;
import com.yuns.exam.entity.QuestionOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.QuestionOptionDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = " - Controller" )
    @RestController
@RequestMapping("/questionOption" )
    public class QuestionOptionController {

    @Autowired
    private IQuestionOptionService iQuestionOptionService;

    @ApiOperation(value = "添加 - gt_question_option" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody QuestionOption model){
            //  QuestionOption entity=DtoUtil.convertObject(model,QuestionOption.class);
            boolean flag=iQuestionOptionService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - gt_question_option" )
    @PostMapping(value = "up/{id}" )
    public ResultJson update(@RequestBody QuestionOption model){
            //QuestionOption entity=DtoUtil.convertObject(model,QuestionOption.class);
            boolean flag=iQuestionOptionService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - gt_question_option" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iQuestionOptionService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - gt_question_option" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
            if(!frontPage.is_search()){
            Page<QuestionOption> page=new Page<QuestionOption>();
            Page<QuestionOption> list=page.setRecords(iQuestionOptionService.selectQuestionOptionList(frontPage));
            return ResultJson.ok(list);
            }
            Page<QuestionOption> page=new Page<QuestionOption>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<QuestionOption> list=page.setRecords(iQuestionOptionService.selectQuestionOptionList(frontPage));
            return ResultJson.ok(list);
            }
        }

