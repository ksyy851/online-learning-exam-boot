package com.yuns.exam.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.ExamAnswerRecord;
import com.yuns.exam.service.IExamAnswerRecordService;
import com.yuns.exam.vo.ExamAnswerRecordVO;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//import com.yuns.web.dto.ExamAnswerRecordDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = "试卷试题答题答案表 - Controller")
@RestController
@RequestMapping("/examAnswerRecord")
public class ExamAnswerRecordController {

    @Autowired
    private IExamAnswerRecordService iExamAnswerRecordService;

    @ApiOperation(value = "添加 - gt_exam_answer_record")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody ExamAnswerRecord model) {
        //  ExamAnswerRecord entity=DtoUtil.convertObject(model,ExamAnswerRecord.class);
        boolean flag = iExamAnswerRecordService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - gt_exam_answer_record")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody ExamAnswerRecord model) {
        //ExamAnswerRecord entity=DtoUtil.convertObject(model,ExamAnswerRecord.class);
        boolean flag = iExamAnswerRecordService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - gt_exam_answer_record")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iExamAnswerRecordService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - gt_exam_answer_record")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<ExamAnswerRecord> frontPage) {
        if (!frontPage.is_search()) {
//            Page<ExamAnswerRecordVO> page = new Page<ExamAnswerRecordVO>();
//            Page<ExamAnswerRecordVO> list = page.setRecords();
            return ResultJson.ok(iExamAnswerRecordService.selectExamAnswerRecordList(frontPage));
        }
        Page<ExamAnswerRecordVO> page = new Page<ExamAnswerRecordVO>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<ExamAnswerRecordVO> list = page.setRecords(iExamAnswerRecordService.selectExamAnswerRecordList(frontPage));
        return ResultJson.ok(list);
    }
}

