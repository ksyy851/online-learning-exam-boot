package com.yuns.exam.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.exam.entity.ExamAnswer;
import com.yuns.exam.service.IExamAnswerService;

import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//import com.yuns.web.dto.ExamAnswerDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = " - Controller" )
    @RestController
@RequestMapping("/examAnswer" )
    public class ExamAnswerController {

    @Autowired
    private IExamAnswerService iExamAnswerService;

    @ApiOperation(value = "添加 - gt_exam_answer" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody ExamAnswer model){
            //  ExamAnswer entity=DtoUtil.convertObject(model,ExamAnswer.class);
            boolean flag=iExamAnswerService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - gt_exam_answer" )
    @PostMapping(value = "up/{id}" )
    public ResultJson update(@RequestBody ExamAnswer model){
            //ExamAnswer entity=DtoUtil.convertObject(model,ExamAnswer.class);
            boolean flag=iExamAnswerService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - gt_exam_answer" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iExamAnswerService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - gt_exam_answer" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
            if(!frontPage.is_search()){
            Page<ExamAnswer> page=new Page<ExamAnswer>();
            Page<ExamAnswer> list=page.setRecords(iExamAnswerService.selectExamAnswerList(frontPage));
            return ResultJson.ok(list);
            }
            Page<ExamAnswer> page=new Page<ExamAnswer>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<ExamAnswer> list=page.setRecords(iExamAnswerService.selectExamAnswerList(frontPage));
            return ResultJson.ok(list);
            }
        }

