package com.yuns.exam.controller;

import com.mysql.jdbc.StringUtils;
import com.yuns.exam.dto.ExamDto;
import com.yuns.exam.dto.QuestionDto;
import com.yuns.exam.entity.ExamQuestion;
import com.yuns.exam.entity.Question;
import com.yuns.exam.entity.QuestionOption;
import com.yuns.exam.req.QuestionParam;
import com.yuns.exam.service.IExamQuestionService;
import com.yuns.exam.service.IQuestionOptionService;
import com.yuns.exam.service.IQuestionService;
import com.yuns.exception.MyException;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import io.netty.util.internal.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = "题目管理 - Controller" )
@RestController
@RequestMapping("/question" )
    public class QuestionController {

    @Autowired
    private IQuestionService iQuestionService;

    @Autowired
    private IQuestionOptionService questionOptionService;

    @Autowired
    private IExamQuestionService examQuestionService;

    @ApiOperation(value = "题目管理添加题目 - gt_question" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody QuestionDto model){
        boolean flag =false;
            Question question = model.getQuestion();
            if(question.getId()==null || StringUtil.isNullOrEmpty(String.valueOf(question.getId()))) {
                 flag = iQuestionService.insert(question);
            }else{

                // 修改
                flag =  iQuestionService.updateById(question);
            }
            List<QuestionOption> questionOption = model.getQuestionOptionList();
            if(null!=questionOption) {
                // 题目ID
                String questionId = String.valueOf(question.getId());
                // 当题目是多选题或者单选题时 保存多选题选项  判断题也有对错选项
                if (1 == question.getQuestionTypeSel() || 0 == question.getQuestionTypeSel()
                        || 2 == question.getQuestionTypeSel()
                ) {
                    questionOption.forEach(i->{
                        if(i.getId()==null || StringUtils.isNullOrEmpty(String.valueOf(i.getId()))) {
                            questionOptionService.insert(QuestionOption.builder().questionId(questionId).
                                    code(i.getCode()).createTime(new Date()).
                                    content(i.getContent()).build());
                        }else{
                            // 修改
                            questionOptionService.updateById(QuestionOption.builder().questionId(questionId).
                                    id(i.getId()).
                                    code(i.getCode()).updateTime(new Date()).
                                    content(i.getContent()).build());
                        }
                    });
                }
            }
            // 选择了题库
            List<ExamDto> examList = model.getExamList();

        // 先删除原来的归属  进行新的归属题库操作 这道题目重新进行归属
        HashMap m = new HashMap<>();
        m.put("question_id",question.getId());
        m.put("question_type",question.getQuestionTypeSel());
//        m.put("exam_id",item.getId());
        examQuestionService.deleteByMap(m);
            // 题目保存到题库中
            examList.forEach(item->{


                if(item.getId()!=null && !StringUtils.isNullOrEmpty(item.getId())) { // 新增题库值
                    examQuestionService.insert(ExamQuestion.builder().questionId(String.valueOf(question.getId())).
                            questionType(question.getQuestionTypeSel()).grade(question.getQuestionScore()).
                            questionSource(1). // 题目生成同时生成题库
                            analysis(question.getAnalysis()).examId(item.getId()).questionTitle(question.getQuestionTitle()).build());
                }
            });
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - gt_question" )
    @PostMapping(value = "update")
    public ResultJson update(@RequestBody Question model){
            boolean flag=iQuestionService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - gt_question" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable Long id){
            boolean flag=iQuestionService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - gt_question" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
           Object list = iQuestionService.selectQuestionList(frontPage);
            return ResultJson.ok(list);
            }



    @ApiOperation(value = "题目管理查询列表 - gt_question" )
    @PostMapping(value = "queryQuestionList" )
    public ResultJson queryQuestionList(@RequestBody QuestionParam frontPage){
        Object list = iQuestionService.selectQuestionListByCondition(frontPage);
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "题目管理列表查询详情数据 - gt_question" )
    @PostMapping(value = "queryQuestionDetailData" )
    public ResultJson queryQuestionDetailData(@RequestBody QuestionParam frontPage){
        Object list = iQuestionService.selectQuestionDetail(frontPage);
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "导出题目列表 - exportList" )
    @PostMapping(value = "exportQuestionList" )
    public ResultJson exportList(@RequestBody QuestionParam frontPage, HttpServletResponse response) {
        iQuestionService.exportList(frontPage,response);
        return ResultJson.ok();
    }

}
