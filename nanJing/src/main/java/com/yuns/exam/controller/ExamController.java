package com.yuns.exam.controller;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yuns.config.WebsocketServer;
import com.yuns.exam.req.ExamDoorParam;
import com.yuns.exam.req.ExamDto;
import com.yuns.exam.req.ExamQueryReq;
import com.yuns.exam.service.IExamQuestionService;
import com.yuns.exam.service.IExamService;
import com.yuns.exam.entity.Exam;
import com.yuns.exam.vo.ExamDataVO;
import com.yuns.exam.vo.ExamQuestionStatisVO;
import com.yuns.exam.vo.ExamVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.web.param.QueryParam;

import java.io.IOException;
import java.util.*;

//import com.yuns.web.dto.ExamDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = " 题库管理(试卷管理)- Controller")
@RestController
@RequestMapping("/exam")
@Slf4j
public class ExamController {

    @Autowired
    private IExamService iExamService;

    @Autowired
    private IExamQuestionService examQuestionService;

    @ApiOperation(value = "添加 - gt_exam", hidden = true)
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody Exam model) {
        boolean flag = iExamService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - gt_exam", hidden = true)
    @PostMapping(value = "update")
    public ResultJson update(@RequestBody Exam model) {
        boolean flag = iExamService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除试卷 - gt_exam", hidden = true)
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable Long id) {
        boolean flag = iExamService.deleteById(id);
        // 删除试卷单独存储的题目的题目
        HashMap m  = new HashMap();
        m.put("exam_id",id);
        examQuestionService.deleteByMap(m);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - gt_exam", hidden = true)
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage frontPage) {
        Object list = iExamService.selectExamList(frontPage);
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "试卷管理查询试卷 - gt_exam")
    @PostMapping(value = "queryExamList")
    public ResultJson queryExamList(@RequestBody ExamQueryReq frontPage) {
        if (!frontPage.is_search()) {
            Page<ExamVO> page = new Page<ExamVO>();
            Object list =iExamService.selectExamDataList(frontPage);
            return ResultJson.ok(list);
        }

        Object list =iExamService.selectExamDataList(frontPage);
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "考试管理列表详情 - gt_exam")
    @PostMapping(value = "selectExamDetailList")
    public ResultJson selectExamDetailList(@RequestBody ExamVO model) {
        return ResultJson.ok(iExamService.selectExamDetailList(model));
    }

    @ApiOperation(value = "题库管理查询列表 - gt_exam")
    @PostMapping(value = "selectAllExamList")
    public ResultJson selectAllExamList(@RequestBody ExamQueryReq frontPage) {
        Page<ExamQuestionStatisVO> list = iExamService.selectAllExamList(frontPage);
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "试卷管理新增试卷选择题库列表- gt_exam")
    @PostMapping(value = "selectQuestionLibList")
    public ResultJson selectQuestionLibList(@RequestBody ExamQueryReq frontPage) {
        if (!frontPage.is_search()) {
            Object list = (iExamService.selectQuestionLibList());
            return ResultJson.ok(list);
        }
        Page<ExamDataVO> page = new Page<ExamDataVO>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<ExamDataVO> list = page.setRecords(iExamService.selectQuestionLibList());
        return ResultJson.ok(list);
    }


    @ApiOperation(value = "新增试卷")
    @PostMapping(value = "insertExamData")
    public ResultJson insertExamData(@RequestBody ExamDto model) {
        //  Exam entity=DtoUtil.convertObject(model,Exam.class);
        boolean flag = iExamService.insertExamData(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }
    //***=================== 门户 接口==============================****//

    @ApiOperation(value = "门户考试中心 - gt_exam")
    @PostMapping(value = "getExamListForDoor")
    public ResultJson getExamListForDoor(@RequestBody ExamDoorParam frontPage) {
        Object list = iExamService.getExamListForDoor(frontPage);
        return ResultJson.ok(list);
    }

    /*@ApiOperation(value = "发送考试倒计时消息的接口")
    @GetMapping(value = "/push/{toUserId}")
    public ResultJson pushToWeb(String message, @PathVariable String toUserId) throws IOException {
        HashMap map = new HashMap();
        map.put("selectedType","id");
        map.put("type","1252601196249673730"); // 1252601196249673730 1252624993434529807
        List<HashMap<String,Object>> examList = iExamService.selectList(map);
        if (examList != null && examList.size() > 0) {
            Map exam1 = examList.get(0);
            Date bt = (Date) exam1.get("start_time");
            Date et = (Date) exam1.get("end_time");
        } else {
            int midTime = 60*60*60;
            String msg="";
            while (midTime > 0) {
                midTime--;
                long hh = midTime / 60 / 60 % 60;
                long mm = midTime / 60 % 60;
                long ss = midTime % 60;
                log.info("还剩" + hh + "小时" + mm + "分钟" + ss + "秒");
                msg = "还剩" + hh + "小时" + mm + "分钟" + ss + "秒";
                WebsocketServer.sendInfo(msg, toUserId);
                if(hh == 0 && mm==0 && ss==0){
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        return ResultJson.ok("MSG SEND SUCCESS");
    }*/

}

