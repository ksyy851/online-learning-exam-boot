package com.yuns.exam.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 考试记录表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_record")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExamRecord extends Model<ExamRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private String id;
    /**
     * 答题开始
     */
    @ApiParam(value = "答题开始")
   @ApiModelProperty(value = "答题开始")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date BTime;
    /**
     * 答题结束
     */
    @ApiParam(value = "答题结束")
   @ApiModelProperty(value = "答题结束")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ETime;
    /**
     * 试卷ID
     */
    @ApiParam(value = "试卷ID")
   @ApiModelProperty(value = "试卷ID")
    private Long ExaminationInfoID;
    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
   @ApiModelProperty(value = "会员ID")
    private Integer MemberID;
    /**
     * 所属企业
     */
    @ApiParam(value = "所属企业")
   @ApiModelProperty(value = "所属企业")
    @TableField("EID")
    private Integer eid;
    /**
     * 答题时长
     */
    @ApiParam(value = "答题时长")
   @ApiModelProperty(value = "答题时长")
    private Integer ALong;
    /**
     * 成绩
     */
    @ApiParam(value = "总分")
   @ApiModelProperty(value = "总分")
    private BigDecimal AScroe;

    @ApiParam(value = "得分")
    @ApiModelProperty(value = "得分")
    private BigDecimal score;

    @ApiParam(value = "是否已阅卷")
    @ApiModelProperty(value = "是否已阅卷")
    private Integer isCheck;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField("update_user")
    private Long updateUser;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
