package com.yuns.exam.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 资讯类型
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("info_type_e")
public class InfoTypeE extends Model<InfoTypeE> {

    private static final long serialVersionUID = 1L;

    /**
     * 对象ID
     */
    @ApiParam(value = "对象ID")
   @ApiModelProperty(value = "对象ID")
    @TableId(value = "dxid", type = IdType.AUTO)
    private String dxid;
    /**
     * 名称
     */
    @ApiParam(value = "名称")
   @ApiModelProperty(value = "名称")
    private String mc;
    /**
     * 编码
     */
    @ApiParam(value = "编码")
   @ApiModelProperty(value = "编码")
    private String bm;
    /**
     * 上级对象ID
     */
    @ApiParam(value = "上级对象ID")
   @ApiModelProperty(value = "上级对象ID")
    private String sjdxid;
    /**
     * 排序
     */
    @ApiParam(value = "排序")
   @ApiModelProperty(value = "排序")
    private Integer px;
    /**
     * 类型说明
     */
    @ApiParam(value = "类型说明")
   @ApiModelProperty(value = "类型说明")
    private String bz;


    public String getDxid() {
        return dxid;
    }

    public void setDxid(String dxid) {
        this.dxid = dxid;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getBm() {
        return bm;
    }

    public void setBm(String bm) {
        this.bm = bm;
    }

    public String getSjdxid() {
        return sjdxid;
    }

    public void setSjdxid(String sjdxid) {
        this.sjdxid = sjdxid;
    }

    public Integer getPx() {
        return px;
    }

    public void setPx(Integer px) {
        this.px = px;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    @Override
    protected Serializable pkVal() {
        return this.dxid;
    }

    @Override
    public String toString() {
        return "InfoTypeE{" +
        ", dxid=" + dxid +
        ", mc=" + mc +
        ", bm=" + bm +
        ", sjdxid=" + sjdxid +
        ", px=" + px +
        ", bz=" + bz +
        "}";
    }
}
