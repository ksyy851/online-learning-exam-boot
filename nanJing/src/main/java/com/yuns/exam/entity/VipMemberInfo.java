package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 会员信息表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("vip_member_info")
public class VipMemberInfo extends Model<VipMemberInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 账号/手机号
     */
    @ApiParam(value = "账号/手机号")
   @ApiModelProperty(value = "账号/手机号")
    private String Tel;
    /**
     * 密码
     */
    @ApiParam(value = "密码")
   @ApiModelProperty(value = "密码")
    private String Pass;
    /**
     * 昵称
     */
    @ApiParam(value = "昵称")
   @ApiModelProperty(value = "昵称")
    private String Pet;
    /**
     * 是否后台人员
     */
    @ApiParam(value = "是否后台人员")
   @ApiModelProperty(value = "是否后台人员")
    private String IsSupp;
    /**
     * 后台ID
     */
    @ApiParam(value = "后台ID")
   @ApiModelProperty(value = "后台ID")
    private Integer SuppID;
    /**
     * 注册日期
     */
    @ApiParam(value = "注册日期")
   @ApiModelProperty(value = "注册日期")
    private Date MDate;
    /**
     * 消息令牌
     */
    @ApiParam(value = "消息令牌")
   @ApiModelProperty(value = "消息令牌")
    private String token;
    /**
     * 积分
     */
    @ApiParam(value = "积分")
   @ApiModelProperty(value = "积分")
    private Integer acc;
    /**
     * 最后一次签到
     */
    @ApiParam(value = "最后一次签到")
   @ApiModelProperty(value = "最后一次签到")
    private Date LastSign;
    /**
     * 连续签到多到天
     */
    @ApiParam(value = "连续签到多到天")
   @ApiModelProperty(value = "连续签到多到天")
    private Integer SignDays;
    /**
     * 姓名
     */
    @ApiParam(value = "姓名")
   @ApiModelProperty(value = "姓名")
    private String Name;
    /**
     * 详细地址
     */
    @ApiParam(value = "详细地址")
   @ApiModelProperty(value = "详细地址")
    private String Addr;
    /**
     * 所属企业
     */
    @ApiParam(value = "所属企业")
   @ApiModelProperty(value = "所属企业")
    @TableField("EID")
    private Integer eid;
    /**
     * 企业岗位
     */
    @ApiParam(value = "企业岗位")
   @ApiModelProperty(value = "企业岗位")
    private String Content;
    /**
     * 身份证号
     */
    @ApiParam(value = "身份证号")
   @ApiModelProperty(value = "身份证号")
    @TableField("PID")
    private String pid;
    /**
     * 微信号
     */
    @ApiParam(value = "微信号")
   @ApiModelProperty(value = "微信号")
    private String WeChat;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTel() {
        return Tel;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    public String getPet() {
        return Pet;
    }

    public void setPet(String Pet) {
        this.Pet = Pet;
    }

    public String getIsSupp() {
        return IsSupp;
    }

    public void setIsSupp(String IsSupp) {
        this.IsSupp = IsSupp;
    }

    public Integer getSuppID() {
        return SuppID;
    }

    public void setSuppID(Integer SuppID) {
        this.SuppID = SuppID;
    }

    public Date getMDate() {
        return MDate;
    }

    public void setMDate(Date MDate) {
        this.MDate = MDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getAcc() {
        return acc;
    }

    public void setAcc(Integer acc) {
        this.acc = acc;
    }

    public Date getLastSign() {
        return LastSign;
    }

    public void setLastSign(Date LastSign) {
        this.LastSign = LastSign;
    }

    public Integer getSignDays() {
        return SignDays;
    }

    public void setSignDays(Integer SignDays) {
        this.SignDays = SignDays;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getAddr() {
        return Addr;
    }

    public void setAddr(String Addr) {
        this.Addr = Addr;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getWeChat() {
        return WeChat;
    }

    public void setWeChat(String WeChat) {
        this.WeChat = WeChat;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "VipMemberInfo{" +
        ", id=" + id +
        ", Tel=" + Tel +
        ", Pass=" + Pass +
        ", Pet=" + Pet +
        ", IsSupp=" + IsSupp +
        ", SuppID=" + SuppID +
        ", MDate=" + MDate +
        ", token=" + token +
        ", acc=" + acc +
        ", LastSign=" + LastSign +
        ", SignDays=" + SignDays +
        ", Name=" + Name +
        ", Addr=" + Addr +
        ", eid=" + eid +
        ", Content=" + Content +
        ", pid=" + pid +
        ", WeChat=" + WeChat +
        "}";
    }
}
