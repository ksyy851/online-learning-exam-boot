package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_testing")
public class Testing extends Model<Testing> {

    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField("create_user")
    private Long createUser;
    @TableField("create_dept")
    private Long createDept;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_user")
    private Long updateUser;
    @TableField("update_time")
    private Date updateTime;
    private Integer status;
    @TableField("is_deleted")
    private Integer isDeleted;
    /**
     * 练习人(用户登录名）
     */
    @ApiParam(value = "练习人(用户登录名）")
   @ApiModelProperty(value = "练习人(用户登录名）")
    @TableField("user_name")
    private String userName;
    /**
     * 练习时间
     */
    @ApiParam(value = "练习时间")
   @ApiModelProperty(value = "练习时间")
    @TableField("testing_time")
    private Date testingTime;
    /**
     * 题目ID
     */
    @ApiParam(value = "题目ID")
   @ApiModelProperty(value = "题目ID")
    @TableField("question_id")
    private Long questionId;
    /**
     * 答题状态（0：正确 1：错误)
     */
    @ApiParam(value = "答题状态（0：正确 1：错误)")
   @ApiModelProperty(value = "答题状态（0：正确 1：错误)")
    @TableField("answer_status")
    private String answerStatus;
    /**
     * 答题结果（A/B/C/D)
     */
    @ApiParam(value = "答题结果（A/B/C/D)")
   @ApiModelProperty(value = "答题结果（A/B/C/D)")
    @TableField("answer_result")
    private String answerResult;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getCreateDept() {
        return createDept;
    }

    public void setCreateDept(Long createDept) {
        this.createDept = createDept;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getTestingTime() {
        return testingTime;
    }

    public void setTestingTime(Date testingTime) {
        this.testingTime = testingTime;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getAnswerStatus() {
        return answerStatus;
    }

    public void setAnswerStatus(String answerStatus) {
        this.answerStatus = answerStatus;
    }

    public String getAnswerResult() {
        return answerResult;
    }

    public void setAnswerResult(String answerResult) {
        this.answerResult = answerResult;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Testing{" +
        ", id=" + id +
        ", createUser=" + createUser +
        ", createDept=" + createDept +
        ", createTime=" + createTime +
        ", updateUser=" + updateUser +
        ", updateTime=" + updateTime +
        ", status=" + status +
        ", isDeleted=" + isDeleted +
        ", userName=" + userName +
        ", testingTime=" + testingTime +
        ", questionId=" + questionId +
        ", answerStatus=" + answerStatus +
        ", answerResult=" + answerResult +
        "}";
    }
}
