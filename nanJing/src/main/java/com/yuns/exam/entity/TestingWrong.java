package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_testing_wrong")
public class TestingWrong extends Model<TestingWrong> {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 错题id
     */
    @ApiParam(value = "错题id")
   @ApiModelProperty(value = "错题id")
    @TableField("question_id")
    private Long questionId;
    /**
     * 练习人(用户登录名）
     */
    @ApiParam(value = "练习人(用户登录名）")
   @ApiModelProperty(value = "练习人(用户登录名）")
    @TableField("user_name")
    private String userName;
    /**
     * 练习时间
     */
    @ApiParam(value = "练习时间")
   @ApiModelProperty(value = "练习时间")
    @TableField("testing_time")
    private Date testingTime;
    /**
     * 答题结果（A/B/C/D)
     */
    @ApiParam(value = "答题结果（A/B/C/D)")
   @ApiModelProperty(value = "答题结果（A/B/C/D)")
    @TableField("answer_result")
    private String answerResult;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getTestingTime() {
        return testingTime;
    }

    public void setTestingTime(Date testingTime) {
        this.testingTime = testingTime;
    }

    public String getAnswerResult() {
        return answerResult;
    }

    public void setAnswerResult(String answerResult) {
        this.answerResult = answerResult;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TestingWrong{" +
        ", id=" + id +
        ", questionId=" + questionId +
        ", userName=" + userName +
        ", testingTime=" + testingTime +
        ", answerResult=" + answerResult +
        "}";
    }
}
