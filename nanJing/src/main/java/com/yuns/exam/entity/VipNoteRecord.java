package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 会员笔记信息表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("vip_note_record")
public class VipNoteRecord extends Model<VipNoteRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
   @ApiModelProperty(value = "会员ID")
    private Integer MemberID;
    /**
     * 笔记日期
     */
    @ApiParam(value = "笔记日期")
   @ApiModelProperty(value = "笔记日期")
    private Date NDate;
    /**
     * 笔记类型
     */
    @ApiParam(value = "笔记类型")
   @ApiModelProperty(value = "笔记类型")
    private String NType;
    /**
     * 试题ID
     */
    @ApiParam(value = "试题ID")
   @ApiModelProperty(value = "试题ID")
    private Integer LinkID;
    /**
     * 笔记内容
     */
    @ApiParam(value = "笔记内容")
   @ApiModelProperty(value = "笔记内容")
    private String Note;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer MemberID) {
        this.MemberID = MemberID;
    }

    public Date getNDate() {
        return NDate;
    }

    public void setNDate(Date NDate) {
        this.NDate = NDate;
    }

    public String getNType() {
        return NType;
    }

    public void setNType(String NType) {
        this.NType = NType;
    }

    public Integer getLinkID() {
        return LinkID;
    }

    public void setLinkID(Integer LinkID) {
        this.LinkID = LinkID;
    }

    public String getNote() {
        return Note;
    }

    public void setNote(String Note) {
        this.Note = Note;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "VipNoteRecord{" +
        ", id=" + id +
        ", MemberID=" + MemberID +
        ", NDate=" + NDate +
        ", NType=" + NType +
        ", LinkID=" + LinkID +
        ", Note=" + Note +
        "}";
    }
}
