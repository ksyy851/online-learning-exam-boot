package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("info_rdxw_e")
public class InfoRdxwE extends Model<InfoRdxwE> {

    private static final long serialVersionUID = 1L;

    /**
     * 对象ID
     */
    @ApiParam(value = "对象ID")
   @ApiModelProperty(value = "对象ID")
    @TableId(value = "dxid", type = IdType.AUTO)
    private String dxid;
    /**
     * 舆情对象ID
     */
    @ApiParam(value = "舆情对象ID")
   @ApiModelProperty(value = "舆情对象ID")
    private String yqdxid;
    /**
     * 图片1
     */
    @ApiParam(value = "图片1")
   @ApiModelProperty(value = "图片1")
    private String tp1;
    /**
     * 图片2
     */
    @ApiParam(value = "图片2")
   @ApiModelProperty(value = "图片2")
    private String tp2;
    /**
     * 图片3
     */
    @ApiParam(value = "图片3")
   @ApiModelProperty(value = "图片3")
    private String tp3;
    /**
     * 描述
     */
    @ApiParam(value = "描述")
   @ApiModelProperty(value = "描述")
    private String ms;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
   @ApiModelProperty(value = "创建时间")
    private Date cjsj;


    public String getDxid() {
        return dxid;
    }

    public void setDxid(String dxid) {
        this.dxid = dxid;
    }

    public String getYqdxid() {
        return yqdxid;
    }

    public void setYqdxid(String yqdxid) {
        this.yqdxid = yqdxid;
    }

    public String getTp1() {
        return tp1;
    }

    public void setTp1(String tp1) {
        this.tp1 = tp1;
    }

    public String getTp2() {
        return tp2;
    }

    public void setTp2(String tp2) {
        this.tp2 = tp2;
    }

    public String getTp3() {
        return tp3;
    }

    public void setTp3(String tp3) {
        this.tp3 = tp3;
    }

    public String getMs() {
        return ms;
    }

    public void setMs(String ms) {
        this.ms = ms;
    }

    public Date getCjsj() {
        return cjsj;
    }

    public void setCjsj(Date cjsj) {
        this.cjsj = cjsj;
    }

    @Override
    protected Serializable pkVal() {
        return this.dxid;
    }

    @Override
    public String toString() {
        return "InfoRdxwE{" +
        ", dxid=" + dxid +
        ", yqdxid=" + yqdxid +
        ", tp1=" + tp1 +
        ", tp2=" + tp2 +
        ", tp3=" + tp3 +
        ", ms=" + ms +
        ", cjsj=" + cjsj +
        "}";
    }
}
