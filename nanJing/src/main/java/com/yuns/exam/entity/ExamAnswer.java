package com.yuns.exam.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_answer")
public class ExamAnswer extends Model<ExamAnswer> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiParam(value = "主键")
   @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * gt_exam_student表id
     */
    @ApiParam(value = "gt_exam_student表id")
   @ApiModelProperty(value = "gt_exam_student表id")
    @TableField("exam_stu_id")
    private Long examStuId;
    /**
     * gt_exam表id
     */
    @ApiParam(value = "gt_exam表id")
   @ApiModelProperty(value = "gt_exam表id")
    @TableField("exam_id")
    private Long examId;
    /**
     * gt_exam_question表id
     */
    @ApiParam(value = "gt_exam_question表id")
   @ApiModelProperty(value = "gt_exam_question表id")
    @TableField("exam_qu_id")
    private Long examQuId;
    /**
     * blade_user表account
     */
    @ApiParam(value = "blade_user表account")
   @ApiModelProperty(value = "blade_user表account")
    @TableField("user_name")
    private String userName;
    /**
     * 所选答案（A、B、C、D）
     */
    @ApiParam(value = "所选答案（A、B、C、D）")
   @ApiModelProperty(value = "所选答案（A、B、C、D）")
    @TableField("answer_code")
    private String answerCode;
    /**
     * 是否正确  0：否  1：是
     */
    @ApiParam(value = "是否正确  0：否  1：是")
   @ApiModelProperty(value = "是否正确  0：否  1：是")
    private String isright;
    /**
     * 所得分数
     */
    @ApiParam(value = "所得分数")
   @ApiModelProperty(value = "所得分数")
    private String grade;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExamStuId() {
        return examStuId;
    }

    public void setExamStuId(Long examStuId) {
        this.examStuId = examStuId;
    }

    public Long getExamId() {
        return examId;
    }

    public void setExamId(Long examId) {
        this.examId = examId;
    }

    public Long getExamQuId() {
        return examQuId;
    }

    public void setExamQuId(Long examQuId) {
        this.examQuId = examQuId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getAnswerCode() {
        return answerCode;
    }

    public void setAnswerCode(String answerCode) {
        this.answerCode = answerCode;
    }

    public String getIsright() {
        return isright;
    }

    public void setIsright(String isright) {
        this.isright = isright;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamAnswer{" +
        ", id=" + id +
        ", examStuId=" + examStuId +
        ", examId=" + examId +
        ", examQuId=" + examQuId +
        ", userName=" + userName +
        ", answerCode=" + answerCode +
        ", isright=" + isright +
        ", grade=" + grade +
        "}";
    }
}
