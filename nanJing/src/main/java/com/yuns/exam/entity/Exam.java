package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Exam extends Model<Exam> {

    private static final long serialVersionUID = 1L;

    /**
     * 试卷名称
     */
    @ApiParam(value = "试卷名称")
    @ApiModelProperty(value = "试卷名称")
    @TableField("exam_name")
    private String examName;


    /**
     * 题目备注
     */
    @ApiParam(value = "题目备注")
    @ApiModelProperty(value = "题目备注")
    @TableField("exam_remark")
    private String examRemark;

    /**
     * 主键
     */
    @ApiParam(value = "主键")
   @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 试卷编号
     */
    @ApiParam(value = "试卷编号")
   @ApiModelProperty(value = "试卷编号")
    @TableField("exam_code")
    private String examCode;




    /**
     * 考试开始时间
     */
    @ApiParam(value = "考试开始时间")
   @ApiModelProperty(value = "考试开始时间")
    @TableField("start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;
    /**
     * 考试结束时间
     */
    @ApiParam(value = "考试结束时间")
    @ApiModelProperty(value = "考试结束时间")
    @TableField("end_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;
    /**
     * 考试时长（单位：分钟）
     */
    @ApiParam(value = "考试时长（单位：分钟）")
   @ApiModelProperty(value = "考试时长（单位：分钟）")
    @TableField("exam_time")
    private String examTime;
    /**
     * 试卷口令
     */
    @ApiParam(value = "试卷口令")
   @ApiModelProperty(value = "试卷口令")
    private String password;
    /**
     * 试卷总分
     */
    @ApiParam(value = "试卷总分")
   @ApiModelProperty(value = "试卷总分")
    private String grade;

    public String getExamObjCompanyType() {
        return examObjCompanyType;
    }

    public void setExamObjCompanyType(String examObjCompanyType) {
        this.examObjCompanyType = examObjCompanyType;
    }

    @ApiParam(value = "考试对象")
    @ApiModelProperty(value = "考试对象")
    @TableField("exam_obj_company_type")
    private String examObjCompanyType;

    public Integer getNeedStudyTime() {
        return needStudyTime;
    }

    public void setNeedStudyTime(Integer needStudyTime) {
        this.needStudyTime = needStudyTime;
    }

    @ApiParam(value = "满足的学分")
    @ApiModelProperty(value = "满足的学分")
    @TableField("need_study_time")
    private Integer needStudyTime;

    public Integer getQuestionSource() {
        return questionSource;
    }

    public void setQuestionSource(Integer questionSource) {
        this.questionSource = questionSource;
    }

    /**
     * 题目来源 0：试题创建 1（题库） 通过题目生成
     */
    @ApiParam(value = "题目来源 0：试题创建 1（题库） 通过题目生成")
    @ApiModelProperty(value = "题目来源 0：试题创建 1（题库） 通过题目生成")
    @TableField("question_source")
    private Integer questionSource;

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    /**
     * 题目类型（0：单选题 1多选题 2 判断题 3 简答题）
     */
    @ApiModelProperty(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
    private Integer questionType;

    public Integer getCountId() {
        return countId;
    }

    public void setCountId(Integer countId) {
        this.countId = countId;
    }

    @ApiModelProperty(value = "题目总数")
    private Integer countId;

    public String getPassGrade() {
        return passGrade;
    }

    public void setPassGrade(String passGrade) {
        this.passGrade = passGrade;
    }

    @ApiParam(value = "及格分")
    @ApiModelProperty(value = "及格分")
    @TableField("pass_grade")
    private String passGrade;

    @TableField("create_user")
    private Long createUser;

    @TableField("create_dept")
    private Long createDept;

    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @TableField("update_user")
    private Long updateUser;

    @TableField("update_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @TableField("status")
    private Integer status;

    @TableField("is_deleted")
    private Integer isDeleted;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExamCode() {
        return examCode;
    }

    public void setExamCode(String examCode) {
        this.examCode = examCode;
    }

    public String getExamName() {
        return examName;
    }
    public String getExamRemark() {
        return examRemark;
    }

    public void setExamName(String examName) {
        this.examName = examName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getExamTime() {
        return examTime;
    }

    public void setExamTime(String examTime) {
        this.examTime = examTime;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getCreateDept() {
        return createDept;
    }

    public void setCreateDept(Long createDept) {
        this.createDept = createDept;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Exam{" +
        ", id=" + id +
        ", examCode=" + examCode +
        ", examName=" + examName +
        ", examRemark=" + examRemark +
        ", startTime=" + startTime +
        ", endTime=" + endTime +
        ", examTime=" + examTime +
        ", password=" + password +
        ", grade=" + grade +
        ", createUser=" + createUser +
        ", createDept=" + createDept +
        ", createTime=" + createTime +
        ", updateUser=" + updateUser +
        ", updateTime=" + updateTime +
        ", status=" + status +
        ", isDeleted=" + isDeleted +
        "}";
    }

    public void setExamRemark(String examRemark) {
        this.examRemark = examRemark;
    }
}
