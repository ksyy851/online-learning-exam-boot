package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 会员收藏信息
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("vip_favorite_record")
public class VipFavoriteRecord extends Model<VipFavoriteRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 类别
     */
    @ApiParam(value = "类别")
   @ApiModelProperty(value = "类别")
    private String FType;
    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
   @ApiModelProperty(value = "会员ID")
    private Integer MemberID;
    /**
     * 收藏日期
     */
    @ApiParam(value = "收藏日期")
   @ApiModelProperty(value = "收藏日期")
    private Date FDate;
    /**
     * 关联ID
     */
    @ApiParam(value = "关联ID")
   @ApiModelProperty(value = "关联ID")
    private Integer LinkID;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFType() {
        return FType;
    }

    public void setFType(String FType) {
        this.FType = FType;
    }

    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer MemberID) {
        this.MemberID = MemberID;
    }

    public Date getFDate() {
        return FDate;
    }

    public void setFDate(Date FDate) {
        this.FDate = FDate;
    }

    public Integer getLinkID() {
        return LinkID;
    }

    public void setLinkID(Integer LinkID) {
        this.LinkID = LinkID;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "VipFavoriteRecord{" +
        ", id=" + id +
        ", FType=" + FType +
        ", MemberID=" + MemberID +
        ", FDate=" + FDate +
        ", LinkID=" + LinkID +
        "}";
    }
}
