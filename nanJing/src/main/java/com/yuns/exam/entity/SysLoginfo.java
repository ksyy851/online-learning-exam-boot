package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 系统日志信息表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("sys_loginfo")
public class SysLoginfo extends Model<SysLoginfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 操作动作
     */
    @ApiParam(value = "操作动作")
   @ApiModelProperty(value = "操作动作")
    private Date optime;
    /**
     * 账号
     */
    @ApiParam(value = "账号")
   @ApiModelProperty(value = "账号")
    private String account;
    /**
     * 动作
     */
    @ApiParam(value = "动作")
   @ApiModelProperty(value = "动作")
    private String action;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getOptime() {
        return optime;
    }

    public void setOptime(Date optime) {
        this.optime = optime;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysLoginfo{" +
        ", id=" + id +
        ", optime=" + optime +
        ", account=" + account +
        ", action=" + action +
        "}";
    }
}
