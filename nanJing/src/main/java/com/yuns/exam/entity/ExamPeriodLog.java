package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 学时日志表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_period_log")
public class ExamPeriodLog extends Model<ExamPeriodLog> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
   @ApiModelProperty(value = "会员ID")
    private Integer MemberID;
    /**
     * 时间
     */
    @ApiParam(value = "时间")
   @ApiModelProperty(value = "时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date pDate;
    /**
     * 学时时长
     */
    @ApiParam(value = "学时时长")
   @ApiModelProperty(value = "学时时长")
    private Integer pLong;
    /**
     * 法规
     */
    @ApiParam(value = "法规")
   @ApiModelProperty(value = "法规")
    private String law;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer MemberID) {
        this.MemberID = MemberID;
    }

    public Date getpDate() {
        return pDate;
    }

    public void setpDate(Date pDate) {
        this.pDate = pDate;
    }

    public Integer getpLong() {
        return pLong;
    }

    public void setpLong(Integer pLong) {
        this.pLong = pLong;
    }

    public String getLaw() {
        return law;
    }

    public void setLaw(String law) {
        this.law = law;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamPeriodLog{" +
        ", id=" + id +
        ", MemberID=" + MemberID +
        ", pDate=" + pDate +
        ", pLong=" + pLong +
        ", law=" + law +
        "}";
    }
}
