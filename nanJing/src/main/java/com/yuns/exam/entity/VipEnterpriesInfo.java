package com.yuns.exam.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 企业信息表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("vip_enterpries_info")
public class VipEnterpriesInfo extends Model<VipEnterpriesInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 企业代码
     */
    @ApiParam(value = "企业代码")
   @ApiModelProperty(value = "企业代码")
    private String ECode;
    /**
     * 统一社会信用代码
     */
    @ApiParam(value = "统一社会信用代码")
   @ApiModelProperty(value = "统一社会信用代码")
    private String UniCode;
    /**
     * 企业类别
     */
    @ApiParam(value = "企业类别")
   @ApiModelProperty(value = "企业类别")
    private String EType;
    /**
     * 名称
     */
    @ApiParam(value = "名称")
   @ApiModelProperty(value = "名称")
    private String EName;
    /**
     * 地址
     */
    @ApiParam(value = "地址")
   @ApiModelProperty(value = "地址")
    private String Addr;
    /**
     * 法人
     */
    @ApiParam(value = "法人")
   @ApiModelProperty(value = "法人")
    private String corporation;
    /**
     * 区县
     */
    @ApiParam(value = "区县")
   @ApiModelProperty(value = "区县")
    private String District;
    /**
     * 法定代表人身份证
     */
    @ApiParam(value = "法定代表人身份证")
   @ApiModelProperty(value = "法定代表人身份证")
    @TableField("CID")
    private String cid;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getECode() {
        return ECode;
    }

    public void setECode(String ECode) {
        this.ECode = ECode;
    }

    public String getUniCode() {
        return UniCode;
    }

    public void setUniCode(String UniCode) {
        this.UniCode = UniCode;
    }

    public String getEType() {
        return EType;
    }

    public void setEType(String EType) {
        this.EType = EType;
    }

    public String getEName() {
        return EName;
    }

    public void setEName(String EName) {
        this.EName = EName;
    }

    public String getAddr() {
        return Addr;
    }

    public void setAddr(String Addr) {
        this.Addr = Addr;
    }

    public String getCorporation() {
        return corporation;
    }

    public void setCorporation(String corporation) {
        this.corporation = corporation;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String District) {
        this.District = District;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "VipEnterpriesInfo{" +
        ", id=" + id +
        ", ECode=" + ECode +
        ", UniCode=" + UniCode +
        ", EType=" + EType +
        ", EName=" + EName +
        ", Addr=" + Addr +
        ", corporation=" + corporation +
        ", District=" + District +
        ", cid=" + cid +
        "}";
    }
}
