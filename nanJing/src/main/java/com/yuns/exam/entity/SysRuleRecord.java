package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 违章/要求信息表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("sys_rule_record")
public class SysRuleRecord extends Model<SysRuleRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 分类
     */
    @ApiParam(value = "分类")
   @ApiModelProperty(value = "分类")
    private String RType;
    /**
     * 日期
     */
    @ApiParam(value = "日期")
   @ApiModelProperty(value = "日期")
    private Date RDate;
    /**
     * 处理期限
     */
    @ApiParam(value = "处理期限")
   @ApiModelProperty(value = "处理期限")
    private Date EDate;
    /**
     * 企业
     */
    @ApiParam(value = "企业")
   @ApiModelProperty(value = "企业")
    @TableField("EID")
    private Integer eid;
    /**
     * 事件
     */
    @ApiParam(value = "事件")
   @ApiModelProperty(value = "事件")
    private String event;
    /**
     * 责任人
     */
    @ApiParam(value = "责任人")
   @ApiModelProperty(value = "责任人")
    private String liable;
    /**
     * 责任人电话
     */
    @ApiParam(value = "责任人电话")
   @ApiModelProperty(value = "责任人电话")
    private String Phone;
    /**
     * 是否需要学习
     */
    @ApiParam(value = "是否需要学习")
   @ApiModelProperty(value = "是否需要学习")
    private Integer IsLean;
    /**
     * 是否需要考试
     */
    @ApiParam(value = "是否需要考试")
   @ApiModelProperty(value = "是否需要考试")
    private Integer IsExam;
    /**
     * 备注
     */
    @ApiParam(value = "备注")
   @ApiModelProperty(value = "备注")
    private String intro;
    /**
     * 罚款
     */
    @ApiParam(value = "罚款")
   @ApiModelProperty(value = "罚款")
    private String fine;
    /**
     * 实际罚款
     */
    @ApiParam(value = "实际罚款")
   @ApiModelProperty(value = "实际罚款")
    private String Fee;
    /**
     * 试卷ID
     */
    @ApiParam(value = "试卷ID")
   @ApiModelProperty(value = "试卷ID")
    private Integer ExaminationInfoID;
    /**
     * 行政处罚决定书号
     */
    @ApiParam(value = "行政处罚决定书号")
   @ApiModelProperty(value = "行政处罚决定书号")
    private String BookCode;
    /**
     * 案件名称
     */
    @ApiParam(value = "案件名称")
   @ApiModelProperty(value = "案件名称")
    private String CaseName;
    /**
     * 处罚事由
     */
    @ApiParam(value = "处罚事由")
   @ApiModelProperty(value = "处罚事由")
    private String Reason;
    /**
     * 处罚种类
     */
    @ApiParam(value = "处罚种类")
   @ApiModelProperty(value = "处罚种类")
    private String RuleClass;
    /**
     * 处罚依据
     */
    @ApiParam(value = "处罚依据")
   @ApiModelProperty(value = "处罚依据")
    private String according;
    /**
     * 处罚结果
     */
    @ApiParam(value = "处罚结果")
   @ApiModelProperty(value = "处罚结果")
    private String RResult;
    /**
     * 处罚截止期
     */
    @ApiParam(value = "处罚截止期")
   @ApiModelProperty(value = "处罚截止期")
    private Date EndDate;
    /**
     * 处罚决定日期
     */
    @ApiParam(value = "处罚决定日期")
   @ApiModelProperty(value = "处罚决定日期")
    private Date DoDate;
    private Date SendDate;
    private String RDep;
    private String LocCode;
    private String RProc;
    private String RCode;
    private String InfoScope;
    private String ProDegree;
    private Date PubEndDate;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRType() {
        return RType;
    }

    public void setRType(String RType) {
        this.RType = RType;
    }

    public Date getRDate() {
        return RDate;
    }

    public void setRDate(Date RDate) {
        this.RDate = RDate;
    }

    public Date getEDate() {
        return EDate;
    }

    public void setEDate(Date EDate) {
        this.EDate = EDate;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getLiable() {
        return liable;
    }

    public void setLiable(String liable) {
        this.liable = liable;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public Integer getIsLean() {
        return IsLean;
    }

    public void setIsLean(Integer IsLean) {
        this.IsLean = IsLean;
    }

    public Integer getIsExam() {
        return IsExam;
    }

    public void setIsExam(Integer IsExam) {
        this.IsExam = IsExam;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getFine() {
        return fine;
    }

    public void setFine(String fine) {
        this.fine = fine;
    }

    public String getFee() {
        return Fee;
    }

    public void setFee(String Fee) {
        this.Fee = Fee;
    }

    public Integer getExaminationInfoID() {
        return ExaminationInfoID;
    }

    public void setExaminationInfoID(Integer ExaminationInfoID) {
        this.ExaminationInfoID = ExaminationInfoID;
    }

    public String getBookCode() {
        return BookCode;
    }

    public void setBookCode(String BookCode) {
        this.BookCode = BookCode;
    }

    public String getCaseName() {
        return CaseName;
    }

    public void setCaseName(String CaseName) {
        this.CaseName = CaseName;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String Reason) {
        this.Reason = Reason;
    }

    public String getRuleClass() {
        return RuleClass;
    }

    public void setRuleClass(String RuleClass) {
        this.RuleClass = RuleClass;
    }

    public String getAccording() {
        return according;
    }

    public void setAccording(String according) {
        this.according = according;
    }

    public String getRResult() {
        return RResult;
    }

    public void setRResult(String RResult) {
        this.RResult = RResult;
    }

    public Date getEndDate() {
        return EndDate;
    }

    public void setEndDate(Date EndDate) {
        this.EndDate = EndDate;
    }

    public Date getDoDate() {
        return DoDate;
    }

    public void setDoDate(Date DoDate) {
        this.DoDate = DoDate;
    }

    public Date getSendDate() {
        return SendDate;
    }

    public void setSendDate(Date SendDate) {
        this.SendDate = SendDate;
    }

    public String getRDep() {
        return RDep;
    }

    public void setRDep(String RDep) {
        this.RDep = RDep;
    }

    public String getLocCode() {
        return LocCode;
    }

    public void setLocCode(String LocCode) {
        this.LocCode = LocCode;
    }

    public String getRProc() {
        return RProc;
    }

    public void setRProc(String RProc) {
        this.RProc = RProc;
    }

    public String getRCode() {
        return RCode;
    }

    public void setRCode(String RCode) {
        this.RCode = RCode;
    }

    public String getInfoScope() {
        return InfoScope;
    }

    public void setInfoScope(String InfoScope) {
        this.InfoScope = InfoScope;
    }

    public String getProDegree() {
        return ProDegree;
    }

    public void setProDegree(String ProDegree) {
        this.ProDegree = ProDegree;
    }

    public Date getPubEndDate() {
        return PubEndDate;
    }

    public void setPubEndDate(Date PubEndDate) {
        this.PubEndDate = PubEndDate;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysRuleRecord{" +
        ", id=" + id +
        ", RType=" + RType +
        ", RDate=" + RDate +
        ", EDate=" + EDate +
        ", eid=" + eid +
        ", event=" + event +
        ", liable=" + liable +
        ", Phone=" + Phone +
        ", IsLean=" + IsLean +
        ", IsExam=" + IsExam +
        ", intro=" + intro +
        ", fine=" + fine +
        ", Fee=" + Fee +
        ", ExaminationInfoID=" + ExaminationInfoID +
        ", BookCode=" + BookCode +
        ", CaseName=" + CaseName +
        ", Reason=" + Reason +
        ", RuleClass=" + RuleClass +
        ", according=" + according +
        ", RResult=" + RResult +
        ", EndDate=" + EndDate +
        ", DoDate=" + DoDate +
        ", SendDate=" + SendDate +
        ", RDep=" + RDep +
        ", LocCode=" + LocCode +
        ", RProc=" + RProc +
        ", RCode=" + RCode +
        ", InfoScope=" + InfoScope +
        ", ProDegree=" + ProDegree +
        ", PubEndDate=" + PubEndDate +
        "}";
    }
}
