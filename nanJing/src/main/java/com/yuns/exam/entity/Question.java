package com.yuns.exam.entity;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_question")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Question extends Model<Question> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiParam(value = "主键")
   @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 题目描述
     */
    @ApiParam(value = "题目描述")
   @ApiModelProperty(value = "题目描述")
    @TableField("question_title")
    private String questionTitle;
    /**
     * 题目类型（0：单选题 1多选题 2 判断题 3 简答题）
     */
    @ApiParam(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
   @ApiModelProperty(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
    @TableField("question_type_sel")
    private Integer questionTypeSel;
    /**
     * 题目类型  0：岗位职责   1：问病荐药  2：综合练习
     */
    @ApiParam(value = "题目类型  0：岗位职责   1：问病荐药  2：综合练习")
   @ApiModelProperty(value = "题目类型  0：岗位职责   1：问病荐药  2：综合练习")
    @TableField("question_type")
    private Integer questionType;

    @ApiParam(value = "题目分数")
    @ApiModelProperty(value = "题目分数")
    @TableField("question_score")
    private Integer questionScore;

    /**
     * 正确答案选项 （A、B、C、D）
     */
    @ApiParam(value = "正确答案选项 （A、B、C、D）")
   @ApiModelProperty(value = "正确答案选项 （A、B、C、D）")
    @TableField("question_answer")
    private String questionAnswer;
    /**
     * 简答题正确答案
     */
    @ApiParam(value = "简答题正确答案")
   @ApiModelProperty(value = "简答题正确答案")
    @TableField("question_answer_reply")
    private String questionAnswerReply;
    /**
     * 题目解析
     */
    @ApiParam(value = "题目解析")
   @ApiModelProperty(value = "题目解析")
    private String analysis;
    /**
     * 图片url
     */
    @ApiParam(value = "图片url")
   @ApiModelProperty(value = "图片url")
    @TableField("image_url")
    private String imageUrl;
    /**
     * 图片原名称
     */
    @ApiParam(value = "图片原名称")
   @ApiModelProperty(value = "图片原名称")
    @TableField("image_name")
    private String imageName;
    /**
     * 解析图片url
     */
    @ApiParam(value = "解析图片url")
   @ApiModelProperty(value = "解析图片url")
    @TableField("analysis_img_url")
    private String analysisImgUrl;
    /**
     * 解析图片原名称
     */
    @ApiParam(value = "解析图片原名称")
   @ApiModelProperty(value = "解析图片原名称")
    @TableField("analysis_img_name")
    private String analysisImgName;
    @TableField("create_user")
    private Long createUser;
    @TableField("create_dept")
    private Long createDept;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @TableField("update_user")
    private Long updateUser;
    /**
     * 修改时间
     */
    @ApiParam(value = "修改时间")
    @ApiModelProperty(value = "修改时间")
    @TableField("update_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /**
     * 状态
     */
    @ApiParam(value = "状态")
   @ApiModelProperty(value = "状态")
    private Integer status;
    /**
     * 是否删除
     */
    @ApiParam(value = "是否删除")
   @ApiModelProperty(value = "是否删除")
    @TableField("is_deleted")
    private Integer isDeleted;

//    @ApiParam(value = "考试id")
//    @ApiModelProperty(value = "考试id")
//    @TableField("is_deleted")
//    private String exam_id;

    public Integer getQuestionNum() {
        return questionNum;
    }

    public void setQuestionNum(Integer questionNum) {
        this.questionNum = questionNum;
    }

    @ApiParam(value = "题目数量")
    @ApiModelProperty(value = "题目数量")
    @Transient
    private Integer questionNum;

    public List<Long> getExamIds() {
        return examIds;
    }

    public void setExamIds(List<Long> examIds) {
        this.examIds = examIds;
    }

    @ApiParam(value = "题库ids")
    @ApiModelProperty(value = "题库ids")
    private List<Long> examIds;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public Integer getQuestionTypeSel() {
        return questionTypeSel;
    }

    public void setQuestionTypeSel(Integer questionTypeSel) {
        this.questionTypeSel = questionTypeSel;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public String getQuestionAnswer() {
        return questionAnswer;
    }

    public void setQuestionAnswer(String questionAnswer) {
        this.questionAnswer = questionAnswer;
    }

    public String getQuestionAnswerReply() {
        return questionAnswerReply;
    }

    public void setQuestionAnswerReply(String questionAnswerReply) {
        this.questionAnswerReply = questionAnswerReply;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getAnalysisImgUrl() {
        return analysisImgUrl;
    }

    public void setAnalysisImgUrl(String analysisImgUrl) {
        this.analysisImgUrl = analysisImgUrl;
    }

    public String getAnalysisImgName() {
        return analysisImgName;
    }

    public void setAnalysisImgName(String analysisImgName) {
        this.analysisImgName = analysisImgName;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getCreateDept() {
        return createDept;
    }

    public void setCreateDept(Long createDept) {
        this.createDept = createDept;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Question{" +
        ", id=" + id +
        ", questionTitle=" + questionTitle +
        ", questionTypeSel=" + questionTypeSel +
        ", questionType=" + questionType +
        ", questionAnswer=" + questionAnswer +
        ", questionAnswerReply=" + questionAnswerReply +
        ", analysis=" + analysis +
        ", imageUrl=" + imageUrl +
        ", imageName=" + imageName +
        ", analysisImgUrl=" + analysisImgUrl +
        ", analysisImgName=" + analysisImgName +
        ", createUser=" + createUser +
        ", createDept=" + createDept +
        ", createTime=" + createTime +
        ", updateUser=" + updateUser +
        ", updateTime=" + updateTime +
        ", status=" + status +
        ", isDeleted=" + isDeleted +
        "}";
    }

    public Integer getQuestionScore() {
        return questionScore;
    }

    public void setQuestionScore(Integer questionScore) {
        this.questionScore = questionScore;
    }
}
