package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 试题纠错表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_title_err")
public class ExamTitleErr extends Model<ExamTitleErr> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
   @ApiModelProperty(value = "会员ID")
    private Integer MemberID;
    /**
     * 时间
     */
    @ApiParam(value = "时间")
   @ApiModelProperty(value = "时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date eDate;
    /**
     * 试卷试题表ID
     */
    @ApiParam(value = "试卷试题表ID")
   @ApiModelProperty(value = "试卷试题表ID")
    @TableField("ETID")
    private Integer etid;
    /**
     * 说明
     */
    @ApiParam(value = "说明")
   @ApiModelProperty(value = "说明")
    private String intro;
    /**
     * 处理老师
     */
    @ApiParam(value = "处理老师")
   @ApiModelProperty(value = "处理老师")
    private Integer MemberTecher;
    /**
     * 处理时间
     */
    @ApiParam(value = "处理时间")
   @ApiModelProperty(value = "处理时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date doDate;
    /**
     * 处理说明
     */
    @ApiParam(value = "处理说明")
   @ApiModelProperty(value = "处理说明")
    private String doIntro;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer MemberID) {
        this.MemberID = MemberID;
    }

    public Date geteDate() {
        return eDate;
    }

    public void seteDate(Date eDate) {
        this.eDate = eDate;
    }

    public Integer getEtid() {
        return etid;
    }

    public void setEtid(Integer etid) {
        this.etid = etid;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public Integer getMemberTecher() {
        return MemberTecher;
    }

    public void setMemberTecher(Integer MemberTecher) {
        this.MemberTecher = MemberTecher;
    }

    public Date getDoDate() {
        return doDate;
    }

    public void setDoDate(Date doDate) {
        this.doDate = doDate;
    }

    public String getDoIntro() {
        return doIntro;
    }

    public void setDoIntro(String doIntro) {
        this.doIntro = doIntro;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamTitleErr{" +
        ", id=" + id +
        ", MemberID=" + MemberID +
        ", eDate=" + eDate +
        ", etid=" + etid +
        ", intro=" + intro +
        ", MemberTecher=" + MemberTecher +
        ", doDate=" + doDate +
        ", doIntro=" + doIntro +
        "}";
    }
}
