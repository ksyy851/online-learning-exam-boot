package com.yuns.exam.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @description: TODO: 考试实体
 * @author: zhansang
 * @create: 2020-11-10 18:32
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ExamEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @ApiParam(value = "主键")
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 试卷编号
     */
    @ApiParam(value = "试卷编号")
    @ApiModelProperty(value = "试卷编号")
    @TableField("exam_code")
    private String examCode;
    /**
     * 试卷名称
     */
    @ApiParam(value = "试卷名称")
    @ApiModelProperty(value = "试卷名称")
    @TableField("exam_name")
    private String examName;

    /**
     * 题目备注
     */
    @ApiParam(value = "题目备注")
    @ApiModelProperty(value = "题目备注")
    @TableField("exam_remark")
    private String examRemark;

    /**
     * 考试开始时间
     */
    @ApiParam(value = "考试开始时间")
    @ApiModelProperty(value = "考试开始时间")
    @TableField("start_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date startTime;
    /**
     * 考试结束时间
     */
    @ApiParam(value = "考试结束时间")
    @ApiModelProperty(value = "考试结束时间")
    @TableField("end_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date endTime;
    /**
     * 考试时长（单位：分钟）
     */
    @ApiParam(value = "考试时长（单位：分钟）")
    @ApiModelProperty(value = "考试时长（单位：分钟）")
    @TableField("exam_time")
    private String examTime;
    /**
     * 试卷口令
     */
    @ApiParam(value = "试卷口令")
    @ApiModelProperty(value = "试卷口令")
    private String password;
    /**
     * 试卷总分
     */
    @ApiParam(value = "试卷总分")
    @ApiModelProperty(value = "试卷总分")
    private String grade;

}
