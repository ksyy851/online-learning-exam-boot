package com.yuns.exam.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 后台信息表(环保信息中心、宣教中心、市局主要处室人员、 各区环保局管理人员、题库管理人员、技术公司平台维护人员)
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("sys_supp_info")
public class SysSuppInfo extends Model<SysSuppInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 姓名
     */
    @ApiParam(value = "姓名")
   @ApiModelProperty(value = "姓名")
    private String Name;
    /**
     * 简历
     */
    @ApiParam(value = "简历")
   @ApiModelProperty(value = "简历")
    private String Resume;
    /**
     * 业务介绍
     */
    @ApiParam(value = "业务介绍")
   @ApiModelProperty(value = "业务介绍")
    private String Intro;
    /**
     * 照片文件
     */
    @ApiParam(value = "照片文件")
   @ApiModelProperty(value = "照片文件")
    private String AFile;
    /**
     * 后台角色
     */
    @ApiParam(value = "后台角色")
   @ApiModelProperty(value = "后台角色")
    private Integer RoleID;
    /**
     * 后台人员类别
     */
    @ApiParam(value = "后台人员类别")
   @ApiModelProperty(value = "后台人员类别")
    private String SType;
    /**
     * 区县
     */
    @ApiParam(value = "区县")
   @ApiModelProperty(value = "区县")
    private String District;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getResume() {
        return Resume;
    }

    public void setResume(String Resume) {
        this.Resume = Resume;
    }

    public String getIntro() {
        return Intro;
    }

    public void setIntro(String Intro) {
        this.Intro = Intro;
    }

    public String getAFile() {
        return AFile;
    }

    public void setAFile(String AFile) {
        this.AFile = AFile;
    }

    public Integer getRoleID() {
        return RoleID;
    }

    public void setRoleID(Integer RoleID) {
        this.RoleID = RoleID;
    }

    public String getSType() {
        return SType;
    }

    public void setSType(String SType) {
        this.SType = SType;
    }

    public String getDistrict() {
        return District;
    }

    public void setDistrict(String District) {
        this.District = District;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysSuppInfo{" +
        ", id=" + id +
        ", Name=" + Name +
        ", Resume=" + Resume +
        ", Intro=" + Intro +
        ", AFile=" + AFile +
        ", RoleID=" + RoleID +
        ", SType=" + SType +
        ", District=" + District +
        "}";
    }
}
