package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Data
@Builder
@TableName("gt_exam_question")
@AllArgsConstructor
@NoArgsConstructor
public class ExamQuestion extends Model<ExamQuestion> {

    private static final long serialVersionUID = 1L;

    /**
     * 题目来源 0：试题创建 1（题库） 通过题目生成
     */
    @ApiParam(value = "题目来源 0：试题创建 1（题库） 通过题目生成")
   @ApiModelProperty(value = "题目来源 0：试题创建 1（题库） 通过题目生成")
    @TableField("question_source")
    private Integer questionSource;
    @TableId(value = "id", type = IdType.AUTO)
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;
    /**
     * 考试id
     */
    @ApiParam(value = "考试id")
   @ApiModelProperty(value = "考试id")
    @TableField("exam_id")
    private String examId;
    @TableField("question_id")
    private String questionId;
    /**
     * 题目分数
     */
    @ApiParam(value = "题目分数")
   @ApiModelProperty(value = "题目分数")
    private Integer grade;
    /**
     * 题目描述
     */
    @ApiParam(value = "题目描述")
   @ApiModelProperty(value = "题目描述")
    @TableField("question_title")
    private String questionTitle;
    /**
     * 题目类型  0：类型一   1：类型二
     */
    @ApiParam(value = "题目类型  0：类型一   1：类型二")
   @ApiModelProperty(value = "题目类型  0：类型一   1：类型二")
    @TableField("question_type")
    private Integer questionType;
    /**
     * 正确答案选项 （A、B、C、D）
     */
    @ApiParam(value = "正确答案选项 （A、B、C、D）")
   @ApiModelProperty(value = "正确答案选项 （A、B、C、D）")
    @TableField("question_answer")
    private String questionAnswer;
    /**
     * 正确答案（简答题）
     */
    @ApiParam(value = "正确答案（简答题）")
   @ApiModelProperty(value = "正确答案（简答题）")
    @TableField("question_answer_reply")
    private String questionAnswerReply;
    /**
     * 题目解析
     */
    @ApiParam(value = "题目解析")
   @ApiModelProperty(value = "题目解析")
    private String analysis;
    /**
     * 图片url
     */
    @ApiParam(value = "图片url")
   @ApiModelProperty(value = "图片url")
    @TableField("image_url")
    private String imageUrl;
    /**
     * 图片原名称
     */
    @ApiParam(value = "图片原名称")
   @ApiModelProperty(value = "图片原名称")
    @TableField("image_name")
    private String imageName;
    /**
     * 解析图片url
     */
    @ApiParam(value = "解析图片url")
   @ApiModelProperty(value = "解析图片url")
    @TableField("analysis_img_url")
    private String analysisImgUrl;
    /**
     * 解析图片原名称
     */
    @ApiParam(value = "解析图片原名称")
   @ApiModelProperty(value = "解析图片原名称")
    @TableField("analysis_img_name")
    private String analysisImgName;

    @TableField("create_user")
    private Long createUser;

    @TableField("create_dept")
    private Long createDept;

    @TableField("create_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @TableField("update_user")
    private Long updateUser;

    @TableField("update_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @TableField("status")
    private Integer status;

    @TableField("is_deleted")
    private Integer isDeleted;


    public Integer getQuestionSource() {
        return questionSource;
    }

    public void setQuestionSource(Integer questionSource) {
        this.questionSource = questionSource;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public Integer getQuestionType() {
        return questionType;
    }

    public void setQuestionType(Integer questionType) {
        this.questionType = questionType;
    }

    public String getQuestionAnswer() {
        return questionAnswer;
    }

    public void setQuestionAnswer(String questionAnswer) {
        this.questionAnswer = questionAnswer;
    }

    public String getQuestionAnswerReply() {
        return questionAnswerReply;
    }

    public void setQuestionAnswerReply(String questionAnswerReply) {
        this.questionAnswerReply = questionAnswerReply;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getAnalysisImgUrl() {
        return analysisImgUrl;
    }

    public void setAnalysisImgUrl(String analysisImgUrl) {
        this.analysisImgUrl = analysisImgUrl;
    }

    public String getAnalysisImgName() {
        return analysisImgName;
    }

    public void setAnalysisImgName(String analysisImgName) {
        this.analysisImgName = analysisImgName;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getCreateDept() {
        return createDept;
    }

    public void setCreateDept(Long createDept) {
        this.createDept = createDept;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamQuestion{" +
        ", questionSource=" + questionSource +
        ", id=" + id +
        ", examId=" + examId +
        ", questionId=" + questionId +
        ", grade=" + grade +
        ", questionTitle=" + questionTitle +
        ", questionType=" + questionType +
        ", questionAnswer=" + questionAnswer +
        ", questionAnswerReply=" + questionAnswerReply +
        ", analysis=" + analysis +
        ", imageUrl=" + imageUrl +
        ", imageName=" + imageName +
        ", analysisImgUrl=" + analysisImgUrl +
        ", analysisImgName=" + analysisImgName +
        ", createUser=" + createUser +
        ", createDept=" + createDept +
        ", createTime=" + createTime +
        ", updateUser=" + updateUser +
        ", updateTime=" + updateTime +
        ", status=" + status +
        ", isDeleted=" + isDeleted +
        "}";
    }
}
