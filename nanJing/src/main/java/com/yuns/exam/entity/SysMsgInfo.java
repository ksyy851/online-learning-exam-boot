package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 消息信息表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("sys_msg_info")
public class SysMsgInfo extends Model<SysMsgInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 消息标题
     */
    @ApiParam(value = "消息标题")
   @ApiModelProperty(value = "消息标题")
    private String Name;
    /**
     * 消息体
     */
    @ApiParam(value = "消息体")
   @ApiModelProperty(value = "消息体")
    private String Msg;
    /**
     * 推送类别
     */
    @ApiParam(value = "推送类别")
   @ApiModelProperty(value = "推送类别")
    private String mType;
    /**
     * 会员ID
     */
    @ApiParam(value = "会员ID")
   @ApiModelProperty(value = "会员ID")
    private Integer MemberID;
    /**
     * 消息方式
     */
    @ApiParam(value = "消息方式")
   @ApiModelProperty(value = "消息方式")
    private String sType;
    /**
     * 开始时间
     */
    @ApiParam(value = "开始时间")
   @ApiModelProperty(value = "开始时间")
    private Date BegTime;
    /**
     * 发送状态
     */
    @ApiParam(value = "发送状态")
   @ApiModelProperty(value = "发送状态")
    private String SendState;
    /**
     * 关联ID
     */
    @ApiParam(value = "关联ID")
   @ApiModelProperty(value = "关联ID")
    private Integer LinkID;
    /**
     * URL
     */
    @ApiParam(value = "URL")
   @ApiModelProperty(value = "URL")
    @TableField("URL")
    private String url;
    /**
     * 结束时间
     */
    @ApiParam(value = "结束时间")
   @ApiModelProperty(value = "结束时间")
    private Date EndTime;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
   @ApiModelProperty(value = "创建时间")
    private Date CreateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String Msg) {
        this.Msg = Msg;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public Integer getMemberID() {
        return MemberID;
    }

    public void setMemberID(Integer MemberID) {
        this.MemberID = MemberID;
    }

    public String getsType() {
        return sType;
    }

    public void setsType(String sType) {
        this.sType = sType;
    }

    public Date getBegTime() {
        return BegTime;
    }

    public void setBegTime(Date BegTime) {
        this.BegTime = BegTime;
    }

    public String getSendState() {
        return SendState;
    }

    public void setSendState(String SendState) {
        this.SendState = SendState;
    }

    public Integer getLinkID() {
        return LinkID;
    }

    public void setLinkID(Integer LinkID) {
        this.LinkID = LinkID;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getEndTime() {
        return EndTime;
    }

    public void setEndTime(Date EndTime) {
        this.EndTime = EndTime;
    }

    public Date getCreateTime() {
        return CreateTime;
    }

    public void setCreateTime(Date CreateTime) {
        this.CreateTime = CreateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysMsgInfo{" +
        ", id=" + id +
        ", Name=" + Name +
        ", Msg=" + Msg +
        ", mType=" + mType +
        ", MemberID=" + MemberID +
        ", sType=" + sType +
        ", BegTime=" + BegTime +
        ", SendState=" + SendState +
        ", LinkID=" + LinkID +
        ", url=" + url +
        ", EndTime=" + EndTime +
        ", CreateTime=" + CreateTime +
        "}";
    }
}
