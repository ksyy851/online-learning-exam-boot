package com.yuns.exam.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.ToStringSerializer;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 敏感词表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_badword")
public class Badword extends Model<Badword> {

    private static final long serialVersionUID = 1L;

    @JSONField(serializeUsing = ToStringSerializer.class)
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    private String NotWord;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotWord() {
        return NotWord;
    }

    public void setNotWord(String NotWord) {
        this.NotWord = NotWord;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Badword{" +
        ", id=" + id +
        ", NotWord=" + NotWord +
        "}";
    }
}
