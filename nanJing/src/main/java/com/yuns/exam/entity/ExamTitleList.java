package com.yuns.exam.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 试题选项表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_title_list")
public class ExamTitleList extends Model<ExamTitleList> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 试题ID
     */
    @ApiParam(value = "试题ID")
   @ApiModelProperty(value = "试题ID")
    private Integer TitleID;
    /**
     * 选项标识
     */
    @ApiParam(value = "选项标识")
   @ApiModelProperty(value = "选项标识")
    private String Single;
    /**
     * 显示顺序
     */
    @ApiParam(value = "显示顺序")
   @ApiModelProperty(value = "显示顺序")
    private Integer orderID;
    /**
     * 显示内容
     */
    @ApiParam(value = "显示内容")
   @ApiModelProperty(value = "显示内容")
    private String sList;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTitleID() {
        return TitleID;
    }

    public void setTitleID(Integer TitleID) {
        this.TitleID = TitleID;
    }

    public String getSingle() {
        return Single;
    }

    public void setSingle(String Single) {
        this.Single = Single;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public String getsList() {
        return sList;
    }

    public void setsList(String sList) {
        this.sList = sList;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamTitleList{" +
        ", id=" + id +
        ", TitleID=" + TitleID +
        ", Single=" + Single +
        ", orderID=" + orderID +
        ", sList=" + sList +
        "}";
    }
}
