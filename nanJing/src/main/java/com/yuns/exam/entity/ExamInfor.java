package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 试卷定义表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_infor")
public class ExamInfor extends Model<ExamInfor> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 试卷主题
     */
    @ApiParam(value = "试卷主题")
   @ApiModelProperty(value = "试卷主题")
    private String Name;
    /**
     * 试卷信息分类
     */
    @ApiParam(value = "试卷信息分类")
   @ApiModelProperty(value = "试卷信息分类")
    private String InfoType;
    /**
     * 试卷说明
     */
    @ApiParam(value = "试卷说明")
   @ApiModelProperty(value = "试卷说明")
    private String AFile;
    /**
     * 定义时间
     */
    @ApiParam(value = "定义时间")
   @ApiModelProperty(value = "定义时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date DoDate;
    /**
     * 定制人
     */
    @ApiParam(value = "定制人")
   @ApiModelProperty(value = "定制人")
    private Integer DoMan;
    /**
     * 制定说明
     */
    @ApiParam(value = "制定说明")
   @ApiModelProperty(value = "制定说明")
    private String Intro;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getInfoType() {
        return InfoType;
    }

    public void setInfoType(String InfoType) {
        this.InfoType = InfoType;
    }

    public String getAFile() {
        return AFile;
    }

    public void setAFile(String AFile) {
        this.AFile = AFile;
    }

    public Date getDoDate() {
        return DoDate;
    }

    public void setDoDate(Date DoDate) {
        this.DoDate = DoDate;
    }

    public Integer getDoMan() {
        return DoMan;
    }

    public void setDoMan(Integer DoMan) {
        this.DoMan = DoMan;
    }

    public String getIntro() {
        return Intro;
    }

    public void setIntro(String Intro) {
        this.Intro = Intro;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamInfor{" +
        ", id=" + id +
        ", Name=" + Name +
        ", InfoType=" + InfoType +
        ", AFile=" + AFile +
        ", DoDate=" + DoDate +
        ", DoMan=" + DoMan +
        ", Intro=" + Intro +
        "}";
    }
}
