package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 资讯内容
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("info_content_e")
public class InfoContentE extends Model<InfoContentE> {

    private static final long serialVersionUID = 1L;

    /**
     * 对象ID
     */
    @ApiParam(value = "对象ID")
   @ApiModelProperty(value = "对象ID")
    @TableId(value = "dxid", type = IdType.AUTO)
    private String dxid;
    /**
     * 类型对象ID
     */
    @ApiParam(value = "类型对象ID")
   @ApiModelProperty(value = "类型对象ID")
    private String lxdxid;
    /**
     * 来源
     */
    @ApiParam(value = "来源")
   @ApiModelProperty(value = "来源")
    private String ly;
    /**
     * 标题
     */
    @ApiParam(value = "标题")
   @ApiModelProperty(value = "标题")
    private String bt;
    /**
     * 发布人
     */
    @ApiParam(value = "发布人")
   @ApiModelProperty(value = "发布人")
    private String fbr;
    /**
     * 缩略图
     */
    @ApiParam(value = "缩略图")
   @ApiModelProperty(value = "缩略图")
    private String tp;
    /**
     * 内容
     */
    @ApiParam(value = "内容")
   @ApiModelProperty(value = "内容")
    private String nr;
    /**
     * 发布时间
     */
    @ApiParam(value = "发布时间")
   @ApiModelProperty(value = "发布时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xwfbsj;
    /**
     * 单位对象id
     */
    @ApiParam(value = "单位对象id")
   @ApiModelProperty(value = "单位对象id")
    private String dwdxid;
    /**
     * 0-不通过，1-通过
     */
    @ApiParam(value = "0-不通过，1-通过")
   @ApiModelProperty(value = "0-不通过，1-通过")
    private Integer sh;
    /**
     * 创建时间
     */
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiParam(value = "创建时间")
   @ApiModelProperty(value = "创建时间")
    private Date createTime;
    /**
     * 更新时间
     */
    @ApiParam(value = "更新时间")
   @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date gxsj;


    public String getDxid() {
        return dxid;
    }

    public void setDxid(String dxid) {
        this.dxid = dxid;
    }

    public String getLxdxid() {
        return lxdxid;
    }

    public void setLxdxid(String lxdxid) {
        this.lxdxid = lxdxid;
    }

    public String getLy() {
        return ly;
    }

    public void setLy(String ly) {
        this.ly = ly;
    }

    public String getBt() {
        return bt;
    }

    public void setBt(String bt) {
        this.bt = bt;
    }

    public String getFbr() {
        return fbr;
    }

    public void setFbr(String fbr) {
        this.fbr = fbr;
    }

    public String getTp() {
        return tp;
    }

    public void setTp(String tp) {
        this.tp = tp;
    }

    public String getNr() {
        return nr;
    }

    public void setNr(String nr) {
        this.nr = nr;
    }

    public Date getXwfbsj() {
        return xwfbsj;
    }

    public void setXwfbsj(Date xwfbsj) {
        this.xwfbsj = xwfbsj;
    }

    public String getDwdxid() {
        return dwdxid;
    }

    public void setDwdxid(String dwdxid) {
        this.dwdxid = dwdxid;
    }

    public Integer getSh() {
        return sh;
    }

    public void setSh(Integer sh) {
        this.sh = sh;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date cjsj) {
        this.createTime = cjsj;
    }

    public Date getGxsj() {
        return gxsj;
    }

    public void setGxsj(Date gxsj) {
        this.gxsj = gxsj;
    }

    @Override
    protected Serializable pkVal() {
        return this.dxid;
    }

    @Override
    public String toString() {
        return "InfoContentE{" +
        ", dxid=" + dxid +
        ", lxdxid=" + lxdxid +
        ", ly=" + ly +
        ", bt=" + bt +
        ", fbr=" + fbr +
        ", tp=" + tp +
        ", nr=" + nr +
        ", xwfbsj=" + xwfbsj +
        ", dwdxid=" + dwdxid +
        ", sh=" + sh +
        ", createTime=" + createTime +
        ", gxsj=" + gxsj +
        "}";
    }
}
