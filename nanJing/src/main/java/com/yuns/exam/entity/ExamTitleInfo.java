package com.yuns.exam.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 习题试题信息表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("gt_exam_titleinfo")
public class ExamTitleInfo extends Model<ExamTitleInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 练习/考试
     */
    @ApiParam(value = "练习/考试")
   @ApiModelProperty(value = "练习/考试")
    private String Examin;
    /**
     * 法规
     */
    @ApiParam(value = "法规")
   @ApiModelProperty(value = "法规")
    private String Law;
    /**
     * 法规明细
     */
    @ApiParam(value = "法规明细")
   @ApiModelProperty(value = "法规明细")
    private String QPoint;
    /**
     * 适用范围
     */
    @ApiParam(value = "适用范围")
   @ApiModelProperty(value = "适用范围")
    private String Content;
    /**
     * 题型
     */
    @ApiParam(value = "题型")
   @ApiModelProperty(value = "题型")
    private String QType;
    /**
     * 描述
     */
    @ApiParam(value = "描述")
   @ApiModelProperty(value = "描述")
    private String Title;
    /**
     * 变更日期
     */
    @ApiParam(value = "变更日期")
   @ApiModelProperty(value = "变更日期")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date CDate;
    /**
     * 标准备用答案
     */
    @ApiParam(value = "标准备用答案")
   @ApiModelProperty(value = "标准备用答案")
    private String Solution;
    /**
     * 分析
     */
    @ApiParam(value = "分析")
   @ApiModelProperty(value = "分析")
    private String analysis;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getExamin() {
        return Examin;
    }

    public void setExamin(String Examin) {
        this.Examin = Examin;
    }

    public String getLaw() {
        return Law;
    }

    public void setLaw(String Law) {
        this.Law = Law;
    }

    public String getQPoint() {
        return QPoint;
    }

    public void setQPoint(String QPoint) {
        this.QPoint = QPoint;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String Content) {
        this.Content = Content;
    }

    public String getQType() {
        return QType;
    }

    public void setQType(String QType) {
        this.QType = QType;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public Date getCDate() {
        return CDate;
    }

    public void setCDate(Date CDate) {
        this.CDate = CDate;
    }

    public String getSolution() {
        return Solution;
    }

    public void setSolution(String Solution) {
        this.Solution = Solution;
    }

    public String getAnalysis() {
        return analysis;
    }

    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ExamTitleinfo{" +
        ", id=" + id +
        ", Examin=" + Examin +
        ", Law=" + Law +
        ", QPoint=" + QPoint +
        ", Content=" + Content +
        ", QType=" + QType +
        ", Title=" + Title +
        ", CDate=" + CDate +
        ", Solution=" + Solution +
        ", analysis=" + analysis +
        "}";
    }
}
