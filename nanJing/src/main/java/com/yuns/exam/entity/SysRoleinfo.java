package com.yuns.exam.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 后台角色表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@TableName("sys_roleinfo")
public class SysRoleinfo extends Model<SysRoleinfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 名称
     */
    @ApiParam(value = "名称")
   @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 角色
     */
    @ApiParam(value = "角色")
   @ApiModelProperty(value = "角色")
    private String Rights;
    /**
     * 导入企业
     */
    @ApiParam(value = "导入企业")
   @ApiModelProperty(value = "导入企业")
    private Integer enterpries;
    /**
     * 导入违章
     */
    @ApiParam(value = "导入违章")
   @ApiModelProperty(value = "导入违章")
    private Integer rules;
    /**
     * 录入试题
     */
    @ApiParam(value = "录入试题")
   @ApiModelProperty(value = "录入试题")
    private Integer Title;
    /**
     * 录入视频
     */
    @ApiParam(value = "录入视频")
   @ApiModelProperty(value = "录入视频")
    private Integer video;
    /**
     * 录入学习材料
     */
    @ApiParam(value = "录入学习材料")
   @ApiModelProperty(value = "录入学习材料")
    private Integer word;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRights() {
        return Rights;
    }

    public void setRights(String Rights) {
        this.Rights = Rights;
    }

    public Integer getEnterpries() {
        return enterpries;
    }

    public void setEnterpries(Integer enterpries) {
        this.enterpries = enterpries;
    }

    public Integer getRules() {
        return rules;
    }

    public void setRules(Integer rules) {
        this.rules = rules;
    }

    public Integer getTitle() {
        return Title;
    }

    public void setTitle(Integer Title) {
        this.Title = Title;
    }

    public Integer getVideo() {
        return video;
    }

    public void setVideo(Integer video) {
        this.video = video;
    }

    public Integer getWord() {
        return word;
    }

    public void setWord(Integer word) {
        this.word = word;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "SysRoleinfo{" +
        ", id=" + id +
        ", name=" + name +
        ", Rights=" + Rights +
        ", enterpries=" + enterpries +
        ", rules=" + rules +
        ", Title=" + Title +
        ", video=" + video +
        ", word=" + word +
        "}";
    }
}
