package com.yuns.exam.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 随机题目参数
 * @author: zhansang
 * @create: 2020-10-29 16:27
 * @version: 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuestionRandomDto {

    @ApiModelProperty(value = "主键")
    private Long id;
    @ApiModelProperty(value = "题目描述")
    private String questionTitle;
    @ApiModelProperty(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
    private Integer questionTypeSel;

    @ApiModelProperty(value = "题目分数")
    private Integer questionScore;
    @ApiModelProperty(value = "正确答案选项 （A、B、C、D）")
    private String questionAnswer;
    @ApiModelProperty(value = "简答题正确答案")
    private String questionAnswerReply;



    @ApiModelProperty(value = "题目解析")
    private String analysis;
    @ApiModelProperty(value = "状态")
    private Integer status;

}
