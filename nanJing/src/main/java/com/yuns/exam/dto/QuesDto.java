package com.yuns.exam.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description: TODO: 题目参数
 * @author: zhansang
 * @create: 2020-11-15 20:32
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuesDto {

    /*   * 主键
     */
    @ApiParam(value = "主键")
    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 题目描述
     */
    @ApiParam(value = "题目描述")
    @ApiModelProperty(value = "题目描述")
    @TableField("question_title")
    private String questionTitle;
    /**
     * 题目类型（0：单选题 1多选题 2 判断题 3 简答题）
     */
    @ApiParam(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
    @ApiModelProperty(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）")
    @TableField("question_type_sel")
    private Integer questionTypeSel;
    /**
     * 题目类型  0：岗位职责   1：问病荐药  2：综合练习
     */
    @ApiParam(value = "题目类型  0：岗位职责   1：问病荐药  2：综合练习")
    @ApiModelProperty(value = "题目类型  0：岗位职责   1：问病荐药  2：综合练习")
    @TableField("question_type")
    private Integer questionType;

    @ApiParam(value = "题目分数")
    @ApiModelProperty(value = "题目分数")
    @TableField("question_score")
    private Integer questionScore;

    /**
     * 正确答案选项 （A、B、C、D）
     */
    @ApiParam(value = "正确答案选项 （A、B、C、D）")
    @ApiModelProperty(value = "正确答案选项 （A、B、C、D）")
    @TableField("question_answer")
    private String questionAnswer;
    /**
     * 简答题正确答案
     */
    @ApiParam(value = "简答题正确答案")
    @ApiModelProperty(value = "简答题正确答案")
    @TableField("question_answer_reply")
    private String questionAnswerReply;
    /**
     * 题目解析
     */
    @ApiParam(value = "题目解析")
    @ApiModelProperty(value = "题目解析")
    @TableField("analysis")
    private String analysis;
    /**
     * 图片url
     */
    @ApiParam(value = "图片url")
    @ApiModelProperty(value = "图片url")
    @TableField("image_url")
    private String imageUrl;
    /**
     * 图片原名称
     */
    @ApiParam(value = "图片原名称")
    @ApiModelProperty(value = "图片原名称")
    @TableField("image_name")
    private String imageName;
    /**
     * 解析图片url
     */
    @ApiParam(value = "解析图片url")
    @ApiModelProperty(value = "解析图片url")
    @TableField("analysis_img_url")
    private String analysisImgUrl;
    /**
     * 解析图片原名称
     */
    @ApiParam(value = "解析图片原名称")
    @ApiModelProperty(value = "解析图片原名称")
    @TableField("analysis_img_name")
    private String analysisImgName;
    @TableField("create_user")
    private Long createUser;
    @TableField("create_dept")
    private Long createDept;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;
    @TableField("update_user")
    private Long updateUser;
    /**
     * 修改时间
     */
    @ApiParam(value = "修改时间")
    @ApiModelProperty(value = "修改时间")
    @TableField("update_time")
    private Date updateTime;
    /**
     * 状态
     */
    @ApiParam(value = "状态")
    @ApiModelProperty(value = "状态")
    private Integer status;
    /**
     * 是否删除
     */
    @ApiParam(value = "是否删除")
    @ApiModelProperty(value = "是否删除")
    @TableField("is_deleted")
    private Integer isDeleted;

}
