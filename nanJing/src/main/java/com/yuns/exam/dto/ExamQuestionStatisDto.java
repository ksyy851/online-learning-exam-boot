package com.yuns.exam.dto;

import com.yuns.exam.entity.Exam;
import com.yuns.exam.entity.ExamRecord;
import com.yuns.exam.vo.ExamRecordVO;
import lombok.Builder;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: TODO: 考试列表统计信息
 * @author: zhansang
 * @create: 2020-11-24 22:58
 * @version: 1.0
 **/
@Data
@Builder
public class ExamQuestionStatisDto {
    private List<ExamQuestionDto> questionDto;
    private Map<Integer,  Long> totalMap;
    private HashMap<String,Object> exam;
    private ExamRecordVO examRecord;
}
