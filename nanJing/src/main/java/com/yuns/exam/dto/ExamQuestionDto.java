package com.yuns.exam.dto;

import com.yuns.exam.entity.ExamAnswerRecord;
import com.yuns.exam.entity.ExamQuestion;
import com.yuns.exam.entity.ExamQuestionOption;
import com.yuns.exam.entity.QuestionOption;
import com.yuns.exam.vo.ExamQuestionVO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * @description: TODO: 考试题目详情
 * @author: zhansang
 * @create: 2020-11-18 19:02
 * @version: 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExamQuestionDto {

    private ExamQuestionVO examQuestionVO;
    private ExamQuestion examQuestion;
    private ExamAnswerRecord examAnswerRecord;
    private List<QuestionOption> questionOptions;

}
