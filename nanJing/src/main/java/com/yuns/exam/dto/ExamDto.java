package com.yuns.exam.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 题库实体
 * @author: zhansang
 * @create: 2020-11-15 17:28
 * @version: 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExamDto {
    @ApiModelProperty(value = "题目表")
    private String id;

}
