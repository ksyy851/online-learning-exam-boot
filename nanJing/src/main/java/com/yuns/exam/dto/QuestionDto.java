package com.yuns.exam.dto;

import com.yuns.exam.entity.Exam;
import com.yuns.exam.entity.Question;
import com.yuns.exam.entity.QuestionOption;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @description: TODO: 题目入参
 * @author: zhansang
 * @create: 2020-10-25 17:25
 * @version: 1.0
 **/
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDto {
    @ApiModelProperty(value = "题目表")
    private Question question;
    @ApiModelProperty(value = "题目选项表")
    private List<QuestionOption> questionOptionList;
    @ApiModelProperty(value = "题库")
    private List<ExamDto> examList;
}
