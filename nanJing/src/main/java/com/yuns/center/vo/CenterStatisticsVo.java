package com.yuns.center.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 个人中心 图表统计VO
 */
@Data
public class CenterStatisticsVo {

    /**
     * 企业名称
     */
    @ApiParam(value = "类型")
    @ApiModelProperty(value = "类型")
    private String name;

    /**
     *数值
     */
    @ApiParam(value = "数值")
    @ApiModelProperty(value = "数值")
    private Integer value;
}
