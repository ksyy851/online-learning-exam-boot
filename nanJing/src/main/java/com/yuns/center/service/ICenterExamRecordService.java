package com.yuns.center.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.center.entity.CenterExamRecord;
import com.yuns.center.entity.CenterIntegralRecord;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface ICenterExamRecordService extends IService<CenterExamRecord> {

    Object queryExamRecord(BasePage frontPage);

}


