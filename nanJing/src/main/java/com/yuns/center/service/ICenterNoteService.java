package com.yuns.center.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.center.entity.CenterExamTitleNote;
import com.yuns.center.entity.CenterCourseNote;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface ICenterNoteService extends IService<CenterCourseNote> {

    Object queryCourseNote(BasePage frontPage);

    Object queryExamTitleNote(BasePage frontPage);
}
