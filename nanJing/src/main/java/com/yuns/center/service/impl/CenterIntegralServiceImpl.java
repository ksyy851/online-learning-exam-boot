package com.yuns.center.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.center.entity.CenterIntegralRank;
import com.yuns.center.entity.CenterIntegralRecord;
import com.yuns.center.mapper.CenterIntegralMapper;
import com.yuns.center.service.ICenterIntegralService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CenterIntegralServiceImpl extends ServiceImpl<CenterIntegralMapper, CenterIntegralRecord> implements ICenterIntegralService {
    @Override
    public Object queryIntegralRecord(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryIntegralRecord(frontPage);
        }
        Page<CenterIntegralRecord> page = new Page<CenterIntegralRecord>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryIntegralRecord(page,frontPage));
    }

    @Override
    public Object queryIntegralRank(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryIntegralRank(frontPage);
        }
        Page<CenterIntegralRank> page = new Page<CenterIntegralRank>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryIntegralRank(page,frontPage));
    }
}
