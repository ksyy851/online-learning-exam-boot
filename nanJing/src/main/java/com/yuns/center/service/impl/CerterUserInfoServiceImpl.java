package com.yuns.center.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.center.entity.CenterUserStatistics;
import com.yuns.center.entity.CerterUserInfo;
import com.yuns.center.mapper.CerterUserInfoMapper;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.service.ICerterUserInfoService;
import com.yuns.center.vo.CenterStatisticsVo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CerterUserInfoServiceImpl extends ServiceImpl<CerterUserInfoMapper, CerterUserInfo>  implements ICerterUserInfoService {

    @Override
    public CerterUserInfo queryByUserName(CerterUserParam certerUserParam) {
        return baseMapper.queryByUserName(certerUserParam);
    }

    @Override
    public List<CenterUserStatistics> queryUserStatisticsInfo(CerterUserParam certerUserParam) {
        return baseMapper.queryUserStatisticsInfo(certerUserParam);
    }

    @Override
    public List<CenterStatisticsVo> queryCourseHoursStatistics(CerterUserParam certerUserParam) {
        return baseMapper.queryCourseHoursStatistics(certerUserParam);
    }

    @Override
    public List<CenterStatisticsVo> queryIntegralStatistics(CerterUserParam certerUserParam) {
        return baseMapper.queryIntegralStatistics(certerUserParam);
    }
}
