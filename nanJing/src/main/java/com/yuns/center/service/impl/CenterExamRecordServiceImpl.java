package com.yuns.center.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.center.entity.CenterExamRecord;
import com.yuns.center.mapper.CenterExamRecordMapper;
import com.yuns.center.service.ICenterExamRecordService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CenterExamRecordServiceImpl extends ServiceImpl<CenterExamRecordMapper, CenterExamRecord> implements ICenterExamRecordService {

    @Override
    public Object queryExamRecord(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryExamRecord(frontPage);
        }
        Page<CenterExamRecord> page = new Page<CenterExamRecord>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryExamRecord(page,frontPage));
    }
}
