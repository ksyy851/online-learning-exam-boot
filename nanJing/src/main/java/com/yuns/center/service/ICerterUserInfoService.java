package com.yuns.center.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.center.entity.CenterUserStatistics;
import com.yuns.center.entity.CerterUserInfo;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.vo.CenterStatisticsVo;

import java.util.List;

public interface ICerterUserInfoService extends IService<CerterUserInfo> {

    CerterUserInfo queryByUserName(CerterUserParam certerUserParam);

    List<CenterUserStatistics> queryUserStatisticsInfo(CerterUserParam certerUserParam);

    List<CenterStatisticsVo> queryCourseHoursStatistics(CerterUserParam certerUserParam);

    List<CenterStatisticsVo> queryIntegralStatistics(CerterUserParam certerUserParam);
}
