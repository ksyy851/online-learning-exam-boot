package com.yuns.center.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.center.entity.CenterHoursRecord;
import com.yuns.center.entity.CenterWarnEduRecord;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface ICenterWarnEduService extends IService<CenterWarnEduRecord> {

    Object queryWarnEduRecord(BasePage frontPage);

    Object queryHoursRecord(BasePage frontPage);

}
