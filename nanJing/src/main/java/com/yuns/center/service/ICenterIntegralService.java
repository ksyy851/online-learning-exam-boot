package com.yuns.center.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.center.entity.CenterIntegralRank;
import com.yuns.center.entity.CenterIntegralRecord;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface ICenterIntegralService extends IService<CenterIntegralRecord> {
    Object queryIntegralRecord(BasePage frontPage);

    Object queryIntegralRank(BasePage frontPage);
}
