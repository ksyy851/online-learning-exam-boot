package com.yuns.center.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.center.entity.CenterWrongTitle;
import com.yuns.center.mapper.CenterWrongTitleMapper;
import com.yuns.center.service.ICenterWrongTitleService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CenterWrongTitleServiceImpl extends ServiceImpl<CenterWrongTitleMapper, CenterWrongTitle> implements ICenterWrongTitleService {

    @Override
    public Object queryWrongTitle(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryWrongTitle(frontPage);
        }
        Page<CenterWrongTitle> page = new Page<CenterWrongTitle>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryWrongTitle(page,frontPage));
    }
}

