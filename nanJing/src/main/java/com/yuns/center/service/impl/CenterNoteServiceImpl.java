package com.yuns.center.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.center.entity.CenterExamTitleNote;
import com.yuns.center.entity.CenterCourseNote;
import com.yuns.center.mapper.CenterNoteMapper;
import com.yuns.center.service.ICenterNoteService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CenterNoteServiceImpl extends ServiceImpl<CenterNoteMapper, CenterCourseNote> implements ICenterNoteService {
    @Override
    public Object queryCourseNote(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryCourseNote(frontPage);
        }
        Page<CenterCourseNote> page = new Page<CenterCourseNote>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryCourseNote(page,frontPage));
    }

    @Override
    public Object queryExamTitleNote(BasePage frontPage) {
        if (!frontPage.is_search()) {
            baseMapper.queryExamTitleNote(frontPage);
        }
        Page<CenterExamTitleNote> page = new Page<CenterExamTitleNote>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryExamTitleNote(page,frontPage));
    }
}
