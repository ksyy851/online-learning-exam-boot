package com.yuns.center.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.center.entity.CenterLearnCourse;
import com.yuns.center.entity.CenterLearnLive;
import com.yuns.center.entity.CenterLearnText;
import com.yuns.center.entity.CenterLearnVideo;
import com.yuns.center.mapper.CenterLearnMapper;
import com.yuns.center.service.ICenterLearnService;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

@Service
public class CenterLearnServiceImpl extends ServiceImpl<CenterLearnMapper, CenterLearnCourse> implements ICenterLearnService {


    @Override
    public Object queryLearnCourse(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryLearnCourse(frontPage);
        }
        Page<CenterLearnCourse> page = new Page<CenterLearnCourse>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryLearnCourse(page,frontPage));
    }

    @Override
    public Object queryLearnLive(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryLearnLive(frontPage);
        }
        Page<CenterLearnLive> page = new Page<CenterLearnLive>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryLearnLive(page,frontPage));
    }

    @Override
    public Object queryLearnVideo(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryLearnVideo(frontPage);
        }
        Page<CenterLearnVideo> page = new Page<CenterLearnVideo>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryLearnVideo(page,frontPage));
    }

    @Override
    public Object queryLearnText(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryLearnText(frontPage);
        }
        Page<CenterLearnText> page = new Page<CenterLearnText>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryLearnText(page,frontPage));
    }
}
