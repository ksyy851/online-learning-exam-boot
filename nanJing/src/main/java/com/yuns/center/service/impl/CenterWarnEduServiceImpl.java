package com.yuns.center.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.center.entity.CenterHoursRecord;
import com.yuns.center.entity.CenterWarnEduRecord;
import com.yuns.center.mapper.CerterWarnEduInfoMapper;
import com.yuns.center.service.ICenterWarnEduService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CenterWarnEduServiceImpl extends ServiceImpl<CerterWarnEduInfoMapper, CenterWarnEduRecord> implements ICenterWarnEduService {
    @Override
    public Object queryWarnEduRecord(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryWarnEduRecord(frontPage);
        }
        Page<CenterWarnEduRecord> page = new Page<CenterWarnEduRecord>(frontPage.getPageNumber(), frontPage.getPageSize());
        return ResultJson.ok(page.setRecords(baseMapper.queryWarnEduRecord(page,frontPage)));

    }

    @Override
    public Object queryHoursRecord(BasePage frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.queryHoursRecord(frontPage);
        }
        Page<CenterHoursRecord> page = new Page<>(frontPage.getPageNumber(), frontPage.getPageSize());
        return page.setRecords(baseMapper.queryHoursRecord(page,frontPage));
    }

}
