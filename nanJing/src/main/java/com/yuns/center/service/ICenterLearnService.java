package com.yuns.center.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.center.entity.CenterLearnCourse;
import com.yuns.center.entity.CenterLearnLive;
import com.yuns.center.entity.CenterLearnText;
import com.yuns.center.entity.CenterLearnVideo;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface ICenterLearnService extends IService<CenterLearnCourse> {

    Object queryLearnCourse(BasePage frontPage);
    Object queryLearnLive(BasePage frontPage);
    Object queryLearnVideo(BasePage frontPage);
    Object queryLearnText(BasePage frontPage);

}
