package com.yuns.center.service;

import com.baomidou.mybatisplus.service.IService;
import com.yuns.center.entity.CenterWrongTitle;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface ICenterWrongTitleService extends IService<CenterWrongTitle> {

    Object queryWrongTitle(BasePage frontPage);

}
