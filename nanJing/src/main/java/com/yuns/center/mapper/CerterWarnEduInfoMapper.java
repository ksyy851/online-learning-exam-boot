package com.yuns.center.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterHoursRecord;
import com.yuns.center.entity.CenterWarnEduRecord;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface CerterWarnEduInfoMapper extends BaseMapper<CenterWarnEduRecord> {

    List<CenterWarnEduRecord> queryWarnEduRecord(Page<CenterWarnEduRecord> page,BasePage frontPage);

    List<CenterWarnEduRecord> queryWarnEduRecord(BasePage frontPage);

    List<CenterHoursRecord> queryHoursRecord(BasePage frontPage);

    List<CenterHoursRecord> queryHoursRecord(Page<CenterHoursRecord> page,BasePage frontPage);

}
