package com.yuns.center.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterCourseNote;
import com.yuns.center.entity.CenterExamTitleNote;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface CenterNoteMapper extends BaseMapper<CenterCourseNote> {

    List<CenterCourseNote> queryCourseNote(BasePage frontPage);
    List<CenterCourseNote> queryCourseNote(Page<CenterCourseNote> page, BasePage frontPage);

    List<CenterExamTitleNote> queryExamTitleNote(BasePage frontPage);
    List<CenterExamTitleNote> queryExamTitleNote(Page<CenterExamTitleNote> page,BasePage frontPage);
}
