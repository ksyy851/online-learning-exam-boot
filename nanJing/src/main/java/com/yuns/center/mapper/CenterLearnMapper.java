package com.yuns.center.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterLearnCourse;
import com.yuns.center.entity.CenterLearnLive;
import com.yuns.center.entity.CenterLearnText;
import com.yuns.center.entity.CenterLearnVideo;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface CenterLearnMapper extends BaseMapper<CenterLearnCourse> {

    List<CenterLearnCourse> queryLearnCourse(BasePage frontPage);
    List<CenterLearnCourse> queryLearnCourse(Page<CenterLearnCourse> page, BasePage frontPage);

    List<CenterLearnLive> queryLearnLive(BasePage frontPage);
    List<CenterLearnLive> queryLearnLive(Page<CenterLearnLive> page,BasePage frontPage);

    List<CenterLearnVideo> queryLearnVideo(BasePage frontPage);
    List<CenterLearnVideo> queryLearnVideo(Page<CenterLearnVideo> page,BasePage frontPage);

    List<CenterLearnText> queryLearnText(BasePage frontPage);
    List<CenterLearnText> queryLearnText(Page<CenterLearnText> page,BasePage frontPage);

}
