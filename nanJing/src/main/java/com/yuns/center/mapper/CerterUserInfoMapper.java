package com.yuns.center.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.center.entity.CenterUserStatistics;
import com.yuns.center.entity.CerterUserInfo;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.vo.CenterStatisticsVo;

import java.util.List;

/**
 * 个人中心-用户信息接口
 * @author zhangzhan
 * @since 2020-11-14
 */
public interface CerterUserInfoMapper extends BaseMapper<CerterUserInfo> {
    CerterUserInfo queryByUserName(CerterUserParam certerUserParam);

    List<CenterUserStatistics> queryUserStatisticsInfo(CerterUserParam certerUserParam);

    List<CenterStatisticsVo> queryCourseHoursStatistics(CerterUserParam certerUserParam);

    List<CenterStatisticsVo> queryIntegralStatistics(CerterUserParam certerUserParam);

}