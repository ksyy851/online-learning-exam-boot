package com.yuns.center.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterWrongTitle;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface CenterWrongTitleMapper extends BaseMapper<CenterWrongTitle> {
     List<CenterWrongTitle> queryWrongTitle(BasePage frontPage);
     List<CenterWrongTitle> queryWrongTitle(Page<CenterWrongTitle> page, BasePage frontPage);
}
