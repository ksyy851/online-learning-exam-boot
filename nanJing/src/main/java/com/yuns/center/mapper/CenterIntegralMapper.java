package com.yuns.center.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterIntegralRank;
import com.yuns.center.entity.CenterIntegralRecord;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface CenterIntegralMapper extends BaseMapper<CenterIntegralRecord> {
    public List<CenterIntegralRecord> queryIntegralRecord(BasePage frontPage);
    public List<CenterIntegralRecord> queryIntegralRecord(Page<CenterIntegralRecord> page, BasePage frontPage);

    public List<CenterIntegralRank> queryIntegralRank(BasePage frontPage);
    public List<CenterIntegralRank> queryIntegralRank(Page<CenterIntegralRank> page,BasePage frontPage);
}
