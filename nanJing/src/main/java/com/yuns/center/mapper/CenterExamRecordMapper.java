package com.yuns.center.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterExamRecord;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

public interface CenterExamRecordMapper extends BaseMapper<CenterExamRecord> {

    List<CenterExamRecord> queryExamRecord(BasePage frontPage);

    List<CenterExamRecord> queryExamRecord(Page<CenterExamRecord> page, BasePage frontPage);
}
