package com.yuns.center.param;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Description:个人中心-我的笔记 课堂笔记 题目笔记
 * User: zhangzhan
 * Date: 2020-11-14
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CenterWrongTitleParam {

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;

    /**
     * 笔记类型
     */
    @ApiModelProperty(value = "题目类型（0：单选题 1多选题 2 判断题 3 简答题）", required = true)
    private Integer wrongTitleType;

}
