package com.yuns.center.param;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Description:个人中心-用户信息查询入参
 * User: zhangzhan
 * Date: 2020-10-28
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CerterUserParam implements Serializable {

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID", required = true)
    private String userId;


}
