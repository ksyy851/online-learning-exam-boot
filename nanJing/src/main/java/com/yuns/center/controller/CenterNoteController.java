package com.yuns.center.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterCourseNote;
import com.yuns.center.entity.CenterExamTitleNote;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.service.ICenterNoteService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 - 我的笔记 Controller" )
@RestController
@RequestMapping("/centerNote" )
public class CenterNoteController {

    @Autowired
    private ICenterNoteService iCenterNoteService;

    @ApiOperation(value = "课堂笔记 - queryCourseNote" )
    @PostMapping(value = "queryCourseNote" )
    public ResultJson queryCourseNote(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterNoteService.queryCourseNote(frontPage));
    }

    @ApiOperation(value = "题目笔记 - queryExamTitleNote" )
    @PostMapping(value = "queryExamTitleNote" )
    public ResultJson queryExamTitleNote(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterNoteService.queryExamTitleNote(frontPage));
    }

}
