package com.yuns.center.controller;

import com.yuns.center.param.CerterUserParam;
import com.yuns.center.service.ICerterUserInfoService;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 - Controller" )
@RestController
@RequestMapping("/center" )
public class CenterController {

    @Autowired
    private ICerterUserInfoService iCerterUserInfoService;

    @ApiOperation(value = "个人中心-用户信息查询 - queryCerterUserInfo" )
    @PostMapping(value = "queryCerterUserInfo" )
    public ResultJson queryCerterUserInfo(@RequestBody CerterUserParam certerUserParam){
        return ResultJson.ok(iCerterUserInfoService.queryByUserName(certerUserParam));
    }

    @ApiOperation(value = "个人中心-积分、考试记录、学习记录等 - queryUserStatisticsInfo" )
    @PostMapping(value = "queryUserStatisticsInfo" )
    public ResultJson queryUserStatisticsInfo(@RequestBody CerterUserParam certerUserParam){
        return ResultJson.ok(iCerterUserInfoService.queryUserStatisticsInfo(certerUserParam));
    }

    @ApiOperation(value = "个人中心-我的各类别课程学时统计 - queryCourseHoursStatistics" )
    @PostMapping(value = "queryCourseHoursStatistics" )
    public ResultJson queryCourseHoursStatistics(@RequestBody CerterUserParam certerUserParam){
        return ResultJson.ok(iCerterUserInfoService.queryCourseHoursStatistics(certerUserParam));
    }

    @ApiOperation(value = "个人中心-我的各类积分占比 - queryIntegralStatistics" )
    @PostMapping(value = "queryIntegralStatistics" )
    public ResultJson queryIntegralStatistics(@RequestBody CerterUserParam certerUserParam){
        return ResultJson.ok(iCerterUserInfoService.queryIntegralStatistics(certerUserParam));
    }

}
