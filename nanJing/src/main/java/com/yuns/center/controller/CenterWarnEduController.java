package com.yuns.center.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterHoursRecord;
import com.yuns.center.entity.CenterWarnEduRecord;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.service.ICenterWarnEduService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 警示教育 - Controller" )
@RestController
@RequestMapping("/centerWarnEdu" )
public class CenterWarnEduController {

    @Autowired
    private ICenterWarnEduService iCenterWarnEduService;

    @ApiOperation(value = "警示教育记录 - queryWarnEduRecord" )
    @PostMapping(value = "queryWarnEduRecord" )
    public ResultJson queryWarnEduRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterWarnEduService.queryWarnEduRecord(frontPage));
    }

    @ApiOperation(value = "学时记录 - queryHoursRecord" )
    @PostMapping(value = "queryHoursRecord" )
    public ResultJson queryHoursRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterWarnEduService.queryHoursRecord(frontPage));
    }

}
