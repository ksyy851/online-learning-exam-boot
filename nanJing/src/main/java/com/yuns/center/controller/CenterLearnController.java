package com.yuns.center.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterLearnCourse;
import com.yuns.center.entity.CenterLearnLive;
import com.yuns.center.entity.CenterLearnText;
import com.yuns.center.entity.CenterLearnVideo;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.service.ICenterLearnService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 - 学习记录 Controller" )
@RestController
@RequestMapping("/centerLearn" )
public class CenterLearnController {
    @Autowired
    private ICenterLearnService iCenterLearnService;

    @ApiOperation(value = "课程 - queryLearnCourse" )
    @PostMapping(value = "queryLearnCourse" )
    public ResultJson queryLearnCourse(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterLearnService.queryLearnCourse(frontPage));
    }

    @ApiOperation(value = "直播 - queryLearnLive" )
    @PostMapping(value = "queryLearnLive" )
    public ResultJson queryLearnLive(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterLearnService.queryLearnLive(frontPage));
    }

    @ApiOperation(value = "视频资料 - queryLearnVideo" )
    @PostMapping(value = "queryLearnVideo" )
    public ResultJson queryLearnVideo(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterLearnService.queryLearnVideo(frontPage));
    }

    @ApiOperation(value = "文字资料 - queryLearnText" )
    @PostMapping(value = "queryLearnText" )
    public ResultJson queryLearnText(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterLearnService.queryLearnText(frontPage));
    }
}
