package com.yuns.center.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterIntegralRank;
import com.yuns.center.entity.CenterIntegralRecord;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.service.ICenterIntegralService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 我的积分 - Controller" )
@RestController
@RequestMapping("/centerIntegral" )
public class CenterIntegralController {

    @Autowired
    private ICenterIntegralService iCenterIntegralService;

    @ApiOperation(value = "积分记录 - queryIntegralRecord" )
    @PostMapping(value = "queryIntegralRecord" )
    public ResultJson queryIntegralRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterIntegralService.queryIntegralRecord(frontPage));
    }

    @ApiOperation(value = "积分排名 - queryIntegralRank" )
    @PostMapping(value = "queryIntegralRank" )
    public ResultJson queryIntegralRank(@RequestBody BasePage frontPage){
        return ResultJson.ok(iCenterIntegralService.queryIntegralRank(frontPage));
    }
}
