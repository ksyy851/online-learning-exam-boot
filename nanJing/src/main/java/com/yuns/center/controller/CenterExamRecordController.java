package com.yuns.center.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterExamRecord;
import com.yuns.center.param.CerterUserParam;
import com.yuns.center.service.ICenterExamRecordService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 考试记录 - Controller" )
@RestController
@RequestMapping("/centerExamRecord" )
public class CenterExamRecordController {

    @Autowired
    private ICenterExamRecordService iCenterExamRecordService;

    @ApiOperation(value = "考试记录 - queryExamRecord" )
    @PostMapping(value = "queryExamRecord" )
    public ResultJson queryExamRecord(@RequestBody BasePage<CerterUserParam> frontPage){
        return ResultJson.ok(iCenterExamRecordService.queryExamRecord(frontPage));
    }

}
