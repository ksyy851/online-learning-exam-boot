package com.yuns.center.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.center.entity.CenterWrongTitle;
import com.yuns.center.param.CenterWrongTitleParam;
import com.yuns.center.service.ICenterWrongTitleService;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "个人中心 错题报告 - Controller" )
@RestController
@RequestMapping("/centerWrongTitle" )
public class CenterWrongTitleController {

    @Autowired
    private ICenterWrongTitleService iCenterWrongTitleService;

    @ApiOperation(value = "错题报告 - queryWrongTitle" )
    @PostMapping(value = "queryWrongTitle" )
    public ResultJson queryWrongTitle(@RequestBody BasePage<CenterWrongTitleParam> frontPage){
        return ResultJson.ok(iCenterWrongTitleService.queryWrongTitle(frontPage));
    }

}
