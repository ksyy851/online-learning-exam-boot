package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 个人中心-错题报告VO
 * @author zhangzhan
 * @since 2020-11-14
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CenterWrongTitle {

    /**
     * 题目
     */
    @ApiParam(value = "题目")
    @ApiModelProperty(value = "题目")
    @TableField("question_title")
    private String questionTitle;

    /**
     * 错误答案
     */
    @ApiParam(value = "错误答案")
    @ApiModelProperty(value = "错误答案")
    @TableField("question_erro_answer")
    private String questionErroAnswer;

    /**
     * 正确答案
     */
    @ApiParam(value = "正确答案")
    @ApiModelProperty(value = "正确答案")
    @TableField("question_answer")
    private String questionAnswer;



    /**
     * 题目解析
     */
    @ApiParam(value = "题目解析")
    @ApiModelProperty(value = "题目解析")
    private String analysis;


}
