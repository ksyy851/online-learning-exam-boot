package com.yuns.center.entity;


import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.Date;

/**
 * 个人中心-警示教育VO
 * @author zhangzhan
 * @since 2020-11-14
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CenterWarnEduRecord {

    /**
     * 日期
     */
    @ApiParam(value = "日期")
    @ApiModelProperty(value = "日期")
    @TableField("pulish_time")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date pulishTime;

    /**
     * 类别
     */
    @ApiParam(value = "类别")
    @ApiModelProperty(value = "类别")
    @TableField("name")
    private String name;

    /**
     * 具体事项
     */
    @ApiParam(value = "具体事项")
    @ApiModelProperty(value = "具体事项")
    @TableField("intro")
    private String intro;

}
