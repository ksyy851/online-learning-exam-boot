package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.math.BigInteger;
import java.util.Date;

/**
 * 个人中心 考试记录
 */
@Data
public class CenterExamRecord {

    /**
     * 试卷主题
     */
    @ApiParam(value = "试卷主题")
    @ApiModelProperty(value = "试卷主题")
    private String name;

    /**
     * 总分
     */
    @ApiParam(value = "总分")
    @ApiModelProperty(value = "总分")
    @TableField("total_points")
    private Integer totalPoints;

    /**
     * 考试分数
     */
    @ApiParam(value = "考试分数")
    @ApiModelProperty(value = "考试分数")
    private Integer grade;

    /**
     * 考试开始时间
     */
    @ApiParam(value = "考试开始时间")
    @ApiModelProperty(value = "考试开始时间")
    @TableField("start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    /**
     * 考试结束时间
     */
    @ApiParam(value = "考试结束时间")
    @ApiModelProperty(value = "考试结束时间")
    @TableField("end_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endTime;

    /**
     * 考试用时
     */
    @ApiParam(value = "考试用时")
    @ApiModelProperty(value = "考试用时")
    @TableField("used_time")
    private Integer usedTime;

    /**
     * 答题结束时间
     */
    @ApiParam(value = "答题结束时间")
    @ApiModelProperty(value = "答题结束时间")
    @TableField("end_answer_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date endAnswerTime;



    /**
     * 试卷id
     */
    @ApiParam(value = "试卷id")
    @ApiModelProperty(value = "试卷id")
    @TableField("exam_id")
    private BigInteger examId;

}
