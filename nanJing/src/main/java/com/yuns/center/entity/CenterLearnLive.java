package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CenterLearnLive {

    /**
     *直播名称
     */
    @ApiParam(value = "直播名称")
    @ApiModelProperty(value = "直播名称")
    @TableField("live_name")
    private String liveName;

    /**
     *直播文件ID
     */
    @ApiParam(value = "直播文件ID")
    @ApiModelProperty(value = "直播文件ID")
    @TableField("live_file_no")
    private String liveFileNo;

    /**
     *上次观看时间
     */
    @ApiParam(value = "上次观看时间")
    @ApiModelProperty(value = "上次观看时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date time;

    /**
     *文件路径
     */
    @ApiParam(value = "文件路径")
    @ApiModelProperty(value = "文件路径")
    @TableField("file_url")
    private String fileUrl;

}
