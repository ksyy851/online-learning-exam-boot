package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.Date;

/**
 * 个人中心 我的积分记录
 */
@Data
public class CenterIntegralRecord {

    /**
     * 积分
     */
    @ApiParam(value = "积分")
    @ApiModelProperty(value = "积分")
    private Integer integral;

    /**
     * 课程ID(直播ID)
     */
    @ApiParam(value = "课程ID(直播ID)")
    @ApiModelProperty(value = "课程ID(直播ID)")
    private Integer pid;
    /**
     * 课程名称(直播名称)
     */
    @ApiParam(value = "课程名称(直播名称)")
    @ApiModelProperty(value = "课程名称(直播名称)")
    private String name;

    /**
     * 观看时间
     */
    @ApiParam(value = "观看时间")
    @ApiModelProperty(value = "观看时间")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date time;

    /**
     * 学习渠道:0:课程学习;1:直播学习
     */
    @ApiParam(value = "学习渠道:0:课程学习;1:直播学习")
    @ApiModelProperty(value = "学习渠道:0:课程学习;1:直播学习")
    private Integer type;

    /**
     * 文件路径
     */
    @ApiParam(value = "文件路径")
    @ApiModelProperty(value = "文件路径")
    @TableField("file_url")
    private String fileUrl;
}
