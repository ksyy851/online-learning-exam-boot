package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.Date;


/**
 * 个人中心 学时记录
 */
@Data
public class CenterHoursRecord {

    /**
     * 名称  课程、直播、视频资料、文字资料
     */
    @ApiParam(value = "名称  课程、直播、视频资料、文字资料")
    @ApiModelProperty(value = "名称  课程、直播、视频资料、文字资料")
    private String name;

    /**
     *  学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private String period;

    /**
     * 观看时间
     */
    @ApiParam(value = "观看时间")
    @ApiModelProperty(value = "观看时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @TableField("watch_time")
    private Date watchTime;

    /**
     *文件路径
     */
    @ApiParam(value = "文件路径")
    @ApiModelProperty(value = "文件路径")
    @TableField("file_url")
    private String fileUrl;

    /**
     *  类型
     */
    @ApiParam(value = "类型")
    @ApiModelProperty(value = "类型")
    private String type;

}

