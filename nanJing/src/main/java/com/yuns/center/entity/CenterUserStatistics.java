package com.yuns.center.entity;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.math.BigInteger;

/**
 * 个人中心 积分、考试记录、学习记录等
 */
@Data
public class CenterUserStatistics {
    /**
     *我的积分
     */
    @ApiParam(value = "我的积分")
    @ApiModelProperty(value = "我的积分")
    private BigInteger integral;

    /**
     *考试记录
     */
    @ApiParam(value = "考试记录")
    @ApiModelProperty(value = "考试记录")
    private BigInteger testRecords;

    /**
     *错题报告
     */
    @ApiParam(value = "错题报告")
    @ApiModelProperty(value = "错题报告")
    private BigInteger wrongTopicReport;

    /**
     *学习记录
     */
    @ApiParam(value = "学习记录")
    @ApiModelProperty(value = "学习记录")
    private BigInteger learningRecord;

    /**
     *我的笔记
     */
    @ApiParam(value = "我的笔记")
    @ApiModelProperty(value = "我的笔记")
    private BigInteger note;

    /**
     *警示教育
     */
    @ApiParam(value = "警示教育")
    @ApiModelProperty(value = "警示教育")
    private BigInteger warningEducation;

    /**
     *学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private BigInteger period;

}
