package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.util.Date;

/**
 * 个人中心 我的笔记 课堂笔记
 */
@Data
public class CenterCourseNote {

    /**
     *课程名称
     */
    @ApiParam(value = "课程名称")
    @ApiModelProperty(value = "课程名称")
    @TableField("course_name")
    private String courseName;

    /**
     *章节名称
     */
    @ApiParam(value = "章节名称")
    @ApiModelProperty(value = "章节名称")
    @TableField("chapter_name")
    private String chapterName;

    /**
     *笔记内容
     */
    @ApiParam(value = "笔记内容")
    @ApiModelProperty(value = "笔记内容")
    @TableField("note")
    private String note;

    /**
     * 日期
     */
    @ApiParam(value = "日期")
    @ApiModelProperty(value = "日期")
    @TableField("create_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;
}
