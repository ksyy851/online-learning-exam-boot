package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class CenterLearnCourse {

    /**
     *课程名称
     */
    @ApiParam(value = "课程名称")
    @ApiModelProperty(value = "课程名称")
    @TableField("course_name")
    private String courseName;

    /**
     *视频图片文件ID
     */
    @ApiParam(value = "视频图片文件ID")
    @ApiModelProperty(value = "视频图片文件ID")
    @TableField("file_no")
    private String fileNo;

    /**
     *上次观看时间
     */
    @ApiParam(value = "上次观看时间")
    @ApiModelProperty(value = "上次观看时间")
    @TableField("watch_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private String watchTime;

    /**
     *文件路径
     */
    @ApiParam(value = "文件路径")
    @ApiModelProperty(value = "文件路径")
    @TableField("file_url")
    private String fileUrl;

}
