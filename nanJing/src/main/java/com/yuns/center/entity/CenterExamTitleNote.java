package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 个人中心 我的笔记 题目笔记
 */
@Data
public class CenterExamTitleNote {

    /**
     *题目
     */
    @ApiParam(value = "题目")
    @ApiModelProperty(value = "题目")
    @TableField("question_title")
    private String questionTitle;

    /**
     *笔记内容
     */
    @ApiParam(value = "笔记内容")
    @ApiModelProperty(value = "笔记内容")
    private String note;

    /**
     *日期
     */
    @ApiParam(value = "日期")
    @ApiModelProperty(value = "日期")
    @TableField("create_time")
    private String createTime;

}
