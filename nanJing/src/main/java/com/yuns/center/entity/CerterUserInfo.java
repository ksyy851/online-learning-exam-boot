package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 个人中心-用户信息VO
 * @author zhangzhan
 * @since 2020-11-14
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CerterUserInfo {

    /**
     * 用户名
     */
    @ApiParam(value = "用户名")
    @ApiModelProperty(value = "用户名")
    @TableField("user_name")
    private String userName;
    /**
     * 昵称
     */
    @ApiParam(value = "昵称")
    @ApiModelProperty(value = "昵称")
    @TableField("nickname")
    private String nickname;
    /**
     * 工作岗位
     */
    @ApiParam(value = "工作岗位")
    @ApiModelProperty(value = "工作岗位")
    @TableField("job")
    private String job;

    /**
     * 企业名称
     */
    @ApiParam(value = "企业名称")
    @ApiModelProperty(value = "企业名称")
    @TableField("company_name")
    private String companyName;

    /**
     * 微信号
     */
    @ApiParam(value = "微信号")
    @ApiModelProperty(value = "微信号")
    private String wechat;
    /**
     * 电话号码
     */
    @ApiParam(value = "电话号码")
    @ApiModelProperty(value = "电话号码")
    private String tel;

    /**
     * 证书编号
     */
    @ApiParam(value = "证书编号")
    @ApiModelProperty(value = "证书编号")
    @TableField("cert_no")
    private String certNo;


}
