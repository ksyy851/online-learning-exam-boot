package com.yuns.center.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 个人中心 我的积分 积分排名
 */
@Data
public class CenterIntegralRank {

    /**
     * 积分
     */
    @ApiParam(value = "排名")
    @ApiModelProperty(value = "排名")
    private Integer rank;

    /**
     * 积分
     */
    @ApiParam(value = "积分")
    @ApiModelProperty(value = "积分")
    private Integer integral;

    /**
     * 企业名称
     */
    @ApiParam(value = "企业名称")
    @ApiModelProperty(value = "企业名称")
    @TableField("company_name")
    private String companyName;

}
