package com.yuns.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.*;


/**
 *
 */
@Component
@Slf4j
@SuppressWarnings("unchecked")
public class AutoUpdateTask {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 验证充值命令是否已经成功
     * 0 0/1 * * * ?
     */
//    @Scheduled(cron = "0 0/1 * * * ? ")
    public void getCommandRecordsByQueryDate() {

    }

}