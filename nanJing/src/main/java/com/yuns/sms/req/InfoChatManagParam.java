package com.yuns.sms.req;

import com.yuns.sms.entity.InfoChatManag;
import com.yuns.web.param.FrontPage;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 聊天管理请求参数
 * @author: zhansang
 * @create: 2020-11-08 18:22
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InfoChatManagParam extends FrontPage<InfoChatManagReq> {

    @ApiModelProperty(value = "是否回复 1 回复 0 未回复")
    private Integer replyContentHas;

    /**
     * 回复账号
     */
    @ApiParam(value = "回复账号")
    @ApiModelProperty(value = "回复账号")
    private Integer replyId;

    @ApiParam(value = "咨询人id")
    @ApiModelProperty(value = "咨询人id")
    private Integer consultants;
}
