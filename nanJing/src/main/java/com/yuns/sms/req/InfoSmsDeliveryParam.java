package com.yuns.sms.req;

import com.yuns.sms.entity.InfoSmsDelivery;
import com.yuns.web.param.FrontPage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: TODO: 短信下发请求参数
 * @author: zhansang
 * @create: 2020-11-08 17:33
 * @version: 1.0
 **/
@Data
@Builder
public class InfoSmsDeliveryParam extends FrontPage<InfoSmsDelivery> {
}
