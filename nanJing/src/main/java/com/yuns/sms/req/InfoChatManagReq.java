package com.yuns.sms.req;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @description: TODO: 聊天管理入参
 * @author: zhansang
 * @create: 2020-11-14 16:17
 * @version: 1.0
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class InfoChatManagReq {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 直播名称
     */
    @ApiParam(value = "直播名称")
    @ApiModelProperty(value = "直播名称")
    @TableField("live_broadcast_name")
    private String liveBroadcastName;
    /**
     * 咨询人id
     */
    @ApiParam(value = "咨询人id")
    @ApiModelProperty(value = "咨询人id")
    private Integer consultants;
    /**
     * 直播时间
     */
    @ApiParam(value = "直播时间")
    @ApiModelProperty(value = "直播时间")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date liveTime;
    /**
     * 咨询内容
     */
    @ApiParam(value = "咨询内容")
    @ApiModelProperty(value = "咨询内容")
    private String consultantsContent;
    /**
     * 回复内容
     */
    @ApiParam(value = "回复内容")
    @ApiModelProperty(value = "回复内容")
    private String replyContent;

    @ApiModelProperty(value = "是否回复")
    private Integer replyContentHas;

    /**
     * 回复账号
     */
    @ApiParam(value = "回复账号")
    @ApiModelProperty(value = "回复账号")
    private Integer replyId;
}
