package com.yuns.sms.controller;

import com.yuns.sms.entity.InfoSmsDelivery;
import com.yuns.sms.req.InfoSmsDeliveryParam;
import com.yuns.sms.service.IInfoSmsDeliveryService;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//import com.yuns.web.dto.InfoSmsDeliveryDto;


/**
 * @author ${author}
 * @since 2020-11-07
 */
@Api(tags = "短信下发 - Controller")
@RestController
@RequestMapping("/infoSmsDelivery")
public class InfoSmsDeliveryController {

    @Autowired
    private IInfoSmsDeliveryService iInfoSmsDeliveryService;

    @ApiOperation(value = "添加 - info_sms_delivery")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody InfoSmsDelivery model) {
        //  InfoSmsDelivery entity=DtoUtil.convertObject(model,InfoSmsDelivery.class);
        boolean flag = iInfoSmsDeliveryService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - info_sms_delivery")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody InfoSmsDelivery model) {
        //InfoSmsDelivery entity=DtoUtil.convertObject(model,InfoSmsDelivery.class);
        boolean flag = iInfoSmsDeliveryService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - info_sms_delivery")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iInfoSmsDeliveryService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - info_sms_delivery")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody InfoSmsDeliveryParam frontPage) {
        Object list = iInfoSmsDeliveryService.selectInfoSmsDeliveryList(frontPage);
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "查询短信模板 - info_sms_delivery")
    @PostMapping(value = "querySmsTemplate")
    public ResultJson querySmsTemplate(@RequestBody InfoSmsDeliveryParam frontPage) {
        String receive = frontPage.getData().getReceiver();
        String phoneNum = frontPage.getData().getPhoneNum();
        String msgTemp ="【南京市环境保护局】尊敬的[%s]%s:南京市环境保护局提醒您 请于2020.10.10 9:00:00准时参加《垃圾分类》的考试!";
        String message = String.format(msgTemp,phoneNum,receive);
        return ResultJson.ok(message);
    }

}

