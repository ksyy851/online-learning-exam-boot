package com.yuns.sms.controller;

import com.yuns.sms.entity.InfoChatManag;
import com.yuns.sms.req.InfoChatManagParam;
import com.yuns.sms.service.IInfoChatManagService;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//import com.yuns.web.dto.InfoChatManagDto;


/**
 * @author ${author}
 * @since 2020-11-07
 */
@Api(tags = "聊天管理 - Controller")
@RestController
@RequestMapping("/infoChatManag")
public class InfoChatManagController {

    @Autowired
    private IInfoChatManagService iInfoChatManagService;

    @ApiOperation(value = "添加 - info_chat_manag")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody InfoChatManag model) {
        //  InfoChatManag entity=DtoUtil.convertObject(model,InfoChatManag.class);
        boolean flag = iInfoChatManagService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - info_chat_manag")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody InfoChatManag model) {
        //InfoChatManag entity=DtoUtil.convertObject(model,InfoChatManag.class);
        boolean flag = iInfoChatManagService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - info_chat_manag")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iInfoChatManagService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - info_chat_manag")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody InfoChatManagParam frontPage) {

        Object list = iInfoChatManagService.selectInfoChatManagList(frontPage);
        return ResultJson.ok(list);
    }
}

