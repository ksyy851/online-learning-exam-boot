package com.yuns.sms.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.sms.entity.InfoSmsDelivery;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.sms.req.InfoSmsDeliveryParam;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;
/**
 * <p>
 * 短信下发 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface InfoSmsDeliveryMapper extends BaseMapper<InfoSmsDelivery> {
        List<InfoSmsDelivery> selectInfoSmsDeliveryList(@Param("page") Page<InfoSmsDelivery> page, @Param("frontPage") InfoSmsDeliveryParam frontPage);
        List<InfoSmsDelivery> selectInfoSmsDeliveryList(@Param("frontPage") InfoSmsDeliveryParam frontPage);
}
