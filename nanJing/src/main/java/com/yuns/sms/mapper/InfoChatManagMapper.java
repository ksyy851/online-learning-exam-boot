package com.yuns.sms.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.sms.entity.InfoChatManag;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.sms.req.InfoChatManagParam;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 聊天管理 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface InfoChatManagMapper extends BaseMapper<InfoChatManag> {
//        List<InfoChatManag> selectInfoChatManagList(BasePage frontPage);

        List<InfoChatManag> selectInfoChatManagList(@Param("page") Page<InfoChatManag> page,
                                                    @Param("frontPage") InfoChatManagParam frontPage);
        List<InfoChatManag> selectInfoChatManagList(@Param("frontPage") InfoChatManagParam frontPage);
}
