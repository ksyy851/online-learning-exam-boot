package com.yuns.sms.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.sms.entity.InfoSmsDelivery;
import com.yuns.sms.mapper.InfoSmsDeliveryMapper;
import com.yuns.sms.req.InfoSmsDeliveryParam;
import com.yuns.sms.service.IInfoSmsDeliveryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 短信下发 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@Service
public class InfoSmsDeliveryServiceImpl extends ServiceImpl<InfoSmsDeliveryMapper, InfoSmsDelivery> implements IInfoSmsDeliveryService {
        @Override
        public Object selectInfoSmsDeliveryList(InfoSmsDeliveryParam frontPage) {
            if(!frontPage.is_search()){
                return   baseMapper.selectInfoSmsDeliveryList(frontPage);
            }
            Page<InfoSmsDelivery> page = new Page<>(frontPage.getPageNumber(),frontPage.getPageSize());
            List<InfoSmsDelivery> list = baseMapper.selectInfoSmsDeliveryList(page,frontPage);
            return   page.setRecords(list);
     }
}
