package com.yuns.sms.service;

import com.yuns.sms.entity.InfoSmsDelivery;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.sms.req.InfoSmsDeliveryParam;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 短信下发 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface IInfoSmsDeliveryService extends IService<InfoSmsDelivery> {
        Object selectInfoSmsDeliveryList(InfoSmsDeliveryParam frontPage);
}
