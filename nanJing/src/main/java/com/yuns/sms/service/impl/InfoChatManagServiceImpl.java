package com.yuns.sms.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.live.entity.LiveComment;
import com.yuns.live.mapper.LiveBaseMapper;
import com.yuns.live.mapper.LiveCommentMapper;
import com.yuns.live.service.ILiveCommentService;
import com.yuns.sms.entity.InfoChatManag;
import com.yuns.sms.mapper.InfoChatManagMapper;
import com.yuns.sms.req.InfoChatManagParam;
import com.yuns.sms.service.IInfoChatManagService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 聊天管理 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@Service
public class InfoChatManagServiceImpl extends ServiceImpl<InfoChatManagMapper, InfoChatManag> implements IInfoChatManagService {

    @Resource
    private LiveCommentMapper liveCommentMapper;

    @Override
    public Object selectInfoChatManagList(InfoChatManagParam frontPage) {
        if (!frontPage.is_search()) {
            return baseMapper.selectInfoChatManagList(frontPage);
        }
        Page<InfoChatManag> page = new Page<>(frontPage.getPageNumber(), frontPage.getPageSize());
        List<InfoChatManag> list = baseMapper.selectInfoChatManagList(page, frontPage);
        return page.setRecords(list);
    }

    @Transactional
    @Override
    public boolean updateById(InfoChatManag model){
        Integer flag=baseMapper.updateById(model);
        if(flag!=-1){
            LiveComment liveComment=new LiveComment();
            liveComment.setCompanyUserId(model.getConsultants());
            liveComment.setCommentId(model.getCommentId());
            liveComment.setAnswerId(UUID.randomUUID().toString());
            liveComment.setComment(model.getReplyContent());
            liveComment.setLiveId(model.getLiveId());
            liveCommentMapper.insert(liveComment);
            return true;
        }
        return false;
    }

    @Transactional
    @Override
    public boolean deleteById(Integer id){
        //根据ID查询评论
        InfoChatManag chatManag=baseMapper.selectById(id);
        EntityWrapper wrapper=new EntityWrapper();
        LiveComment liveComment=new LiveComment();
        liveComment.setCommentId(chatManag.getCommentId());
        wrapper.setEntity(liveComment);
        liveCommentMapper.delete(wrapper);
        baseMapper.deleteById(id);
        return true;
    }

}
