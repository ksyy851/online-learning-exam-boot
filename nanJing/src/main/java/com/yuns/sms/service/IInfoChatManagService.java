package com.yuns.sms.service;

import com.yuns.sms.entity.InfoChatManag;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.sms.req.InfoChatManagParam;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 聊天管理 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface IInfoChatManagService extends IService<InfoChatManag> {
        Object selectInfoChatManagList(InfoChatManagParam frontPage);

        boolean updateById(InfoChatManag model);

        boolean deleteById(Integer id);
}
