package com.yuns.sms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 聊天管理
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@Data
@TableName("info_chat_manag")
public class InfoChatManag extends Model<InfoChatManag> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 直播ID
     */
    @ApiParam(value = "直播ID")
    @ApiModelProperty(value = "直播ID")
    @TableField("live_id")
    private Integer liveId;
    /**
     * 直播名称
     */
    @ApiParam(value = "直播名称")
   @ApiModelProperty(value = "直播名称")
    @TableField("live_broadcast_name")
    private String liveBroadcastName;
    /**
     * 评论ID
     */
    @ApiParam(value = "评论ID")
    @ApiModelProperty(value = "评论ID")
    @TableField("comment_id")
    private String commentId;
    /**
     * 咨询人id
     */
    @ApiParam(value = "咨询人id")
   @ApiModelProperty(value = "咨询人id")
    private Integer consultants;
    /**
     * 直播时间
     */
    @ApiParam(value = "直播时间")
   @ApiModelProperty(value = "直播时间")
    @TableField("live_time")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String liveTime;
    /**
     * 咨询内容
     */
    @ApiParam(value = "咨询内容")
   @ApiModelProperty(value = "咨询内容")
    @TableField("consultants_content")
    private String consultantsContent;
    /**
     * 回复内容
     */
    @ApiParam(value = "回复内容")
   @ApiModelProperty(value = "回复内容")
    @TableField("reply_content")
    private String replyContent;
    /**
     * 回复账号
     */
    @ApiParam(value = "回复账号")
   @ApiModelProperty(value = "回复账号")
    @TableField("reply_id")
    private Integer replyId;
    @TableField("create_user")
    private Long createUser;
    @TableField("create_dept")
    private Long createDept;
    @TableField("create_time")
    private String createTime;
    @TableField("update_user")
    private Long updateUser;
    @TableField("update_time")
    private String updateTime;
    private Integer status;
    @TableField("is_deleted")
    private Integer isDeleted;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
