package com.yuns.sms.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 短信下发
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@TableName("info_sms_delivery")
public class InfoSmsDelivery extends Model<InfoSmsDelivery> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户名称
     */
    @ApiParam(value = "用户名称")
   @ApiModelProperty(value = "用户名称")
    @TableField("user_name")
    private String userName;
    /**
     * 发送人信息
     */
    @ApiParam(value = "发送人信息")
   @ApiModelProperty(value = "发送人信息")
    @TableField("sender_info")
    private String senderInfo;
    /**
     * 手机号码
     */
    @ApiParam(value = "手机号码")
   @ApiModelProperty(value = "手机号码")
    @TableField("phone_num")
    private String phoneNum;
    /**
     * 短信类别（0考试预约）
     */
    @ApiParam(value = "短信类别（0考试预约）")
   @ApiModelProperty(value = "短信类别（0考试预约）")
    @TableField("msg_type")
    private Integer msgType;
    /**
     * 短信内容
     */
    @ApiParam(value = "短信内容")
   @ApiModelProperty(value = "短信内容")
    @TableField("msg_content")
    private String msgContent;
    /**
     * 接收人
     */
    @ApiParam(value = "接收人")
   @ApiModelProperty(value = "接收人")
    private String receiver;
    @TableField("create_user")
    private Long createUser;
    @TableField("create_dept")
    private Long createDept;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
   @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField("create_time")
    private Date createTime;
    @TableField("update_user")
    private Long updateUser;
    @TableField("update_time")
    private Date updateTime;
    private Integer status;
    @TableField("is_deleted")
    private Integer isDeleted;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSenderInfo() {
        return senderInfo;
    }

    public void setSenderInfo(String senderInfo) {
        this.senderInfo = senderInfo;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public Long getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Long createUser) {
        this.createUser = createUser;
    }

    public Long getCreateDept() {
        return createDept;
    }

    public void setCreateDept(Long createDept) {
        this.createDept = createDept;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Long updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "InfoSmsDelivery{" +
        ", id=" + id +
        ", userName=" + userName +
        ", senderInfo=" + senderInfo +
        ", phoneNum=" + phoneNum +
        ", msgType=" + msgType +
        ", msgContent=" + msgContent +
        ", receiver=" + receiver +
        ", createUser=" + createUser +
        ", createDept=" + createDept +
        ", createTime=" + createTime +
        ", updateUser=" + updateUser +
        ", updateTime=" + updateTime +
        ", status=" + status +
        ", isDeleted=" + isDeleted +
        "}";
    }
}
