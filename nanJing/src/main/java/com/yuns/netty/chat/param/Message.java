package com.yuns.netty.chat.param;


import io.swagger.annotations.ApiModelProperty;

public class Message {

    @ApiModelProperty(value = " 消息类型",example = "type")
    private Integer type; // 消息类型

    @ApiModelProperty(value = " 聊天消息")
    private TbChatRecord chatRecord;    // 聊天消息

    @ApiModelProperty(value = " 设备类型")
    private Integer deviceType;    // 设备类型

    @ApiModelProperty(value = " 扩展消息字段",example = "ext")
    private Object ext;  // 扩展消息字段

     @ApiModelProperty(value = "经度和纬度",example = "ext")
    private Float lgt;  // 扩展消息字段

     @ApiModelProperty(value = " 经度和纬度",example = "ext")
    private Float lat;  // 扩展消息字段

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public TbChatRecord getChatRecord() {
        return chatRecord;
    }

    public void setChatRecord(TbChatRecord chatRecord) {
        this.chatRecord = chatRecord;
    }

    public Object getExt() {
        return ext;
    }

    public void setExt(Object ext) {
        this.ext = ext;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public Float getLgt() {
        return lgt;
    }

    public void setLgt(Float lgt) {
        this.lgt = lgt;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }
}
