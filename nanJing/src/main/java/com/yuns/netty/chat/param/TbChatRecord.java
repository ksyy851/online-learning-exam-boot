package com.yuns.netty.chat.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

public class TbChatRecord {

    @ApiModelProperty("消息id")
    private String id;

    @ApiModelProperty(value = " 消息类型", example = "1")
    private Integer type;

    @ApiModelProperty(value = "用户id", example = "1")
    private Integer userId;


    @ApiModelProperty(value = "朋友Id", example = "1")
    private Integer friendId;


    @ApiModelProperty(value = "是否已读", example = "0")
    private Integer hasRead;

    @ApiModelProperty(value = "创建时间", example = "2020-03-10 00:00:00")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createTime;

    @ApiModelProperty(value = "是否删除", example = "0")
    private Integer hasDelete;

    @ApiModelProperty(value = "消息", example = "我是第一个消息")
    private String message;

    @ApiModelProperty(value = "项目id消息", example = "1")
    private Integer projectId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }


    public Integer getHasRead() {
        return hasRead;
    }

    public void setHasRead(Integer hasRead) {
        this.hasRead = hasRead;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getHasDelete() {
        return hasDelete;
    }

    public void setHasDelete(Integer hasDelete) {
        this.hasDelete = hasDelete;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFriendId() {
        return friendId;
    }

    public void setFriendId(Integer friendId) {
        this.friendId = friendId;
    }

}
