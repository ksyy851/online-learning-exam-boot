package com.yuns.netty.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class NettyeListener implements ServletContextListener {

    @Value(value = "${socketPort}")
    private static   Integer port;

    @Autowired
    private static RedisTemplate redisTemplate;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.err.println("聊天端口打开 Startup!");
        new Thread() {
            @Override
            public void run() {
                try {
//                    System.out.println(port+"端口");
                    new WebSocketServer().start(7892);
                } catch (Exception e) {
                    e.printStackTrace();
                 }
            }
        }.start();
        System.err.println("聊天端口打开 end!");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
