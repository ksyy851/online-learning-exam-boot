package com.yuns.netty.param;

import io.netty.channel.Channel;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * 建立用户ID与通道的关联
 */
public class UserChannelMap {
    // 用户保存用户id与通道的Map对象
    private static Map<String, Map<Integer, Channel>> userChannelMap;

    static {
        userChannelMap = new HashMap<String, Map<Integer, Channel>>();
    }

    /**
     * 添加用户id与channel的关联
     * @param userid
     * @param channel
     */
    public static void put(Integer userid,Integer type, Channel channel) {
        Map<Integer, Channel> map=new HashMap<>();
        map.put(type,channel);
        userChannelMap.put(userid.toString(), map);
    }

    /**
     * 根据用户id移除用户id与channel的关联
     * @param userid
     */
    public static void remove(String userid,Integer type) {
         Map<Integer, Channel> integerChannelMap = userChannelMap.get(userid);
        if (integerChannelMap.containsKey(type)){
            integerChannelMap.remove(type);
           }
          int size = integerChannelMap.size();
         if (size>0){
            userChannelMap.put(userid,integerChannelMap);
         }else {
            userChannelMap.remove(userid);
         }

    }

    /**
     * 根据通道id移除用户与channel的关联
     * @param channelId 通道的id
     */
    public static void removeByChannelId(String channelId) {
        if(!StringUtils.isNotBlank(channelId)) {
            return;
        }
        for (String s : userChannelMap.keySet()) {
            Map<Integer, Channel> integerChannelMap = userChannelMap.get(s);
             for (Integer key :integerChannelMap.keySet()){
                  Channel channel = integerChannelMap.get(key);
                  if(channelId.equals(channel.id().asLongText())) {
                     System.out.println("客户端连接断开,取消用户" + s + "与通道" + channelId + "的关联");
                     integerChannelMap.remove(s);
                      if (integerChannelMap.size()>0){
                          userChannelMap.put(s,integerChannelMap);
                      }else {
                          userChannelMap.remove(s);
                      }
                     break;
                 }
             }

        }
    }


    // 打印所有的用户与通道的关联数据
    public static void print() {
        for (String s : userChannelMap.keySet()) {
            Map<Integer, Channel> integerChannelMap = userChannelMap.get(s);
            if (integerChannelMap!=null){

                Channel channel = integerChannelMap.get(0);
                Channel channel2 = integerChannelMap.get(1);
                if (channel!=null){
                    System.out.println("用户id:" + s + " 通道:" + channel.id());
                }
                  if (channel2!=null){
                    System.out.println("用户id:" + s + " 通道:" + channel2.id());
                }
           }

        }
    }

    /**
     * 根据好友id获取对应的通道
     * @param friendid 好友id
     * @return Netty通道
     */
    public static List<Channel> getChannel(String friendid) {
        List<Channel> list=new ArrayList<>();
        Map<Integer, Channel> integerChannelMap = userChannelMap.get(friendid);
        if (integerChannelMap==null){
            return list;
        }
        Collection<Channel> values = integerChannelMap.values();

        if (values.size()>0){
            list.addAll(values);
        }
        return list;
    }


    /**
     * 根据好友id登陆类型获取对应的通道
     * @param friendid 好友id
     * @return Netty通道
     */
    public static Channel getChannelByType(String friendid, Integer type) {
        Map<Integer, Channel> integerChannelMap = userChannelMap.get(friendid);
         if (integerChannelMap==null){
             return null;
         }
        return integerChannelMap.get(type);
    }
}
