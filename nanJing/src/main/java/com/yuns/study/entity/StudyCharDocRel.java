package com.yuns.study.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 课程-法律法规关联表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@TableName("study_char_doc_rel")
public class StudyCharDocRel extends Model<StudyCharDocRel> {

    private static final long serialVersionUID = 1L;

    /**
     * 文字资料ID
     */
    @ApiParam(value = "文字资料ID")
   @ApiModelProperty(value = "文字资料ID")
    @TableField("char_id")
    private Integer charId;
    /**
     * 法律法规ID
     */
    @ApiParam(value = "法律法规ID")
   @ApiModelProperty(value = "法律法规ID")
    @TableField("doc_id")
    private Integer docId;


    public Integer getCharId() {
        return charId;
    }

    public void setCharId(Integer charId) {
        this.charId = charId;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    @Override
    protected Serializable pkVal() {
        return this.charId;
    }

    @Override
    public String toString() {
        return "StudyCharDocRel{" +
        ", charId=" + charId +
        ", docId=" + docId +
        "}";
    }
}
