package com.yuns.study.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 法律法规提纲数据表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Data
@TableName("info_doc_i")
public class InfoDocI extends Model<InfoDocI> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;
    /**
     * 名称
     */
    @ApiParam(value = "名称")
   @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 内容
     */
    @ApiParam(value = "内容")
   @ApiModelProperty(value = "内容")
    private String intro;

    /**
     * 课程数量
     */
    @ApiParam(value = "课程数量")
    @ApiModelProperty(value = "课程数量")
    private Integer courseCount;

    /**
     * 是否删除  -1：已删除  0：正常
     */
    @ApiParam(value = "是否删除  -1：已删除  0：正常")
    @TableField("del")
    private Integer del;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
