package com.yuns.study.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 文字资料详情表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Data
@TableName("study_char_info")
public class StudyCharInfo extends Model<StudyCharInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer pid;
    /**
     * 浏览量
     */
    @ApiParam(value = "浏览量")
   @ApiModelProperty(value = "浏览量")
    @TableField("page_views")
    private Integer pageViews;
    /**
     * 上下架(0:上架;1:下架)
     */
    @ApiParam(value = "上下架(0:上架;1:下架)")
   @ApiModelProperty(value = "上下架(0:上架;1:下架)")
    private Integer type;

    /**
     * 是否删除(0:未删除；1：已删除)
     */
    @ApiParam(value = "是否删除(0:未删除；1：已删除)")
    @ApiModelProperty(value = "是否删除(0:未删除；1：已删除)")
    private Integer del;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
