package com.yuns.study.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 文字资料表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Data
@TableName("study_char_base")
public class StudyCharBase extends Model<StudyCharBase> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 资料标题
     */
    @ApiParam(value = "资料标题")
   @ApiModelProperty(value = "资料标题")
    @TableField("char_name")
    private String charName;
    /**
     * 学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private Integer period;
    /**
     * 资料内容
     */
    @ApiParam(value = "资料内容")
   @ApiModelProperty(value = "资料内容")
    @TableField("char_info")
    private String charInfo;
    /**
     * 资料积分
     */
    @ApiParam(value = "资料积分")
   @ApiModelProperty(value = "资料积分")
    @TableField("char_integral")
    private String charIntegral;
    /**
     * 上传时间
     */
    @ApiParam(value = "上传时间")
   @ApiModelProperty(value = "上传时间")
    @TableField("upload_time")
    private String uploadTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
