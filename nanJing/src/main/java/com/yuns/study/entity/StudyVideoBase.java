package com.yuns.study.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 视频资料表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Data
@TableName("study_video_base")
public class StudyVideoBase extends Model<StudyVideoBase> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 视频名称
     */
    @ApiParam(value = "视频名称")
   @ApiModelProperty(value = "视频名称")
    @TableField("video_name")
    private String videoName;
    /**
     * 学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private Integer period;
    /**
     * 视频详情
     */
    @ApiParam(value = "视频详情")
   @ApiModelProperty(value = "视频详情")
    @TableField("video_info")
    private String videoInfo;
    /**
     * 视频积分
     */
    @ApiParam(value = "视频积分")
   @ApiModelProperty(value = "视频积分")
    @TableField("video_integral")
    private String videoIntegral;
    /**
     * 视频时长
     */
    @ApiParam(value = "视频时长")
   @ApiModelProperty(value = "视频时长")
    @TableField("video_period")
    private Integer videoPeriod;
    /**
     * 视频文件ID
     */
    @ApiParam(value = "视频文件ID")
   @ApiModelProperty(value = "视频文件ID")
    @TableField("video_file_no")
    private String videoFileNo;
    /**
     * 视频文件名称
     */
    @ApiParam(value = "视频文件名称")
   @ApiModelProperty(value = "视频文件名称")
    @TableField("video_file_name")
    private String videoFileName;
    /**
     * 视频路径
     */
    @ApiParam(value = "视频路径")
   @ApiModelProperty(value = "视频路径")
    @TableField("video_file_path")
    private String videoFilePath;
    /**
     * 上传时间
     */
    @ApiParam(value = "上传时间")
   @ApiModelProperty(value = "上传时间")
    @TableField("upload_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String uploadTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
