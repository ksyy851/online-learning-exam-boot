package com.yuns.study.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

@Data
public class StudyBaseVo implements Serializable {

    /**
     * 上传时间
     */
    @ApiParam(value = "上传时间")
    @ApiModelProperty(value = "上传时间")
    @TableField("upload_time")
    private String uploadTime;

    /**
     * 浏览量
     */
    @ApiParam(value = "浏览量")
    @ApiModelProperty(value = "浏览量")
    @TableField("page_views")
    private Integer pageViews;
    /**
     * 上下架(0:上架;1:下架)
     */
    @ApiParam(value = "上下架(0:上架;1:下架)")
    @ApiModelProperty(value = "上下架(0:上架;1:下架)")
    private Integer type;

    /**
     * 学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private Integer period;
}
