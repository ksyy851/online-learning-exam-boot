package com.yuns.study.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 文字资料Vo
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Data
@TableName("study_char_base")
public class StudyCharVO extends StudyBaseVo{

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 文字详情id
     */
    @ApiParam(value = "文字详情id")
    @ApiModelProperty(value = "文字详情id")
    @TableField("info_id")
    private Integer infoId;

    /**
     * 资料内容
     */
    @ApiParam(value = "资料内容")
    @ApiModelProperty(value = "资料内容")
    @TableField("char_info")
    private String charInfo;

    /**
     * 资料名称
     */
    @ApiParam(value = "资料名称")
    @ApiModelProperty(value = "资料名称")
    @TableField("char_name")
    private String charName;
    /**
     * 所属法律法规ID
     */
    @ApiParam(value = "所属法律法规ID")
    @ApiModelProperty(value = "所属法律法规ID")
    @TableField("doc_id")
    private Integer docId;
    /**
     * 所属法律法规
     */
    @ApiParam(value = "所属法律法规")
   @ApiModelProperty(value = "所属法律法规")
    @TableField("doc_name")
    private String docName;

    /**
     * 所属法律法规内容
     */
    @ApiParam(value = "所属法律法规内容")
    @ApiModelProperty(value = "所属法律法规内容")
    private String docIntro;

    /**
     * 资料积分
     */
    @ApiParam(value = "资料积分")
    @ApiModelProperty(value = "资料积分")
    @TableField("char_integral")
    private String charIntegral;

}
