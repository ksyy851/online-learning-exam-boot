package com.yuns.study.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 视频资料表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Data
@TableName("study_video_base")
public class StudyVideoVo extends StudyBaseVo {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private Integer id;

    /**
     * 视频详情id
     */
    @ApiParam(value = "视频详情id")
    @ApiModelProperty(value = "视频详情id")
    @TableField("info_id")
    private Integer infoId;
    /**
     * 视频名称
     */
    @ApiParam(value = "视频名称")
   @ApiModelProperty(value = "视频名称")
    @TableField("video_name")
    private String videoName;

    /**
     * 视频详情
     */
    @ApiParam(value = "视频详情")
    @ApiModelProperty(value = "视频详情")
    @TableField("video_info")
    private String videoInfo;
    /**
     * 视频积分
     */
    @ApiParam(value = "视频积分")
   @ApiModelProperty(value = "视频积分")
    @TableField("video_integral")
    private String videoIntegral;

    /**
     * 视频文件ID
     */
    @ApiParam(value = "视频文件ID")
    @ApiModelProperty(value = "视频文件ID")
    @TableField("video_file_no")
    private String videoFileNo;
    /**
     * 视频文件名称
     */
    @ApiParam(value = "视频文件名称")
    @ApiModelProperty(value = "视频文件名称")
    @TableField("video_file_name")
    private String videoFileName;
    /**
     * 视频路径
     */
    @ApiParam(value = "视频路径")
    @ApiModelProperty(value = "视频路径")
    @TableField("video_file_path")
    private String videoFilePath;

}
