package com.yuns.study.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.StudyVideoBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

import com.yuns.study.vo.StudyVideoVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 视频资料表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface StudyVideoBaseMapper extends BaseMapper<StudyVideoBase> {


    List<StudyVideoVo> selectStudyVideoBaseList(FrontPage frontPage);

    List<StudyVideoVo> selectStudyVideoBaseList(@Param("page")Page<StudyVideoVo> page, FrontPage frontPage);
}
