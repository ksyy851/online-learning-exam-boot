package com.yuns.study.mapper;

import com.yuns.study.entity.StudyCharInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 文字资料详情表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface StudyCharInfoMapper extends BaseMapper<StudyCharInfo> {
        List<StudyCharInfo> selectStudyCharInfoList(FrontPage frontPage);
}
