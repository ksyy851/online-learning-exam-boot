package com.yuns.study.mapper;

import com.yuns.study.entity.StudyVideoInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

/**
 * <p>
 * 视频资料详情表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface StudyVideoInfoMapper extends BaseMapper<StudyVideoInfo> {
        List<StudyVideoInfo> selectStudyVideoInfoList(BasePage frontPage);
}
