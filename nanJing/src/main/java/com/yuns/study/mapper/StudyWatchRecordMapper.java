package com.yuns.study.mapper;

import com.yuns.study.entity.StudyWatchRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播-资料观看记录表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-20
 */
public interface StudyWatchRecordMapper extends BaseMapper<StudyWatchRecord> {
        List<StudyWatchRecord> selectStudyWatchRecordList(StudyWatchRecord frontPage);
}
