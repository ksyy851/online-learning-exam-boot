package com.yuns.study.mapper;

import com.yuns.study.entity.StudyCharDocRel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 课程-法律法规关联表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface StudyCharDocRelMapper extends BaseMapper<StudyCharDocRel> {
        List<StudyCharDocRel> selectStudyCharDocRelList(FrontPage frontPage);

        Integer updateByCharId(StudyCharDocRel rel);
}
