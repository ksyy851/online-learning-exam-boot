package com.yuns.study.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.StudyCharBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.study.vo.StudyCharVO;
import com.yuns.study.vo.StudyVideoVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 文字资料表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface StudyCharBaseMapper extends BaseMapper<StudyCharBase> {
        List<StudyCharVO> selectStudyCharBaseList(BasePage frontPage);

        List<StudyCharVO> selectStudyCharBaseList(@Param("page") Page<StudyCharVO> page, BasePage frontPage);
}
