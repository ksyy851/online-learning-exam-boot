package com.yuns.study.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.InfoDocI;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

import com.yuns.study.vo.StudyCharVO;
import com.yuns.web.sys.param.BasePage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 法律法规提纲数据表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface InfoDocIMapper extends BaseMapper<InfoDocI> {
    List<InfoDocI> selectInfoDocIList(BasePage frontPage);

    List<InfoDocI> selectInfoDocIList(@Param("page") Page<InfoDocI> page, BasePage frontPage);

    List<StudyCharVO> queryForDoor();
}
