package com.yuns.study.controller;

import com.yuns.study.service.IStudyCharDocRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.StudyCharDocRel;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.StudyCharDocRelDto;


/**
 * @author ${author}
 * @since 2020-11-07
 */
@Api(tags = "课程-法律法规关联表 - Controller" )
    @RestController
@RequestMapping("/studyCharDocRel" )
    public class StudyCharDocRelController {

    @Autowired
    private IStudyCharDocRelService iStudyCharDocRelService;

    @ApiOperation(value = "添加 - study_char_doc_rel" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody StudyCharDocRel model){
            //  StudyCharDocRel entity=DtoUtil.convertObject(model,StudyCharDocRel.class);
            boolean flag=iStudyCharDocRelService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - study_char_doc_rel" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody StudyCharDocRel model){
            //StudyCharDocRel entity=DtoUtil.convertObject(model,StudyCharDocRel.class);
            boolean flag=iStudyCharDocRelService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - study_char_doc_rel" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iStudyCharDocRelService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - study_char_doc_rel" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage){
            if(!frontPage.is_search()){
            Page<StudyCharDocRel> page=new Page<StudyCharDocRel>();
            Page<StudyCharDocRel> list=page.setRecords(iStudyCharDocRelService.selectStudyCharDocRelList(frontPage));
            return ResultJson.ok(list);
            }
            Page<StudyCharDocRel> page=new Page<StudyCharDocRel>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<StudyCharDocRel> list=page.setRecords(iStudyCharDocRelService.selectStudyCharDocRelList(frontPage));
            return ResultJson.ok(list);
            }
        }

