package com.yuns.study.controller;

import com.yuns.study.entity.InfoDocI;
import com.yuns.study.entity.StudyVideoInfo;
import com.yuns.study.service.IStudyVideoBaseService;
import com.yuns.study.service.IStudyVideoInfoService;
import com.yuns.study.vo.StudyVideoVo;
import com.yuns.web.sys.param.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.StudyVideoBase;
import com.yuns.web.param.QueryParam;

import javax.servlet.http.HttpServletResponse;

/**
 * 视频资料模块
 *
 * @author liuchengwen
 * @since 2020-11-05
 */
@Api(tags = "视频资料 - Controller")
@RestController
@RequestMapping("/study/video")
public class StudyVideoBaseController {

    @Autowired
    private IStudyVideoBaseService iStudyVideoBaseService;

    @Autowired
    private IStudyVideoInfoService iStudyVideoInfoService;

    @ApiOperation(value = "添加 - insert")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody StudyVideoBase model) {
        //  StudyVideoBase entity=DtoUtil.convertObject(model,StudyVideoBase.class);
        boolean flag = iStudyVideoBaseService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - up")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody StudyVideoBase model) {
        // StudyVideoBase entity=DtoUtil.convertObject(model,StudyVideoBase.class);
        boolean flag = iStudyVideoBaseService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - delete")
    @PostMapping(value = "delete/{infoId}")
    public ResultJson delete(@PathVariable Integer infoId) {
        StudyVideoInfo info = new StudyVideoInfo();
        info.setId(infoId);
        info.setDel(1);
        boolean flag = iStudyVideoInfoService.updateById(info);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "上下架 - updateForType")
    @PostMapping(value = "updateForType")
    public ResultJson updateForType(@RequestBody StudyVideoInfo model) {
        StudyVideoInfo info = new StudyVideoInfo();
        info.setId(model.getId());
        info.setType(model.getType());
        boolean flag = iStudyVideoInfoService.updateById(info);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - query")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<StudyVideoVo> frontPage) {
        if (!frontPage.is_search()) {
            Page<StudyVideoVo> page = new Page<StudyVideoVo>();
            Page<StudyVideoVo> list =
                    page.setRecords(iStudyVideoBaseService.selectStudyVideoBaseList(frontPage));
            return ResultJson.ok(list);
        }
        Page<StudyVideoVo> page =
                new Page<StudyVideoVo>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<StudyVideoVo> list =
                page.setRecords(iStudyVideoBaseService.selectStudyVideoBaseList(page,frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "编辑 - edit")
    @PostMapping(value = "edit/{id}")
    public ResultJson edit(@PathVariable("id") Integer id) {
        return ResultJson.ok(iStudyVideoBaseService.edit(id));
    }

    @ApiOperation(value = "导出视频资料列表 - exportList")
    @PostMapping(value = "exportList")
    public ResultJson exportList(@RequestBody FrontPage<StudyVideoVo> frontPage, HttpServletResponse response) {
        iStudyVideoBaseService.exportList(frontPage, response);
        return ResultJson.ok();
    }
}
