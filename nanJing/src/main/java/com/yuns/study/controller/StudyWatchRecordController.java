package com.yuns.study.controller;

import com.yuns.study.service.IStudyWatchRecordService;
import com.yuns.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import com.yuns.study.entity.StudyWatchRecord;
import org.springframework.web.bind.annotation.*;


/**
 * @author ${author}
 * @since 2020-11-20
 */
@Api(tags = "直播-资料观看记录表 - Controller")
@RestController
@RequestMapping("/studyWatchRecord")
public class StudyWatchRecordController {

    @Autowired
    private IStudyWatchRecordService iStudyWatchRecordService;

    @ApiOperation(value = "添加 - study_watch_record")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody StudyWatchRecord model) {
        //  StudyWatchRecord entity=DtoUtil.convertObject(model,StudyWatchRecord.class);
        boolean flag = iStudyWatchRecordService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - study_watch_record")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody StudyWatchRecord model) {
        //StudyWatchRecord entity=DtoUtil.convertObject(model,StudyWatchRecord.class);
        boolean flag = iStudyWatchRecordService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - study_watch_record")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iStudyWatchRecordService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - study_watch_record")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody StudyWatchRecord frontPage) {
        return ResultJson.ok(iStudyWatchRecordService.selectStudyWatchRecordList(frontPage));
    }
}

