package com.yuns.study.controller;

import com.yuns.study.service.IStudyCharInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.StudyCharInfo;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.StudyCharInfoDto;


/**
 * @author ${author}
 * @since 2020-11-05
 */
@Api(tags = "文字资料详情表 - Controller")
@RestController
@RequestMapping("/studyCharInfo")
public class StudyCharInfoController {

    @Autowired
    private IStudyCharInfoService iStudyCharInfoService;

    @ApiOperation(value = "添加 - study_char_info")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody StudyCharInfo model) {
        //  StudyCharInfo entity=DtoUtil.convertObject(model,StudyCharInfo.class);
        boolean flag = iStudyCharInfoService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - study_char_info")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody StudyCharInfo model) {
        //StudyCharInfo entity=DtoUtil.convertObject(model,StudyCharInfo.class);
        boolean flag = iStudyCharInfoService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - study_char_info")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iStudyCharInfoService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - study_char_info")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<StudyCharInfo> frontPage) {
        if (!frontPage.is_search()) {
            Page<StudyCharInfo> page = new Page<StudyCharInfo>();
            Page<StudyCharInfo> list = page.setRecords(iStudyCharInfoService.selectStudyCharInfoList(frontPage));
            return ResultJson.ok(list);
        }
        Page<StudyCharInfo> page = new Page<StudyCharInfo>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<StudyCharInfo> list = page.setRecords(iStudyCharInfoService.selectStudyCharInfoList(frontPage));
        return ResultJson.ok(list);
    }
}

