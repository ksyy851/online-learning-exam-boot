package com.yuns.study.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yuns.study.entity.InfoDocI;
import com.yuns.study.dto.StudyCharDto;
import com.yuns.study.entity.StudyCharBase;
import com.yuns.study.entity.StudyCharDocRel;
import com.yuns.study.entity.StudyCharInfo;
import com.yuns.study.mapper.StudyCharDocRelMapper;
import com.yuns.study.service.IStudyCharBaseService;
import com.yuns.study.service.IStudyCharDocRelService;
import com.yuns.study.service.IStudyCharInfoService;
import com.yuns.study.vo.StudyCharVO;
import com.yuns.study.vo.StudyVideoVo;
import com.yuns.web.sys.param.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 文字资料模块
 *
 * @author liuchengwen
 * @since 2020-11-05
 */
@Api(tags = "文字资料 - Controller")
@RestController
@RequestMapping("/study/char")
public class StudyCharBaseController {

    @Autowired
    private IStudyCharBaseService iStudyCharBaseService;
    @Autowired
    private IStudyCharInfoService iStudyCharInfoService;
    @Autowired
    private IStudyCharDocRelService iStudyCharDocRelService;

    @Resource
    private StudyCharDocRelMapper studyCharDocRelMapper;


    @ApiOperation(value = "添加 - insert")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody StudyCharDto model) {
        boolean flag = iStudyCharBaseService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - study_char_base")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody StudyCharDto model) {
        // StudyCharBase entity=DtoUtil.convertObject(model,StudyCharBase.class);
        boolean flag = iStudyCharBaseService.updateById(model);
        List<InfoDocI> infoDocIList = model.getInfoDocIList();
        infoDocIList.stream().forEach(item -> {
            StudyCharDocRel rel = new StudyCharDocRel();
            rel.setCharId(model.getId());
            rel.setDocId(item.getId());
            studyCharDocRelMapper.updateByCharId(rel);
        });

        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - delete")
    @PostMapping(value = "delete/{infoId}")
    @Transactional
    public ResultJson delete(@PathVariable("infoId") Integer infoId) {
        StudyCharInfo info = new StudyCharInfo();
        info.setId(infoId);
        info.setDel(1);
        boolean flag = iStudyCharInfoService.updateById(info);
        //根据info找id
        StudyCharInfo charInfo=iStudyCharInfoService.selectById(infoId);
        EntityWrapper<StudyCharDocRel> relEntityWrapper=new EntityWrapper<>();
        StudyCharDocRel rel=new StudyCharDocRel();
        rel.setCharId(charInfo.getPid());
        relEntityWrapper.setEntity(rel);
        iStudyCharDocRelService.delete(relEntityWrapper);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "上下架 - updateForType")
    @PostMapping(value = "updateForType")
    public ResultJson updateForType(@RequestBody StudyCharInfo model) {
        StudyCharInfo info = new StudyCharInfo();
        info.setId(model.getId());
        info.setType(model.getType());
        boolean flag = iStudyCharInfoService.updateById(info);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - query")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody BasePage<StudyCharVO> frontPage) {
        if (!frontPage.is_search()) {
            Page<StudyCharVO> page = new Page<StudyCharVO>();
            Page<StudyCharVO> list =
                    page.setRecords(iStudyCharBaseService.selectStudyCharBaseList(frontPage));
            return ResultJson.ok(list);
        }
        Page<StudyCharVO> page =
                new Page<StudyCharVO>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<StudyCharVO> list =
                page.setRecords(iStudyCharBaseService.selectStudyCharBaseList(page,frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "编辑 - edit")
    @PostMapping(value = "edit/{id}")
    public ResultJson edit(@PathVariable("id") Integer id) {
        return ResultJson.ok(iStudyCharBaseService.edit(id));
    }

    @ApiOperation(value = "导出文字资料列表 - exportList" )
    @PostMapping(value = "exportList" )
    public ResultJson exportList(@RequestBody BasePage<StudyCharVO> frontPage, HttpServletResponse response) {
        iStudyCharBaseService.exportList(frontPage,response);
        return ResultJson.ok();
    }
}
