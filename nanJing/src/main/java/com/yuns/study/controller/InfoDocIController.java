package com.yuns.study.controller;

import com.yuns.course.param.CourseParam;
import com.yuns.study.service.IInfoDocIService;
import com.yuns.study.entity.InfoDocI;
import com.yuns.web.sys.param.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;

import javax.servlet.http.HttpServletResponse;

// import com.yuns.web.dto.InfoDocIDto;

/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = "法律法规提纲数据表 - Controller")
@RestController
@RequestMapping("/infoDocI")
public class InfoDocIController {

    @Autowired
    private IInfoDocIService iInfoDocIService;

    @ApiOperation(value = "添加 - info_doc_i")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody InfoDocI model) {
        //  InfoDocI entity=DtoUtil.convertObject(model,InfoDocI.class);
        boolean flag = iInfoDocIService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - info_doc_i")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody InfoDocI model) {
        // InfoDocI entity=DtoUtil.convertObject(model,InfoDocI.class);
        boolean flag = iInfoDocIService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - info_doc_i")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        InfoDocI i = new InfoDocI();
        i.setId(id);
        i.setDel(1);
        boolean flag = iInfoDocIService.updateById(i);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - info_doc_i")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody BasePage<InfoDocI> frontPage) {
        if (!frontPage.is_search()) {
            Page<InfoDocI> page = new Page<InfoDocI>();
            Page<InfoDocI> list = page.setRecords(iInfoDocIService.selectInfoDocIList(frontPage));
            return ResultJson.ok(list);
        }
        Page<InfoDocI> page = new Page<InfoDocI>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<InfoDocI> list = page.setRecords(iInfoDocIService.selectInfoDocIList(page,frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "编辑 - edit")
    @PostMapping(value = "edit/{id}")
    public ResultJson edit(@PathVariable("id") Integer id) {
        return ResultJson.ok(iInfoDocIService.selectById(id));
    }

    @ApiOperation(value = "门户法律法规查询 - queryForDoor")
    @PostMapping(value = "queryForDoor")
    public ResultJson queryForDoor() {
      return ResultJson.ok(iInfoDocIService.queryForDoor());
    }


    @ApiOperation(value = "导出法律法规列表 - exportList" )
    @PostMapping(value = "exportList" )
    public ResultJson exportList(@RequestBody BasePage<InfoDocI> frontPage, HttpServletResponse response) {
        iInfoDocIService.exportList(frontPage,response);
        return ResultJson.ok();
    }
}
