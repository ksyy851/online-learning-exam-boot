package com.yuns.study.controller;

import com.yuns.study.service.IStudyVideoInfoService;
import com.yuns.web.sys.param.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.StudyVideoInfo;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.StudyVideoInfoDto;


/**
 * @author ${author}
 * @since 2020-11-05
 */
@Api(tags = "视频资料详情表 - Controller" )
    @RestController
@RequestMapping("/studyVideoInfo" )
    public class StudyVideoInfoController {

    @Autowired
    private IStudyVideoInfoService iStudyVideoInfoService;

    @ApiOperation(value = "添加 - study_video_info" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody StudyVideoInfo model){
            //  StudyVideoInfo entity=DtoUtil.convertObject(model,StudyVideoInfo.class);
            boolean flag=iStudyVideoInfoService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - study_video_info" )
    @PostMapping(value = "up}" )
    public ResultJson update(@RequestBody StudyVideoInfo model){
            //StudyVideoInfo entity=DtoUtil.convertObject(model,StudyVideoInfo.class);
            boolean flag=iStudyVideoInfoService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - study_video_info" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iStudyVideoInfoService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - study_video_info" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody BasePage<StudyVideoInfo> frontPage){
            if(!frontPage.is_search()){
            Page<StudyVideoInfo> page=new Page<StudyVideoInfo>();
            Page<StudyVideoInfo> list=page.setRecords(iStudyVideoInfoService.selectStudyVideoInfoList(frontPage));
            return ResultJson.ok(list);
            }
            Page<StudyVideoInfo> page=new Page<StudyVideoInfo>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<StudyVideoInfo> list=page.setRecords(iStudyVideoInfoService.selectStudyVideoInfoList(frontPage));
            return ResultJson.ok(list);
            }
        }

