package com.yuns.study.service.impl;

import com.yuns.study.entity.StudyWatchRecord;
import com.yuns.study.mapper.StudyWatchRecordMapper;
import com.yuns.study.service.IStudyWatchRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.param.FrontPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 直播-资料观看记录表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-20
 */
@Service
public class StudyWatchRecordServiceImpl extends ServiceImpl<StudyWatchRecordMapper, StudyWatchRecord> implements IStudyWatchRecordService {
        @Override
        public List<StudyWatchRecord> selectStudyWatchRecordList(StudyWatchRecord frontPage) {
        return   baseMapper.selectStudyWatchRecordList(frontPage);
     }
}
