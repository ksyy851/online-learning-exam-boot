package com.yuns.study.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.vo.CourseVO;
import com.yuns.study.entity.InfoDocI;
import com.yuns.study.mapper.InfoDocIMapper;
import com.yuns.study.service.IInfoDocIService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.study.vo.StudyCharVO;
import com.yuns.util.CommonExcel;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 法律法规提纲数据表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class InfoDocIServiceImpl extends ServiceImpl<InfoDocIMapper, InfoDocI> implements IInfoDocIService {
    @Override
    public List<InfoDocI> selectInfoDocIList(BasePage frontPage) {
        return baseMapper.selectInfoDocIList(frontPage);
    }

    @Override
    public List<InfoDocI> selectInfoDocIList(Page<InfoDocI> page, BasePage frontPage) {
        return baseMapper.selectInfoDocIList(page,frontPage);
    }

    @Override
    public List<StudyCharVO> queryForDoor() {
        return baseMapper.queryForDoor();
    }

    @Override
    public void exportList(BasePage<InfoDocI> frontPage, HttpServletResponse response) {
        try {
            List<InfoDocI> list = baseMapper.selectInfoDocIList(frontPage);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String fileName = "法律法规列表-" + df.format(new Date()) + ".xls";

            String[] rowsName = new String[]{"序号","名称","绑定课程数量","备注"};
            List<Object[]>  dataList = new ArrayList<Object[]>();
            Object[] objs = null;
            for (int i = 0; i < list.size(); i++) {
                objs = new Object[rowsName.length];
                objs[0] = i;
                objs[1] = list.get(i).getName();
                objs[2] = list.get(i).getCourseCount();
                objs[3] = list.get(i).getIntro();
                dataList.add(objs);
            }
            CommonExcel ex = new CommonExcel("法律法规导出情况", rowsName, dataList,response,fileName);
            ex.downloadExcel();
        }catch (Exception e){
            throw  new RuntimeException("法律法规列表导出失败");
        }
    }
}
