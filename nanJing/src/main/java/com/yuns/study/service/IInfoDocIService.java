package com.yuns.study.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.InfoDocI;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.study.vo.StudyCharVO;
import com.yuns.web.sys.param.BasePage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <p>
 * 法律法规提纲数据表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface IInfoDocIService extends IService<InfoDocI> {
    List<InfoDocI> selectInfoDocIList(BasePage frontPage);

    List<InfoDocI> selectInfoDocIList(Page<InfoDocI> page ,BasePage frontPage);

    List<StudyCharVO> queryForDoor();

    void exportList(BasePage<InfoDocI> frontPage, HttpServletResponse response);
}
