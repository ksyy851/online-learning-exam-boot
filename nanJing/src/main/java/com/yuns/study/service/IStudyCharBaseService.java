package com.yuns.study.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.dto.StudyCharDto;
import com.yuns.study.entity.StudyCharBase;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.study.vo.StudyCharVO;
import com.yuns.study.vo.StudyVideoVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
/**
 * 文字资料表 服务类
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface IStudyCharBaseService extends IService<StudyCharBase> {
  boolean insert(StudyCharDto base);

  List<StudyCharVO> selectStudyCharBaseList(BasePage frontPage);

  List<StudyCharVO> selectStudyCharBaseList(Page<StudyCharVO> page ,BasePage frontPage);

  StudyCharVO edit(Integer id);

  void exportList(BasePage<StudyCharVO> frontPage, HttpServletResponse response);
}
