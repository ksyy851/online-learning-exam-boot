package com.yuns.study.service.impl;

import com.yuns.study.entity.StudyCharInfo;
import com.yuns.study.mapper.StudyCharInfoMapper;
import com.yuns.study.service.IStudyCharInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 文字资料详情表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Service
public class StudyCharInfoServiceImpl extends ServiceImpl<StudyCharInfoMapper, StudyCharInfo> implements IStudyCharInfoService {
        @Override
        public List<StudyCharInfo> selectStudyCharInfoList(FrontPage frontPage) {
        return   baseMapper.selectStudyCharInfoList(frontPage);
     }
}
