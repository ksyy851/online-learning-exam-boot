package com.yuns.study.service.impl;

import com.yuns.study.entity.StudyCharDocRel;
import com.yuns.study.mapper.StudyCharDocRelMapper;
import com.yuns.study.service.IStudyCharDocRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程-法律法规关联表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
@Service
public class StudyCharDocRelServiceImpl extends ServiceImpl<StudyCharDocRelMapper, StudyCharDocRel> implements IStudyCharDocRelService {
        @Override
        public List<StudyCharDocRel> selectStudyCharDocRelList(FrontPage frontPage) {
        return   baseMapper.selectStudyCharDocRelList(frontPage);
     }
}
