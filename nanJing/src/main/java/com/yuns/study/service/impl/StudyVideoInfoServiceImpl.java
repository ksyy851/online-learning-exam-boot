package com.yuns.study.service.impl;

import com.yuns.study.entity.StudyVideoInfo;
import com.yuns.study.mapper.StudyVideoInfoMapper;
import com.yuns.study.service.IStudyVideoInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 视频资料详情表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Service
public class StudyVideoInfoServiceImpl extends ServiceImpl<StudyVideoInfoMapper, StudyVideoInfo> implements IStudyVideoInfoService {
        @Override
        public List<StudyVideoInfo> selectStudyVideoInfoList(BasePage frontPage) {
        return   baseMapper.selectStudyVideoInfoList(frontPage);
     }
}
