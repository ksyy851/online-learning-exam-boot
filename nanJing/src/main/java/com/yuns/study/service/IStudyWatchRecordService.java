package com.yuns.study.service;

import com.yuns.study.entity.StudyWatchRecord;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播-资料观看记录表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-20
 */
public interface IStudyWatchRecordService extends IService<StudyWatchRecord> {
        List<StudyWatchRecord> selectStudyWatchRecordList(StudyWatchRecord frontPage);
}
