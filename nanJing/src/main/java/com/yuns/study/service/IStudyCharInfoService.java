package com.yuns.study.service;

import com.yuns.study.entity.StudyCharInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 文字资料详情表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface IStudyCharInfoService extends IService<StudyCharInfo> {
        List<StudyCharInfo> selectStudyCharInfoList(FrontPage frontPage);
}
