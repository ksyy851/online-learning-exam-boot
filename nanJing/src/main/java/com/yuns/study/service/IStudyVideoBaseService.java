package com.yuns.study.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.study.entity.StudyVideoBase;
import com.yuns.study.vo.StudyVideoVo;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 视频资料表 服务类
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface IStudyVideoBaseService extends IService<StudyVideoBase> {

    boolean insert(StudyVideoBase model);

    List<StudyVideoVo> selectStudyVideoBaseList(FrontPage frontPage);

    List<StudyVideoVo> selectStudyVideoBaseList(Page<StudyVideoVo> page , FrontPage frontPage);

    StudyVideoVo edit(Integer id);

    void exportList(FrontPage<StudyVideoVo> frontPage, HttpServletResponse response);
}
