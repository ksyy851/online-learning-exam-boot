package com.yuns.study.service;

import com.yuns.study.entity.StudyVideoInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

import java.util.List;
/**
 * <p>
 * 视频资料详情表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
public interface IStudyVideoInfoService extends IService<StudyVideoInfo> {
        List<StudyVideoInfo> selectStudyVideoInfoList(BasePage frontPage);
}
