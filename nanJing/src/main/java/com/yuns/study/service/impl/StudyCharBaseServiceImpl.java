package com.yuns.study.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.study.entity.InfoDocI;
import com.yuns.study.dto.StudyCharDto;
import com.yuns.study.entity.StudyCharBase;
import com.yuns.study.entity.StudyCharDocRel;
import com.yuns.study.entity.StudyCharInfo;
import com.yuns.study.mapper.StudyCharBaseMapper;
import com.yuns.study.mapper.StudyCharDocRelMapper;
import com.yuns.study.mapper.StudyCharInfoMapper;
import com.yuns.study.service.IStudyCharBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.study.vo.StudyCharVO;
import com.yuns.study.vo.StudyVideoVo;
import com.yuns.util.CommonExcel;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 文字资料表 服务实现类
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Service
public class StudyCharBaseServiceImpl extends ServiceImpl<StudyCharBaseMapper, StudyCharBase>
        implements IStudyCharBaseService {

    @Resource
    StudyCharInfoMapper studyCharInfoMapper;

    @Resource
    StudyCharDocRelMapper studyCharDocRelMapper;

    /**
     * 文字资料新增
     *
     * @param base
     * @return
     */
    @Transactional
    @Override
    public boolean insert(StudyCharDto base) {
        Integer flag = baseMapper.insert(base);
        if (flag != -1) {
            StudyCharInfo info = new StudyCharInfo();
            info.setPid(base.getId());
            info.setPageViews(0);
            info.setType(0);
            info.setDel(0);
            studyCharInfoMapper.insert(info);
            List<InfoDocI> infoDocIList = base.getInfoDocIList();
            infoDocIList.stream()
                    .forEach(
                            item -> {
                                StudyCharDocRel rel = new StudyCharDocRel();
                                rel.setCharId(base.getId());
                                rel.setDocId(item.getId());
                                studyCharDocRelMapper.insert(rel);
                            });
            return true;
        }
        return false;
    }

    /**
     * 列表查询
     *
     * @param frontPage
     * @return
     */
    @Override
    public List<StudyCharVO> selectStudyCharBaseList(BasePage frontPage) {
        return baseMapper.selectStudyCharBaseList(frontPage);
    }

    @Override
    public List<StudyCharVO> selectStudyCharBaseList(Page<StudyCharVO> page ,BasePage frontPage) {
        return baseMapper.selectStudyCharBaseList(page,frontPage);
    }

    /**
     * 编辑
     *
     * @param id
     * @return
     */
    @Override
    public StudyCharVO edit(Integer id) {
        StudyCharBase base = baseMapper.selectById(id);
        StudyCharVO vo = new StudyCharVO();
        vo.setId(base.getId());
        vo.setCharName(base.getCharName());
        vo.setCharIntegral(base.getCharIntegral());
        vo.setCharInfo(base.getCharInfo());
        StudyCharDocRel rel = studyCharDocRelMapper.selectById(base.getId());
        vo.setDocId(rel.getDocId());
        return vo;
    }

  /**
   * 导出
   * @param frontPage
   * @param response
   */
    @Override
    public void exportList(BasePage<StudyCharVO> frontPage, HttpServletResponse response) {
        try {
            List<StudyCharVO> list = baseMapper.selectStudyCharBaseList(frontPage);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String fileName = "文字资料列表-" + df.format(new Date()) + ".xls";

            String[] rowsName = new String[]{"序号", "视频名称", "上传时间", "浏览量", "积分"};
            List<Object[]> dataList = new ArrayList<Object[]>();
            Object[] objs = null;
            for (int i = 0; i < list.size(); i++) {
                objs = new Object[rowsName.length];
                objs[0] = i;
                objs[1] = list.get(i).getCharName();
                objs[2] = df.format(list.get(i).getUploadTime());
                objs[3] = list.get(i).getPageViews();
                objs[4] = list.get(i).getCharIntegral();
                dataList.add(objs);
            }
            CommonExcel ex = new CommonExcel("文字资料列表情况", rowsName, dataList, response, fileName);
            ex.downloadExcel();
        } catch (Exception e) {
            throw new RuntimeException("文字资料列表导出失败");
        }
    }
}
