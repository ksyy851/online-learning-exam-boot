package com.yuns.study.service;

import com.yuns.study.entity.StudyCharDocRel;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程-法律法规关联表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-07
 */
public interface IStudyCharDocRelService extends IService<StudyCharDocRel> {
        List<StudyCharDocRel> selectStudyCharDocRelList(FrontPage frontPage);
}
