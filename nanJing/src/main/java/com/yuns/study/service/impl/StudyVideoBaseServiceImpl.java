package com.yuns.study.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveVideoFile;
import com.yuns.live.service.ILiveVideoFileService;
import com.yuns.study.entity.InfoDocI;
import com.yuns.study.entity.StudyVideoBase;
import com.yuns.study.entity.StudyVideoInfo;
import com.yuns.study.mapper.StudyVideoBaseMapper;
import com.yuns.study.mapper.StudyVideoInfoMapper;
import com.yuns.study.service.IStudyVideoBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.study.vo.StudyVideoVo;
import com.yuns.util.CommonExcel;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 视频资料表 服务实现类
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Service
public class StudyVideoBaseServiceImpl extends ServiceImpl<StudyVideoBaseMapper, StudyVideoBase>
        implements IStudyVideoBaseService {

    @Resource
    private StudyVideoInfoMapper studyVideoInfoMapper;

    @Resource
    private ILiveVideoFileService fileService;

    /**
     * 新增视频资料
     *
     * @param model
     * @return
     */
    @Transactional
    @Override
    public boolean insert(StudyVideoBase model) {
        Integer flag = baseMapper.insert(model);
        if (flag != -1) {
            StudyVideoInfo studyVideoInfo = new StudyVideoInfo();
            studyVideoInfo.setPid(model.getId());
            studyVideoInfo.setPageViews(0);
            studyVideoInfo.setType(0);
            studyVideoInfo.setDel(0);
            studyVideoInfoMapper.insert(studyVideoInfo);
            return true;
        }
        return false;
    }

    /**
     * 视频列表查询
     *
     * @param frontPage
     * @return
     */
    @Override
    public List<StudyVideoVo> selectStudyVideoBaseList(FrontPage frontPage) {
        return baseMapper.selectStudyVideoBaseList(frontPage);
    }

    @Override
    public List<StudyVideoVo> selectStudyVideoBaseList(Page<StudyVideoVo> page ,FrontPage frontPage) {
        return baseMapper.selectStudyVideoBaseList(page,frontPage);
    }

    /**
     * 编辑
     *
     * @param id
     * @return
     */
    @Override
    public StudyVideoVo edit(Integer id) {
        StudyVideoBase base = baseMapper.selectById(id);
        LiveVideoFile file = fileService.selectById(base.getVideoFileNo());
        StudyVideoVo vo = new StudyVideoVo();
        vo.setId(base.getId());
        vo.setVideoName(base.getVideoName());
        vo.setVideoInfo(base.getVideoInfo());
        vo.setVideoIntegral(base.getVideoIntegral());
        vo.setVideoFileName(file.getFileName());
        return vo;
    }

    /**
     * 导出
     * @param frontPage
     * @param response
     */
    @Override
    public void exportList(FrontPage<StudyVideoVo> frontPage, HttpServletResponse response) {
        try {
            List<StudyVideoVo> list = baseMapper.selectStudyVideoBaseList(frontPage);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            String fileName = "视频资料列表-" + df.format(new Date()) + ".xls";

            String[] rowsName = new String[]{"序号", "视频名称", "上传时间", "浏览量","积分"};
            List<Object[]> dataList = new ArrayList<Object[]>();
            Object[] objs = null;
            for (int i = 0; i < list.size(); i++) {
                objs = new Object[rowsName.length];
                objs[0] = i;
                objs[1] = list.get(i).getVideoName();
                objs[2] = df.format(list.get(i).getUploadTime());
                objs[3] = list.get(i).getPageViews();
                objs[4] = list.get(i).getVideoIntegral();
                dataList.add(objs);
            }
            CommonExcel ex = new CommonExcel("视频资料列表情况", rowsName, dataList, response, fileName);
            ex.downloadExcel();
        } catch (Exception e) {
            throw new RuntimeException("视频资料列表导出失败");
        }
    }
}
