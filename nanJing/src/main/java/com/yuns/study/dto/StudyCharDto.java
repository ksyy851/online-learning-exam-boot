package com.yuns.study.dto;

import com.baomidou.mybatisplus.annotations.TableName;
import com.yuns.study.entity.InfoDocI;
import com.yuns.study.entity.StudyCharBase;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * <p>
 * 文字资料入参
 * </p>
 *
 * @author liuchengwen
 * @since 2020-11-05
 */
@Data
@TableName("study_char_base")
public class StudyCharDto extends StudyCharBase {

    @ApiModelProperty(value = "法律法规集合" ,required = true)
    List<InfoDocI> infoDocIList;

}
