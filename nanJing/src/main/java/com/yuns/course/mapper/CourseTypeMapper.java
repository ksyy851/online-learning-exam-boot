package com.yuns.course.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseType;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

/**
 * <p>
 * 课程类型基础表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {
        List<CourseType> selectCourseTypeList(BasePage frontPage);
        List<CourseType> selectCourseTypeList(Page<CourseType> page,BasePage frontPage);
}
