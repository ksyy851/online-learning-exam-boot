package com.yuns.course.mapper;

import com.yuns.course.entity.CourseWatchRecord;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 课程-章节观看记录表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-13
 */
public interface CourseWatchRecordMapper extends BaseMapper<CourseWatchRecord> {
        List<CourseWatchRecord> selectCourseWatchRecordList(CourseWatchRecord frontPage);
}
