package com.yuns.course.mapper;

import com.yuns.course.entity.CourseChapter;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 课程章节表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface CourseChapterMapper extends BaseMapper<CourseChapter> {
        List<CourseChapter> selectCourseChapterList(FrontPage frontPage);
}
