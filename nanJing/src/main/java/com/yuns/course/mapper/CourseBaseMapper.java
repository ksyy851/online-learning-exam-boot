package com.yuns.course.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

import com.yuns.course.param.CourseParam;
import com.yuns.course.vo.CourseVO;
import com.yuns.web.param.FrontPage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 课程基础表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface CourseBaseMapper extends BaseMapper<CourseBase> {
    List<CourseBase> selectCourseBaseList(FrontPage frontPage);

    List<CourseVO> selectCourseList(Page<CourseVO> page, CourseParam frontPage);

    List<CourseVO> selectCourseList( CourseParam frontPage);

    CourseVO queryByIdForDoc(Integer id);

    CourseVO queryByIdForChapter(Integer id);
}
