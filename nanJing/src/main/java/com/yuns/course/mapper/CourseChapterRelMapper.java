package com.yuns.course.mapper;

import com.yuns.course.entity.CourseChapterRel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface CourseChapterRelMapper extends BaseMapper<CourseChapterRel> {
        List<CourseChapterRel> selectCourseChapterRelList(FrontPage frontPage);
}
