package com.yuns.course.mapper;

import com.yuns.course.entity.CourseTypeRel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 课程-课程类型关联表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface CourseTypeRelMapper extends BaseMapper<CourseTypeRel> {
        List<CourseTypeRel> selectCourseTypeRelList(FrontPage frontPage);
}
