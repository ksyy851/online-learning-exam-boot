package com.yuns.course.mapper;

import com.yuns.course.entity.CourseNote;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.course.vo.CourseNoteVO;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;

import java.util.List;

/**
 * <p>
 * 课程笔记表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
public interface CourseNoteMapper extends BaseMapper<CourseNote> {
        List<CourseNoteVO> selectCourseNoteList(CourseNote model);
}
