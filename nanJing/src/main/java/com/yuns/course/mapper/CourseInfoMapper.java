package com.yuns.course.mapper;

import com.yuns.course.entity.CourseInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 课程详情表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface CourseInfoMapper extends BaseMapper<CourseInfo> {
        List<CourseInfo> selectCourseInfoList(FrontPage frontPage);
}
