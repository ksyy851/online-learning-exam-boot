package com.yuns.course.dto;

import com.yuns.course.entity.CourseBase;
import com.yuns.course.entity.CourseChapter;
import com.yuns.study.entity.InfoDocI;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @description: TODO: 课程入参
 * @author: liuChengWen
 * @create: 2020-10-26
 * @version: 1.0
 **/
@Data
public class CourseDto extends CourseBase {

    @ApiModelProperty(value = "课程类型集合" ,required = true)
    Integer courseType;

    @ApiModelProperty(value = "法律法规集合" ,required = false)
    List<InfoDocI> infoDocIList;

    @ApiModelProperty(value = "课程章节集合" ,required = false)
    List<CourseChapter> courseChapterList;

    public CourseDto(){

    }
}
