package com.yuns.course.param;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

/**
 * Description:课程管理列表查询入参
 * User: liuChengWen
 * Date: 2020-10-28
 * Time: 12:38
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class CourseParam implements Serializable {


    @ApiModelProperty(value = "上传开始时间", required = false)
    private String bgTm;

    @ApiModelProperty(value = "上传结束时间", required = false)
    private String endTm;

    /**
     * 课程名称
     */
    @ApiModelProperty(value = "课程名称", required = false)
    private String courseName;

    /**
     * 课程类型ID
     */
    @ApiModelProperty(value = "课程类型", required = false)
    private Integer courseType;

    @ApiModelProperty(value = "是否是分页")
    private boolean _search;


    //	每页显示条数
    @ApiModelProperty(value = "每页显示条数")
    private int pageSize;

    //当前页数
    @ApiModelProperty(value = "当前页数")
    private int pageNumber;

    /**
     * 上下架(0:上架;1:下架)
     */
    @ApiParam(value = "上下架(0:上架;1:下架)")
    @ApiModelProperty(value = "上下架(0:上架;1:下架)")
    private Integer type;
}
