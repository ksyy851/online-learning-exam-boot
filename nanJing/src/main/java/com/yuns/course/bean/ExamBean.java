package com.yuns.course.bean;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * @description: TODO:
 * @author: zhansang
 * @create: 2020-11-21 19:53
 * @version: 1.0
 **/
@Data
public class ExamBean {

    /**
     * 试卷名称
     */
    @ApiParam(value = "试卷名称")
    @ApiModelProperty(value = "试卷名称")
    private String examName;


    /**
     * 题目备注
     */
    @ApiParam(value = "题目备注")
    @ApiModelProperty(value = "题目备注")
    private String examRemark;
}
