package com.yuns.course.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.yuns.study.entity.InfoDocI;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 法律法规提纲数据表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Data
public class InfoDocIBean {

    /**
     * 名称
     */
    @ApiParam(value = "名称")
   @ApiModelProperty(value = "名称")
    private String name;
    /**
     * 内容
     */
    @ApiParam(value = "内容")
   @ApiModelProperty(value = "内容")
    private String intro;
}
