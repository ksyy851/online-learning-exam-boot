package com.yuns.course.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 视频资料表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-05
 */
@Data
public class StudyVideoBean {

    /**
     * 视频名称
     */
    @ApiParam(value = "视频名称")
   @ApiModelProperty(value = "视频名称")
    @TableField("video_name")
    private String videoName;
    /**
     * 学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private Integer period;
    /**
     * 视频详情
     */
    @ApiParam(value = "视频详情")
   @ApiModelProperty(value = "视频详情")
    @TableField("video_info")
    private String videoInfo;
    /**
     * 视频积分
     */
    @ApiParam(value = "视频积分")
   @ApiModelProperty(value = "视频积分")
    @TableField("video_integral")
    private String videoIntegral;
}
