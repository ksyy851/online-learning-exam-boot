package com.yuns.course.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 课程类型基础表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Data
public class CourseTypeBean {

    /**
     * 课程名称
     */
    @ApiParam(value = "课程名称")
   @ApiModelProperty(value = "课程名称")
    private String name;

    /**
     * 备注
     */
    @ApiParam(value = "备注")
    @ApiModelProperty(value = "备注")
    private String remake;

}
