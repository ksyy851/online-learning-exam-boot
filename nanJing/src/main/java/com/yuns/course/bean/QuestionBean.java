package com.yuns.course.bean;

import lombok.Data;
import org.apache.commons.lang.StringUtils;

/**
 * @description: TODO: 题目导入
 * @author: zhansang
 * @create: 2020-11-21 20:20
 * @version: 1.0
 **/
@Data
public class QuestionBean {

    /***
     * 题目类型
     */
    private String questionType;

    /**
     * 获取题目类型  题目类型（0：单选题 1多选题 2 判断题 3 简答题）
     * @return
     */
    public Integer getQuestionType(){
        if(this.questionType == null) { return 0;}
        if(this.questionType.equals("单选题")){
            return 0;

        }else if(this.questionType.equals("多选题")){
            return 1;
        }else if(this.questionType.equals("判断题")){
            return 2;
        }else  if(this.questionType.equals("简答题")){
            return 3;
        }

        return  0;
    }

    /**
     * 所属题库
     */
    private String examName;

    /**
     *题目
     */
    private String questionTitle;

    /**
     * 题目解析
     */
    private String questionAnswer;

    private String questionOptionA;      //判断题 A.对    B.错
    private String questionOptionAIsRight;
    private String questionOptionB;
    private String questionOptionBIsRight;
    private String questionOptionC;
    private String questionOptionCIsRight;
    private String questionOptionD;
    private String questionOptionDIsRight;

    public String getQuestionOptionARightAnswer(){
        if(this.getQuestionType() != 3){
            if(this.getQuestionType() == 0) { // 单选
                if (this.getQuestionOptionAIsRight().equals("是")) {
                    return "A";
                } else if (this.getQuestionOptionBIsRight().equals("是")) {
                    return "B";
                } else if (this.getQuestionOptionCIsRight().equals("是")) {
                    return "C";
                } else if (this.getQuestionOptionDIsRight().equals("是")) {
                    return "D";
                }
            }
            if(this.getQuestionType() == 2) { //判断题目
                if (this.getQuestionOptionAIsRight().equals("是")) {
                    return "A";
                } else if (this.getQuestionOptionBIsRight().equals("是")) {
                    return "B";
                }
            }
            if(this.getQuestionType() == 1) { // 多选
                StringBuffer rightAnswer = new StringBuffer();
                if (StringUtils.isNotEmpty(this.getQuestionOptionAIsRight())&&
                        this.getQuestionOptionAIsRight().equals("是")) {
                    rightAnswer.append("A").append(",");

                }
                if (StringUtils.isNotEmpty(this.getQuestionOptionBIsRight())&&
                        this.getQuestionOptionBIsRight().equals("是")) {
                    rightAnswer.append("B").append(",");

                }
                if (StringUtils.isNotEmpty(this.getQuestionOptionCIsRight())&&
                        this.getQuestionOptionCIsRight().equals("是")) {
                    rightAnswer.append("C").append(",");
                }
                if (StringUtils.isNotEmpty(this.getQuestionOptionDIsRight())&&
                        this.getQuestionOptionDIsRight().equals("是")) {
                    rightAnswer.append("D").append(",");
                }
                return  rightAnswer.toString().substring(0,rightAnswer.length()-1);
            }
        }
        return this.questionOptionA;
    }
}
