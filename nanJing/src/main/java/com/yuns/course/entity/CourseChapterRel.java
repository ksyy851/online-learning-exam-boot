package com.yuns.course.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 *
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@TableName("course_chapter_rel")
public class CourseChapterRel extends Model<CourseChapterRel> {

    private static final long serialVersionUID = 1L;

    /**
     * 课程ID
     */
    @ApiParam(value = "课程ID")
   @ApiModelProperty(value = "课程ID")
    @TableId(value = "course_id")
    private Integer courseId;
    /**
     * 章节ID
     */
    @ApiParam(value = "章节ID")
    @ApiModelProperty(value = "章节ID")
    @TableField("chapter_id")
    private Integer chapterId;


    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getChapterId() {
        return chapterId;
    }

    public void setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
    }

    @Override
    protected Serializable pkVal() {
        return this.courseId;
    }

    @Override
    public String toString() {
        return "CourseChapterRel{" +
        ", courseId=" + courseId +
        ", chapterId=" + chapterId +
        "}";
    }
}
