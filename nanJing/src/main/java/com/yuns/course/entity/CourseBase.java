package com.yuns.course.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 课程基础表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Data
@TableName("course_base")
public class CourseBase extends Model<CourseBase> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 课程名称
     */
    @ApiParam(value = "课程名称")
   @ApiModelProperty(value = "课程名称")
    @TableField("course_name")
    private String courseName;
    /**
     * 积分
     */
    @ApiParam(value = "积分")
   @ApiModelProperty(value = "积分")
    private Integer integral;
    /**
     * 课程详情
     */
    @ApiParam(value = "课程详情")
   @ApiModelProperty(value = "课程详情")
    @TableField("course_info")
    private String courseInfo;
    /**
     * 上传时间
     */
    @ApiParam(value = "上传时间")
   @ApiModelProperty(value = "上传时间")
    @TableField("upload_time")
    private String uploadTime;
    /**
     * 课时
     */
    @ApiParam(value = "课时")
   @ApiModelProperty(value = "课时")
    private Integer period;

    /**
     * 图片文件ID
     */
    @ApiParam(value = "图片文件ID")
    @ApiModelProperty(value = "图片文件ID")
    private String fileNo;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
