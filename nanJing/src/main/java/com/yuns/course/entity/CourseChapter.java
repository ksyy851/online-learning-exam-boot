package com.yuns.course.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 课程章节表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Data
@TableName("course_chapter")
public class CourseChapter extends Model<CourseChapter> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private Integer period;
    /**
     * 章节名称
     */
    @ApiParam(value = "章节名称")
   @ApiModelProperty(value = "章节名称")
    @TableField("chapter_name")
    private String chapterName;
    /**
     * 章节讲师名称
     */
    @ApiParam(value = "章节讲师名称")
   @ApiModelProperty(value = "章节讲师名称")
    @TableField("chapter_teacher_name")
    private String chapterTeacherName;
    /**
     * 章节视频名称
     */
    @ApiParam(value = "章节视频名称")
   @ApiModelProperty(value = "章节视频名称")
    @TableField("chapter_file_name")
    private String chapterFileName;
    /**
     * 章节视频路径
     */
    @ApiParam(value = "章节视频路径")
   @ApiModelProperty(value = "章节视频路径")
    @TableField("chapter_file_path")
    private String chapterFilePath;

    /**
     * 章节视频时长
     */
    @ApiParam(value = "章节视频时长")
    @ApiModelProperty(value = "章节视频时长")
    @TableField("chapter_file_period")
    private String chapterFilePeriod;

    /**
     * 视频文件ID
     */
    @ApiParam(value = "视频文件ID")
    @ApiModelProperty(value = "视频文件ID")
    @TableField("file_id")
    private String fileId;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
