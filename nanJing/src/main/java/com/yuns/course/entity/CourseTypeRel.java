package com.yuns.course.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 课程-课程类型关联表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@TableName("course_type_rel")
public class CourseTypeRel extends Model<CourseTypeRel> {

    private static final long serialVersionUID = 1L;

    /**
     * 课程ID
     */
    @ApiParam(value = "课程ID")
   @ApiModelProperty(value = "课程ID")
    @TableId("course_id")
    private Integer courseId;
    /**
     * 课程类型ID
     */
    @ApiParam(value = "课程类型ID")
   @ApiModelProperty(value = "课程类型ID")
    @TableField("course_type_id")
    private Integer courseTypeId;


    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getCourseTypeId() {
        return courseTypeId;
    }

    public void setCourseTypeId(Integer courseTypeId) {
        this.courseTypeId = courseTypeId;
    }

    @Override
    protected Serializable pkVal() {
        return this.courseId;
    }

    @Override
    public String toString() {
        return "CourseTypeRel{" +
        ", courseId=" + courseId +
        ", courseTypeId=" + courseTypeId +
        "}";
    }
}
