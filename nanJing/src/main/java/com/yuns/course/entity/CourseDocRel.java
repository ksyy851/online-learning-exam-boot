package com.yuns.course.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 *
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@TableName("course_doc_rel")
public class CourseDocRel extends Model<CourseDocRel> {

    private static final long serialVersionUID = 1L;

    /**
     * 课程ID
     */
    @ApiParam(value = "课程ID")
   @ApiModelProperty(value = "课程ID")
    @TableId(value = "course_id")
    private Integer courseId;
    /**
     * 法律法规ID
     */
    @ApiParam(value = "法律法规ID")
   @ApiModelProperty(value = "法律法规ID")
    @TableField("doc_id")
    private Integer docId;


    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    @Override
    protected Serializable pkVal() {
        return this.courseId;
    }

    @Override
    public String toString() {
        return "CourseDocRel{" +
        ", courseId=" + courseId +
        ", docId=" + docId +
        "}";
    }
}
