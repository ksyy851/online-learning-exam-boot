package com.yuns.course.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 课程笔记表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
@Data
@TableName("course_note")
public class CourseNote extends Model<CourseNote> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
   @ApiModelProperty(value = "用户ID")
    @TableField("company_user_id")
    private String companyUserId;

    /**
     * 课程ID
     */
    @ApiParam(value = "课程ID")
    @ApiModelProperty(value = "课程ID")
    @TableField("course_id")
    private String courseId;
    /**
     * 章节ID
     */
    @ApiParam(value = "章节ID")
   @ApiModelProperty(value = "章节ID")
    @TableField("chapter_id")
    private String chapterId;
    /**
     * 笔记内容
     */
    @ApiParam(value = "笔记内容")
   @ApiModelProperty(value = "笔记内容")
    private String note;
    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
   @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private String createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
