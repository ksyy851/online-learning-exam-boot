package com.yuns.course.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 课程类型基础表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Data
@TableName("course_type")
public class CourseType extends Model<CourseType> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 课程名称
     */
    @ApiParam(value = "课程名称")
   @ApiModelProperty(value = "课程名称")
    private String name;

    /**
     * 备注
     */
    @ApiParam(value = "备注")
    @ApiModelProperty(value = "备注")
    private String remake;

    /**
     * 课程数量
     */
    @ApiParam(value = "课程数量")
    @ApiModelProperty(value = "课程数量")
    private Integer courseCount;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
