package com.yuns.course.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseBase;
import com.yuns.course.mapper.CourseBaseMapper;
import com.yuns.course.param.CourseParam;
import com.yuns.course.service.ICourseBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.course.vo.CourseVO;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 课程基础表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Service
public class CourseBaseServiceImpl extends ServiceImpl<CourseBaseMapper, CourseBase> implements ICourseBaseService {
    @Override
    public List<CourseBase> selectCourseBaseList(FrontPage frontPage) {
        return baseMapper.selectCourseBaseList(frontPage);
    }

    @Override
    public List<CourseVO> selectCourseList(CourseParam frontPage) {
        List<CourseVO> list = baseMapper.selectCourseList(frontPage);
        return list;
    }

    @Override
    public List<CourseVO> selectCourseList(Page<CourseVO> page, CourseParam frontPage) {
        return baseMapper.selectCourseList(page,frontPage);
    }

    @Override
    public CourseVO queryById(Integer id) {
        CourseVO vo=baseMapper.queryByIdForDoc(id);
        CourseVO chapter=baseMapper.queryByIdForChapter(id);
        vo.setChapterId(chapter.getChapterId());
        vo.setChapterName(chapter.getChapterName());
        vo.setChapterFileId(chapter.getChapterFileId());
        vo.setChapterFileName(chapter.getChapterFileName());
        vo.setChapterTeacherName(chapter.getChapterTeacherName());
        vo.setChapterFileUrl(chapter.getChapterFileUrl());
        vo.setChapterPeriod(chapter.getChapterPeriod());
        return vo;
    }
}
