package com.yuns.course.service.impl;

import com.yuns.course.entity.CourseWatchRecord;
import com.yuns.course.mapper.CourseWatchRecordMapper;
import com.yuns.course.service.ICourseWatchRecordService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.param.FrontPage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 课程-章节观看记录表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-13
 */
@Service
public class CourseWatchRecordServiceImpl extends ServiceImpl<CourseWatchRecordMapper, CourseWatchRecord> implements ICourseWatchRecordService {
        @Override
        public List<CourseWatchRecord> selectCourseWatchRecordList(CourseWatchRecord frontPage) {
        return   baseMapper.selectCourseWatchRecordList(frontPage);
     }
}
