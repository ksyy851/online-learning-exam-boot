package com.yuns.course.service;

import com.yuns.course.entity.CourseWatchRecord;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 课程-章节观看记录表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-13
 */
public interface ICourseWatchRecordService extends IService<CourseWatchRecord> {
        List<CourseWatchRecord> selectCourseWatchRecordList(CourseWatchRecord frontPage);
}
