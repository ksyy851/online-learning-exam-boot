package com.yuns.course.service.impl;

import com.yuns.course.entity.CourseNote;
import com.yuns.course.mapper.CourseNoteMapper;
import com.yuns.course.service.ICourseNoteService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.course.vo.CourseNoteVO;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 课程笔记表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
@Service
public class CourseNoteServiceImpl extends ServiceImpl<CourseNoteMapper, CourseNote> implements ICourseNoteService {
        @Override
        public List<CourseNoteVO> selectCourseNoteList(CourseNote model) {
        return   baseMapper.selectCourseNoteList(model);
     }
}
