package com.yuns.course.service.impl;

import com.yuns.course.entity.CourseChapter;
import com.yuns.course.mapper.CourseChapterMapper;
import com.yuns.course.service.ICourseChapterService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程章节表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Service
public class CourseChapterServiceImpl extends ServiceImpl<CourseChapterMapper, CourseChapter> implements ICourseChapterService {
        @Override
        public List<CourseChapter> selectCourseChapterList(FrontPage frontPage) {
        return   baseMapper.selectCourseChapterList(frontPage);
     }
}
