package com.yuns.course.service.impl;

import com.yuns.course.entity.CourseInfo;
import com.yuns.course.mapper.CourseInfoMapper;
import com.yuns.course.service.ICourseInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程详情表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Service
public class CourseInfoServiceImpl extends ServiceImpl<CourseInfoMapper, CourseInfo> implements ICourseInfoService {
        @Override
        public List<CourseInfo> selectCourseInfoList(FrontPage frontPage) {
        return   baseMapper.selectCourseInfoList(frontPage);
     }
}
