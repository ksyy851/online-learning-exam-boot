package com.yuns.course.service;

import com.yuns.course.entity.CourseChapterRel;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface ICourseChapterRelService extends IService<CourseChapterRel> {
        List<CourseChapterRel> selectCourseChapterRelList(FrontPage frontPage);
}
