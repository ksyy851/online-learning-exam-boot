package com.yuns.course.service;

import com.yuns.course.entity.CourseInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程详情表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface ICourseInfoService extends IService<CourseInfo> {
        List<CourseInfo> selectCourseInfoList(FrontPage frontPage);
}
