package com.yuns.course.service;

import com.yuns.course.entity.CourseTypeRel;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程-课程类型关联表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface ICourseTypeRelService extends IService<CourseTypeRel> {
        List<CourseTypeRel> selectCourseTypeRelList(FrontPage frontPage);
}
