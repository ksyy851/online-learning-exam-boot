package com.yuns.course.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.dto.CourseDto;
import com.yuns.course.param.CourseParam;
import com.yuns.course.vo.CourseVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 课程基础表 服务类
 * </p>
 *
 * @author liuChengWen
 * @since 2020-10-26
 */
public interface ICourseService {

    boolean insert(CourseDto model);

    boolean up(CourseDto model);

    boolean deleteById(Integer id);

    List<CourseVO> selectCourseList(CourseParam frontPage);

    List<CourseVO> selectCourseList(  Page<CourseVO> page,CourseParam frontPage);

    void exportList(CourseParam courseParam, HttpServletResponse response);

    CourseVO queryById(Integer id);

}
