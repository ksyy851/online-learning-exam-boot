package com.yuns.course.service;

import com.yuns.course.entity.CourseNote;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.course.vo.CourseNoteVO;

import java.util.List;

/**
 * <p>
 * 课程笔记表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
public interface ICourseNoteService extends IService<CourseNote> {
        List<CourseNoteVO> selectCourseNoteList(CourseNote model);
}
