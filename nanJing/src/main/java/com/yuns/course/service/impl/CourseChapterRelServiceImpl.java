package com.yuns.course.service.impl;

import com.yuns.course.entity.CourseChapterRel;
import com.yuns.course.mapper.CourseChapterRelMapper;
import com.yuns.course.service.ICourseChapterRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Service
public class CourseChapterRelServiceImpl extends ServiceImpl<CourseChapterRelMapper, CourseChapterRel> implements ICourseChapterRelService {
        @Override
        public List<CourseChapterRel> selectCourseChapterRelList(FrontPage frontPage) {
        return   baseMapper.selectCourseChapterRelList(frontPage);
     }
}
