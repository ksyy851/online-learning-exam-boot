package com.yuns.course.service;

import com.yuns.course.entity.CourseChapter;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程章节表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface ICourseChapterService extends IService<CourseChapter> {
        List<CourseChapter> selectCourseChapterList(FrontPage frontPage);
}
