package com.yuns.course.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseType;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import com.yuns.web.sys.param.BasePage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 课程类型基础表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface ICourseTypeService extends IService<CourseType> {
    List<CourseType> selectCourseTypeList(BasePage frontPage);

    List<CourseType> selectCourseTypeList(Page<CourseType> page,BasePage frontPage);

    Object importFile(Integer type, MultipartFile file) throws IOException;
}
