package com.yuns.course.service.impl;

import com.yuns.course.entity.CourseTypeRel;
import com.yuns.course.mapper.CourseTypeRelMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.course.service.ICourseTypeRelService;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程-课程类型关联表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Service
public class CourseTypeRelServiceImpl extends ServiceImpl<CourseTypeRelMapper, CourseTypeRel> implements ICourseTypeRelService {
        @Override
        public List<CourseTypeRel> selectCourseTypeRelList(FrontPage frontPage) {
        return   baseMapper.selectCourseTypeRelList(frontPage);
     }
}
