package com.yuns.course.service.impl;

import com.yuns.course.entity.CourseDocRel;
import com.yuns.course.mapper.CourseDocRelMapper;
import com.yuns.course.service.ICourseDocRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
@Service
public class CourseDocRelServiceImpl extends ServiceImpl<CourseDocRelMapper, CourseDocRel> implements ICourseDocRelService {
        @Override
        public List<CourseDocRel> selectCourseDocRelList(FrontPage frontPage) {
        return   baseMapper.selectCourseDocRelList(frontPage);
     }
}
