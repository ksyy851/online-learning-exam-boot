package com.yuns.course.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseBase;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.course.param.CourseParam;
import com.yuns.course.vo.CourseVO;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 课程基础表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-26
 */
public interface ICourseBaseService extends IService<CourseBase> {
        List<CourseBase> selectCourseBaseList(FrontPage frontPage);

        List<CourseVO>  selectCourseList(CourseParam frontPage);

        List<CourseVO>  selectCourseList(Page<CourseVO> page,CourseParam frontPage);
        CourseVO queryById(Integer id);
}
