package com.yuns.course.vo;

import lombok.Data;

@Data
public class UploadVideoVO {

    //文件编号
    private String fileNo;
    //文件名称（带后缀全称）
    private String fileName;

    //文件类型 doc、txt 等
    private String fileType;
    //文件路径
    private String filePath;
    //数据大小
    private String fileSize;
    //时长
    private String filePeriod;
}
