package com.yuns.course.controller;

import com.yuns.course.service.ICourseChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseChapter;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CourseChapterDto;


/**
 * @author ${author}
 * @since 2020-10-26
 */
@Api(tags = "课程章节表 - Controller")
@RestController
@RequestMapping("/courseChapter")
public class CourseChapterController {

    @Autowired
    private ICourseChapterService iCourseChapterService;

    @ApiOperation(value = "添加 - course_chapter")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CourseChapter model) {
        //  CourseChapter entity=DtoUtil.convertObject(model,CourseChapter.class);
        boolean flag = iCourseChapterService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - course_chapter")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody CourseChapter model) {
        //CourseChapter entity=DtoUtil.convertObject(model,CourseChapter.class);
        boolean flag = iCourseChapterService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - course_chapter")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iCourseChapterService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - course_chapter")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<CourseChapter> frontPage) {
        if (!frontPage.is_search()) {
            Page<CourseChapter> page = new Page<CourseChapter>();
            Page<CourseChapter> list = page.setRecords(iCourseChapterService.selectCourseChapterList(frontPage));
            return ResultJson.ok(list);
        }
        Page<CourseChapter> page = new Page<CourseChapter>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<CourseChapter> list = page.setRecords(iCourseChapterService.selectCourseChapterList(frontPage));
        return ResultJson.ok(list);
    }
}

