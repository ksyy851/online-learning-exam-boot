package com.yuns.course.controller;

import com.yuns.course.service.ICourseDocRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseDocRel;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CourseDocRelDto;


/**
 * @author ${author}
 * @since 2020-10-26
 */
@Api(tags = " - Controller" )
    @RestController
@RequestMapping("/courseDocRel" )
    public class CourseDocRelController {

    @Autowired
    private ICourseDocRelService iCourseDocRelService;

    @ApiOperation(value = "添加 - course_doc_rel" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CourseDocRel model){
            //  CourseDocRel entity=DtoUtil.convertObject(model,CourseDocRel.class);
            boolean flag=iCourseDocRelService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - course_doc_rel" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody CourseDocRel model){
            //CourseDocRel entity=DtoUtil.convertObject(model,CourseDocRel.class);
            boolean flag=iCourseDocRelService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - course_doc_rel" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iCourseDocRelService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - course_doc_rel" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<CourseDocRel> frontPage){
            if(!frontPage.is_search()){
            Page<CourseDocRel> page=new Page<CourseDocRel>();
            Page<CourseDocRel> list=page.setRecords(iCourseDocRelService.selectCourseDocRelList(frontPage));
            return ResultJson.ok(list);
            }
            Page<CourseDocRel> page=new Page<CourseDocRel>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<CourseDocRel> list=page.setRecords(iCourseDocRelService.selectCourseDocRelList(frontPage));
            return ResultJson.ok(list);
            }
        }

