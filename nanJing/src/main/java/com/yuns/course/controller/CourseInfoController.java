package com.yuns.course.controller;

import com.yuns.course.service.ICourseInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseInfo;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CourseInfoDto;


/**
 * @author ${author}
 * @since 2020-10-26
 */
@Api(tags = "课程详情表 - Controller")
@RestController
@RequestMapping("/courseInfo")
public class CourseInfoController {

    @Autowired
    private ICourseInfoService iCourseInfoService;

    @ApiOperation(value = "添加 - course_info")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CourseInfo model) {
        //  CourseInfo entity=DtoUtil.convertObject(model,CourseInfo.class);
        boolean flag = iCourseInfoService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - course_info")
    @PostMapping(value = "update")
    public ResultJson update(@RequestBody CourseInfo model) {
        //CourseInfo entity=DtoUtil.convertObject(model,CourseInfo.class);
        boolean flag = iCourseInfoService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - course_info")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iCourseInfoService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - course_info")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<CourseInfo> frontPage) {
        if (!frontPage.is_search()) {
            Page<CourseInfo> page = new Page<CourseInfo>();
            Page<CourseInfo> list = page.setRecords(iCourseInfoService.selectCourseInfoList(frontPage));
            return ResultJson.ok(list);
        }
        Page<CourseInfo> page = new Page<CourseInfo>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<CourseInfo> list = page.setRecords(iCourseInfoService.selectCourseInfoList(frontPage));
        return ResultJson.ok(list);
    }
}

