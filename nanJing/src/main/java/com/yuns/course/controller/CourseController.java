package com.yuns.course.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.dto.CourseDto;
import com.yuns.course.entity.CourseChapter;
import com.yuns.course.entity.CourseInfo;
import com.yuns.course.param.CourseParam;
import com.yuns.course.service.ICourseBaseService;
import com.yuns.course.service.ICourseInfoService;
import com.yuns.course.service.ICourseService;
import com.yuns.course.vo.CourseVO;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author liuChengWen
 * @since 2020-10-26
 */
@Api(tags = "课程管理 - Controller" )
@RestController
@RequestMapping("/course" )
public class CourseController {

    @Autowired
    private ICourseService iCourseService;
    @Autowired
    private ICourseBaseService iCourseBaseService;

    @Autowired
    private ICourseInfoService iCourseInfoService;

    @ApiOperation(value = "添加 - insert" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CourseDto model){
        boolean flag=iCourseService.insert(model);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "保存 - up" )
    @PostMapping(value = "up" )
    public ResultJson up(@RequestBody CourseDto model){
        boolean flag=iCourseService.up(model);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - delete" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
        boolean flag=iCourseService.deleteById(id);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "课程列表查询 - query" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody CourseParam courseParam){
        if (!courseParam.is_search()) {
            Page<CourseVO> page = new Page<CourseVO>();
            Page<CourseVO> list = page.setRecords(iCourseService.selectCourseList(courseParam));
            return ResultJson.ok(list);
        }
        Page<CourseVO> page = new Page<CourseVO>(courseParam.getPageNumber(), courseParam.getPageSize());
        Page<CourseVO> list = page.setRecords(iCourseService.selectCourseList(page,courseParam));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "导出课程列表 - exportList" )
    @PostMapping(value = "exportList" )
    public ResultJson exportList(@RequestBody CourseParam courseParam, HttpServletResponse response) {
        iCourseService.exportList(courseParam,response);
        return ResultJson.ok();
    }

    @ApiOperation(value = "课程上下架 - updateForType" )
    @PostMapping(value = "updateForType" )
    public ResultJson updateForType(@RequestBody CourseInfo model){
        boolean flag = iCourseInfoService.updateById(model);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "编辑 - edit" )
    @PostMapping(value = "edit/{id}")
    public ResultJson edit(@PathVariable("id") Integer id){
        return ResultJson.ok(iCourseService.queryById(id));
    }

}
