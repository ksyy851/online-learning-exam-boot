package com.yuns.course.controller;

import com.yuns.course.service.ICourseChapterRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseChapterRel;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CourseChapterRelDto;


/**
 * @author ${author}
 * @since 2020-10-26
 */
@Api(tags = " - Controller" )
    @RestController
@RequestMapping("/courseChapterRel" )
    public class CourseChapterRelController {

    @Autowired
    private ICourseChapterRelService iCourseChapterRelService;

    @ApiOperation(value = "添加 - course_chapter_rel" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CourseChapterRel model){
            //  CourseChapterRel entity=DtoUtil.convertObject(model,CourseChapterRel.class);
            boolean flag=iCourseChapterRelService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - course_chapter_rel" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody CourseChapterRel model){
            //CourseChapterRel entity=DtoUtil.convertObject(model,CourseChapterRel.class);
            boolean flag=iCourseChapterRelService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - course_chapter_rel" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iCourseChapterRelService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - course_chapter_rel" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<CourseChapterRel> frontPage){
            if(!frontPage.is_search()){
            Page<CourseChapterRel> page=new Page<CourseChapterRel>();
            Page<CourseChapterRel> list=page.setRecords(iCourseChapterRelService.selectCourseChapterRelList(frontPage));
            return ResultJson.ok(list);
            }
            Page<CourseChapterRel> page=new Page<CourseChapterRel>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<CourseChapterRel> list=page.setRecords(iCourseChapterRelService.selectCourseChapterRelList(frontPage));
            return ResultJson.ok(list);
            }
        }

