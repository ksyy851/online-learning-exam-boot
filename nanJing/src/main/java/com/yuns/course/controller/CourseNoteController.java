package com.yuns.course.controller;


import com.yuns.course.service.ICourseNoteService;
import com.yuns.util.ResultJson;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import com.yuns.course.entity.CourseNote;
import org.springframework.web.bind.annotation.*;



/**
 * @author ${author}
 * @since 2020-11-12
 */
@Api(tags = "课堂笔记相关 - Controller")
@RestController
@RequestMapping("/courseNote")
public class CourseNoteController {

    @Autowired
    private ICourseNoteService iCourseNoteService;

    @ApiOperation(value = "添加 - course_note")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CourseNote model) {
        //  CourseNote entity=DtoUtil.convertObject(model,CourseNote.class);
        boolean flag = iCourseNoteService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改笔记 - course_note")
    @PostMapping(value = "update")
    public ResultJson update(@RequestBody CourseNote model) {
        //CourseNote entity=DtoUtil.convertObject(model,CourseNote.class);
        boolean flag = iCourseNoteService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - course_note")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iCourseNoteService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "笔记列表查询 - course_note")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody CourseNote model) {
        return ResultJson.ok(iCourseNoteService.selectCourseNoteList(model));
    }
}

