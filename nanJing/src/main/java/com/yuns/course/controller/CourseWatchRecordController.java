package com.yuns.course.controller;


import com.yuns.course.service.ICourseWatchRecordService;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseWatchRecord;
import org.springframework.web.bind.annotation.*;

//import com.yuns.com.yuns.web.sys.dto.CourseWatchRecordDto;


/**
 * @author ${author}
 * @since 2020-11-13
 */
@Api(tags = "课程-章节观看记录表 - Controller")
@RestController
@RequestMapping("/courseWatchRecord")
public class CourseWatchRecordController {

    @Autowired
    private ICourseWatchRecordService iCourseWatchRecordService;

    @ApiOperation(value = "添加 - course_watch_record")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CourseWatchRecord model) {
        //  CourseWatchRecord entity=DtoUtil.convertObject(model,CourseWatchRecord.class);
        boolean flag = iCourseWatchRecordService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - course_watch_record")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody CourseWatchRecord model) {
        //CourseWatchRecord entity=DtoUtil.convertObject(model,CourseWatchRecord.class);
        boolean flag = iCourseWatchRecordService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - course_watch_record")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable Integer id) {
        boolean flag = iCourseWatchRecordService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - course_watch_record")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody CourseWatchRecord frontPage) {
        return ResultJson.ok(iCourseWatchRecordService.selectCourseWatchRecordList(frontPage));
    }
}

