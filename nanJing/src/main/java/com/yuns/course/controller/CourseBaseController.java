package com.yuns.course.controller;

import com.yuns.course.service.ICourseBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseBase;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CourseBaseDto;


/**
 * @author ${author}
 * @since 2020-10-26
 */
@Api(tags = "课程基础表 - Controller" )
    @RestController
@RequestMapping("/courseBase" )
    public class CourseBaseController {

    @Autowired
    private ICourseBaseService iCourseBaseService;

    @ApiOperation(value = "添加 - course_base" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CourseBase model){
            //  CourseBase entity=DtoUtil.convertObject(model,CourseBase.class);
            boolean flag=iCourseBaseService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - course_base" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody CourseBase model){
            //CourseBase entity=DtoUtil.convertObject(model,CourseBase.class);
            boolean flag=iCourseBaseService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - course_base" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iCourseBaseService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - course_base" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<CourseBase> frontPage){
            if(!frontPage.is_search()){
            Page<CourseBase> page=new Page<CourseBase>();
            Page<CourseBase> list=page.setRecords(iCourseBaseService.selectCourseBaseList(frontPage));
            return ResultJson.ok(list);
            }
            Page<CourseBase> page=new Page<CourseBase>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<CourseBase> list=page.setRecords(iCourseBaseService.selectCourseBaseList(frontPage));
            return ResultJson.ok(list);
            }
        }

