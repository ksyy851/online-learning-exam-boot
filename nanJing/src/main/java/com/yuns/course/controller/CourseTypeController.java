package com.yuns.course.controller;

import com.yuns.course.entity.CourseInfo;
import com.yuns.course.service.ICourseTypeService;
import com.yuns.web.sys.param.BasePage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseType;
import com.yuns.web.param.QueryParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

//import com.yuns.web.dto.CourseTypeDto;


/**
 * @author ${author}
 * @since 2020-10-26
 */
@Api(tags = "课程类型基础表 - Controller")
@RestController
@RequestMapping("/courseType")
public class CourseTypeController {

    @Autowired
    private ICourseTypeService iCourseTypeService;

    @ApiOperation(value = "添加 - insert")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody CourseType model) {
        //  CourseType entity=DtoUtil.convertObject(model,CourseType.class);
        boolean flag = iCourseTypeService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - up")
    @PostMapping(value = "up")
    public ResultJson update(@RequestBody CourseType model) {
        //CourseType entity=DtoUtil.convertObject(model,CourseType.class);
        boolean flag = iCourseTypeService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - delete")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iCourseTypeService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - query")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody BasePage<CourseType> frontPage) {
        if(!frontPage.is_search()){
            Page<CourseType> page = new Page<CourseType>();
            Page<CourseType> list = page.setRecords(iCourseTypeService.selectCourseTypeList(frontPage));
            return ResultJson.ok(list);
        }
        Page<CourseType> page = new Page<CourseType>(frontPage.getPageNumber(),frontPage.getPageSize());
        Page<CourseType> list = page.setRecords(iCourseTypeService.selectCourseTypeList(page,frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "编辑 - edit")
    @PostMapping(value = "edit/{id}")
    public ResultJson edit(@PathVariable("id") Integer id) {
        return ResultJson.ok(iCourseTypeService.selectById(id));
    }

    @ApiOperation(value = "文件导入(0:课程类型导入;1:法律法规导入;2:视频资料导入;3:题库;4:题目;5重点排污企业导入;6:警示教育企业导入) - importFile")
    @RequestMapping(value = "importFile")
    public ResultJson importFile(Integer type, MultipartFile file) throws IOException {
        return ResultJson.ok(iCourseTypeService.importFile(type,file));
    }

}

