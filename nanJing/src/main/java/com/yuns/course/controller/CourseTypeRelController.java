package com.yuns.course.controller;

import com.yuns.course.service.ICourseTypeRelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.course.entity.CourseTypeRel;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.CourseTypeRelDto;


/**
 * @author ${author}
 * @since 2020-10-26
 */
@Api(tags = "课程-课程类型关联表 - Controller" )
    @RestController
@RequestMapping("/courseTypeRel" )
    public class CourseTypeRelController {

    @Autowired
    private ICourseTypeRelService iCourseTypeRelService;

    @ApiOperation(value = "添加 - course_type_rel" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody CourseTypeRel model){
            //  CourseTypeRel entity=DtoUtil.convertObject(model,CourseTypeRel.class);
            boolean flag=iCourseTypeRelService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - course_type_rel" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody CourseTypeRel model){
            //CourseTypeRel entity=DtoUtil.convertObject(model,CourseTypeRel.class);
            boolean flag=iCourseTypeRelService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - course_type_rel" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iCourseTypeRelService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - course_type_rel" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<CourseTypeRel> frontPage){
            if(!frontPage.is_search()){
            Page<CourseTypeRel> page=new Page<CourseTypeRel>();
            Page<CourseTypeRel> list=page.setRecords(iCourseTypeRelService.selectCourseTypeRelList(frontPage));
            return ResultJson.ok(list);
            }
            Page<CourseTypeRel> page=new Page<CourseTypeRel>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<CourseTypeRel> list=page.setRecords(iCourseTypeRelService.selectCourseTypeRelList(frontPage));
            return ResultJson.ok(list);
            }
        }

