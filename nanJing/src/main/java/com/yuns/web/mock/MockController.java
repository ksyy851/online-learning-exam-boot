package com.yuns.web.mock;

import com.yuns.util.ResultJson;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
@Slf4j
public class MockController {
	/**
	   * 实时磁盘监控
	 * @param request
	 * @param response
	 * @return
	 */
	@GetMapping("/queryDiskInfo")
	public ResultJson<List<Map<String,Object>>> queryDiskInfo(HttpServletRequest request, HttpServletResponse response){
		ResultJson<List<Map<String,Object>>> res = new ResultJson<>();
		try {
			// 当前文件系统类
	        FileSystemView fsv = FileSystemView.getFileSystemView();
	        // 列出所有windows 磁盘
	        File[] fs = File.listRoots();
	        log.info("查询磁盘信息:"+fs.length+"个");
	        List<Map<String,Object>> list = new ArrayList<>();

	        for (int i = 0; i < fs.length; i++) {
	        	if(fs[i].getTotalSpace()==0) {
	        		continue;
	        	}
	        	Map<String,Object> map = new HashMap<>();
	        	map.put("name", fsv.getSystemDisplayName(fs[i]));
	        	map.put("max", fs[i].getTotalSpace());
	        	map.put("rest", fs[i].getFreeSpace());
	        	map.put("restPPT", fs[i].getFreeSpace()*100/fs[i].getTotalSpace());
	        	list.add(map);
	        	log.info(map.toString());
	        }
	        res.ok(list,"查询成功");
		} catch (Exception e) {
			res.error("查询失败"+e.getMessage());
		}
		return res;
	}
	/**
	 * 读取json格式文件
	 * @param jsonSrc
	 * @return
	 */
	private String readJson(String jsonSrc) {
		String json = "";
		try {
			//File jsonFile = ResourceUtils.getFile(jsonSrc);
			//json = FileUtils.re.readFileToString(jsonFile);
			//换个写法，解决springboot读取jar包中文件的问题
			InputStream stream = getClass().getClassLoader().getResourceAsStream(jsonSrc.replace("classpath:", ""));
			json = IOUtils.toString(stream);
		} catch (IOException e) {
			log.error(e.getMessage(),e);
		}
		return json;
	}
}
