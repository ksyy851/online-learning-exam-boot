package com.yuns.web.dto;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 操作日志
 * </p>
 *
 * @author luwenchao
 * @since 2019-10-28
 */
public class OperationLogDto {

    /**
     * id
     */
    @ApiModelProperty(value = "id")
    private Integer id;
    /**
     * 操作类型
     */
    @ApiModelProperty(value = "操作类型")
    private String type;
    /**
     * 操作内容
     */
    @ApiModelProperty(value = "操作内容")
    @TableField("operation_value")
    private String operationValue;
    /**
     * 操作员
     */
    @ApiModelProperty(value = "操作员")
    @TableField("create_by")
    private String createBy;
    /**
     * 操作模块
     */
    @ApiModelProperty(value = "操作模块")
    @TableField("operation_model")
    private String operationModel;
    /**
     * 操作时间
     */
    @ApiModelProperty(value = "操作时间")
    @TableField("create_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOperationValue() {
        return operationValue;
    }

    public void setOperationValue(String operationValue) {
        this.operationValue = operationValue;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getOperationModel() {
        return operationModel;
    }

    public void setOperationModel(String operationModel) {
        this.operationModel = operationModel;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}
