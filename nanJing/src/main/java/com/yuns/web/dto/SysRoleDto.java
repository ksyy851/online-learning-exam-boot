package com.yuns.web.dto;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 角色表
 * </p>
 *
 * @author luwenchao
 * @since 2018-07-26
 */
public class SysRoleDto {

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "角色ID")
    private Long id;
    /**
     * 父级角色ID
     */
    @ApiModelProperty(value = "父级角色ID")
    @TableField("parent_tr_id")
    private Long parentTrId;
    /**
     * 角色名称
     */
    @ApiModelProperty(value = "角色名称")
    @TableField("role_name")
    private String roleName;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField("created_by")
    private String createdBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField("created_date")
    private Date createdDate;
    /**
     * 最后修改人
     */
    @ApiModelProperty(value = "最后修改人")
    @TableField("last_modified_by")
    private String lastModifiedBy;
    /**
     * 最后修改时间
     */
    @ApiModelProperty(value = "最后修改时间")
    @TableField("last_modified_date")
    private Date lastModifiedDate;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String description;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentTrId() {
        return parentTrId;
    }

    public void setParentTrId(Long parentTrId) {
        this.parentTrId = parentTrId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
