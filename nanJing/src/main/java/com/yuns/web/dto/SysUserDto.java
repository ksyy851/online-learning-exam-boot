package com.yuns.web.dto;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author luwenchao
 * @since 2019-10-29
 */
public class SysUserDto {

    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    private Long id;
    /**
     * 登录帐号
     */
    @ApiModelProperty(value = "登录帐号")
    @TableField("user_name")
    private String userName;
    /**
     * 用户编号
     */
    @ApiModelProperty(value = "用户编号")
    @TableField("user_number")
    private String userNumber;
    @ApiModelProperty("表类型id")
    private Integer waterId;

    public Integer getWaterId() {
        return waterId;
    }

    public void setWaterId(Integer waterId) {
        this.waterId = waterId;
    }
    /**
     * 表编号
     */
    @ApiParam(value = "表编号")
    @TableField("table_number")
    private String tableNumber;
    /**
     * 余额
     */
    @ApiModelProperty(value = "余额")
    @TableField("balance")
    private Double balance;
    /**
     * 地址
     */
    @ApiParam(value = "地址")
    @TableField("address")
    private String address;
    /**
     * 用户密码
     */
    @ApiModelProperty(value = "用户密码")
    private String password;
    /**
     * 简称
     */
    @ApiModelProperty(value = "简称")
    @TableField("short_name")
    private String shortName;
    /**
     * 全称
     */
    @ApiModelProperty(value = "全称")
    @TableField("full_name")
    private String fullName;
    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号")
    private String phone;
    @ApiModelProperty("手册id")
    @TableField("handbook_id")
    private Integer handbookId;

    @ApiModelProperty("用水类型")
    private Integer waterType;
    @ApiModelProperty("表口径")
    private Integer caliberId;
    /**
     * 电子邮箱
     */
    @ApiModelProperty(value = "电子邮箱")
    private String email;
    /**
     * 用户身份(0:用水用户 1:平台用户)
     */
    @ApiModelProperty(value = "用户身份(0:用水用户 1:平台用户)")
    private String type;
    /**
     * 登录时间
     */
    @ApiModelProperty(value = "登录时间")
    @TableField("login_date")
    private Date loginDate;
    /**
     * 上次登录时间
     */
    @ApiModelProperty(value = "上次登录时间")
    @TableField("last_login_date")
    private Date lastLoginDate;
    /**
     * 登录次数
     */
    @ApiModelProperty(value = "登录次数")
    private Long count;
    /**
     * 组织id
     */
    @ApiModelProperty(value = "组织id")
    @TableField("dept_id")
    private Long deptId;

    @ApiModelProperty("磁卡卡号")
    @TableField("magnetic_number")
    private String magneticNumber;
    /**
     * 身份证
     */
    @ApiModelProperty(value = "身份证")
    @TableField("brith_id")
    private String brithId;
    /**
     * 性别
     */
    @ApiModelProperty(value = "性别")
    private String sex;
    /**
     * 用户头像
     */
    @ApiModelProperty(value = "用户头像")
    private String pic;
    /**
     * 设备类型
     */
    @ApiModelProperty(value = "设备类型")
    @TableField("device_type")
    private String deviceType;
    /**
     * 所在区域
     */
    @ApiModelProperty(value = "所在区域")
    @TableField("area_id")
    private Integer areaId;
    /**
     * 用户状态
     */
    @ApiModelProperty(value = "用户状态")
    private String status;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")
    @TableField("created_by")
    private String createdBy;
    /**
     * 创建日期
     */
    @ApiModelProperty(value = "创建日期")
    @TableField("created_date")
    private Date createdDate;
    /**
     * 最后更新人
     */
    @ApiModelProperty(value = "最后更新人")
    @TableField("last_modified_by")
    private String lastModifiedBy;
    /**
     * 更新日期
     */
    @ApiModelProperty(value = "更新日期")
    @TableField("last_modified_date")
    private Date lastModifiedDate;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 盐值
     */
    @ApiModelProperty(value = "盐值")
    private String salt;
    /**
     * 经度
     */
    @ApiModelProperty(value = "经度")
    private String longitude;

    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度")
    private String latitude;
    /**
     * 手机绑定时间
     */
    @ApiModelProperty(value = "手机绑定时间")
    @TableField("phone_bindtime")
    private Date phoneBindtime;
    /**
     * 简介
     */
    @ApiModelProperty(value = "简介")
    private String profile;
    /**
     * 设备
     */
    @ApiModelProperty(value = "设备")
    private String instrument;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getFullName() {
        return fullName;
    }

    public String getMagneticNumber() {
        return magneticNumber;
    }

    public void setMagneticNumber(String magneticNumber) {
        this.magneticNumber = magneticNumber;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getLoginDate() {
        return loginDate;
    }

    public void setLoginDate(Date loginDate) {
        this.loginDate = loginDate;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public String getBrithId() {
        return brithId;
    }

    public void setBrithId(String brithId) {
        this.brithId = brithId;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Date getPhoneBindtime() {
        return phoneBindtime;
    }

    public void setPhoneBindtime(Date phoneBindtime) {
        this.phoneBindtime = phoneBindtime;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public Integer getHandbookId() {
        return handbookId;
    }

    public void setHandbookId(Integer handbookId) {
        this.handbookId = handbookId;
    }

    public Integer getWaterType() {
        return waterType;
    }

    public void setWaterType(Integer waterType) {
        this.waterType = waterType;
    }

    public Integer getCaliberId() {
        return caliberId;
    }

    public void setCaliberId(Integer caliberId) {
        this.caliberId = caliberId;
    }
}
