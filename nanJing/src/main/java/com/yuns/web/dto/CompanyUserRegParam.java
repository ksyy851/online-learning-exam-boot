package com.yuns.web.dto;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 企业注册入参
 */
@Data
public class CompanyUserRegParam {

    @ApiParam(value = "企业ID")
    @ApiModelProperty(value = "企业ID" ,required = true)
    private Integer companyId;

    @ApiParam(value = "姓名")
    @ApiModelProperty(value = "姓名",required = true)
    private String userName;

    @ApiParam(value = "昵称")
    @ApiModelProperty(value = "昵称")
    private String nickname;

    @ApiParam(value = "职位")
    @ApiModelProperty(value = "职位")
    private String job;

    @ApiParam(value = "密码")
    @ApiModelProperty(value = "密码",required = true)
    private String password;

    /**
     * 微信号
     */
    @ApiParam(value = "微信号")
    @ApiModelProperty(value = "微信号")
    private String wechat;
    /**
     * 电话号码
     */
    @ApiParam(value = "电话号码",required = true)
    @ApiModelProperty(value = "电话号码")
    private String tel;

    /**
     * 身份证
     */
    @ApiParam(value = "身份证",required = true)
    @ApiModelProperty(value = "身份证")
    @TableField("id_card")
    private String idCard;
}
