package com.yuns.web.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: luchao
 * Date: 2019-02-03
 * Time: 12:38
 */
@Data
public class QueryParam implements Serializable {


    @ApiModelProperty(value = "开始时间", required = false)
    private String bgTm;

    @ApiModelProperty(value = "结束时间", required = false)
    private String endTm;

    /**
     * 数据标签名称
     */
    @ApiModelProperty(value = "查询条件")
    private String keywords;

    /**
     * 当前用户id
     */
    @ApiModelProperty(value = "当前用户id")
    private Integer userId;



    /**
     * 数据标签名称
     */
    @ApiModelProperty(value = "查询类型")
    private Integer type;

    /**
     * 时间周期
     */
    @ApiModelProperty(value = "时间周期")
    private Integer noUpTime;


    /**
     * 状态
     */
    @ApiModelProperty(value = "状态")
    private Integer status;

    /**
     * 直播ID
     */
    @ApiModelProperty(value = "直播ID")
    private Integer liveId;

}
