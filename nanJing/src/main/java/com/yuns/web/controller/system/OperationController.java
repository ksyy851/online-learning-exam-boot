package com.yuns.web.controller.system;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.OperationLog;
import com.yuns.model.service.OperationLogService;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/userOperationLog")
@Api(tags = "操作日志-OperationLogController")
public class OperationController {

    @Autowired
    private OperationLogService iOperationLogService;

    /**
     * 查询操作日志
     *
     * @return
     */
    @ApiOperation(value = "查询操作日志")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage frontPage) {
        Page<OperationLog> list = iOperationLogService.selectOperationLogList(frontPage);
        return ResultJson.ok(list);
    }

}

