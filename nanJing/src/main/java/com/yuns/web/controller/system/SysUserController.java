package com.yuns.web.controller.system;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.aspect.SyslogAnno;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.service.ICompanyUserBaseService;
import com.yuns.model.entity.SysDept;
import com.yuns.model.entity.SysUser;

import com.yuns.model.service.SysUserService;
import com.yuns.util.ResultJson;
import com.yuns.web.dto.CompanyUserRegParam;
import com.yuns.web.param.FrontPage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/sysUser")
@Api(tags = "用户权限")
public class SysUserController {


    @Autowired
    private SysUserService isys_userService;

    @Autowired
    private ICompanyUserBaseService companyUserBaseService;

    @PostMapping(value = "insert")
    @ApiOperation("新增用户")
    @SyslogAnno("新增系统用户")
    public ResultJson insertSysUser(@RequestBody SysUser entity) {
        boolean flag = isys_userService.insertSysUser(entity);

        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }


    @PutMapping(value = "up/{id}")
    @ApiOperation("修改用户")
    @SyslogAnno("修改系统用户")
    @Transactional
    public ResultJson update(@RequestBody SysUser model) {
        model.setAccount(null);
        boolean flag = isys_userService.updateById(model);
        Integer companyUserId = Integer.parseInt(model.getInstrument());
        CompanyUserBase companyUserBase=new CompanyUserBase();
        companyUserBase.setId(companyUserId);
        companyUserBase.setUserName(model.getUserName());
        companyUserBase.setEmail(model.getEmail());
        companyUserBase.setTel(model.getPhone());
        companyUserBase.setUserStatus(Integer.parseInt(model.getStatus()));
        companyUserBase.setWechat(model.getWechat());
        companyUserBase.setIdCard(model.getBrithId());
        companyUserBaseService.updateById(companyUserBase);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }


    @DeleteMapping(value = "delete/{id}")
    @ApiOperation("禁用用户")
    @SyslogAnno("禁用用户")
    public ResultJson delete(@PathVariable("id") Integer id) {
        SysUser sysUser = isys_userService.selectById(id);
        if (sysUser == null || sysUser.getStatus().equals("1")) {
            return ResultJson.error("当前用户不存在，或已被禁用");
        }
        sysUser.setStatus("1");
        boolean flag = isys_userService.updateById(sysUser);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }


    @ApiOperation(value = "查询所有系统用户信息")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<SysUser> frontPage) {
        Page<SysUser> page = new Page<SysUser>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<SysUser> list = page.setRecords(isys_userService.selectUserList(page,frontPage));
        return ResultJson.ok(list);
    }


    /**
     * 校验数据
     *
     * @param data
     * @param type
     * @return
     */
    @GetMapping("check/{data}/{type}")
    @ApiOperation(value = "校验数据")
    public ResultJson checkUserData(
            @PathVariable("data") String data, @PathVariable("type") Integer type) {
        return ResultJson.ok(isys_userService.checkData(data, type));
    }

    @ApiOperation(value = "部分人员级联接口")
    @PostMapping(value = "queryUserList")
    public ResultJson queryUserList() {
        List<SysDept> list = isys_userService.selectSysDeptUserList();
        return ResultJson.ok(list);
    }


    @ApiOperation(value = "根据部门id查询管理用户信息")
    @PostMapping(value = "queryUserBySysDeptId/{id}")
    public ResultJson queryUserBySysDeptId(@PathVariable("id") Integer id, @RequestBody FrontPage frontPage) {
        if (frontPage.is_search()) {
            Page<SysUser> page = new Page<SysUser>(frontPage.getPageNumber(), frontPage.getPageSize());
            Page<SysUser> list = page.setRecords(isys_userService.queryUserBySysDeptId(id));
            return ResultJson.ok(list);
        } else {
            List<SysUser> list = isys_userService.queryUserBySysDeptId(id);
            return ResultJson.ok(list);
        }
    }


    /**
     * 发送短信
     *
     * @param phone
     * @return
     */
    @ApiOperation(value = "发送短信")
    @GetMapping("code")
    public ResultJson sendCode(@RequestParam("phone") String phone) {
        isys_userService.sendCode(phone);
        return ResultJson.ok(HttpStatus.NO_CONTENT);
    }

    /**
     * 手机号注册
     * @return
     */
    @ApiOperation(value = "手机号注册")
    @PostMapping("registerByTel")
    public Object registerByTel(@RequestBody SysUser userDto) {
        return isys_userService.registerByTel(userDto);
    }

    /**
     * 企业员工注册
     * @param companyUserRegParam
     * @return
     */
    @ApiOperation(value = "企业员工注册")
    @PostMapping("register")
    public Object register(@RequestBody CompanyUserRegParam companyUserRegParam) {
        return isys_userService.register(companyUserRegParam);
    }

    /**
     * 账号登录
     *
     * @param username
     * @param password
     * @return
     */
    @ApiOperation(value = "账号登录")
    @GetMapping("accountLogin")
    public Object accountLogin(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    ) {
        return isys_userService.accountLogin(username, password);
    }

    /**
     * 手机号登录
     * @param tel
     * @param code
     * @return
     */
    @ApiOperation(value = "手机号登录")
    @PostMapping("telLogin")
    public Object telLogin(@RequestParam("tel") String tel, @RequestParam("code") String code) {
        return isys_userService.telLogin(tel, code);
    }
}

