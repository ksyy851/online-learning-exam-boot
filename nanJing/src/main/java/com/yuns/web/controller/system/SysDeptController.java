package com.yuns.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.model.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yuns.util.DtoUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysDept;
import com.yuns.web.dto.SysDeptDto;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysDept")
@Api(tags = "部门-SysDeptController")
public class SysDeptController {


    @Autowired
    private SysDeptService isys_deptService;

    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody SysDeptDto model) {
        SysDept entity = DtoUtil.convertObject(model, SysDept.class);
        boolean flag = isys_deptService.insert(entity);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @PutMapping(value = "up/{id}")
    public ResultJson update(@RequestBody SysDeptDto model) {
        SysDept entity = DtoUtil.convertObject(model, SysDept.class);
        boolean flag = isys_deptService.updateById(entity);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation("删除部门")
    @DeleteMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable Long id) {
        boolean flag = isys_deptService.deleteSysDeptById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }


    /**
     * 查询部门树状结构
     *
     * @return
     */
    @ApiOperation(value = "查询部门树状结构")
    @GetMapping(value = "query")
    public ResultJson query() {
        List<SysDept> list = isys_deptService.selectSysDeptList();
        return ResultJson.ok(list);
    }


    /**
     * 根据子id查询部门树状结构
     *
     * @return
     */
    @ApiOperation(value = "根据子id查询部门树状结构")
    @GetMapping(value = "querySysDeptByChildren")
    public ResultJson querySysDeptByChildren(@RequestParam String deptId) {
        SysDept list = isys_deptService.querySysDeptByChildren(deptId);
        return ResultJson.ok(list);
    }


    /**
     * 根据子id查询部门树状结构
     *
     * @return
     */
    @ApiOperation(value = "根据子id查询部门树状结构")
    @GetMapping(value = "querySysDeptByChildrenLong")
    public ResultJson querySysDeptByChildrenLong(@RequestParam Long deptId) {
        List<Long> list = isys_deptService.querySysDeptByChildrenLong(deptId);
        return ResultJson.ok(list);
    }

}

