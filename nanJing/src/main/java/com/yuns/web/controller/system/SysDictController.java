package com.yuns.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.model.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yuns.util.DtoUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysDict;
import com.yuns.web.dto.SysDictDto;


/**
 * @author luwenchao
 * @since 2019-11-14
 */
@Api(tags = "字典表 - Controller" )
    @RestController
@RequestMapping("/sysDict")
    public class SysDictController {

    @Autowired
    private SysDictService iSysDictService;

    @ApiOperation(value = "添加 - sys_dict")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody SysDictDto model){
        SysDict entity=DtoUtil.convertObject(model,SysDict.class);
            boolean flag=iSysDictService.insert(entity);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - sys_dict")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody SysDictDto model){
        SysDict entity=DtoUtil.convertObject(model,SysDict.class);
            boolean flag=iSysDictService.updateById(entity);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - sys_dict")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iSysDictService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - sys_dict")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage frontPage){
      List<SysDict> sysDicts= iSysDictService.selectDictList();

      return ResultJson.ok(sysDicts);
            }
    @ApiOperation(value = "根据id查询 - sys_dict")
    @GetMapping(value = "selectById")
    public ResultJson selectById(@RequestParam("id") Integer id){
        SysDict  model= iSysDictService.selectById(id);
            return ResultJson.ok(model);
            }

        }

