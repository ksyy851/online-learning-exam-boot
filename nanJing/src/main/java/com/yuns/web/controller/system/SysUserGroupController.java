package com.yuns.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.model.service.SysUserGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yuns.util.DtoUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysUserGroup;
import com.yuns.web.dto.SysUserGroupDto;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysUserGroup")
@Api(tags = "用户分组" ,hidden = true)
    public class SysUserGroupController {



    @Autowired
    private SysUserGroupService isys_user_groupService;

    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody SysUserGroupDto model){
        SysUserGroup entity=DtoUtil.convertObject(model,SysUserGroup.class);
        boolean flag=isys_user_groupService.insert(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @PutMapping(value = "up/{id}")
    public ResultJson update(@RequestBody SysUserGroupDto model){
    SysUserGroup entity=DtoUtil.convertObject(model,SysUserGroup.class);
        boolean flag=isys_user_groupService.updateById(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @DeleteMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id){
        boolean flag=isys_user_groupService.deleteById(id);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @GetMapping(value = "query")
    public ResultJson query(){
        Page<SysUserGroup> page=new Page<SysUserGroup>();
        Page<SysUserGroup> list = page.setRecords(isys_user_groupService.selectList(null));
        return ResultJson.ok(list);
    }


}

