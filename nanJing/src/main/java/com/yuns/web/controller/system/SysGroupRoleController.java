package com.yuns.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.model.service.SysGroupRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yuns.util.DtoUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysGroupRole;
import com.yuns.web.dto.SysGroupRoleDto;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysGroupRole")
@Api(tags = "分组角色" ,hidden = true)
    public class SysGroupRoleController {



    @Autowired
    private SysGroupRoleService isys_group_roleService;

    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody SysGroupRoleDto model){
        SysGroupRole entity=DtoUtil.convertObject(model,SysGroupRole.class);
        boolean flag=isys_group_roleService.insert(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @PutMapping(value = "up/{id}")
    public ResultJson update(@RequestBody SysGroupRoleDto model){
    SysGroupRole entity=DtoUtil.convertObject(model,SysGroupRole.class);
        boolean flag=isys_group_roleService.updateById(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @DeleteMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id){
        boolean flag=isys_group_roleService.deleteById(id);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @GetMapping(value = "query")
    public ResultJson query(){
        Page<SysGroupRole> page=new Page<SysGroupRole>();
        Page<SysGroupRole> list = page.setRecords(isys_group_roleService.selectList(null));
        return ResultJson.ok(list);
    }


}

