package com.yuns.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.aspect.SyslogAnno;
import com.yuns.model.service.SysRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yuns.util.DtoUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysRolePermission;
import com.yuns.web.dto.SysRolePermissionDto;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysRolePermission")
@Api(tags = "角色权限")
    public class SysRolePermissionController {



    @Autowired
    private SysRolePermissionService isys_role_permissionService;

    /**
     * 修改和新增角色权限
     * @param model
     * @return
     */
    @ApiOperation(value = "修改和新增绑定角色权限")
    @PostMapping(value = "insert")
    @SyslogAnno("修改和新增绑定角色权限")
    public ResultJson insert(@RequestBody List<SysRolePermission> model){
        boolean flag=isys_role_permissionService.insertRolePermission(model);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

}

