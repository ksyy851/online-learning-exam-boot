package com.yuns.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.model.service.SysGroupPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yuns.util.DtoUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysGroupPermission;
import com.yuns.web.dto.SysGroupPermissionDto;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysGroupPermission")
@Api(tags = "分组权限" ,hidden = true)
    public class SysGroupPermissionController {



    @Autowired
    private SysGroupPermissionService isys_group_permissionService;

    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody SysGroupPermissionDto model){
        SysGroupPermission entity=DtoUtil.convertObject(model,SysGroupPermission.class);
        boolean flag=isys_group_permissionService.insert(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @PutMapping(value = "up")
    public ResultJson update(@RequestBody SysGroupPermissionDto model){
    SysGroupPermission entity=DtoUtil.convertObject(model,SysGroupPermission.class);
        boolean flag=isys_group_permissionService.updateById(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @DeleteMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id){
        boolean flag=isys_group_permissionService.deleteById(id);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @GetMapping(value = "query")
    public ResultJson query(){
        Page<SysGroupPermission> page=new Page<SysGroupPermission>();
        Page<SysGroupPermission> list = page.setRecords(isys_group_permissionService.selectList(null));
        return ResultJson.ok(list);
    }


}

