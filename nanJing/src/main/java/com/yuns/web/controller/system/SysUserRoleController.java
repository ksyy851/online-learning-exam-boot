package com.yuns.web.controller.system;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.yuns.aspect.SyslogAnno;
import com.yuns.model.service.SysUserRoleService;
import com.yuns.web.sys.param.UserRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.yuns.util.DtoUtil;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysUserRole;
import com.yuns.web.dto.SysUserRoleDto;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysUserRole")
@Api(tags = "用户角色")
public class SysUserRoleController {


    @Autowired
    private SysUserRoleService isys_user_roleService;

    @ApiOperation("用户角色管理")
    @PostMapping(value = "insert")
    @SyslogAnno("绑定用户角色")
    public ResultJson insert(@RequestBody UserRoles sysUserRoles) {
        boolean flag = isys_user_roleService.insertSysUserRole(sysUserRoles);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

}

