package com.yuns.web.sys.param;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: luchao
 * Date: 2020-01-09
 * Time: 14:15
 */
public class LoginBean {

    @ApiModelProperty("用户密码")
    private String userPwd;
    @ApiModelProperty("用户账号")
    private String userName;

    public String getUserPwd() {
        return userPwd;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
