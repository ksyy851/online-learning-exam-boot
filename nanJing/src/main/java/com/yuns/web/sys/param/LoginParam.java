package com.yuns.web.sys.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 登录参数
 *
 * @author WeiZiDong
 */
@SuppressWarnings("serial")
@ApiModel("登录/注册参数")
public class LoginParam implements Serializable {
    /**
     * 用户名.
     */
    @ApiModelProperty(value = "用户名,必填", required = true)
    private String name;

    /**
     * 用户手机号
     */
    @ApiModelProperty(value = "手机号码,必填", required = true)
    private String phone;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码，必须MD5加密,必填", example = "e10adc3949ba59abbe56e057f20f883e")
    private String pwd;

    /**
     * 业务渠道.
     */
    @ApiModelProperty(value = "业务渠道：01智学，其他待定 ", required = true)
    private String businessChannels;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "验证码", example = "123456")
    private String code;

    /**
     * 经度
     */
    @ApiModelProperty(value = "经度,登录时必填", example = "123.456781")
    private String longitude;

    /**
     * 纬度
     */
    @ApiModelProperty(value = "纬度,登录时必填", example = "456.568798416")
    private String latitude;

    /**
     * 设备类型
     */
    @ApiModelProperty(value = "0：未知，1：iOS，2：Android,登录时必填", example = "1")
    private Integer deviceType;

    @ApiModelProperty(value = "邀请码(选填)", example = "e8ycvk")
    private String userPid;
    /**
     *意号.
     */
    @ApiModelProperty(value = "意号")
    private String idealNo;

    public String getIdealNo() {
        return idealNo;
    }

    public void setIdealNo(String idealNo) {
        this.idealNo = idealNo;
    }

    public String getBusinessChannels() {
        return businessChannels;
    }

    public void setBusinessChannels(String businessChannels) {
        this.businessChannels = businessChannels;
    }

    public Integer getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUserPid() {
        return userPid;
    }

    public void setUserPid(String userPid) {
        this.userPid = userPid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
