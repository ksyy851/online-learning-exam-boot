package com.yuns.web.sys.param;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: luchao
 * Date: 2020-01-08
 * Time: 17:24
 */
public class Meta {
           private String title;

           private String target;

           private String icon;

           private Boolean  show;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Boolean getShow() {
        return show;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }
}
