package com.yuns.web.sys.param;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by luwenchao on 2020-06-07.
 */
public class UserRoles {

    @ApiModelProperty("用户id")
    private  Integer  userId;

    @ApiModelProperty("用户角色ids")
    private  String[]  ids;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }
}
