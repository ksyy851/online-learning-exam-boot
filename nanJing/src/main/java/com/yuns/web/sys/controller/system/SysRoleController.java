package com.yuns.web.sys.controller.system;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.aspect.SyslogAnno;
import com.yuns.model.entity.SysRole;
import com.yuns.model.service.SysRoleService;
import com.yuns.util.DtoUtil;
import com.yuns.util.ResultJson;
import com.yuns.web.dto.SysRoleDto;
import com.yuns.web.param.FrontPage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysRole")
@Api(tags = "角色" )
    public class SysRoleController {



    @Autowired
    private SysRoleService isys_roleService;

    @PostMapping(value = "insert")
    @SyslogAnno("新增角色")
    public ResultJson insert(@RequestBody SysRole model){
        boolean flag=isys_roleService.insert(model);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @PutMapping(value = "up")
    @SyslogAnno("修改角色")
    public ResultJson update(@RequestBody SysRoleDto model){
    SysRole entity=DtoUtil.convertObject(model,SysRole.class);
        boolean flag=isys_roleService.updateById(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @PostMapping(value = "delete/{id}")
    @SyslogAnno("删除角色")
    public ResultJson delete(@PathVariable Long id){
        boolean flag=isys_roleService.deleteById(id);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation("查询所有角色")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage frontPage){
        if (!frontPage.is_search()){
            Page<SysRole> page=new Page<SysRole>();
            List<SysRole> sysRoles = isys_roleService.selectSysRoleList();
            return ResultJson.ok(sysRoles);
        }
        Page<SysRole> page=new Page<SysRole>(frontPage.getPageNumber(),frontPage.getPageSize());
        Page<SysRole> list = page.setRecords(isys_roleService.selectSysRoleList());
        return ResultJson.ok(list);
    }


    @ApiOperation("查询所有角色权限ids")
    @PostMapping(value = "queryPermissionIdS")
    public ResultJson queryPermissionIdS(@RequestBody FrontPage frontPage){
        if (!frontPage.is_search()){
            Page<SysRole> page=new Page<SysRole>();
            List<SysRole> sysRoles = isys_roleService.queryPermissionIdS();
            return ResultJson.ok(sysRoles);
        }

        Page<SysRole> page=new Page<SysRole>(frontPage.getPageNumber(),frontPage.getPageSize());
        Page<SysRole> list = page.setRecords(isys_roleService.queryPermissionIdS());
        return ResultJson.ok(list);

    }


    /**
     * 根据用户id查询用户角色权限
     */
    @ApiOperation("根据用户id查询用户角色权限")
    @PostMapping(value = "queryRoleByUserId")
    public ResultJson queryRoleByUserId(){
        List<SysRole> roleList =isys_roleService.queryRoleByUserId(1);
        return ResultJson.ok(roleList);
    }

}

