package com.yuns.web.sys.param;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: luchao
 * Date: 2020-01-08
 * Time: 17:24
 */
public class MetaParam {

   private Integer key;

   private Integer id;

   private String name;

   private Integer orderNum;

   private String path;

   private String component;

   private String redirect;

   private Long parentId;

   private Meta meta;

   private String children;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    public String getRedirect() {
        return redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public String getChildren() {
        return children;
    }

    public void setChildren(String children) {
        this.children = children;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getId() {
        return id;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MetaParam metaParam = (MetaParam) o;

        if (key != null ? !key.equals(metaParam.key) : metaParam.key != null) return false;
        if (id != null ? !id.equals(metaParam.id) : metaParam.id != null) return false;
        if (name != null ? !name.equals(metaParam.name) : metaParam.name != null) return false;
        if (orderNum != null ? !orderNum.equals(metaParam.orderNum) : metaParam.orderNum != null) return false;
        if (path != null ? !path.equals(metaParam.path) : metaParam.path != null) return false;
        if (component != null ? !component.equals(metaParam.component) : metaParam.component != null) return false;
        if (redirect != null ? !redirect.equals(metaParam.redirect) : metaParam.redirect != null) return false;
        if (parentId != null ? !parentId.equals(metaParam.parentId) : metaParam.parentId != null) return false;
        if (meta != null ? !meta.equals(metaParam.meta) : metaParam.meta != null) return false;
        return children != null ? children.equals(metaParam.children) : metaParam.children == null;
    }

    @Override
    public int hashCode() {
        int result = key != null ? key.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (orderNum != null ? orderNum.hashCode() : 0);
        result = 31 * result + (path != null ? path.hashCode() : 0);
        result = 31 * result + (component != null ? component.hashCode() : 0);
        result = 31 * result + (redirect != null ? redirect.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (meta != null ? meta.hashCode() : 0);
        result = 31 * result + (children != null ? children.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "MetaParam{" +
                "key=" + key +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", orderNum=" + orderNum +
                ", path='" + path + '\'' +
                ", component='" + component + '\'' +
                ", redirect='" + redirect + '\'' +
                ", parentId=" + parentId +
                ", meta=" + meta +
                ", children='" + children + '\'' +
                '}';
    }
}
