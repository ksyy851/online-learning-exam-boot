package com.yuns.web.sys.dto;

import java.util.Objects;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: luchao
 * Date: 2019-12-13
 * Time: 15:40
 */
public class Action {

   private String action ;

   private String describe ;

   private Boolean defaultCheck;


   public String getAction() {
      return action;
   }

   public void setAction(String action) {
      this.action = action;
   }

   public String getDescribe() {
      return describe;
   }

   public void setDescribe(String describe) {
      this.describe = describe;
   }

   public Boolean getDefaultCheck() {
      return defaultCheck;
   }

   public void setDefaultCheck(Boolean defaultCheck) {
      this.defaultCheck = defaultCheck;
   }

   @Override
   public String toString() {
      return "Action{" +
              "action='" + action + '\'' +
              ", describe='" + describe + '\'' +
              ", defaultCheck=" + defaultCheck +
              '}';
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Action action1 = (Action) o;
      return Objects.equals(action, action1.action) &&
              Objects.equals(describe, action1.describe) &&
              Objects.equals(defaultCheck, action1.defaultCheck);
   }

   @Override
   public int hashCode() {

      return Objects.hash(action, describe, defaultCheck);
   }
}
