package com.yuns.web.sys.controller.system;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysGroup;
import com.yuns.model.service.SysGroupService;
import com.yuns.util.DtoUtil;
import com.yuns.util.ResultJson;
import com.yuns.web.dto.SysGroupDto;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author luwenchao
 * @since 2018-07-26
 */
@RestController
@RequestMapping("/sysGroup")
@Api(tags = "分组-SysGroupController",hidden = true)
    public class SysGroupController {



    @Autowired
    private SysGroupService isys_groupService;

    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody SysGroupDto model){
        SysGroup entity=DtoUtil.convertObject(model,SysGroup.class);
        boolean flag=isys_groupService.insert(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @PutMapping(value = "up/{id}")
    public ResultJson update(@RequestBody SysGroupDto model){
    SysGroup entity=DtoUtil.convertObject(model,SysGroup.class);
        boolean flag=isys_groupService.updateById(entity);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @DeleteMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable Long id){
        boolean flag=isys_groupService.deleteById(id);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @GetMapping(value = "query")
    public ResultJson query(){
        Page<SysGroup> page=new Page<SysGroup>();
        Page<SysGroup> list = page.setRecords(isys_groupService.selectList(null));
        return ResultJson.ok(list);
    }


}

