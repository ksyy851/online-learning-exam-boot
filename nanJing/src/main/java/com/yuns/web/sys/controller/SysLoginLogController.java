package com.yuns.web.sys.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.model.entity.SysLogin;
import com.yuns.model.service.SysLoginService;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author luwenchao
 * @since 2019-10-28
 */
@Api(tags = "操作日志 - Controller")
@RestController
@RequestMapping("/operationLog")
public class SysLoginLogController {

    @Autowired
    private SysLoginService sysLoginService;

    @ApiOperation(value = "添加 - SysLogin_log")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody SysLogin entity) {
        boolean flag = sysLoginService.insert(entity);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }
    @ApiOperation(value = "修改 - SysLogin_log")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody SysLogin model) {
        boolean flag = sysLoginService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - SysLogin_log")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = sysLoginService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - SysLogin_log")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage) {
        Page<SysLogin> list = sysLoginService.selectSysLoginList(frontPage);
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "根据id查询 - SysLogin_log")
    @GetMapping(value = "selectById")
    public ResultJson selectById(@RequestParam("id") Integer id) {
        SysLogin model = sysLoginService.selectById(id);
        return ResultJson.ok(model);
    }
}

