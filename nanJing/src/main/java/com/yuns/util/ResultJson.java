package com.yuns.util;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@ApiModel(description = "数据传输对象")
@Component
@SuppressWarnings("unused")
public class ResultJson<T> implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 成功
     */
    private static final Integer SUCCESS = 200;
    private static final String SUCCESS_MSG = "success";
    /**
     * 失败
     */
    private static final Integer FAIL = 900;
    public static final String FAIL_MSG = "fail";



    @ApiModelProperty("数据消息")
    private String msg;
    @ApiModelProperty("传输状态码")
    private Integer code;
    @ApiModelProperty("传输数据")
    private T data;



    public static ResultJson error() {
        return error(FAIL, "未知异常，请联系管理员");
    }

    public static ResultJson error(String msg) {
        return error(FAIL, msg);
    }

    public static ResultJson error(int code, String msg) {
        ResultJson r = new ResultJson();
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }
    public static ResultJson error(int code, String msg,Object data) {
        ResultJson r = new ResultJson();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }

//    public static ResultJson ok(String msg) {
//        ResultJson r = new ResultJson();
//        r.setMsg(msg);
//        return r;
//    }
    public static ResultJson ok(Integer code ,String msg) {
        ResultJson r = new ResultJson();
        r.setMsg(msg);
        r.setCode(code);
        return r;
    }
    public static ResultJson ok() {
        ResultJson r = new ResultJson();
        r.setCode(SUCCESS);
        r.setMsg(SUCCESS_MSG);
        return r;
    }

    public static ResultJson ok(Object data) {
        ResultJson r = new ResultJson();
        r.setCode(SUCCESS);
        r.setMsg(SUCCESS_MSG);
        r.setData(data);
        return r;
    }

    public static ResultJson ok(Object data,String msg) {
        ResultJson r = new ResultJson();
        r.setData(data);
        r.setCode(SUCCESS);
        r.setMsg(msg);
        return r;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "resultJson [code=" + code + ", message=" + msg + ", data=" + data + "]";
    }


}
