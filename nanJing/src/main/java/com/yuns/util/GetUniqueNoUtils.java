package com.yuns.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class GetUniqueNoUtils {
    /**
     * 生成随机订单号
     *
     * @return
     */
    public static String getRandomOrderNo() {
        SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
        String now = format.format(new Date());
        String randomString = getRandomString(6);
        return now + randomString;
    }

    public static String getRandomString(int length) { // length表示生成字符串的长度
        String base = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
}
