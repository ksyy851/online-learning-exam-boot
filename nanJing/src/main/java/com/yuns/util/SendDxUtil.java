package com.yuns.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 *
 * @author ASUS
 * @此工具是为启瑞云的短信包
 * @since 2018-11-20
 *
 */
public class SendDxUtil {

//    APIKey : 1135910011 
//    APISecret : acd7d8ba54d3e7e65fca



    //500917980001
//    private static  String apiKey="1135910011";
    private static  String apiKey="11359100112";
    //10B3F0A1639A284DB85DE0998AD3B8FB
//    private static  String apiSecret="acd7d8ba54d3e7e65fca";
    private static  String apiSecret="acd7d8ba54d3e7e65fca1";
    private final String url  = "http://api.qirui.com:7891/mt";
    private static Integer yzlength=4;

    /**
     *
     * @param apiKey
     * @param apiSecret
     * @这两个参数需要到瑞启云申请Api帐号,请自行申请
     */
//
//    public SendDxUtil(String apiKey, String apiSecret) {
//        this.apiKey=apiKey;
//        this.apiSecret=apiSecret;
//    }



    /**
     *
     * @param mobile 发送的对象
     * @param msg 短信内容(【签名】+短信内容)，系统提供的测试签名和内容，
     * 			如需要发送自己的短信内容请在启瑞云平台申请签名和模板,内容必须和模板一样
     * 			类似于 String message   = "【xxxx】您的验证码是:"+yzcode;
     * @return 发送短信的状态
     * @throws ClientProtocolException
     * @throws IOException
     */

    public static String senddxUtil(String mobile,String msg) throws ClientProtocolException, IOException{
        String url  = "http://api.qirui.com:7891/mt";
        String message   = msg;

        StringBuilder sb = new StringBuilder(2000);
        sb.append(url);
        sb.append("?dc=15");
        sb.append("&sm=").append(URLEncoder.encode(message, "utf8"));
        sb.append("&da=").append(mobile);
        sb.append("&un=").append(apiKey);
        sb.append("&pw=").append(apiSecret);
        sb.append("&tf=3&rd=1&rf=2");   //短信内容编码为 urlencode+utf8

        String request = sb.toString();
        //System.out.println(request);


        CloseableHttpClient client = HttpClients.createDefault();

        HttpGet httpGet = new HttpGet(request);

        CloseableHttpResponse response = client.execute(httpGet);

        String respStr = null;

        HttpEntity entity = response.getEntity();
        if(entity != null) {
            respStr = EntityUtils.toString(entity, "UTF-8");
        }
        return  respStr;
    }
    /**
     *
     * @param mobile 电话号码
     * @param qm 签名 【百度公司】验证码为: ****
     * 	类似于：短信内容(【签名】+短信内容)，系统提供的测试签名和内容，如需要发送自己的短信内容请在启瑞云平台申请签名和模板
     * @param yzlength 验证码的长度
     * @return HashMap 内有yzm 和 短信发送状态
     */
    public HashMap sendYzm(String mobile, String qm, Integer yzlength){
        HashMap map = new HashMap();
        String yzm=MakeRound.round(yzlength);
        String msg=qm+yzm;
        String zt="";
        try {
            zt=senddxUtil(mobile,msg);
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            yzm=null;
            System.out.println("发送验证码失败");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            yzm=null;
            System.out.println("发送验证码失败");
        }catch (Exception e){
            // TODO Auto-generated catch block
            e.printStackTrace();
            yzm=null;
            System.out.println("发送验证码失败");
        }
        map.put("yzm", yzm);
        map.put("zt", zt);
        return map;

    }

    /**
     *
     * @param mobile
     * @param msg 短信内容(【签名】+短信内容)，系统提供的测试签名和内容，
     * 			如需要发送自己的短信内容请在启瑞云平台申请签名和模板,内容必须和模板一样
     * 			类似于 String message   = "【xxxx】:"+短信内容;
     * @return zt 返回发送短信的状态
     */
    public static String sendDxtz(String mobile,String msg){
        String zt=null;
        try {;
            zt=senddxUtil(mobile,"【宁县城乡供水站】"+ msg);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.out.println("短信发送失败");
        }
        return zt;
    }

    public static void main(String[] args) {
//        String sendDxtz = SendDxUtil.sendDxtz("18653288437", "我在测试");
//        String sendDxtz = SendDxUtil.sendDxtz("15827716903", "尊敬的王五，您好！户号1112，您的可用水量已经低于7吨，为避免停水对您的生活造成不便，请尽快缴费，详情请致电2345678!");
        String sendDxtz = SendDxUtil.sendDxtz("15827716903", "尊敬的王五，您已成功缴费4元，现账户余额45元，如有疑问询566666666，谢谢！");
        System.out.println(sendDxtz);
    }

}
