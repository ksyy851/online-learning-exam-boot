package com.yuns.util;


import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class PasswordHelper {
	//private RandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
	private String algorithmName = "md5";
	private int hashIterations = 2;

	public static String encryptPassword(String name , String pwd) {
		//String salt=randomNumberGenerator.nextBytes().toHex();
		String newPassword = new SimpleHash("md5", pwd,  ByteSource.Util.bytes(name), 2).toHex();
//		String newPassword = new SimpleHash("md5", com.yuns.user.getPwd()).toHex();
		return newPassword;
	}

	public static void main(String[] args) {
		String pwd = PasswordHelper.encryptPassword("luwenchao" , "1111");
		System.out.println(pwd);
	}
}
