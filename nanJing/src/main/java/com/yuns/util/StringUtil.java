package com.yuns.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串 工具类
 *
 * @author dengchao
 * @date 2018/4/8
 */
public class StringUtil {

    /**
     * 是否包含特殊字符检查
     *
     * @param str 字符串
     * @return boolean true 包含
     */
    public static boolean specialCharacterCheck(String str) {
        if (null != str) {
            String regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(str);
            if (m.find()) {
                return true;
            }
        }
        return false;
    }

    /**
     * 字符串长度是否大于指定长度
     *
     * @param str 字符串
     * @return boolean true 大于
     */
    public static boolean lengthCheck(String str, int length) {
        if (null != str) {
            if (str.length() > length) {
                return true;
            }
        }
        return false;
    }

    /**
     * 不能为空字符串
     *
     * @param str 字符串
     * @return boolean  true 不为空字符串
     */
    public static boolean notEmptyString(String str) {
        if (str != null && "".equals(str)) {
            return false;
        }
        return true;
    }

    /**
     * 中文校验
     *
     * @param str 字符串
     * @return boolean  true 纯中文字符
     */
    public static boolean chineseCheck(String str) {
        String regex = "[\u4E00-\u9FA5]+";
        if (!str.matches(regex)) {
            return false;
        }
        return true;
    }

    /**
     * 判断字符串中是否包含字母
     *
     * @param str 待检验的字符串
     * @return 返回是否包含 false: 包含字母 ;ture 不包含字母
     */
    public static boolean characterCheck(String str) {
        String regex = ".*[a-zA-Z]+.*";
        Matcher m = Pattern.compile(regex).matcher(str);
        return !m.matches();
    }

    /**
     * 可以为null
     * 不能空字符串
     * 长度 不能大于指定长度
     * 不能有非法字符校验
     *
     * @param str    字符串
     * @param length 字符指定长度
     * @return boolean  true 符合要求
     */
    public static boolean editingInformationCheck(String str, int length) {
        if (null != str) {
            String regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
            Pattern p = Pattern.compile(regEx);
            Matcher m = p.matcher(str);
            if (m.find()) {
                return false;
            }
            if ("".equals(str)) {
                return false;
            }
            if (str.length() > length) {
                return false;
            }
        }
        return true;
    }

    /***
     * String 数组转int
     * @param s
     * @return
     */
    public static Integer[] stringToInts(String[] s) {
        Integer[] n = new Integer[s.length];
        for (int i = 0; i < s.length; i++) {
            n[i] = Integer.parseInt(s[i]);
        }
        return n;
    }

    /**
     * 利用正则表达式校验电话号码 数字与11长度
     *
     * @param str 數字
     * @return true 数字
     */
    public static boolean checkPhone(String str) {
        int length = 11;
        if (str.length() != length) {
            return false;
        }
        String regex = "[0-9]*";
        Pattern pattern = Pattern.compile(regex);
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(checkPhone("1234567897a"));
    }
}
