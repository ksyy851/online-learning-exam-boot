package com.yuns.util;


public class Constants {
    public final static Integer MNO_TYPE_1 = 1;
    public final static Integer TYPE_0 = 0;
    public final static Integer TYPE_1 = 1;
    /**
     * STUTAS:1 正常.
     */
    public final static String STUTAS_1 = "1";
    public final static String XXB = "yuns";
    public final static Long COUNT_0 = 0L;
    public final static String CODE_TYPE_REGISTER = "register";
    public final static String CODE_TYPE_RESETPASSWORD= "resetPassword";

}
