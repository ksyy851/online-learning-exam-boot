package com.yuns.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.SimpleTimeZone;

/**
 *  处理日期相关的操作
 * @author ppl
 *
 */
public class DateUtil {

	public final static SimpleDateFormat LONG_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public final static SimpleDateFormat LONG_FORMAT1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public final static SimpleDateFormat MID_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public final static SimpleDateFormat SHORT_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	public final static SimpleDateFormat COMMON_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

	public final static SimpleDateFormat Date_FORMAT = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
	public final static SimpleDateFormat SIMPLE_SHORT_FORMAT = new SimpleDateFormat("yyyyMMdd");

	public final static SimpleDateFormat MONTH_FORMAT = new SimpleDateFormat("yyyy-MM");

	public final static SimpleDateFormat MONTH = new SimpleDateFormat("MM");
	public final static SimpleDateFormat YYYY = new SimpleDateFormat("yyyy");
	public final static SimpleDateFormat DD = new SimpleDateFormat("dd");
	public final static SimpleDateFormat HHMM_FORMAT = new SimpleDateFormat("HH:mm");
	public final static SimpleDateFormat HH = new SimpleDateFormat("HH");

	public final static SimpleDateFormat MONTH_FORMAT1 = new SimpleDateFormat("yyyyMM");
	public final static SimpleDateFormat SEPERATOR_FORMAT = new SimpleDateFormat("yyyy/MM/dd");
	public final static SimpleDateFormat SEPERATOR_FORMAT_HMS = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

	//0时区时间的Format
	public final static SimpleTimeZone timeZone0 = new SimpleTimeZone(0,"GMT");
	public final static SimpleDateFormat LONG_FORMAT_GMT0 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final long DAY_TIME = 60 * 60 * 24 * 1000;
	public static final long JET_LAG = 60 * 60 * 8 * 1000;
	/**
	 * 3天的毫秒数
	 */
	public final static long THREE_DAY_MILLSEC = 3*24*3600*1000;

	/**
	 * 1天的毫秒数
	 */
	public final static long ONE_DAY_MILLSEC = 24*3600*1000;

	/**
	 * 1小时的毫秒数
	 */
	public final static long ONE_HOUR_MILLSEC = 3600*1000;

	/**
	 * 3小时的毫秒数
	 */
	public final static long THREE_HOURS_MILLSEC = 3*3600*1000;

	/**
	 * 12小时的毫秒数
	 */
	public final static long TWELVE_HOURS_MILLSEC = 12*3600*1000;

	/**
	 * 一天的分钟数
	 */
	public final static int ONE_DAY_MINUTE=24*60;
	/**
	 * 一周的小时数
	 */
	public final static int ONE_WEEK_HOUR=7*24;

	public static String getSimpleShortDay(Date date){
		return SIMPLE_SHORT_FORMAT.format(date);
	}

	public static String getShortDay(Date date){
		return SHORT_FORMAT.format(date);
	}

	public static Date getDateByString(String strDate){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日");
		try {
			return sdf.parse(strDate);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

}


