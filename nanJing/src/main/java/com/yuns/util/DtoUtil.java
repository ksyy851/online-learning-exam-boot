package com.yuns.util;

import org.springframework.beans.BeanUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DtoUtil {
    public static <T> T convertObject(Object sourceObj, Class<T> targetClz) {
        if (sourceObj == null) {
            return null;
        }

        if (targetClz == null) {
            throw new IllegalArgumentException("parameter clz shoud not be null");
        }
        try {
            Object targetObj = targetClz.newInstance();
            BeanUtils.copyProperties(sourceObj, targetObj);
            return (T) targetObj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // 校验电话号码是不是数字
    public static boolean checkPhone(String str) {
     /*   int length = 11;
        if (str.length() != length) {
            return false;
        }*/
        String regex = "[0-9]*";
        Pattern pattern = Pattern.compile(regex);
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }


}
