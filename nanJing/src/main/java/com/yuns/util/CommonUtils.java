package com.yuns.util;


import cn.hutool.json.JSONNull;
import com.alibaba.fastjson.JSON;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.params.ExcelExportEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.text.SimpleDateFormat;
import java.lang.reflect.Field;
/**
 * @author xionggang
 * @date 2019/9/20 8:24
 */
public class CommonUtils {

    /**
     * 判断文件夹是否存在，不存在则创建
     *
     * @param path
     */
    public static void checkFileCreate(String path) {
        File file = new File(path);
        if (!file.exists()) {//如果文件夹不存在
            file.mkdirs();//创建文件夹
        }
    }


    /**
     * 根据测站id生成年表
     *
     * @param stationId
     * @return
     */
    public static String stationTableName(String stationId) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        String year = sdf.format(new Date());
        int stationInt = Integer.parseInt(stationId);
        String station = String.format("%06d", stationInt);
        return "d_historydata_" + station + "_" + year;

    }

    //把long类型的时间变成String类型
    public static String longToStringDate(long l) {
        Date date = new Date(l);
        SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String time = simple.format(date);
        return time;
    }


    public static void exportExcel(HttpServletResponse response, List<ExcelExportEntity> entity, List<Map<String, String>> list, String title, String sheetName) {
        for (Map<String, String> map : list) {
            for (String key : map.keySet()) {
                Object object = map.get(key);
                if (object instanceof JSONNull) {
                    map.put(key, "");
                }
            }
        }

        Workbook workbook = ExcelExportUtil.exportExcel(new ExportParams(
                title, sheetName), entity, list);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(new Date());
        try {
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
            response.setHeader("Content-Disposition",
                    "attachment;filename=" + dateString + ".xls");
            workbook.write(response.getOutputStream());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获得当月1号零时零分零秒
     *
     * @return
     */
    public static String initDateByMonth() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(calendar.getTime());
        return dateString;
    }


    /**
     *     * json字符串转list
     *     * @param jsonFile
     *     * @return
     *     * @throws Exception
     *     * @author fanchenxi
     *     
     */
    public static List<Map<String, String>> GetJsonListByString(String jsonFile) throws Exception {
        List<Object> list = JSON.parseArray(jsonFile);
        List<Map<String, String>> result = new ArrayList<Map<String, String>>();
        for (Object object : list) {
            Map<String, String> ret = (Map<String, String>) object;//取出list里面的值转为map
            result.add(ret);
        }
        return result;
    }

//    public static Map<String, Object> convertToMap(Object obj) throws IllegalAccessException  {
//        Map<String, Object> map = new HashMap<String,Object>();
//        Class<?> clazz = obj.getClass();
//        Field[] fields = clazz.getFields();
//        if(fields!=null && fields.length == 0){
//            fields = clazz.getDeclaredFields();
//        }
//        for (Field field : fields) {
//            field.setAccessible(true);
//            String fieldName = field.getName();
//            Object value = StringUtil.nvl(field.get(obj));
//            map.put(fieldName, value);
//        }
//        return map;
//    }

}
