package com.yuns.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Description 系统常量
 * @author wm
 * @create 2018-04-10
 */
@Component
public class SystemConstants {

	/**
	 * 交易成功 000000
	 */
	public static final String SUCCEED = "000000";
	/**
	 * 数字 6
	 */
	public static final int SIX = 6;
	/**
	 * 字符串   1
	 */
	public static final String ZERO="0";
	/**
	 * 字符串   1
	 */
	public static final String ONE="1";
	/**
	 * 字符串  9
	 */
	public static final String NINE="9";
	/**
	 * 登录session有效时间
	 */
	public static long LOGIN_EFFECTIVETIME;

	@Value("${user.login.effectiveTime}")
	public void setLoginEffectiveTime(long loginEffectiveTime) {
		SystemConstants.LOGIN_EFFECTIVETIME = loginEffectiveTime;
    }
}
