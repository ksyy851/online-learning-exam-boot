package com.yuns.util;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.StopWatch;

import java.util.List;

/**
 * TODO：list工具
 *
 * @author: yinhaiquan
 * @date: 2020/10/21 11:30
 * @version: 1.0
 */
@Slf4j
public class ListUtils {
    public static List copy(List<?> source, Class targetCls) {
        StopWatch sw = new StopWatch();
        sw.start();
        List list = JSON.parseArray(JSON.toJSONString(source), targetCls);
        log.info("list列表数据反序列化耗时:{} ms", sw.getTime());
        sw.stop();
        return list;
    }
}
