package com.yuns.util;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 支付信息
 *
 * @author WeiZiDong
 */
public class Payment {
    /**
     * 内部订单号
     */
    private String sn;
    /**
     * 支付金额
     */
    private BigDecimal amount;
    /**
     * 回调地址:绝对路径，不需要填写域名
     */
    private String notifyUrl;
    /**
     * 商品说明
     */
    private String description;
    /**
     * 商品的标题/交易标题/订单标题/订单关键字等。
     */
    private String subject;
    /***
     * 订单失效时间
     */
    private Date timeexpire;

    public Date getTimeexpire() {
        return timeexpire;
    }

    public void setTimeexpire(Date timeexpire) {
        this.timeexpire = timeexpire;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 交易ip
     */
    private String ip;

    public Payment() {
        super();
    }

    /**
     * 支付信息
     *
     * @param sn          内部订单号
     * @param amount      支付金额
     * @param notifyUrl   回调地址:绝对路径，不需要填写域名
     * @param description 商品说明
     * @param subject     商品的标题/交易标题/订单标题/订单关键字等。
     */
    public Payment(String sn, BigDecimal amount, String notifyUrl, String subject, String description) {
        this.sn = sn;
        this.amount = amount;
        this.notifyUrl = notifyUrl;
        this.description = description;
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

}
