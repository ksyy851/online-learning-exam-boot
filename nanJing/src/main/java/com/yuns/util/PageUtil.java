package com.yuns.util;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PageUtil<T> {

    public PageUtil(IService<T> service){
        this.service = service;
    }

    private IService<T> service = null;

    public Page<T> page(int index , int size , Wrapper wrapper){
        return service.selectPage(new Page<T>(index,size),wrapper);
    }
}
