//package com.yuns.util;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
//import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
//import redis.clients.jedis.Jedis;
//import redis.clients.jedis.JedisPool;
//import redis.clients.jedis.JedisPoolConfig;
//import redis.clients.jedis.exceptions.JedisConnectionException;
//
//import java.net.SocketTimeoutException;
//
//@Configuration
//public class RedisUtil {
//
//
//    @Value("${spring.redis.host}")
//    private static String ADDR;
//
//    @Value("${spring.redis.port}")
//    private static int PORT;
//
//    @Value("${spring.redis.password}")
//    private static String AUTH;
//
//    private static int MAX_ACTIVE = 800;
//
//    private static int MAX_IDLE = 100;
//
//    private static long MAX_WAIT = 50000;
//
//    private static int TIMEOUT = 50000;
//
//    private static boolean TEST_ON_BORROW = true;
//
//    public static JedisPool jedisPool = null;
//
//    @Bean
//    JedisPool jedisConnectionFactory() {
//        JedisPoolConfig config = new JedisPoolConfig();
//        config.setMaxTotal(MAX_ACTIVE);
//        config.setMaxIdle(MAX_IDLE);
//        config.setMaxWaitMillis(MAX_WAIT);
//        config.setTestOnBorrow(TEST_ON_BORROW);
//        jedisPool = new JedisPool(config, ADDR, PORT,TIMEOUT);
//        return jedisPool;
//
//    }
//
//    /**
//     * 获取Jedis实例
//     * @return
//     */
//    public synchronized static Jedis getJedis() {
//        int timeoutCount = 0;
//        while (true) // 如果是网络超时则多试几次
//        {
//            try
//            {
//                Jedis jedis = jedisPool.getResource();
//                return jedis;
//            } catch (Exception e)
//            {
//                // 底层原因是SocketTimeoutException，不过redis已经捕捉且抛出JedisConnectionException，不继承于前者
//                if (e instanceof JedisConnectionException || e instanceof SocketTimeoutException)
//                {
//                    timeoutCount++;
//                    System.out.println("getJedis timeoutCount="+timeoutCount);
//                    if (timeoutCount > 3)
//                    {
//                        break;
//                    }
//                }else
//                {
//
//                    break;
//                }
//            }
//        }
//        return null;
//    }
//    /**
//     * 释放jedis资源
//     * @param jedis
//     */
//    public static void returnResource(final Jedis jedis) {
//        if (jedis != null) {
//            jedisPool.returnResource(jedis);
//        }
//    }
//
//    public static void mset(String key,String value){
//        Jedis redis = getJedis();
//        try {
//            redis.set(key, value);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }finally{
//            jedisPool.returnResource(redis);
//        }
//    }
//
//}
