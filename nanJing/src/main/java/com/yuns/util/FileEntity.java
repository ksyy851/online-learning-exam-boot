package com.yuns.util;
import java.sql.Timestamp;
import lombok.Data;

@Data
public class FileEntity {
    private String type;
    private String size;
    private String path;
    private String titleOrig;
    private String titleAlter;
    private Timestamp uploadTime;
}
