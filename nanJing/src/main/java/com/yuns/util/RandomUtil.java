package com.yuns.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class RandomUtil {

    public static Random rand = new Random();

    public static String getRandomString(int length) { //length表示生成字符串的长度
        String base = "abcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }


    /**
     * 生成随机订单号
     *
     * @return
     */
    public static String getRandomOrderNo() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");
        String now = format.format(new Date());
        String randomString = PayCommonUtil.getRandomString(6);
        return now + randomString;
    }

      /**
       * 生成账单编号
       *
       * @return
       */
      public static String getRandomdbBilelistNo() {
          SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
          String now = format.format(new Date());
          String randomString = PayCommonUtil.getRandomString(6);
          return now + randomString;
      }

    /**
     * 生成退款随机订单号
     *
     * @return
     */
    public static String getRandomOrderTNo() {

        SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
        String now ="T"+ format.format(new Date());
        String randomString = PayCommonUtil.getRandomString(6);
        return now + randomString;
    }


    /**
     * 生成入库订单
     *
     * @return
     */
    public static String getRandomOrderRNo() {

        SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
        String now ="R"+ format.format(new Date());
        String randomString = PayCommonUtil.getRandomString(6);
        return now + randomString;
    }



    /**
     * 生成报装订单
     *
     * @return
     */
    public static String getRandomOrderBNo() {

        SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
        String now ="B"+ format.format(new Date());
        String randomString = PayCommonUtil.getRandomString(6);
        return now + randomString;
    }

    /**
     * 生成报装单号
     *
     * @return
     */
    public static String getRandomOrderBZNo() {

        SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
        String now ="BZ"+ format.format(new Date());
        String randomString = PayCommonUtil.getRandomString(6);
        return now + randomString;
    }


    /**
     * 生成出库订单
     *
     * @return
     */
    public static String getRandomOrderXNo() {

        SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
        String now ="X"+ format.format(new Date());
        String randomString = PayCommonUtil.getRandomString(6);
        return now + randomString;
    }

    /**
     * 生成出库订单
     *
     * @return
     */
    public static String getRandomOrderPNo() {

        SimpleDateFormat format = new SimpleDateFormat("MMddHHss");
        String now ="P"+ format.format(new Date());
        String randomString = PayCommonUtil.getRandomString(6);
        return now + randomString;
    }


    public static int getRandomNumbe() {
        Random random = new Random();
        return random.nextInt(10);
    }




    public static void main(String[] args) {
//        int count=0;
//        for (int i = 0; i < 1000; i++) {
//            int randomNumbe = getRandomNumbe();
//            if (randomNumbe==3){
//                count++;
//            }
//            System.out.println(randomNumbe
//            );
//        }
//        for (int i = 0; i < 1000; i++) {
//            double count = rand.nextDouble();
//            DecimalFormat df = new DecimalFormat("0.00");
//            System.out.println("@@@@" + df.format(count));
//        }
//
//        BigDecimal decimal=new BigDecimal("3.6777");
//        System.out.println(decimal.intValue());
//        Double doubleVaule= decimal.subtract(new BigDecimal(decimal.intValue())).doubleValue();
//        System.out.println(doubleVaule);
//        BigDecimal setScale = decimal.setScale(2,BigDecimal.ROUND_HALF_DOWN);
//        System.out.println(setScale);
        List<BigDecimal> denominationList = new ArrayList();
        denominationList.add(new BigDecimal("2"));
        denominationList.add(new BigDecimal("4"));
        denominationList.add(new BigDecimal("4"));
        denominationList.add(new BigDecimal("5"));
        boolean isRepeat = denominationList.size() != new HashSet<BigDecimal>(denominationList).size();
        System.out.println(isRepeat);


    }

    /***
     * 随机0到1之间的小数
     * @return
     */
    public static double getRandom() {
        double count = rand.nextDouble();
        DecimalFormat df = new DecimalFormat("0.00");
        return Double.parseDouble(df.format(count));
    }

}
