package com.yuns.util.push;//package com.qcdl.util.push;
//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONObject;
//import com.qcdl.rest.dto.PushMessageDto;
//import com.qcdl.util.push.android.*;
//import com.qcdl.util.push.ios.*;
//import org.wzd.framwork.utils.PropertiesUtil;
//
//import java.util.List;
//
//public class UPushUtils {
//
//    private static PropertiesUtil pu = new PropertiesUtil("/configs/upush.properties");
//    private static String iosAppkey = pu.getProperty("push.ios.APP_KEY");
//    private static String iosAppMasterSecret = pu.getProperty("push.ios.SECRET");
//    private static String androidAppkey = pu.getProperty("push.android.APP_KEY");
//    private static String androidAppMasterSecret = pu.getProperty("push.android.SECRET");
//    private static PushClient client = new PushClient();
//
//    //安卓广播推送
//    private static void sendAndroidBroadcast(PushMessageDto pushMessageDto) throws Exception {
//        AndroidBroadcast broadcast = new AndroidBroadcast(androidAppkey, androidAppMasterSecret);
//        //通知栏提示文字（必填）
//        broadcast.setTicker(pushMessageDto.getTicker());
//        //通知栏标题（必填）
//        broadcast.setTitle(pushMessageDto.getTitle());
//        //通知文字描述（必填）
//        broadcast.setText(pushMessageDto.getText());
//        broadcast.goAppAfterOpen();
//        broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
//        // TODO Set 'production_mode' to 'false' if it's a test device.
//        // For how to register a test device, please see the developer doc.t
//        broadcast.setProductionMode();
//        // Set customized fields
//        //设置扩展字段
//        if (pushMessageDto.getExtra() != null){
//            broadcast.setExtraField("extraField", JSON.toJSONString(pushMessageDto.getExtra()));
//        }
//        client.send(broadcast);
//    }
//
//
//
//    //安卓组播推送
//    private static void sendAndroidGroupcast(PushMessageDto pushMessageDto) throws Exception {
//        AndroidGroupcast groupcast = new AndroidGroupcast(androidAppkey, androidAppMasterSecret);
//        /*  TODO
//         *  Construct the filter condition:
//         *  "where":
//         *	{
//         *		"and":
//         *		[
//         *			{"tag":"test"},
//         *			{"tag":"Test"}
//         *		]
//         *	}
//         */
//        JSONObject filterJson = new JSONObject();
//        JSONObject whereJson = new JSONObject();
//        JSONArray tagArray = new JSONArray();
//        List tags = pushMessageDto.getTags();
//        if (tags != null && tags.size()>0){
//            for (Object tag : tags) {
//                tagArray.add(new JSONObject().put("tag",tag));
//            }
//        }
//        whereJson.put("and", tagArray);
//        filterJson.put("where", whereJson);
//
//        groupcast.setFilter(filterJson);
//        groupcast.setTicker(pushMessageDto.getTicker());
//        groupcast.setTitle(pushMessageDto.getTitle());
//        groupcast.setText(pushMessageDto.getText());
//        groupcast.goAppAfterOpen();
//        groupcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
//        // TODO Set 'production_mode' to 'false' if it's a test device.
//        // For how to register a test device, please see the developer doc.
//        groupcast.setProductionMode();
//        client.send(groupcast);
//    }
//
//    //安卓Customizedcast进行推送
//    private static void sendAndroidCustomizedcast(PushMessageDto pushMessageDto) throws Exception {
//        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(androidAppkey, androidAppMasterSecret);
//        // TODO Set your alias here, and use comma to split them if there are multiple alias.
//        // And if you have many alias, you can also upload a file containing these alias, then
//        // use file_id to send customized notification.
//        customizedcast.setAlias(pushMessageDto.getAlias(), pushMessageDto.getAlias_type());
//        customizedcast.setTicker(pushMessageDto.getTicker());
//        customizedcast.setTitle(pushMessageDto.getTitle());
//        customizedcast.setText(pushMessageDto.getText());
//        customizedcast.goAppAfterOpen();
//        customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
//        // TODO Set 'production_mode' to 'false' if it's a test device.
//        // For how to register a test device, please see the developer doc.
//        customizedcast.setProductionMode();
//        client.send(customizedcast);
//    }
//
//    //CustomizedcastFile推送   将alias封装进file中
//    private static void sendAndroidCustomizedcastFile(PushMessageDto pushMessageDto) throws Exception {
//        AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(androidAppkey, androidAppMasterSecret);
//        // TODO Set your alias here, and use comma to split them if there are multiple alias.
//        // And if you have many alias, you can also upload a file containing these alias, then
//        // use file_id to send customized notification.
//        String fileId = client.uploadContents(androidAppkey, androidAppMasterSecret, "aa" + "\n" + "bb" + "\n" + pushMessageDto.getAlias());
//        customizedcast.setFileId(fileId, pushMessageDto.getAlias_type());
//        customizedcast.setTicker(pushMessageDto.getTicker());
//        customizedcast.setTitle(pushMessageDto.getTitle());
//        customizedcast.setText(pushMessageDto.getText());
//        customizedcast.goAppAfterOpen();
//        customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
//        // TODO Set 'production_mode' to 'false' if it's a test device.
//        // For how to register a test device, please see the developer doc.
//        customizedcast.setProductionMode();
//        client.send(customizedcast);
//    }
//
//    //文件播
//    private static void sendAndroidFilecast(PushMessageDto pushMessageDto) throws Exception {
//        AndroidFilecast filecast = new AndroidFilecast(androidAppkey, androidAppMasterSecret);
//        // TODO upload your device tokens, and use '\n' to split them if there are multiple tokens
//        String fileId = client.uploadContents(androidAppkey, androidAppMasterSecret, "aa" + "\n" + "bb");
//        filecast.setFileId(fileId);
//        filecast.setTicker(pushMessageDto.getTicker());
//        filecast.setTitle(pushMessageDto.getTitle());
//        filecast.setText(pushMessageDto.getText());
//        filecast.goAppAfterOpen();
//        filecast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
//        client.send(filecast);
//    }
//
//    //ios广播推送
//    private static void sendIOSBroadcast(PushMessageDto pushMessageDto) throws Exception {
//        IOSBroadcast broadcast = new IOSBroadcast(iosAppkey, iosAppMasterSecret);
//        broadcast.setAlert(pushMessageDto.getAlert());
//        broadcast.setBadge(0);
//        broadcast.setSound("default");
//        // TODO set 'production_mode' to 'true' if your app is under production mode
//       // broadcast.setTestMode();
//        broadcast.setProductionMode();
//        // Set customized fields
//        //设置扩展信息
//        if (pushMessageDto.getCustomizedField() != null){
//            broadcast.setCustomizedField("customizedField", JSON.toJSONString(pushMessageDto.getCustomizedField()));
//        }
//        client.send(broadcast);
//    }
//
//    //ios单播推送
//    private static void sendIOSUnicast(PushMessageDto pushMessageDto) throws Exception {
//        IOSUnicast unicast = new IOSUnicast(iosAppkey, iosAppMasterSecret);
//        // TODO Set your device token
//        unicast.setDeviceToken(pushMessageDto.getDeviceToken());
//        unicast.setAlert(pushMessageDto.getAlert());
//        unicast.setBadge(0);
//        unicast.setSound("default");
//        // TODO set 'production_mode' to 'true' if your app is under production mode
//        //unicast.setTestMode();
//        unicast.setProductionMode();
//        // Set customized fields
//        //设置扩展信息
//        if (pushMessageDto.getCustomizedField() != null){
//            unicast.setCustomizedField("customizedField", JSON.toJSONString(pushMessageDto.getCustomizedField()));
//        }
//        client.send(unicast);
//    }
//
//    //ios组播
//    private static void sendIOSGroupcast(PushMessageDto pushMessageDto) throws Exception {
//        IOSGroupcast groupcast = new IOSGroupcast(iosAppkey, iosAppMasterSecret);
//        /*  TODO
//         *  Construct the filter condition:
//         *  "where":
//         *	{
//         *		"and":
//         *		[
//         *			{"tag":"iostest"}
//         *		]
//         *	}
//         */
//        JSONObject filterJson = new JSONObject();
//        JSONObject whereJson = new JSONObject();
//        JSONArray tagArray = new JSONArray();
//        List tags = pushMessageDto.getTags();
//        if (tags != null && tags.size()>0){
//            for (Object tag : tags) {
//                tagArray.add(new JSONObject().put("tag",tag));
//            }
//        }
//        whereJson.put("and", tagArray);
//        filterJson.put("where", whereJson);
//
//        // Set filter condition into rootJson
//        groupcast.setFilter(filterJson);
//        groupcast.setAlert(pushMessageDto.getAlert());
//        groupcast.setBadge(0);
//        groupcast.setSound("default");
//        // TODO set 'production_mode' to 'true' if your app is under production mode
//        //groupcast.setTestMode();
//        groupcast.setProductionMode();
//        client.send(groupcast);
//    }
//
//    //IOSCustomizedcast推送
//    private static void sendIOSCustomizedcast(PushMessageDto pushMessageDto) throws Exception {
//        IOSCustomizedcast customizedcast = new IOSCustomizedcast(iosAppkey, iosAppMasterSecret);
//        // TODO Set your alias and alias_type here, and use comma to split them if there are multiple alias.
//        // And if you have many alias, you can also upload a file containing these alias, then
//        // use file_id to send customized notification.
//        customizedcast.setAlias(pushMessageDto.getAlias(), pushMessageDto.getAlias_type());
//        customizedcast.setAlert(pushMessageDto.getAlert());
//        customizedcast.setBadge(0);
//        customizedcast.setSound("default");
//        // TODO set 'production_mode' to 'true' if your app is under production mode
//       // customizedcast.setTestMode();
//        customizedcast.setProductionMode();
//        client.send(customizedcast);
//    }
//
//    //IOSFilecast推送
//    private static void sendIOSFilecast(PushMessageDto pushMessageDto) throws Exception {
//        IOSFilecast filecast = new IOSFilecast(iosAppkey, iosAppMasterSecret);
//        // TODO upload your device tokens, and use '\n' to split them if there are multiple tokens
//        String fileId = client.uploadContents(iosAppkey, iosAppMasterSecret, "aa" + "\n" + "bb");
//        filecast.setFileId(fileId);
//        filecast.setAlert(pushMessageDto.getAlert());
//        filecast.setBadge(0);
//        filecast.setSound("default");
//        // TODO set 'production_mode' to 'true' if your app is under production mode
//        //filecast.setTestMode();
//        filecast.setProductionMode();
//        client.send(filecast);
//    }
//
//
//    public static void pushAll(String androidAppkey) {
//
////        sendIOSCustomizedcast();//苹果
////        sendAndroidCustomizedcast();//安卓
//
//    }
//
//    public static void main(String[] args) {
//        // TODO set your appkey and master secret here
//        Demo demo = new Demo("5abc52898f4a9d2501000050", "xdyeilpbgybrtegtutrgtl7ujxa5fawv");
//        try {
////			demo.sendAndroidUnicast();
//			/* TODO these methods are all available, just fill in some fields and do the test
//			 * demo.sendAndroidCustomizedcastFile();
//			 * demo.sendAndroidBroadcast();
//			 * demo.sendAndroidGroupcast();
//			 * demo.sendAndroidCustomizedcast();
//			 * demo.sendAndroidFilecast();
//			 *
//			 * demo.sendIOSBroadcast();
//			 * demo.sendIOSUnicast();
////			 * demo.sendIOSGroupcast();
//			 * demo.sendIOSCustomizedcast();
//			 * demo.sendIOSFilecast();
//			 */
//            demo.sendIOSCustomizedcast();
//
//            PushMessageDto pushMessageDto =new PushMessageDto();
////            pushMessageDto.setTitle();
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//      }
//
//    //Broadcast推送
//    public static void Broadcast(PushMessageDto pushMessageDto){
//        try {
//            sendAndroidBroadcast(pushMessageDto);
//            sendIOSBroadcast(pushMessageDto);
//        }catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    //Unicast推送
//    public static void Unicast(PushMessageDto pushMessageDto){
//        try {
//            sendAndroidUnicast(pushMessageDto);
//            sendIOSUnicast(pushMessageDto);
//        }catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    //Groupcast推送
//    public static void Groupcast(PushMessageDto pushMessageDto){
//        try {
//            sendAndroidGroupcast(pushMessageDto);
//            sendIOSGroupcast(pushMessageDto);
//        }catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    //Customizedcast推送-----
//    public static void Customizedcast(PushMessageDto pushMessageDto){
//        try {
//            sendAndroidCustomizedcast(pushMessageDto);
//            sendIOSCustomizedcast(pushMessageDto);
//        }catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//
//    //Filecast推送
//    public static void Filecast(PushMessageDto pushMessageDto){
//        try {
//            sendAndroidFilecast(pushMessageDto);
//            sendIOSFilecast(pushMessageDto);
//        }catch (Exception ex) {
//            ex.printStackTrace();
//        }
//    }
//}
