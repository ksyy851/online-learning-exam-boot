package com.yuns.util.push;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import com.yuns.util.push.android.AndroidBroadcast;
import com.yuns.util.push.android.AndroidUnicast;
import com.yuns.util.push.ios.IOSBroadcast;
import com.yuns.util.push.ios.IOSUnicast;
import org.apache.logging.log4j.util.PropertiesUtil;


import java.util.HashMap;
import java.util.Map;


public class UPushUtil {
    //todo 配置秘钥
    private static String appkey_Ios = "5abc52898f4a9d2501000050";
    private static String appMasterSecret_Ios ="xdyeilpbgybrtegtutrgtl7ujxa5fawv";
    private static String appkey_Android = "5abc58a7f43e48163f000027";
    private static String appMasterSecret_Android = "na9emxanxsozq65zq2njynhne7am5lzx";

    private String timestamp = null;
    private static PushClient client = new PushClient();

    //ios广播推送
    private static void sendIOSBroadcast(PushMessageDto pushMessageDto) throws Exception {
        IOSBroadcast broadcast = new IOSBroadcast(appkey_Ios, appMasterSecret_Ios);
        broadcast.setAlert(pushMessageDto.getAlert());
        broadcast.setBadge(0);
        broadcast.setSound("default");
        // TODO set 'production_mode' to 'true' if your app is under production mode
        broadcast.setTestMode();
        //broadcast.setProductionMode();
        // Set customized fields
        //设置扩展信息
        if (pushMessageDto.getCustomizedField() != null){
            broadcast.setCustomizedField("customizedField", JSON.toJSONString(pushMessageDto.getCustomizedField()));
        }
        client.send(broadcast);
    }

    //安卓广播推送
    private static void     sendAndroidBroadcast(PushMessageDto pushMessageDto) throws Exception {
        AndroidBroadcast broadcast = new AndroidBroadcast(appkey_Android, appMasterSecret_Android);
        //通知栏提示文字（必填）
        broadcast.setTicker(pushMessageDto.getTicker());
        //通知栏标题（必填）
        broadcast.setTitle(pushMessageDto.getTitle());
        //通知文字描述（必填）
        broadcast.setText(pushMessageDto.getText());
        broadcast.goAppAfterOpen();
        broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        // TODO Set 'production_mode' to 'false' if it's a test device.
        // For how to register a test device, please see the developer doc.t
//        broadcast.setProductionMode();
        // Set customized fields
        broadcast.setTestMode();
 //       broadcast.setProductionMode();
        //设置扩展字段
        if (pushMessageDto.getExtra() != null){
            broadcast.setExtraField("extraField", JSON.toJSONString(pushMessageDto.getExtra()));
        }
        client.send(broadcast);
    }

        //Broadcast推送
    public static void Broadcast(PushMessageDto pushMessageDto){
        try {
            sendAndroidBroadcast(pushMessageDto);
            sendIOSBroadcast(pushMessageDto);
        }catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    //ios单播推送
    private static void sendIOSUnicast(PushMessageDto pushMessageDto) throws Exception {
        IOSUnicast unicast = new IOSUnicast(appkey_Ios, appMasterSecret_Ios);
        // TODO Set your device token
        unicast.setDeviceToken(pushMessageDto.getDeviceToken());

        unicast.setAlert(pushMessageDto.getAlert());
        unicast.setBadge(1);
        unicast.setSound("default");
        // TODO set 'production_mode' to 'true' if your app is under production mode
        unicast.setTestMode();
//        unicast.setProductionMode();
        // Set customized fields
        //设置扩展信息
        if (pushMessageDto.getCustomizedField() != null){
            unicast.setCustomizedField("text", "heh");
        }

        client.send(unicast);
    }

    //安卓单播推送
    private static void sendAndroidUnicast(PushMessageDto pushMessageDto) throws Exception {
        AndroidUnicast unicast = new AndroidUnicast(appkey_Android, appMasterSecret_Android);
        // TODO Set your device tokenpayload
        unicast.setDeviceToken(pushMessageDto.getDeviceToken());
        unicast.setTicker(pushMessageDto.getTicker());
        unicast.setTitle(pushMessageDto.getTitle());
        unicast.setText(pushMessageDto.getText());
        unicast.goAppAfterOpen();
        unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
        // TODO Set 'production_mode' to 'false' if it's a test device.
        // For how to register a test device, please see the developer doc.
        //unicast.setProductionMode();
        unicast.setTestMode();
        // Set customized fields
        //设置扩展字段
        if (pushMessageDto.getExtra() != null){
            unicast.setExtraField("extraField", JSON.toJSONString(pushMessageDto.getExtra()));
        }
        client.send(unicast);
    }



    /***
     * 推送
     * @return
     */
    public static void push(String deviceToken, String title, String content) throws Exception {

            Map map=new HashMap();
            JSONObject object=new JSONObject();
            object.put("title",title);
            object.put("subtitle",content);
//            object.put("body","测试body");
            PushMessageDto pushMessageDto=new PushMessageDto();
            pushMessageDto.setTitle(title);
            pushMessageDto.setAlert(object);
            pushMessageDto.setCustomizedField(JSON.toJSONString(map));
            pushMessageDto.setText(content);
            pushMessageDto.setDeviceToken(deviceToken);
            sendAndroidUnicast(pushMessageDto);
            sendIOSUnicast(pushMessageDto);//苹果

    }

    /***
     * 推送至全部设备
     * @param title
     * @param content
     */
    public static void pushAll(String title,String content) throws Exception {
        Map map=new HashMap();
        JSONObject object=new JSONObject();
        object.put("title",title);
        object.put("body",content);
//            object.put("body","测试body");
        PushMessageDto pushMessageDto=new PushMessageDto();
        pushMessageDto.setTitle(title);
        pushMessageDto.setAlert(object);
        pushMessageDto.setCustomizedField(JSON.toJSONString(map));
        pushMessageDto.setText(content);
        sendIOSBroadcast(pushMessageDto);
        sendAndroidBroadcast(pushMessageDto);
    }

    public static void main(String[] args) {
        try {
            //PushMessageDto pushMessageDto=new PushMessageDto();

               push("Av54UF1CvPRF7LDoYecdnIqJU0yKBnUpNJOLcmGessvj","haha","最后一遍测试推送!");//单推测试

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
