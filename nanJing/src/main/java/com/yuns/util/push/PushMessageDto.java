package com.yuns.util.push;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 推送消息
 *
 * @author ruanbansheng
 * @date 2018/4/13 17:40
 */
@ApiModel("推送消息")
public class PushMessageDto implements Serializable{

    //安卓推送特有参数
    @ApiModelProperty("通知栏提示文字,必填")
    private String ticker;

    @ApiModelProperty("通知栏标题,必填")
    private String title;

    @ApiModelProperty("通知文字描述,必填")
    private String text;

    @ApiModelProperty("自定义扩展信息,可为空")
    private Object extra;

    //ios推送特有字段
    @ApiModelProperty("推送主要内容,包括title，subtitle，body,可以为string类型或json形式")
    private JSONObject alert;

    @ApiModelProperty("自定义扩展信息,可为空")
    private Object customizedField;

    //安卓、ios共有字段
    @ApiModelProperty("设备标识，单播推送填写")
    private String deviceToken;

    @ApiModelProperty("分组标签，组播推送填写")
    private List tags;

    @ApiModelProperty("Customizedcast推送、CustomizedcastFile推送使用，开发者填写自己的alias, 要求不超过500个alias, 多个alias以英文逗号间隔")
    private String alias;

    @ApiModelProperty("Customizedcast推送时使用")
    private String alias_type;

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Object getExtra() {
        return extra;
    }

    public void setExtra(Object extra) {
        this.extra = extra;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public List getTags() {
        return tags;
    }

    public void setTags(List tags) {
        this.tags = tags;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias_type() {
        return alias_type;
    }

    public void setAlias_type(String alias_type) {
        this.alias_type = alias_type;
    }

    public JSONObject getAlert() {
        return alert;
    }

    public void setAlert(JSONObject alert) {
        this.alert = alert;
    }

    public Object getCustomizedField() {
        return customizedField;
    }

    public void setCustomizedField(Object customizedField) {
        this.customizedField = customizedField;
    }
}
