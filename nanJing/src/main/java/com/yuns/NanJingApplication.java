package com.yuns;

import com.yuns.util.SpringBeanUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.MultipartConfigElement;

@SpringBootApplication
@EnableFeignClients
@ServletComponentScan
@EnableScheduling
public class NanJingApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(NanJingApplication.class, args);
        SpringBeanUtil.setApplicationContext(applicationContext);
    }

    /**
     * 文件上传配置
     *
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //文件最大
        factory.setMaxFileSize("30240MB"); //KB,MB
        /// 设置总上传数据总大小
        factory.setMaxRequestSize("202400MB");
        return factory.createMultipartConfig();
    }

    @Override
    protected WebApplicationContext run(SpringApplication application) {
        WebApplicationContext run = super.run(application);
        SpringBeanUtil.setApplicationContext(run);
        return run;
    }
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(NanJingApplication.class);
    }

}
