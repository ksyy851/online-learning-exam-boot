package com.yuns.statistic.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.statistic.entity.StatisticAboutCompany;
import com.yuns.statistic.entity.StatisticAllKindsUser;
import com.yuns.statistic.entity.StatisticCharts;
import com.yuns.statistic.entity.StatisticInfo;
import com.yuns.statistic.mapper.StatisticMapper;
import com.yuns.statistic.param.StatisticParam;
import com.yuns.statistic.service.IStatisticService;
import com.yuns.web.sys.param.BasePage;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class StatisticServiceImpl extends ServiceImpl<StatisticMapper, StatisticInfo> implements IStatisticService {

    @Override
    public StatisticInfo queryStatisticIndicator() {
        return baseMapper.queryStatisticIndicator();
    }

    @Override
    public List<Map<String, Object>> queryLearnTimeCv(StatisticParam statisticParam) {
        return baseMapper.queryLearnTimeCv(statisticParam);
    }

    @Override
    public List<StatisticCharts> queryIntegral(StatisticParam statisticParam) {
        return baseMapper.queryIntegral(statisticParam);
    }

    @Override
    public List<StatisticAllKindsUser> queryAllKindsUser(BasePage frontPage) {
        return baseMapper.queryAllKindsUser(frontPage);
    }

    @Override
    public List<Map<String, Object>> queryVideoViewCount(StatisticParam statisticParam) {
        return baseMapper.queryVideoViewCount(statisticParam);
    }

    @Override
    public List<Map<String, Object>> queryVideoViewRate(StatisticParam statisticParam) {
        return baseMapper.queryVideoViewRate(statisticParam);
    }

    @Override
    public List<Map<String, Object>> queryLearnTimeRate(StatisticParam statisticParam) {
        return baseMapper.queryLearnTimeRate(statisticParam);
    }

    @Override
    public List<StatisticAboutCompany> queryAboutCompany() {
        return baseMapper.queryAboutCompany();
    }
}
