package com.yuns.statistic.controller;


import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.statistic.entity.StatisticAllKindsUser;
import com.yuns.statistic.param.StatisticParam;
import com.yuns.statistic.service.IStatisticService;
import com.yuns.statistic.utils.StatisticUtils;
import com.yuns.util.ResultJson;
import com.yuns.web.sys.param.BasePage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhangzhan
 * @since 2020-11-14
 */
@Api(tags = "统计分析 - Controller" )
@RestController
@RequestMapping("/statistic" )
public class StatisticController {

    @Autowired
    private IStatisticService iStatisticService;

    @ApiOperation(value = "统计分析-今日课程浏览量、全部课程浏览量、课程观看时长、课程完成率，考试合格率 - queryStatisticIndicator" )
    @PostMapping(value = "queryStatisticIndicator" )
    public ResultJson queryStatisticIndicator(){
        return ResultJson.ok(iStatisticService.queryStatisticIndicator());
    }

    @ApiOperation(value = "统计分析-学时积分统计 - queryLearnTimeAndIntegral" )
    @PostMapping(value = "queryLearnTimeAndIntegral" )
    public ResultJson queryLearnTimeAndIntegral(@RequestBody StatisticParam statisticParam){
        Map<String,Object> resultMap = new HashMap<>();
        List<String> dateList = StatisticUtils.getDateList(statisticParam);//日期mm.dd 集合
        List<String> categoryList= Arrays.asList("学时","浏览量");
        List<Map<String, Object>> leftList = iStatisticService.queryLearnTimeCv(statisticParam);
        resultMap.put("left",StatisticUtils.twoTypeCMap(dateList,categoryList,"rq","count","type",leftList));
        resultMap.put("right",iStatisticService.queryIntegral(statisticParam));
        return ResultJson.ok(resultMap);
    }

    @ApiOperation(value = "统计分析-各类用户统计 - queryAllKindsUser" )
    @PostMapping(value = "queryAllKindsUser" )
    public ResultJson queryAllKindsUser(@RequestBody BasePage<StatisticParam> frontPage){
        if (!frontPage.is_search()) {
            Page<StatisticAllKindsUser> page = new Page<StatisticAllKindsUser>();
            Page<StatisticAllKindsUser> list = page.setRecords(iStatisticService.queryAllKindsUser(frontPage));
            return ResultJson.ok(list);
        }
        Page<StatisticAllKindsUser> page = new Page<StatisticAllKindsUser>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<StatisticAllKindsUser> list = page.setRecords(iStatisticService.queryAllKindsUser(frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "视频播放-观看量 - queryVideoViewCount" )
    @PostMapping(value = "queryVideoViewCount" )
    public ResultJson queryVideoViewCount(@RequestBody StatisticParam statisticParam){
        return ResultJson.ok(iStatisticService.queryVideoViewCount(statisticParam));
    }

    @ApiOperation(value = "视频播放-占全部视频播放比例、占全部学时比例 - queryVideoViewRate" )
    @PostMapping(value = "queryVideoViewRate" )
    public ResultJson queryVideoViewRate(@RequestBody StatisticParam statisticParam){
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("up",iStatisticService.queryVideoViewRate(statisticParam));
        resultMap.put("down",iStatisticService.queryLearnTimeRate(statisticParam));
        return ResultJson.ok(resultMap);
    }

    @ApiOperation(value = "统计分析-关于企业 - queryAboutCompany" )
    @PostMapping(value = "queryAboutCompany" )
    public ResultJson queryAboutCompany(){
        return ResultJson.ok(iStatisticService.queryAboutCompany());
    }

}
