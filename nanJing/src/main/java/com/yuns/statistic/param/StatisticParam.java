package com.yuns.statistic.param;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * Description:统计分析入参
 * User: zhangzhan
 * Date: 2020-11-14
 */
@Data
public class StatisticParam {

    @ApiModelProperty(value = "开始时间yyyy-mm-dd", required = true)
    private String bgTm;

    @ApiModelProperty(value = "结束时间yyyy-mm-dd", required = true)
    private String endTm;

}
