package com.yuns.statistic.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.statistic.entity.StatisticAboutCompany;
import com.yuns.statistic.entity.StatisticAllKindsUser;
import com.yuns.statistic.entity.StatisticCharts;
import com.yuns.statistic.entity.StatisticInfo;
import com.yuns.statistic.param.StatisticParam;
import com.yuns.web.sys.param.BasePage;

import java.util.List;
import java.util.Map;

public interface StatisticMapper  extends BaseMapper<StatisticInfo> {

    StatisticInfo queryStatisticIndicator();

    List<Map<String, Object>> queryLearnTimeCv(StatisticParam statisticParam);

    List<StatisticCharts> queryIntegral(StatisticParam statisticParam);

    List<StatisticAllKindsUser> queryAllKindsUser(BasePage frontPage);

    List<Map<String, Object>> queryVideoViewCount(StatisticParam statisticParam);

    List<Map<String, Object>> queryVideoViewRate(StatisticParam statisticParam);

    List<Map<String, Object>> queryLearnTimeRate(StatisticParam statisticParam);

    List<StatisticAboutCompany> queryAboutCompany();

}
