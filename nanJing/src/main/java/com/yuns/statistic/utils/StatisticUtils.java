package com.yuns.statistic.utils;

import com.yuns.statistic.param.StatisticParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class StatisticUtils {

    private static final Logger logger = LoggerFactory.getLogger(StatisticUtils.class);

    public static List<String> getDateList(StatisticParam statisticParam){
        List<String> dateList = new ArrayList<>();
        try{
            SimpleDateFormat formatImport = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat formatExport = new SimpleDateFormat("MM.dd");
            Date bgTm = formatImport.parse(statisticParam.getBgTm());
            Date endTm = formatImport.parse(statisticParam.getEndTm());
            Calendar calendar = Calendar.getInstance();
            while(bgTm.compareTo(endTm) <= 0){
                calendar.setTime(bgTm);
                dateList.add(formatExport.format(calendar.getTime()));
                calendar.add(calendar.DATE,1);
                bgTm = calendar.getTime();
            }
        }catch (ParseException e){
            logger.error("传入开始时间或结束时间格式有误(yyyy-mm-dd)！");
        }
        return dateList;
    }

    /**
     * 二维柱状图  二维查询结果
     *
     * @param xStringList   横坐标数组
     * @param xField        横坐标字段
     * @param yField        纵坐标字段
     * @param categoryField 分组字段
     * @param values        值列表
     * @return 样例:
     */
    public static List<Map<String, Object>> twoTypeCMap(List<String> xStringList, List<String> categoryList, String xField, String yField, String categoryField, List<Map<String, Object>> values) {
        if (values == null) {
            values = new ArrayList<>();
        }
        Map<String, List<Map<String, Object>>> collect = values.stream().collect(Collectors.groupingBy(e -> (String) e.get(categoryField)));

        return categoryList.stream().map(
                e -> initResultMap(e, oneTypeBMap(xStringList, xField, yField, collect.get(e)))
        ).collect(Collectors.toList());
    }

    public static List<Map<String, Object>> oneTypeAMap(List<String> xStringList, Map<String, Object> values) {
        return xStringList.stream()
                .map(e -> initMap(e, values == null ? 0 : values.get(e)))
                .collect(Collectors.toList());
    }

    public static List<Map<String, Object>> oneTypeBMap(List<String> xStringList, String xField, String yField, List<Map<String, Object>> values) {
        return oneTypeAMap(
                xStringList,
                values == null ? null : values.stream().collect(Collectors.toMap(e -> (String) e.get(xField), e -> e.get(yField)))
        );
    }

    //方法
    public static Map<String, Object> initResultMap(String name, List<Map<String, Object>> result) {
        return initMap(name, result);
    }

    public static Map<String, Object> initMap(String name, Object value) {
        Map<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("value", value == null ? 0 : value);
        return result;
    }

    public static void main(String[] args) {
        StatisticParam statisticParam = new StatisticParam();
        statisticParam.setBgTm("2020-11-19");
        statisticParam.setEndTm("2020-11-22");
        System.out.println(getDateList(statisticParam));
    }
}
