package com.yuns.statistic.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 统计分析-指标
 * @author zhangzhan
 * @since 2020-11-18
 */
@Data
public class StatisticInfo {

    /**
     * 今日课程浏览量
     */
    @ApiParam(value = "今日课程浏览量")
    @ApiModelProperty(value = "今日课程浏览量")
    @TableField("today_course_views")
    private Integer todayCourseViews;

    /**
     * 全部课程浏览量
     */
    @ApiParam(value = "全部课程浏览量")
    @ApiModelProperty(value = "全部课程浏览量")
    @TableField("all_course_views")
    private Integer allCourseViews;

    /**
     *课程观看时长
     */
    @ApiParam(value = "课程观看时长")
    @ApiModelProperty(value = "课程观看时长")
    @TableField("course_view_time")
    private Double courseViewTime;

    /**
     *课程完成率
     */
    @ApiParam(value = "课程完成率")
    @ApiModelProperty(value = "课程完成率")
    @TableField("course_complete_rate")
    private Double courseCompleteRate;

    /**
     *考试合格率
     */
    @ApiParam(value = "考试合格率")
    @ApiModelProperty(value = "考试合格率")
    @TableField("exam_pass_rate")
    private Double examPassRate;

}
