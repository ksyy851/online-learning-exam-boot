package com.yuns.statistic.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class StatisticAllKindsUser {

    /**
     *账号类型
     */
    @ApiParam(value = "账号类型")
    @ApiModelProperty(value = "账号类型")
    @TableField("account_type")
    private String accountType;

    /**
     *账号个数
     */
    @ApiParam(value = "账号个数")
    @ApiModelProperty(value = "账号个数")
    @TableField("account_count")
    private String accountCount;

    /**
     *视频播放时长
     */
    @ApiParam(value = "视频播放时长")
    @ApiModelProperty(value = "视频播放时长")
    @TableField("play_time")
    private String playTime;

    /**
     *获取总积分
     */
    @ApiParam(value = "获取总积分")
    @ApiModelProperty(value = "获取总积分")
    @TableField("total_integral")
    private String totalIntegral;

    /**
     *已学总课时
     */
    @ApiParam(value = "已学总课时")
    @ApiModelProperty(value = "已学总课时")
    @TableField("total_hour")
    private String totalHour;

    /**
     *平均课时
     */
    @ApiParam(value = "平均课时")
    @ApiModelProperty(value = "平均课时")
    @TableField("avg_hour")
    private String avgHour;

    /**
     *考试次数
     */
    @ApiParam(value = "考试次数")
    @ApiModelProperty(value = "考试次数")
    @TableField("exam_count")
    private String examCount;

    /**
     *考试合格率
     */
    @ApiParam(value = "考试合格率")
    @ApiModelProperty(value = "考试合格率")
    @TableField("exam_percent")
    private String examPercent;

}
