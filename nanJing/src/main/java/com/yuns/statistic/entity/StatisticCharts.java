package com.yuns.statistic.entity;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class StatisticCharts {

    /**
     * 类型
     */
    @ApiParam(value = "类型")
    @ApiModelProperty(value = "类型")
    private String name;

    /**
     *数值
     */
    @ApiParam(value = "数值")
    @ApiModelProperty(value = "数值")
    private Float value;

}
