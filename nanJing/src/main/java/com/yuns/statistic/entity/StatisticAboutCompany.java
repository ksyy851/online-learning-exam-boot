package com.yuns.statistic.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class StatisticAboutCompany {

    /**
     *重点排污企业课时完成率
     */
    @ApiParam(value = "重点排污企业课时完成率")
    @ApiModelProperty(value = "重点排污企业课时完成率")
    @TableField("key_complete_rate")
    private String keyCompleteRate;

    /**
     *重点排污企业考试合格率
     */
    @ApiParam(value = "重点排污企业考试合格率")
    @ApiModelProperty(value = "重点排污企业考试合格率")
    @TableField("key_qualified_rate")
    private String keyQualifiedRate;

    /**
     *其他企业课时完成率
     */
    @ApiParam(value = "其他企业课时完成率")
    @ApiModelProperty(value = "其他企业课时完成率")
    @TableField("other_complete_rate")
    private String otherCompleteRate;

    /**
     *其他企业考试合格率
     */
    @ApiParam(value = "其他企业考试合格率")
    @ApiModelProperty(value = "其他企业考试合格率")
    @TableField("other_qualified_rate")
    private String otherQualifiedRate;

}
