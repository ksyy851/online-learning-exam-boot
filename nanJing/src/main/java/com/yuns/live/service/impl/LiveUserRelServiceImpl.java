package com.yuns.live.service.impl;

import com.yuns.live.entity.LiveBase;
import com.yuns.live.entity.LiveUserRel;
import com.yuns.live.mapper.LiveBaseMapper;
import com.yuns.live.mapper.LiveUserRelMapper;
import com.yuns.live.service.ILiveUserRelService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.web.param.FrontPage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 直播特定人群管理表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-22
 */
@Service
public class LiveUserRelServiceImpl extends ServiceImpl<LiveUserRelMapper, LiveUserRel> implements ILiveUserRelService {
    @Resource
    private LiveBaseMapper liveBaseMapper;

    @Override
    public List<LiveUserRel> selectLiveUserRelList(FrontPage frontPage) {
        return baseMapper.selectLiveUserRelList(frontPage);
    }

    /**
     * 根据用户ID查询是否有查看直播权限
     * @param userId
     * @return
     */
    @Override
    public boolean judgeLive(Integer userId ,Integer liveId) {
        LiveUserRel rel=new LiveUserRel();
        rel.setUserId(userId);
        rel.setLiveId(liveId);
        LiveUserRel flag=baseMapper.selectOne(rel);
        if(flag==null){
            LiveBase liveBase=liveBaseMapper.selectById(liveId);
            if(liveBase.getShowType()==0){
                return true;
            }
            return false;
        }
        return true;
    }
}
