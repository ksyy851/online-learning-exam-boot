package com.yuns.live.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yuns.company.entity.CompanyUserBase;
import com.yuns.company.service.ICompanyUserBaseService;
import com.yuns.live.entity.LiveBase;
import com.yuns.live.entity.LiveComment;
import com.yuns.live.mapper.LiveCommentMapper;
import com.yuns.live.param.LiveCommentParam;
import com.yuns.live.service.ILiveBaseService;
import com.yuns.live.service.ILiveCommentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.live.vo.LiveCommentVO;
import com.yuns.sms.entity.InfoChatManag;
import com.yuns.sms.service.IInfoChatManagService;
import com.yuns.study.service.IInfoDocIService;
import com.yuns.web.param.FrontPage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 直播评论表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
@Service
public class LiveCommentServiceImpl extends ServiceImpl<LiveCommentMapper, LiveComment> implements ILiveCommentService {

    @Resource
    private ILiveBaseService liveBaseService;

    @Resource
    private IInfoChatManagService infoChatManagService;

    @Override
    @Transactional
    public boolean insert(LiveComment model){
        String commentId=UUID.randomUUID().toString();
        if(model.getAnswerId()==null || "".equals(model.getAnswerId()) ){
            //根据直播id查询直播详情
            LiveBase liveBase=liveBaseService.selectById(model.getLiveId());
            InfoChatManag chatManag=new InfoChatManag();
            chatManag.setLiveId(model.getLiveId());
            chatManag.setLiveBroadcastName(liveBase.getLiveName());
            chatManag.setConsultants(model.getCompanyUserId());
            chatManag.setLiveTime(liveBase.getLiveTime());
            chatManag.setConsultantsContent(model.getComment());
            chatManag.setStatus(0);
            chatManag.setIsDeleted(0);
            chatManag.setCreateUser((long)model.getCompanyUserId());
            chatManag.setCommentId(commentId);
            infoChatManagService.insert(chatManag);

            model.setCommentId(commentId);
        }else {
            model.setAnswerId(UUID.randomUUID().toString());
            //回复则根据comment_id先查询,再执行更新
            EntityWrapper<InfoChatManag> wrapper=new EntityWrapper<>();
            InfoChatManag infoChatManag=new InfoChatManag();
            infoChatManag.setCommentId(model.getCommentId());
            wrapper.setEntity(infoChatManag);
            InfoChatManag chatManag=infoChatManagService.selectOne(wrapper);
            chatManag.setReplyContent(model.getComment());
            chatManag.setReplyId(model.getCompanyUserId());
            infoChatManagService.updateById(chatManag);
        }
        baseMapper.insert(model);
        return true;
    }

    /**
     * 互动评论查询
     * @param model
     * @return
     */
    @Override
    public List<LiveCommentVO> selectLiveCommentList(LiveCommentParam model) {
        Integer userId=model.getUserId();
        List<LiveCommentVO> commentList=new ArrayList<>();
        //根据用户ID判断是查询所有评论还是查询当个人评论
        EntityWrapper wrapper=new EntityWrapper();
        LiveBase liveBase=new LiveBase();
        liveBase.setCreater(userId);
        liveBase.setCreaterName(model.getUserName());
        wrapper.setEntity(liveBase);
        int count=liveBaseService.selectCount(wrapper);
        if(count>0){
            commentList=baseMapper.selectAll(model);
        }else {
            commentList=baseMapper.selectMyself(model);
        }
        return commentList;
    }
}
