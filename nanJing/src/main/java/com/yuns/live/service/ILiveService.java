package com.yuns.live.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.dto.LiveDto;
import com.yuns.live.entity.LiveBase;
import com.yuns.live.param.LiveParam;
import com.yuns.live.vo.LiveVO;
import com.yuns.live.vo.SpecifyPersonVO;
import com.yuns.util.ResultJson;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.util.List;

public interface ILiveService {
    boolean insert(LiveDto model);

    boolean up(LiveDto model);

    boolean deleteById(Integer id);

    List<LiveVO> selectLiveList(LiveParam frontPage);

    List<LiveVO> selectLiveList(Page<LiveVO> page,LiveParam frontPage);

    void exportList(LiveParam courseParam, HttpServletResponse response);

    LiveVO edit(Integer id) ;

    ResultJson<Object> uploadVideo(MultipartFile file, HttpServletRequest request);

    boolean delFile(String id);

    List<SpecifyPersonVO>  querySpecifyPerson();

    List<SpecifyPersonVO>  querySpecifyPerson(Integer id);
}
