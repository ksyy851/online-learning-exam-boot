package com.yuns.live.service.impl;

import com.yuns.live.entity.LiveVideoFile;
import com.yuns.live.mapper.LiveVideoFileMapper;
import com.yuns.live.service.ILiveVideoFileService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 视频文件表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Service
public class LiveVideoFileServiceImpl extends ServiceImpl<LiveVideoFileMapper, LiveVideoFile> implements ILiveVideoFileService {
        @Override
        public List<LiveVideoFile> selectLiveVideoFileList(FrontPage frontPage) {
        return   baseMapper.selectLiveVideoFileList(frontPage);
     }
}
