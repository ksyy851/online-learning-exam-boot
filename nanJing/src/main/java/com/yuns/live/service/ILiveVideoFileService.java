package com.yuns.live.service;

import com.yuns.live.entity.LiveVideoFile;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 视频文件表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface ILiveVideoFileService extends IService<LiveVideoFile> {
        List<LiveVideoFile> selectLiveVideoFileList(FrontPage frontPage);
}
