package com.yuns.live.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveBase;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.live.param.LiveParam;
import com.yuns.live.vo.LiveVO;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播基础表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
public interface ILiveBaseService extends IService<LiveBase> {
    List<LiveBase> selectLiveBaseList(FrontPage frontPage);

    List<LiveVO> selectLiveList(LiveParam frontPage);

    List<LiveVO> selectLiveList(Page<LiveVO> page, LiveParam frontPage);

    LiveVO selectLiveById(Integer id);
}
