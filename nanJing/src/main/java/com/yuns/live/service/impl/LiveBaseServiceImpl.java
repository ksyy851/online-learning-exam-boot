package com.yuns.live.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveBase;
import com.yuns.live.mapper.LiveBaseMapper;
import com.yuns.live.param.LiveParam;
import com.yuns.live.service.ILiveBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.yuns.live.vo.LiveVO;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播基础表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
@Service
public class LiveBaseServiceImpl extends ServiceImpl<LiveBaseMapper, LiveBase> implements ILiveBaseService {
    @Override
    public List<LiveBase> selectLiveBaseList(FrontPage frontPage) {
        return baseMapper.selectLiveBaseList(frontPage);
    }

    @Override
    public List<LiveVO> selectLiveList(LiveParam frontPage) {
        return baseMapper.selectLiveList(frontPage);
    }

    @Override
    public List<LiveVO> selectLiveList(Page<LiveVO> page, LiveParam frontPage) {
        return baseMapper.selectLiveList(page,frontPage);
    }

    @Override
    public LiveVO selectLiveById(Integer id) {
        return baseMapper.selectLiveById(id);
    }

}
