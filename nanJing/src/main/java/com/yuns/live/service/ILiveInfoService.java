package com.yuns.live.service;

import com.yuns.live.entity.LiveInfo;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 直播详情表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
public interface ILiveInfoService extends IService<LiveInfo> {
        List<LiveInfo> selectLiveInfoList(FrontPage frontPage);
}
