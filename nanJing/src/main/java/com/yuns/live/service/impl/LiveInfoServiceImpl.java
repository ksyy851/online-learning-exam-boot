package com.yuns.live.service.impl;

import com.yuns.live.entity.LiveInfo;
import com.yuns.live.mapper.LiveInfoMapper;
import com.yuns.live.service.ILiveInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.yuns.web.param.FrontPage;
import java.util.List;
/**
 * <p>
 * 直播详情表 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
@Service
public class LiveInfoServiceImpl extends ServiceImpl<LiveInfoMapper, LiveInfo> implements ILiveInfoService {
        @Override
        public List<LiveInfo> selectLiveInfoList(FrontPage frontPage) {
        return   baseMapper.selectLiveInfoList(frontPage);
     }
}
