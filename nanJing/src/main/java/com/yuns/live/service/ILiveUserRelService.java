package com.yuns.live.service;

import com.yuns.live.entity.LiveUserRel;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播特定人群管理表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-22
 */
public interface ILiveUserRelService extends IService<LiveUserRel> {
    List<LiveUserRel> selectLiveUserRelList(FrontPage frontPage);

    boolean judgeLive(Integer userId ,Integer liveId);
}
