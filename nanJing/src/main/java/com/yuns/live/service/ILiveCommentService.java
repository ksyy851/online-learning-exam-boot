package com.yuns.live.service;

import com.yuns.live.entity.LiveComment;
import com.baomidou.mybatisplus.service.IService;
import com.yuns.live.param.LiveCommentParam;
import com.yuns.live.vo.LiveCommentVO;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播评论表 服务类
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
public interface ILiveCommentService extends IService<LiveComment> {

    boolean insert(LiveComment model);

    List<LiveCommentVO> selectLiveCommentList(LiveCommentParam model);
}
