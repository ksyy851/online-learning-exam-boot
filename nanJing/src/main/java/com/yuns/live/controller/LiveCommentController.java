package com.yuns.live.controller;


import com.yuns.live.param.LiveCommentParam;
import com.yuns.live.service.ILiveCommentService;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveComment;
import org.springframework.web.bind.annotation.*;

//import com.yuns.com.yuns.web.sys.dto.LiveCommentDto;


/**
 * @author ${author}
 * @since 2020-11-12
 */
@Api(tags = "直播互动区 - Controller")
@RestController
@RequestMapping("/liveComment")
public class LiveCommentController {

    @Autowired
    private ILiveCommentService iLiveCommentService;

    @ApiOperation(value = "添加 - live_comment")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody LiveComment model) {
        //  LiveComment entity=DtoUtil.convertObject(model,LiveComment.class);
        boolean flag = iLiveCommentService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - live_comment")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iLiveCommentService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - live_comment")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody LiveCommentParam model) {
        return ResultJson.ok(iLiveCommentService.selectLiveCommentList(model));
    }
}

