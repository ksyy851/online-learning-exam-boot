package com.yuns.live.controller;

import com.yuns.live.service.ILiveBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveBase;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.LiveBaseDto;


/**
 * @author ${author}
 * @since 2020-10-27
 */
@Api(tags = "直播基础表 - Controller" )
    @RestController
@RequestMapping("/liveBase" )
    public class LiveBaseController {

    @Autowired
    private ILiveBaseService iLiveBaseService;

    @ApiOperation(value = "添加 - live_base" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody LiveBase model){
            //  LiveBase entity=DtoUtil.convertObject(model,LiveBase.class);
            boolean flag=iLiveBaseService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - live_base" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody LiveBase model){
            //LiveBase entity=DtoUtil.convertObject(model,LiveBase.class);
            boolean flag=iLiveBaseService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - live_base" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iLiveBaseService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - live_base" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<LiveBase> frontPage){
            if(!frontPage.is_search()){
            Page<LiveBase> page=new Page<LiveBase>();
            Page<LiveBase> list=page.setRecords(iLiveBaseService.selectLiveBaseList(frontPage));
            return ResultJson.ok(list);
            }
            Page<LiveBase> page=new Page<LiveBase>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<LiveBase> list=page.setRecords(iLiveBaseService.selectLiveBaseList(frontPage));
            return ResultJson.ok(list);
            }
        }

