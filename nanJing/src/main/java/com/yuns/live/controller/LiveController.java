package com.yuns.live.controller;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.dto.LiveDto;
import com.yuns.live.entity.LiveBase;
import com.yuns.live.entity.LiveInfo;
import com.yuns.live.param.LiveParam;
import com.yuns.live.service.ILiveBaseService;
import com.yuns.live.service.ILiveInfoService;
import com.yuns.live.service.ILiveService;
import com.yuns.live.vo.LiveVO;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

/**
 * @author liuChengWen
 * @since 2020-10-28
 */
@Api(tags = "直播管理 - Controller" )
@RestController
@RequestMapping("/live" )
public class LiveController {

    @Autowired
    private ILiveService iLiveService;

    @Autowired
    private ILiveInfoService infoService;

    @ApiOperation(value = "添加 - insert" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody LiveDto model){
        boolean flag=iLiveService.insert(model);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - up" )
    @PostMapping(value = "up" )
    public ResultJson up(@RequestBody LiveDto model){
        boolean flag=iLiveService.up(model);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - delete" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
        boolean flag=iLiveService.deleteById(id);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "直播列表查询 - query" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody LiveParam liveParam){
        if(!liveParam.is_search()){
            Page<LiveVO> page=new Page<LiveVO>();
            Page<LiveVO> list=page.setRecords(iLiveService.selectLiveList(liveParam));
            return ResultJson.ok(list);
        }
        Page<LiveVO> page=new Page<LiveVO>(liveParam.getPageNumber(),liveParam.getPageSize());
        Page<LiveVO> list=page.setRecords(iLiveService.selectLiveList(page,liveParam));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "导出直播列表 - exportList" )
    @PostMapping(value = "exportList" )
    public ResultJson exportList(@RequestBody LiveParam liveParam, HttpServletResponse response) {
        iLiveService.exportList(liveParam,response);
        return ResultJson.ok();
    }

    @ApiOperation(value = "直播上下架 - updateForType" )
    @PostMapping(value = "updateForType" )
    public ResultJson updateForType(@RequestBody LiveInfo model){
        LiveInfo info=new LiveInfo();
        info.setId(model.getId());
        info.setType(model.getType());
        boolean flag=infoService.updateById(info);
        if(flag){
            return ResultJson.ok();
        }else{
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "编辑 - edit" )
    @PostMapping(value = "edit/{id}" )
    public ResultJson edit(@PathVariable("id") Integer id) {
        return ResultJson.ok(iLiveService.edit(id));
    }

    @ApiOperation(value = "上传文件通用 - uploadVideo" )
    @PostMapping(value = "uploadVideo" )
    public ResultJson<Object> uploadVideo(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        return iLiveService.uploadVideo(file,request);
    }

    @ApiOperation(value = "文件删除通用 - delFile" )
    @PostMapping(value = "delFile/{id}" )
    public ResultJson delFile(@PathVariable String id) {
        return ResultJson.ok(iLiveService.delFile(id));
    }

    @ApiOperation(value = "指定观看人群查询 - querySpecifyPerson" )
    @PostMapping(value = "querySpecifyPerson/{id}" )
    public ResultJson querySpecifyPerson(@PathVariable("id") Integer id) {
        if(id==-1){
            return ResultJson.ok(iLiveService.querySpecifyPerson());
        }else {
            return ResultJson.ok(iLiveService.querySpecifyPerson(id));
        }
    }

}
