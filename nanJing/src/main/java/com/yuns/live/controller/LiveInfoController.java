package com.yuns.live.controller;

import com.yuns.live.service.ILiveInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveInfo;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.LiveInfoDto;


/**
 * @author ${author}
 * @since 2020-10-27
 */
@Api(tags = "直播详情表 - Controller")
@RestController
@RequestMapping("/liveInfo")
public class LiveInfoController {

    @Autowired
    private ILiveInfoService iLiveInfoService;

    @ApiOperation(value = "添加 - live_info")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody LiveInfo model) {
        //  LiveInfo entity=DtoUtil.convertObject(model,LiveInfo.class);
        boolean flag = iLiveInfoService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - live_info")
    @PostMapping(value = "update")
    public ResultJson update(@RequestBody LiveInfo model) {
        //LiveInfo entity=DtoUtil.convertObject(model,LiveInfo.class);
        boolean flag = iLiveInfoService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - live_info")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iLiveInfoService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - live_info")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<LiveInfo> frontPage) {
        if (!frontPage.is_search()) {
            Page<LiveInfo> page = new Page<LiveInfo>();
            Page<LiveInfo> list = page.setRecords(iLiveInfoService.selectLiveInfoList(frontPage));
            return ResultJson.ok(list);
        }
        Page<LiveInfo> page = new Page<LiveInfo>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<LiveInfo> list = page.setRecords(iLiveInfoService.selectLiveInfoList(frontPage));
        return ResultJson.ok(list);
    }

}

