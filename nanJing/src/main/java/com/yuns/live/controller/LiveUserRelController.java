package com.yuns.live.controller;


import com.yuns.live.service.ILiveUserRelService;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
import com.yuns.web.param.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveUserRel;
import org.springframework.web.bind.annotation.*;

//import com.yuns.com.yuns.web.sys.dto.LiveUserRelDto;


/**
 * @author ${author}
 * @since 2020-11-22
 */
@Api(tags = "直播特定人群管理表 - Controller")
@RestController
@RequestMapping("/liveUserRel")
public class LiveUserRelController {

    @Autowired
    private ILiveUserRelService iLiveUserRelService;

    @ApiOperation(value = "添加 - live_user_rel")
    @PostMapping(value = "insert")
    public ResultJson insert(@RequestBody LiveUserRel model) {
        //  LiveUserRel entity=DtoUtil.convertObject(model,LiveUserRel.class);
        boolean flag = iLiveUserRelService.insert(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "修改 - live_user_rel")
    @PostMapping(value = "up/{id}")
    public ResultJson update(@RequestBody LiveUserRel model) {
        //LiveUserRel entity=DtoUtil.convertObject(model,LiveUserRel.class);
        boolean flag = iLiveUserRelService.updateById(model);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "删除 - live_user_rel")
    @PostMapping(value = "delete/{id}")
    public ResultJson delete(@PathVariable("id") Integer id) {
        boolean flag = iLiveUserRelService.deleteById(id);
        if (flag) {
            return ResultJson.ok();
        } else {
            return ResultJson.error();
        }
    }

    @ApiOperation(value = "查询所有 - live_user_rel")
    @PostMapping(value = "query")
    public ResultJson query(@RequestBody FrontPage<QueryParam> frontPage) {
        if (!frontPage.is_search()) {
            Page<LiveUserRel> page = new Page<LiveUserRel>();
            Page<LiveUserRel> list = page.setRecords(iLiveUserRelService.selectLiveUserRelList(frontPage));
            return ResultJson.ok(list);
        }
        Page<LiveUserRel> page = new Page<LiveUserRel>(frontPage.getPageNumber(), frontPage.getPageSize());
        Page<LiveUserRel> list = page.setRecords(iLiveUserRelService.selectLiveUserRelList(frontPage));
        return ResultJson.ok(list);
    }

    @ApiOperation(value = "判断用户是否有权限看直播 - judgeLive")
    @PostMapping(value = "judgeLive")
    public ResultJson judgeLive(@RequestBody QueryParam frontPage ) {
        return ResultJson.ok(iLiveUserRelService.judgeLive(frontPage.getUserId(),frontPage.getLiveId()));
    }
}

