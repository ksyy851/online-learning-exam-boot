package com.yuns.live.controller;

import com.yuns.live.service.ILiveVideoFileService;
import com.yuns.live.entity.LiveVideoFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.ApiOperation;
import com.yuns.web.param.FrontPage;
import com.yuns.util.ResultJson;
import io.swagger.annotations.Api;
import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.web.param.QueryParam;

//import com.yuns.web.dto.LiveVideoFileDto;


/**
 * @author ${author}
 * @since 2020-10-25
 */
@Api(tags = "视频文件表 - Controller" )
    @RestController
@RequestMapping("/liveVideoFile" )
    public class LiveVideoFileController {

    @Autowired
    private ILiveVideoFileService iLiveVideoFileService;

    @ApiOperation(value = "添加 - live_video_file" )
    @PostMapping(value = "insert" )
    public ResultJson insert(@RequestBody LiveVideoFile model){
            //  LiveVideoFile entity=DtoUtil.convertObject(model,LiveVideoFile.class);
            boolean flag=iLiveVideoFileService.insert(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "修改 - live_video_file" )
    @PostMapping(value = "up" )
    public ResultJson update(@RequestBody LiveVideoFile model){
            //LiveVideoFile entity=DtoUtil.convertObject(model,LiveVideoFile.class);
            boolean flag=iLiveVideoFileService.updateById(model);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "删除 - live_video_file" )
    @PostMapping(value = "delete/{id}" )
    public ResultJson delete(@PathVariable("id") Integer id){
            boolean flag=iLiveVideoFileService.deleteById(id);
            if(flag){
            return ResultJson.ok();
            }else{
            return ResultJson.error();
            }
            }

    @ApiOperation(value = "查询所有 - live_video_file" )
    @PostMapping(value = "query" )
    public ResultJson query(@RequestBody FrontPage<LiveVideoFile> frontPage){
            if(!frontPage.is_search()){
            Page<LiveVideoFile> page=new Page<LiveVideoFile>();
            Page<LiveVideoFile> list=page.setRecords(iLiveVideoFileService.selectLiveVideoFileList(frontPage));
            return ResultJson.ok(list);
            }
            Page<LiveVideoFile> page=new Page<LiveVideoFile>(frontPage.getPageNumber(),frontPage.getPageSize());
            Page<LiveVideoFile> list=page.setRecords(iLiveVideoFileService.selectLiveVideoFileList(frontPage));
            return ResultJson.ok(list);
            }
        }

