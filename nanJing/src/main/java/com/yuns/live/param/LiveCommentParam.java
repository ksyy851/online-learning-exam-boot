package com.yuns.live.param;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 直播评论表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
@Data
@TableName("live_comment")
public class LiveCommentParam extends Model<LiveCommentParam> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 评论ID
     */
    @ApiParam(value = "评论ID")
    @ApiModelProperty(value = "评论ID")
    @TableField("comment_id")
    private Integer commentId;

    /**
     * 回复ID
     */
    @ApiParam(value = "回复ID")
    @ApiModelProperty(value = "回复ID")
    @TableField("answer_id")
    private Integer answerId;


    /**
     * 直播ID
     */
    @ApiParam(value = "直播ID")
   @ApiModelProperty(value = "直播ID")
    @TableField("live_id")
    private Integer liveId;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
   @ApiModelProperty(value = "用户ID")
    @TableField("company_user_id")
    private Integer companyUserId;

    /**
     * 创建者Id
     */
    @ApiParam(value = "创建者Id")
    @ApiModelProperty(value = "创建者Id")
    private Integer userId;

    /**
     * 创建者名称
     */
    @ApiParam(value = "创建者名称")
    @ApiModelProperty(value = "创建者名称")
    private String userName;
    /**
     * 评论内容
     */
    @ApiParam(value = "评论内容")
   @ApiModelProperty(value = "评论内容")
    private String comment;
    /**
     * 操作时间
     */
    @ApiParam(value = "操作时间")
    @ApiModelProperty(value = "操作时间")
    @TableField("create_time")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
