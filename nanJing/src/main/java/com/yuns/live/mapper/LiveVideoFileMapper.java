package com.yuns.live.mapper;

import com.yuns.live.entity.LiveVideoFile;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 视频文件表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
public interface LiveVideoFileMapper extends BaseMapper<LiveVideoFile> {
        List<LiveVideoFile> selectLiveVideoFileList(FrontPage frontPage);
}
