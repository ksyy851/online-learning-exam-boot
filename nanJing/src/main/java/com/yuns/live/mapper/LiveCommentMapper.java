package com.yuns.live.mapper;

import com.yuns.live.entity.LiveComment;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.live.param.LiveCommentParam;
import com.yuns.live.vo.LiveCommentVO;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播评论表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
public interface LiveCommentMapper extends BaseMapper<LiveComment> {

    List<LiveCommentVO> selectAll(LiveCommentParam model);

    List<LiveCommentVO> selectMyself(LiveCommentParam model);
}
