package com.yuns.live.mapper;

import com.yuns.live.entity.LiveInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 直播详情表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
public interface LiveInfoMapper extends BaseMapper<LiveInfo> {
        List<LiveInfo> selectLiveInfoList(FrontPage frontPage);
}
