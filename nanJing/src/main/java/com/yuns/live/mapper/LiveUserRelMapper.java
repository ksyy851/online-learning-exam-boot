package com.yuns.live.mapper;

import com.yuns.live.entity.LiveUserRel;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.yuns.web.param.FrontPage;

import java.util.List;

/**
 * <p>
 * 直播特定人群管理表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-11-22
 */
public interface LiveUserRelMapper extends BaseMapper<LiveUserRel> {
        List<LiveUserRel> selectLiveUserRelList(FrontPage frontPage);
}
