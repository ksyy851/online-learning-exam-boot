package com.yuns.live.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.yuns.live.entity.LiveBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.yuns.live.param.LiveParam;
import com.yuns.live.vo.LiveVO;
import com.yuns.live.vo.SpecifyPersonVO;
import com.yuns.util.ResultJson;
import com.yuns.web.param.FrontPage;
/**
 * <p>
 * 直播基础表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
public interface LiveBaseMapper extends BaseMapper<LiveBase> {
    List<LiveBase> selectLiveBaseList(FrontPage frontPage);

    List<LiveVO> selectLiveList(LiveParam frontPage);

    List<LiveVO> selectLiveList(Page<LiveVO> page, LiveParam frontPage);

    LiveVO selectLiveById(Integer id);

    List<SpecifyPersonVO>  querySpecifyPerson();
}
