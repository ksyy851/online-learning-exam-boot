package com.yuns.live.vo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiParam;
import lombok.Data;

/**
 * 特殊人群VO
 */
@Data
public class SpecifyPersonVO {

    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 用户名称
     */
    @ApiParam(value = "用户名称")
    @TableField("user_name")
    private String userName;
    /**
     * 部门ID
     */
    @ApiParam(value = "部门ID")
    @TableField("dept_id")
    private String deptId;
    /**
     * 部门父节点ID
     */
    @ApiParam(value = "部门父节点ID")
    @TableField("dept_parent_id")
    private Long deptParentId;

    /**
     * 部门名称
     */
    @ApiParam(value = "部门名称")
    @TableField("dept_name")
    private String deptName;

    /**
     * 是否勾选
     */
    @ApiParam(value = "是否勾选")
    @TableField("check")
    private Integer check;
}
