package com.yuns.live.vo;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 直播评论表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-12
 */
@Data
public class LiveCommentVO{

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 评论ID
     */
    @ApiParam(value = "评论ID")
    @ApiModelProperty(value = "评论ID")
    @TableField("comment_id")
    private String commentId;

    /**
     * 回复ID
     */
    @ApiParam(value = "回复ID")
    @ApiModelProperty(value = "回复ID")
    @TableField("answer_id")
    private String answerId;


    /**
     * 直播ID
     */
    @ApiParam(value = "直播ID")
    @ApiModelProperty(value = "直播ID")
    @TableField("live_id")
    private Integer liveId;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
    @ApiModelProperty(value = "用户ID")
    @TableField("company_user_id")
    private Integer companyUserId;
    /**
     * 评论内容
     */
    @ApiParam(value = "评论内容")
    @ApiModelProperty(value = "评论内容")
    private String comment;
    /**
     * 操作时间
     */
    @ApiParam(value = "操作时间")
    @ApiModelProperty(value = "操作时间")
    @TableField("create_time")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private String createTime;

    /**
     * 用户名
     */
    @ApiParam(value = "用户名")
    @ApiModelProperty(value = "用户名")
    private String userName;


}
