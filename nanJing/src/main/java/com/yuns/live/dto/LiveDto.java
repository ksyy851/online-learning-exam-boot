package com.yuns.live.dto;

import com.yuns.live.entity.LiveBase;
import com.yuns.live.entity.LiveUserRel;
import lombok.Data;

import java.util.List;


/**
 * @description: TODO: 课程入参
 * @author: liuChengWen
 * @create: 2020-10-26
 * @version: 1.0
 **/
@Data
public class LiveDto extends LiveBase {
    List<LiveUserRel> userRelList;
}
