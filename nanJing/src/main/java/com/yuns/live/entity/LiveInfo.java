package com.yuns.live.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
/**
 * <p>
 * 直播详情表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
@TableName("live_info")
public class LiveInfo extends Model<LiveInfo> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 直播ID
     */
    @ApiParam(value = "直播ID")
   @ApiModelProperty(value = "直播ID")
    private Integer pid;
    /**
     * 浏览量
     */
    @ApiParam(value = "浏览量")
   @ApiModelProperty(value = "浏览量")
    @TableField("page_views")
    private Integer pageViews;
    /**
     * 上下架(0:上架;1:下架)
     */
    @ApiParam(value = "上下架(0:上架;1:下架)")
   @ApiModelProperty(value = "上下架(0:上架;1:下架)")
    private Integer type;
    /**
     * 操作时间
     */
    @ApiParam(value = "操作时间")
   @ApiModelProperty(value = "操作时间")
    @TableField("create_time")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getPageViews() {
        return pageViews;
    }

    public void setPageViews(Integer pageViews) {
        this.pageViews = pageViews;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "LiveInfo{" +
        ", id=" + id +
        ", pid=" + pid +
        ", pageViews=" + pageViews +
        ", type=" + type +
        ", createTime=" + createTime +
        "}";
    }
}
