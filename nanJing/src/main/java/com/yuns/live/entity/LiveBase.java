package com.yuns.live.entity;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 直播基础表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-27
 */
@Data
@TableName("live_base")
public class LiveBase extends Model<LiveBase> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 直播名称
     */
    @ApiParam(value = "直播名称")
    @ApiModelProperty(value = "直播名称")
    @TableField("live_name")
    private String liveName;
    /**
     * 直播老师名称
     */
    @ApiParam(value = "直播老师名称")
    @ApiModelProperty(value = "直播老师名称")
    @TableField("live_teacher_name")
    private String liveTeacherName;
    /**
     * 直播老师简介
     */
    @ApiParam(value = "直播老师简介")
    @ApiModelProperty(value = "直播老师简介")
    @TableField("live_teacher_info")
    private String liveTeacherInfo;
    /**
     * 总积分
     */
    @ApiParam(value = "总积分")
    @ApiModelProperty(value = "总积分")
    private Integer integral;
    /**
     * 学时
     */
    @ApiParam(value = "学时")
    @ApiModelProperty(value = "学时")
    private Integer period;
    /**
     * 直播介绍
     */
    @ApiParam(value = "直播介绍")
    @ApiModelProperty(value = "直播介绍")
    @TableField("live_info")
    private String liveInfo;
    /**
     * 直播时间
     */
    @ApiParam(value = "直播时间")
    @ApiModelProperty(value = "直播时间")
    @TableField("live_time")
    private String liveTime;
    /**
     * 直播时长
     */
    @ApiParam(value = "直播时长")
    @ApiModelProperty(value = "直播时长")
    @TableField("live_period")
    private Integer livePeriod;
    /**
     * 视频文件ID
     */
    @ApiParam(value = "视频文件ID")
    @ApiModelProperty(value = "视频文件ID")
    @TableField("live_file_no")
    private String liveFileNo;
    /**
     * 直播视频名
     */
    @ApiParam(value = "直播视频名")
    @ApiModelProperty(value = "直播视频名")
    @TableField("live_file_name")
    private String liveFileName;
    /**
     * 直播视频路径
     */
    @ApiParam(value = "直播视频路径")
    @ApiModelProperty(value = "直播视频路径")
    @TableField("live_file_path")
    private String liveFilePath;
    /**
     * 封面图文件ID
     */
    @ApiParam(value = "封面图文件ID")
    @ApiModelProperty(value = "封面图文件ID")
    @TableField("pic_file_no")
    private String picFileNo;
    /**
     * 操作时间
     */
    @ApiParam(value = "操作时间")
    @ApiModelProperty(value = "操作时间")
    @TableField("create_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String createTime;

    /**
     * 直播创建者
     */
    @ApiParam(value = "直播创建者")
    @ApiModelProperty(value = "直播创建者")
    @TableField("creater")
    private Integer creater;

    /**
     * 直播创建者名称
     */
    @ApiParam(value = "直播创建者名称")
    @ApiModelProperty(value = "直播创建者名称")
    @TableField("creater_name")
    private String createrName;

    /**
     * 是否公开(0:已公开;1:未公开)
     */
    @ApiParam(value = "是否公开(0:已公开;1:未公开)")
    @ApiModelProperty(value = "是否公开(0:已公开;1:未公开)")
    @TableField("show_type")
    private Integer showType;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
