package com.yuns.live.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 视频文件表
 * </p>
 *
 * @author ${author}
 * @since 2020-10-25
 */
@Data
@TableName("live_video_file")
public class LiveVideoFile extends Model<LiveVideoFile> {

    private static final long serialVersionUID = 1L;
    /**
     * 文件编号
     */
    @ApiParam(value = "文件编号")
    @TableId(value = "file_no")
   @ApiModelProperty(value = "文件编号")
    private String fileNo;
    /**
     * 文件名称
     */
    @ApiParam(value = "文件名称")
   @ApiModelProperty(value = "文件名称")
    private String fileName;
    /**
     * 视频文件大小
     */
    @ApiParam(value = "视频文件大小")
   @ApiModelProperty(value = "视频文件大小")
    private Integer filePeriod;
    /**
     * 文件路径
     */
    @ApiParam(value = "文件路径")
   @ApiModelProperty(value = "文件路径")
    private String fileUrl;
    /**
     * 文件类型
     */
    @ApiParam(value = "文件类型")
   @ApiModelProperty(value = "文件类型")
    private String fileType;
    /**
     * 文件大小
     */
    @ApiParam(value = "文件大小")
   @ApiModelProperty(value = "文件大小")
    private String fileSize;

    /**
     * 下载时间
     */
    @ApiParam(value = "下载时间")
    @ApiModelProperty(value = "下载时间")
    @TableField("download_time")
    private Date downloadTime;

    /**
     * 创建时间
     */
    @ApiParam(value = "创建时间")
    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private Date createTime;

    @Override
    protected Serializable pkVal() {
        return this.fileNo;
    }
}
