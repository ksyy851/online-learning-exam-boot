package com.yuns.live.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 直播特定人群管理表
 * </p>
 *
 * @author ${author}
 * @since 2020-11-22
 */
@Data
@TableName("live_user_rel")
public class LiveUserRel extends Model<LiveUserRel> {

    private static final long serialVersionUID = 1L;

    /**
     * 直播ID
     */
    @ApiParam(value = "直播ID")
   @ApiModelProperty(value = "直播ID")
    @TableField("live_id")
    private Integer liveId;
    /**
     * 用户ID
     */
    @ApiParam(value = "用户ID")
   @ApiModelProperty(value = "用户ID")
    @TableField("user_id")
    private Integer userId;

    @Override
    protected Serializable pkVal() {
        return this.liveId;
    }
}
