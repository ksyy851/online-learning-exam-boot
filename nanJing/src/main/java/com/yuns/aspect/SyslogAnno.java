package com.yuns.aspect;

import java.lang.annotation.*;

/**
 * @Author: luwenchao
 * @Date: 2018/7/4 13:35
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface SyslogAnno {
    String value() default "";
}
