/**
 * EndReason.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.yuns.mas.platform.schema.ussd;

public class EndReason implements java.io.Serializable {
    private String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EndReason(String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final String _UserEnd = "UserEnd";
    public static final String _Busy = "Busy";
    public static final String _UserAbsent = "UserAbsent";
    public static final String _IllegalEquipment = "IllegalEquipment";
    public static final String _SystemError = "SystemError";
    public static final String _TimeOut = "TimeOut";
    public static final EndReason UserEnd = new EndReason(_UserEnd);
    public static final EndReason Busy = new EndReason(_Busy);
    public static final EndReason UserAbsent = new EndReason(_UserAbsent);
    public static final EndReason IllegalEquipment = new EndReason(_IllegalEquipment);
    public static final EndReason SystemError = new EndReason(_SystemError);
    public static final EndReason TimeOut = new EndReason(_TimeOut);
    public String getValue() { return _value_;}
    public static EndReason fromValue(String value)
          throws IllegalArgumentException {
        EndReason enumeration = (EndReason)
            _table_.get(value);
        if (enumeration==null) throw new IllegalArgumentException();
        return enumeration;
    }
    public static EndReason fromString(String value)
          throws IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public String toString() { return _value_;}
    public Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EndReason.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.csapi.org/schema/ussd", "EndReason"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
