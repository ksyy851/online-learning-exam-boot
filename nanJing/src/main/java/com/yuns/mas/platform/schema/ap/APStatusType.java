/**
 * APStatusType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.yuns.mas.platform.schema.ap;

public class APStatusType implements java.io.Serializable {
    private String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected APStatusType(String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final String _Normal = "Normal";
    public static final String _OutofActiveTime = "OutofActiveTime";
    public static final String _NeedRegistration = "NeedRegistration";
    public static final String _OutofService = "OutofService";
    public static final String _Paused = "Paused";
    public static final String _Closed = "Closed";
    public static final String _WaitingforConfirm = "WaitingforConfirm";
    public static final APStatusType Normal = new APStatusType(_Normal);
    public static final APStatusType OutofActiveTime = new APStatusType(_OutofActiveTime);
    public static final APStatusType NeedRegistration = new APStatusType(_NeedRegistration);
    public static final APStatusType OutofService = new APStatusType(_OutofService);
    public static final APStatusType Paused = new APStatusType(_Paused);
    public static final APStatusType Closed = new APStatusType(_Closed);
    public static final APStatusType WaitingforConfirm = new APStatusType(_WaitingforConfirm);
    public String getValue() { return _value_;}
    public static APStatusType fromValue(String value)
          throws IllegalArgumentException {
        APStatusType enumeration = (APStatusType)
            _table_.get(value);
        if (enumeration==null) throw new IllegalArgumentException();
        return enumeration;
    }
    public static APStatusType fromString(String value)
          throws IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public String toString() { return _value_;}
    public Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(APStatusType.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.csapi.org/schema/ap", "APStatusType"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
