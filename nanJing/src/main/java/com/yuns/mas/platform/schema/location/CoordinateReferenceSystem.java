/**
 * CoordinateReferenceSystem.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.yuns.mas.platform.schema.location;

public class CoordinateReferenceSystem  implements java.io.Serializable {
    private String code;

    private String codeSpace;

    private String edition;

    public CoordinateReferenceSystem() {
    }

    public CoordinateReferenceSystem(
           String code,
           String codeSpace,
           String edition) {
           this.code = code;
           this.codeSpace = codeSpace;
           this.edition = edition;
    }


    /**
     * Gets the code value for this CoordinateReferenceSystem.
     *
     * @return code
     */
    public String getCode() {
        return code;
    }


    /**
     * Sets the code value for this CoordinateReferenceSystem.
     *
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }


    /**
     * Gets the codeSpace value for this CoordinateReferenceSystem.
     *
     * @return codeSpace
     */
    public String getCodeSpace() {
        return codeSpace;
    }


    /**
     * Sets the codeSpace value for this CoordinateReferenceSystem.
     *
     * @param codeSpace
     */
    public void setCodeSpace(String codeSpace) {
        this.codeSpace = codeSpace;
    }


    /**
     * Gets the edition value for this CoordinateReferenceSystem.
     *
     * @return edition
     */
    public String getEdition() {
        return edition;
    }


    /**
     * Sets the edition value for this CoordinateReferenceSystem.
     *
     * @param edition
     */
    public void setEdition(String edition) {
        this.edition = edition;
    }

    private Object __equalsCalc = null;
    public synchronized boolean equals(Object obj) {
        if (!(obj instanceof CoordinateReferenceSystem)) return false;
        CoordinateReferenceSystem other = (CoordinateReferenceSystem) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true &&
            ((this.code==null && other.getCode()==null) ||
             (this.code!=null &&
              this.code.equals(other.getCode()))) &&
            ((this.codeSpace==null && other.getCodeSpace()==null) ||
             (this.codeSpace!=null &&
              this.codeSpace.equals(other.getCodeSpace()))) &&
            ((this.edition==null && other.getEdition()==null) ||
             (this.edition!=null &&
              this.edition.equals(other.getEdition())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCode() != null) {
            _hashCode += getCode().hashCode();
        }
        if (getCodeSpace() != null) {
            _hashCode += getCodeSpace().hashCode();
        }
        if (getEdition() != null) {
            _hashCode += getEdition().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CoordinateReferenceSystem.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.csapi.org/schema/location", "CoordinateReferenceSystem"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("code");
        elemField.setXmlName(new javax.xml.namespace.QName("", "code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codeSpace");
        elemField.setXmlName(new javax.xml.namespace.QName("", "codeSpace"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("edition");
        elemField.setXmlName(new javax.xml.namespace.QName("", "edition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
