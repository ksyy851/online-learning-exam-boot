/**
 * MessageFormat.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.yuns.mas.platform.schema.sms;

public class MessageFormat implements java.io.Serializable {
    private String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected MessageFormat(String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final String _ASCII = "ASCII";
    public static final String _UCS2 = "UCS2";
    public static final String _GB18030 = "GB18030";
    public static final String _GB2312 = "GB2312";
    public static final String _Binary = "Binary";
    public static final MessageFormat ASCII = new MessageFormat(_ASCII);
    public static final MessageFormat UCS2 = new MessageFormat(_UCS2);
    public static final MessageFormat GB18030 = new MessageFormat(_GB18030);
    public static final MessageFormat GB2312 = new MessageFormat(_GB2312);
    public static final MessageFormat Binary = new MessageFormat(_Binary);
    public String getValue() { return _value_;}
    public static MessageFormat fromValue(String value)
          throws IllegalArgumentException {
        MessageFormat enumeration = (MessageFormat)
            _table_.get(value);
        if (enumeration==null) throw new IllegalArgumentException();
        return enumeration;
    }
    public static MessageFormat fromString(String value)
          throws IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public String toString() { return _value_;}
    public Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           String mechType,
           Class _javaType,
           javax.xml.namespace.QName _xmlType) {
        return
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(MessageFormat.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.csapi.org/schema/sms", "MessageFormat"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
