/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.wappush;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.wap.SendPushRequest;
import com.yuns.mas.platform.schema.wap.SendPushResponse;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;
import org.apache.axis.types.URI;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.4 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 9:01:57 PM
 */
public class SendPush extends TestBase {

	public static void main(String[] args) throws Exception {
		SendPush test = new SendPush();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/wappush/SendPush.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.sendWapPush();
	}

	public void sendWapPush(){
		String requestIdentifier = "";

		// TODO �Զ���ɷ������
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			SendPushRequest s = new SendPushRequest();
			// Ӧ��ID
			s.setApplicationID(props.getProperty("ApplicationID"));
			String subject = props.getProperty("Subject");

			s.setSubject(new String(subject.getBytes("ISO-8859-1"),"utf-8"));
			s.setTargetURL(new URI(props.getProperty("TargetURL")));
			s.setReceiptRequest(Boolean.parseBoolean(props.getProperty("ReceiptRequest")));

			s.setExtendCode(props.getProperty("ExtendCode"));
			String[] a = props.getProperty("Address").split(";");
			int leng= a.length;
			org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[leng];
			for(int i=0;i<leng;i++){
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(a[i]);
				ary[i]=temp;
			}
			s.setAddresses(ary);


			SendPushResponse rep = stub.sendPush(s);
			requestIdentifier = rep.getRequestIdentifier();
			System.out.println("requestIdentifier="+requestIdentifier);
		} catch (Exception e) {
			// TODO �Զ���� catch ��
			e.printStackTrace();
		}


	}
}
