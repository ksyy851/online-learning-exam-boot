/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.mms;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.mms.MessagePriority;
import com.yuns.mas.platform.schema.mms.SendMessageRequest;
import com.yuns.mas.platform.schema.mms.SendMessageResponse;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;
import org.apache.axis.types.URI;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.3 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:57:43 PM
 */
public class SendMessage extends TestBase{

	public static void main(String []args) throws Exception{
		SendMessage test = new SendMessage();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/mms/SendMessage.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.sendMessage();
	}

	private void sendMessage(){


		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			SendMessageRequest req = new SendMessageRequest();
			req.setApplicationID(props.getProperty("ApplicationId"));
			BufferedInputStream bis=new BufferedInputStream(new FileInputStream(props.getProperty("mmsFilePath")));
			ByteArrayOutputStream bos=new ByteArrayOutputStream();
			int len=0;
			byte[] temp=new byte[1024];
			while ((len=bis.read(temp, 0, 1024))!=-1) {
				bos.write(temp, 0, len);
			}
			byte content []=bos.toByteArray();
			req.setContent(content);
			req.setExtendCode(props.getProperty("ExtendCode"));
			req.setPriority(MessagePriority.fromValue(props.getProperty("Priority")));
			String subject = props.getProperty("Subject");
			req.setSubject(new String(subject.getBytes("ISO-8859-1"),"utf-8"));
			req.setReceiptRequest(Boolean.parseBoolean(props.getProperty("ReceiptRequest")));
			String[] add = props.getProperty("Address").split(";");
	    	URI[] a=new URI[add.length];
			for (int i = 0; i < a.length; i++) {
				a[i] = new URI(add[i]);
			}
			req.setAddresses(a);
			SendMessageResponse s = stub.sendMessage(req);
			System.out.println("RequestIdentifier="+s.getRequestIdentifier());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
