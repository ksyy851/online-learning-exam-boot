/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.sms;

import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.sms.DeliveryInformation;
import com.yuns.mas.platform.schema.sms.GetSmsDeliveryStatusRequest;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 *
 * @version $Revision: 1.2 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:55:33 PM
 */
public class GetSmsDeliveryStatus extends TestBase {

	public static void main(String[] args) throws Exception {
		GetSmsDeliveryStatus test = new GetSmsDeliveryStatus();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/sms/GetSmsDeliveryStatus.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.getSmsDeliveryStatus();
	}

	private void getSmsDeliveryStatus() {

		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			GetSmsDeliveryStatusRequest req = new GetSmsDeliveryStatusRequest();

			req.setApplicationID(props.getProperty("ApplicationID"));
			req.setRequestIdentifier(props.getProperty("RequestIdentifier"));
			com.yuns.mas.platform.schema.sms.DeliveryInformation[] d = stub
					.getSmsDeliveryStatus(req);

			//System.out.println("testGetSmsDeliveryStatus=" + (d == null));

			for (int i = 0; i < d.length; i++) {
				DeliveryInformation dd = d[i];

				System.out.println(dd.getAddress().toString());
				System.out.println(dd.getDeliveryStatus().toString());

			}

		} catch (Exception e) {
			// TODO �Զ���� catch ��
			e.printStackTrace();
		}
	}
}
