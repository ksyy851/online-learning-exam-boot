/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.ap;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.ap.APLogOutReq;
import com.yuns.mas.platform.schema.ap.APLogOutRsp;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.1 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 8:00:10 PM
 */
public class ApLogout  extends TestBase{

	public static void main(String []args) throws Exception{
		ApLogout test = new ApLogout();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/ap/ApLogout.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.apLogout();
	}

	private void apLogout(){
		try {
			URL url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);
			APLogOutReq req = new APLogOutReq();
			req.setAPid(props.getProperty("ApplicationId"));
			req.setAPPid(Integer.parseInt(props.getProperty("ProcessId")));
			APLogOutRsp rsp = stub.APLogOut(req);
			System.out.println(rsp.getLogoutResult());
		} catch (Exception e) {
			// TODO �Զ����� catch ��
			e.printStackTrace();
		}
	}
}
