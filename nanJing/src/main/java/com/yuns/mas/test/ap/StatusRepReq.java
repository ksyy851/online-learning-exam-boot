/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.ap;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.ap.APStatusRepReq;
import com.yuns.mas.platform.schema.ap.APStatusRepRsp;
import com.yuns.mas.platform.schema.ap.APStatusType;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.4 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 8:00:19 PM
 */
public class StatusRepReq extends TestBase{

	public static void main(String []args) throws Exception{
		StatusRepReq test = new StatusRepReq();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/ap/StatusRepReq.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.statusRepReq();
	}

	private void statusRepReq(){
		try {
			URL url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);
			APStatusRepReq req = new APStatusRepReq();
			req.setAPid(props.getProperty("ApplicationId"));
			req.setAPPid(Integer.parseInt(props.getProperty("ProcessId")));
			try{
				APStatusType type = APStatusType.fromValue(props.getProperty("ApStatus"));
				//APStatusType type = APStatusType.fromValue("Okstatus");
				req.setAPStatus(type);
			}
			catch(Exception e){
				e.printStackTrace();
				APStatusType type = APStatusType.fromValue("Okstatus");
				req.setAPStatus(type);
			}

			APStatusRepRsp rsp = stub.APStatusRep(req);
			System.out.println(rsp.getNextInterval());
			System.out.println(rsp.getNextCommand());

		} catch (Exception e) {
			// TODO �Զ���� catch ��
			e.printStackTrace();
		}
	}
}
