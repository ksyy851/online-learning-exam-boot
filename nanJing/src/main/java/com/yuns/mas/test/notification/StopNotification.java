/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.notification;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.notification.StopNotificationRequest;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 *
 * @version $Revision: 1.1 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:59:51 PM
 */
public class StopNotification extends TestBase{

	public static void main(String[] args) throws Exception {
		StopNotification test = new StopNotification();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/notification/StopNotification.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.stopNotification();
	}

	private void stopNotification() {
		try {
			URL url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);
			StopNotificationRequest stopNotificationRequest = new StopNotificationRequest();
			stopNotificationRequest
					.setApplicationId(props.getProperty("ApplicationId"));
			stub.stopNotification(stopNotificationRequest);
		} catch (Exception e) {
			// TODO �Զ����� catch ��
			e.printStackTrace();
		}
	}
}
