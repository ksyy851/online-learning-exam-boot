/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.lbs;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.location.EndNotificationRequest;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.1 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:59:14 PM
 */
public class EndNotification  extends TestBase{

	public static void main(String []args) throws Exception{
		EndNotification test =new EndNotification();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/lbs/EndNotification.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.endNotification();
	}

	private void endNotification(){
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);
			EndNotificationRequest request = new EndNotificationRequest();
			request.setApplicationId(props.getProperty("ApplicationId"));
			request.setCorrelator(props.getProperty("Correlator"));
			stub.endNotification(request);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
