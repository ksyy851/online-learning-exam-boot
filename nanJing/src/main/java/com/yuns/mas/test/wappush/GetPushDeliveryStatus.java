
package com.yuns.mas.test.wappush;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.wap.GetPushDeliveryStatusRequest;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.2 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 9:02:06 PM
 */
public class GetPushDeliveryStatus extends TestBase {
	public static void main(String[] args) throws Exception {
		GetPushDeliveryStatus test = new GetPushDeliveryStatus();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/wappush/GetPushDeliveryStatus.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.getPushDeliveryStatus();
	}

	private void getPushDeliveryStatus(){
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			GetPushDeliveryStatusRequest req = new GetPushDeliveryStatusRequest();

			req.setApplicationID(props.getProperty("ApplicationID"));
			req.setRequestIdentifier(props.getProperty("requestIdentifier"));
			com.yuns.mas.platform.schema.wap.DeliveryInformation[] di = stub.getPushDeliveryStatus(req);
			if(di != null){
				for(com.yuns.mas.platform.schema.wap.DeliveryInformation dd:di){
					System.out.println(dd.getStatus().getValue());
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
}
