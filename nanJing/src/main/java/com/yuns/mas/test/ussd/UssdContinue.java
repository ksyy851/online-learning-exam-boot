/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.ussd;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.ussd.UssdArray;
import com.yuns.mas.platform.schema.ussd.UssdContinueRequest;
import com.yuns.mas.platform.schema.ussd.UssdContinueResponse;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.3 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 8:57:32 PM
 */
public class UssdContinue extends TestBase{

	public static void main(String args[]) throws Exception{
		UssdContinue test = new UssdContinue();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/ussd/UssdContinue.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.ussdContinue();
	}

	private void ussdContinue(){
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			UssdContinueRequest req1 = new UssdContinueRequest();
			req1.setUssdIdentifier(props.getProperty("UssdIdentifier"));
			UssdArray array = new UssdArray();
			String message = props.getProperty("UssdMessage");
			array.setUssdMessage(new String(message.getBytes("ISO-8859-1"),"utf-8"));
			array.setUssdReturnRequest(Boolean.valueOf(props.getProperty("UssdReturnRequest")));
			req1.setUssdMessage(array);
			UssdContinueResponse rsp1 = stub.ussdContinue(req1);
			System.out.println(rsp1.getReturnMessage().getUssdMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
