/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.mms;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.mms.DeliveryInformation;
import com.yuns.mas.platform.schema.mms.GetMessageDeliveryStatusRequest;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.1 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:57:55 PM
 */
public class GetMessageDeliveryStatus extends TestBase{

	public static void main(String []args) throws Exception{
		GetMessageDeliveryStatus test = new GetMessageDeliveryStatus();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/mms/GetMessageDeliveryStatus.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.getMessageDeliveryStatus();
	}

	private void getMessageDeliveryStatus(){
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			GetMessageDeliveryStatusRequest req = new GetMessageDeliveryStatusRequest();
			req.setApplicationID(props.getProperty("ApplicationId"));
			req.setRequestIdentifier(props.getProperty("RequestIdentifier"));

			DeliveryInformation[] d = stub.getMessageDeliveryStatus(req);

			System.out.println(d == null);
		} catch (Exception e) {
			// TODO �Զ����� catch ��
			e.printStackTrace();
		}
	}
}
