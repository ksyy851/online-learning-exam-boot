/*****************************************
 *  锟斤拷锟叫帮拷权(c) 锟较猴拷锟斤拷锟斤拷锟斤拷锟较低筹拷锟斤拷薰锟剿�       *
 *****************************************/
package com.yuns.mas.test.ap;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.ap.APRegResult;
import com.yuns.mas.platform.schema.ap.APRegistrationReq;
import com.yuns.mas.platform.schema.ap.APRegistrationRsp;
import com.yuns.mas.platform.schema.common.v2_0.CMAbility;
import com.yuns.mas.platform.schema.common.v2_0.MessageNotificationType;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 * @version $Revision: 1.3 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 8:00:28 PM
 */
public class ApRegistration extends TestBase{

	private static int lastInterval = 120;
	public static void main(String[] args) throws Exception{
		ApRegistration test = new ApRegistration();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/ap/ApRegistration.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.apRegistration();
	}

	private void apRegistration(){

		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);
			APRegistrationReq req = new APRegistrationReq();
			//锟斤拷锟斤拷锟绞�
			req.setApid(props.getProperty("ApplicationId"));
			//锟斤拷锟絀D
			req.setAPPid(Integer.parseInt(props.getProperty("ProcessId")));
			//IP锟斤拷址
			String hostIp = props.getProperty("Address");
			req.setHostIP(hostIp);
			String apwsURI = props.getProperty("Url");
			String apwsURII = new String(apwsURI);
			org.apache.axis.types.URI uri=new org.apache.axis.types.URI(apwsURII);
			req.setAPWSURI(uri);
			List<MessageNotificationType> list = new ArrayList<MessageNotificationType>();
			int i=0;
			if(!"".equals(props.getProperty("SmsCma").trim())){
				//锟斤拷锟斤拷锟斤拷锟酵拷锟斤拷锟絝
				MessageNotificationType type = new MessageNotificationType();
				type.setCMAbility(CMAbility.fromValue(props.getProperty("SmsCma")));
				//锟斤拷锟斤拷URI
				String t = props.getProperty("SmsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t);
				ary[0]=temp;
				type.setWSURI(ary);
				list.add(type);
				i=i+1;
			}
			if(!"".equals(props.getProperty("MmsCma").trim())){
				//锟斤拷锟斤拷锟斤拷锟酵拷锟斤拷锟絝
				MessageNotificationType type_2 = new MessageNotificationType();
				type_2.setCMAbility(CMAbility.fromValue(props.getProperty("MmsCma")));
				//锟斤拷锟斤拷URI
				String t_2 = props.getProperty("MmsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_2);
				ary[0]=temp;
				type_2.setWSURI(ary);
				list.add(type_2);
				i=i+1;
			}
			if(!"".equals(props.getProperty("WapCma").trim())){
				//锟斤拷锟斤拷Wap通锟斤拷锟斤拷f
				MessageNotificationType type_3 = new MessageNotificationType();
				type_3.setCMAbility(CMAbility.fromValue(props.getProperty("WapCma")));
				//锟斤拷锟斤拷URI
				String t_3 = props.getProperty("WapUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_3);
				ary[0]=temp;
				type_3.setWSURI(ary);
				list.add(type_3);
				i=i+1;
			}
			if(!"".equals(props.getProperty("LbsCma").trim())){
				//锟斤拷锟斤拷LBS通锟斤拷锟斤拷f
				MessageNotificationType type_4 = new MessageNotificationType();
				type_4.setCMAbility(CMAbility.fromValue(props.getProperty("LbsCma")));
				//锟斤拷锟斤拷URI
				String t_4 = props.getProperty("LbsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_4);
				ary[0]=temp;
				type_4.setWSURI(ary);
				list.add(type_4);
				i=i+1;
			}
			if(!"".equals(props.getProperty("UssdCma").trim())){
				//锟斤拷锟斤拷USSD通锟斤拷锟斤拷f
				MessageNotificationType type_5 = new MessageNotificationType();
				type_5.setCMAbility(CMAbility.fromValue(props.getProperty("UssdCma")));
				//锟斤拷锟斤拷URI
				String t_5 = props.getProperty("UssdUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_5);
				ary[0]=temp;
				type_5.setWSURI(ary);
				list.add(type_5);
				i=i+1;
			}
			if(!"".equals(props.getProperty("GprsCma").trim())){
				//锟斤拷锟斤拷GPRS通锟斤拷锟斤拷f
				MessageNotificationType type_6 = new MessageNotificationType();
				type_6.setCMAbility(CMAbility.fromValue(props.getProperty("GprsCma")));
				//锟斤拷锟斤拷URI
				String t_6 = props.getProperty("GprsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_6);
				ary[0]=temp;
				type_6.setWSURI(ary);
				list.add(type_6);
				i=i+1;
			}
			MessageNotificationType[] a = new MessageNotificationType[i];
			req.setMessageNotification(list.toArray(a));
			//锟斤拷始注锟斤拷
			APRegistrationRsp rsp = stub.APRegistration(req);
			APRegResult regResult = rsp.getRegResult();
			System.out.println(regResult.getValue());

//			if("success".equals(regResult.getValue())){
//				while(true){
//					APStatusRepReq StatusRepReq = new APStatusRepReq();
//					StatusRepReq.setAPid(props.getProperty("ApplicationId"));
//					StatusRepReq.setAPPid(Integer.parseInt(props.getProperty("ProcessId")));
//					APStatusType type = APStatusType.fromValue("Normal");
//					StatusRepReq.setAPStatus(type);
//					APStatusRepRsp  StatusRepRsp = stub.APStatusRep(StatusRepReq);
//					System.out.println(StatusRepRsp.getNextInterval());
//					System.out.println(StatusRepRsp.getNextCommand());
//					if(!"".equals(StatusRepRsp.getNextInterval())){
//						int interval = Integer.parseInt(StatusRepRsp.getNextInterval());
//						lastInterval = interval;
//						Thread.sleep(interval*1000);
//					}else{
//						Thread.sleep(lastInterval *1000);
//					}
//				}
//			}
		} catch (Exception e) {
			// TODO 锟皆讹拷锟斤拷锟� catch 锟斤拷
			e.printStackTrace();
		}

}
}
