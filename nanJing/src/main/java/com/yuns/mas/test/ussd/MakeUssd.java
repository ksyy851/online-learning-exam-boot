/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.ussd;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.ussd.MakeUssdRequest;
import com.yuns.mas.platform.schema.ussd.MakeUssdResponse;
import com.yuns.mas.platform.schema.ussd.UssdArray;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.3 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 8:57:05 PM
 */
public class MakeUssd  extends TestBase {

	public static void main(String args[]) throws Exception{
		MakeUssd test = new MakeUssd();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/ussd/MakeUssd.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.makeUssd();
	}

	private void makeUssd(){
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			MakeUssdRequest req = new MakeUssdRequest();
			req.setApplicationID(props.getProperty("ApplicationID"));
			org.apache.axis.types.URI temp=new org.apache.axis.types.URI(props.getProperty("Address"));
			req.setDestinationAddress(temp);
			UssdArray array = new UssdArray();
			array.setUssdMessage(props.getProperty("UssdMessage"));
			array.setUssdReturnRequest(Boolean.valueOf(props.getProperty("UssdReturnRequest")));
			req.setUssdMessage(array);
			MakeUssdResponse rsp = stub.makeUssd(req);
			System.out.println(rsp.getUssdIdentifier());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
