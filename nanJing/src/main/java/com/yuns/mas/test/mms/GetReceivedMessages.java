/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.mms;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.mms.GetReceivedMessagesRequest;
import com.yuns.mas.platform.schema.mms.MessagePriority;
import com.yuns.mas.platform.schema.mms.MessageReference;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.1 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:58:10 PM
 */
public class GetReceivedMessages extends TestBase{

	public static void main(String []args) throws Exception{
		GetReceivedMessages test = new GetReceivedMessages();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/mms/GetReceivedMessages.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.getReceivedMessages();
	}

	private void getReceivedMessages(){
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			GetReceivedMessagesRequest req = new GetReceivedMessagesRequest();
			req.setApplicationID(props.getProperty("ApplicationId"));
			req.setPriority(MessagePriority.fromValue(props.getProperty("Priority")));


			MessageReference[] m = stub.getReceivedMessages(req);
			System.out.println(m == null);
		} catch (Exception e) {
			// TODO �Զ����� catch ��
			e.printStackTrace();
		}
	}
}
