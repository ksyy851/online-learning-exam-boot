/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.sms;

import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.sms.MessageFormat;
import com.yuns.mas.platform.schema.sms.SendMethodType;
import com.yuns.mas.platform.schema.sms.SendSmsRequest;
import com.yuns.mas.platform.schema.sms.SendSmsResponse;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 *
 * @version $Revision: 1.5 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:55:20 PM
 */
public class SendSms extends TestBase {

	public static void main(String[] args) throws Exception {
		SendSms test = new SendSms();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/sms/SendSms.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.sendSms();
	}

	private String sendSms() {

		String requestIdentifier = "";

		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			SendSmsRequest s = new SendSmsRequest();
			s.setApplicationID(props.getProperty("ApplicationID"));
			s.setDeliveryResultRequest(Boolean.parseBoolean(props
					.getProperty("DeliveryResultRequest")));
			s.setExtendCode(props.getProperty("ExtendCode"));
			String message =
				props.getProperty("Message");

			s.setMessage(new String(message.getBytes("ISO-8859-1"),"utf-8"));
			System.out.println(new String(message.getBytes("ISO-8859-1"),"utf-8"));
			s.setMessageFormat(MessageFormat.fromValue(props
					.getProperty("MessageFormat")));
			s.setSendMethod(SendMethodType.fromValue(props
					.getProperty("SendMethod")));
			String[] a = props.getProperty("Address").split(";");
			int leng= a.length;
			org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[leng];
			for(int i=0;i<leng;i++){
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(a[i]);
				ary[i]=temp;
			}
			s.setDestinationAddresses(ary);

			SendSmsResponse rep = stub.sendSms(s);
			requestIdentifier = rep.getRequestIdentifier();
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("requestIdentifier="+requestIdentifier);
		return requestIdentifier;
	}
}
