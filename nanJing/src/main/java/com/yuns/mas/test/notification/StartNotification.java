/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.notification;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.common.v2_0.CMAbility;
import com.yuns.mas.platform.schema.common.v2_0.MessageNotificationType;
import com.yuns.mas.platform.schema.notification.StartNotificationRequest;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Description:
 * @version $Revision: 1.3 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:59:42 PM
 */
public class StartNotification extends TestBase{

	public static void main(String []args) throws Exception{
		StartNotification test = new StartNotification();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/notification/StartNotification.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.startNotification();
	}

	private void startNotification(){

		try {
			URL url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);
			StartNotificationRequest startNotificationRequest = new StartNotificationRequest();
			startNotificationRequest.setApplicationId(props.getProperty("ApplicationId"));
			int i=0;
			List<MessageNotificationType> list = new ArrayList<MessageNotificationType>();
			if(!"".equals(props.getProperty("SmsCma").trim())){
				//锟斤拷锟斤拷锟斤拷锟酵拷锟斤拷锟絝
				MessageNotificationType type = new MessageNotificationType();
				type.setCMAbility(CMAbility.fromValue(props.getProperty("SmsCma")));
				//锟斤拷锟斤拷URI
				String t = props.getProperty("SmsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t);
				ary[0]=temp;
				type.setWSURI(ary);
				list.add(type);
				i=i+1;
			}
			if(!"".equals(props.getProperty("MmsCma").trim())){
				//锟斤拷锟斤拷锟斤拷锟酵拷锟斤拷锟絝
				MessageNotificationType type_2 = new MessageNotificationType();
				type_2.setCMAbility(CMAbility.fromValue(props.getProperty("MmsCma")));
				//锟斤拷锟斤拷URI
				String t_2 = props.getProperty("MmsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_2);
				ary[0]=temp;
				type_2.setWSURI(ary);
				list.add(type_2);
				i=i+1;
			}
			if(!"".equals(props.getProperty("WapCma").trim())){
				//锟斤拷锟斤拷Wap通锟斤拷锟斤拷f
				MessageNotificationType type_3 = new MessageNotificationType();
				type_3.setCMAbility(CMAbility.fromValue(props.getProperty("WapCma")));
				//锟斤拷锟斤拷URI
				String t_3 = props.getProperty("WapUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_3);
				ary[0]=temp;
				type_3.setWSURI(ary);
				list.add(type_3);
				i=i+1;
			}
			if(!"".equals(props.getProperty("LbsCma").trim())){
				//锟斤拷锟斤拷LBS通锟斤拷锟斤拷f
				MessageNotificationType type_4 = new MessageNotificationType();
				type_4.setCMAbility(CMAbility.fromValue(props.getProperty("LbsCma")));
				//锟斤拷锟斤拷URI
				String t_4 = props.getProperty("LbsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_4);
				ary[0]=temp;
				type_4.setWSURI(ary);
				list.add(type_4);
				i=i+1;
			}
			if(!"".equals(props.getProperty("UssdCma").trim())){
				//锟斤拷锟斤拷USSD通锟斤拷锟斤拷f
				MessageNotificationType type_5 = new MessageNotificationType();
				type_5.setCMAbility(CMAbility.fromValue(props.getProperty("UssdCma")));
				//锟斤拷锟斤拷URI
				String t_5 = props.getProperty("UssdUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_5);
				ary[0]=temp;
				type_5.setWSURI(ary);
				list.add(type_5);
				i=i+1;
			}
			if(!"".equals(props.getProperty("GprsCma").trim())){
				//锟斤拷锟斤拷GPRS通锟斤拷锟斤拷f
				MessageNotificationType type_6 = new MessageNotificationType();
				type_6.setCMAbility(CMAbility.fromValue(props.getProperty("GprsCma")));
				//锟斤拷锟斤拷URI
				String t_6 = props.getProperty("GprsUrl");
				org.apache.axis.types.URI [] ary=new org.apache.axis.types.URI[1];
				org.apache.axis.types.URI temp=new org.apache.axis.types.URI(t_6);
				ary[0]=temp;
				type_6.setWSURI(ary);
				list.add(type_6);
				i=i+1;
			}
			MessageNotificationType[] a = new MessageNotificationType[i];
			startNotificationRequest.setMessageNotification(list.toArray(a));
			stub.startNotification(startNotificationRequest);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
