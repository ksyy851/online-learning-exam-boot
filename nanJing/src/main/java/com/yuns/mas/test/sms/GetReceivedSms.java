/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.sms;

import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.sms.GetReceivedSmsRequest;
import com.yuns.mas.platform.schema.sms.SMSMessage;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.2 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 7:55:43 PM
 */
public class GetReceivedSms extends TestBase{

	public static void main(String []args) throws Exception{
		GetReceivedSms test = new GetReceivedSms();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/sms/GetReceivedSms.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.getReceivedSms();
	}

	private void getReceivedSms() {

		// TODO �Զ���ɷ������
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			GetReceivedSmsRequest req = new GetReceivedSmsRequest();
			req.setApplicationID(props.getProperty("ApplicationID"));

			SMSMessage[] d = stub.getReceivedSms(req);

			System.out.println("testGetReceivedSms=" + (d == null));
			for (int i = 0; i < d.length; i++) {
				SMSMessage dd = d[i];
				System.out.println(dd.getMessage());
				System.out.println(dd.getMessageFormat().toString());
				System.out.println(dd.getSenderAddress().toString());
			}
		} catch (Exception e) {
			// TODO �Զ���� catch ��
			e.printStackTrace();
		}
	}
}
