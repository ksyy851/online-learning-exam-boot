/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test.ussd;

import com.yuns.mas.test.sms.PathUtil;
import com.yuns.mas.platform.business.SiMockStub;
import com.yuns.mas.platform.schema.ussd.EndUssdRequest;
import com.yuns.mas.test.TestBase;
import org.apache.axis.client.Service;

import java.net.URL;

/**
 * Description:
 * @version $Revision: 1.2 $
 * @author yan_xt
 * @date: Nov 12, 2007
 * @time: 8:57:41 PM
 */
public class EndUssd extends TestBase {

	public static void main(String args[]) throws Exception{
		EndUssd test = new EndUssd();
		String globalFile="";
		String configFIle="";
		if(args == null || args.length == 0){
			globalFile = PathUtil.getRootPath()+"../test/global.properties";
			configFIle = PathUtil.getRootPath()+"../test/ussd/EndUssd.properties";
		}
		else{
			globalFile = args[0];
			configFIle = args[1];
		}
		test.init(globalFile, configFIle);
		test.endUssd();
	}

	private void endUssd(){
		URL url;
		try {
			url = new URL(global.getProperty("MasWsUrl"));
			Service service = new Service();
			SiMockStub stub = new SiMockStub(url, service);

			EndUssdRequest req = new EndUssdRequest();
			req.setApplicationID(props.getProperty("ApplicationID"));
			req.setUssdIdentifier(props.getProperty("UssdIdentifier"));
			req.setUssdMessage(props.getProperty("UssdMessage"));
			stub.endUssd(req);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
