/*****************************************
 *  ���а�Ȩ(c) �Ϻ��������ϵͳ���޹�˾       *
 *****************************************/
package com.yuns.mas.test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * Description:
 * @version $Revision: 1.1 $
 * @author yan_xt
 * @date: Nov 13, 2007
 * @time: 1:50:01 PM
 */
public abstract class TestBase {
	protected Properties global = new Properties();
	protected Properties props = new Properties();

	protected void init(String globalName, String fullName) throws Exception{
		InputStream in = new FileInputStream(fullName);
		props.load(in);
		InputStream globalin = new FileInputStream(globalName);
		global.load(globalin);
	}
}
