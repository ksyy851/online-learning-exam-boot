package com.yuns.course;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.yuns.course.dto.CourseDto;
import com.yuns.course.entity.CourseChapter;
import com.yuns.course.entity.CourseType;
import com.yuns.course.param.CourseParam;
import com.yuns.course.service.*;
import com.yuns.course.vo.CourseVO;
import com.yuns.study.entity.InfoDocI;
import com.yuns.study.service.IInfoDocIService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程相关-单元测试
 * </p>
 *
 * @author liuChengWen
 * @since 2020-10-26
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class CourseTests {

    @Resource
    ICourseService courseService;

    @Resource
    ICourseTypeService courseTypeService;

    @Resource
    IInfoDocIService infoDocIService;

    @Test
    public void course_insert(){
        CourseDto courseBase=new CourseDto();
        courseBase.setCourseName("单元测试课程2");
        courseBase.setIntegral(200);
        courseBase.setCourseInfo("单元测试详情2");

        EntityWrapper<CourseType> wrapper = new EntityWrapper<>();
        List<CourseType> courseTypeList=courseTypeService.selectList(wrapper);
        courseBase.setCourseType(courseTypeList.get(0).getId());

        EntityWrapper<InfoDocI> docIEntityWrapper = new EntityWrapper<>();
        List<InfoDocI> infoDocIList=infoDocIService.selectList(docIEntityWrapper);
        courseBase.setInfoDocIList(infoDocIList);

        List<CourseChapter> courseChapterList=new ArrayList<>();
        CourseChapter chapter=new CourseChapter();
        chapter.setChapterName("章节1");
        chapter.setChapterFileName("视频路径1");
        chapter.setChapterTeacherName("桑老师");
        courseChapterList.add(chapter);
        chapter.setChapterName("章节2");
        chapter.setChapterFileName("视频路径2");
        chapter.setChapterTeacherName("桑老师2");
        courseChapterList.add(chapter);
        courseBase.setCourseChapterList(courseChapterList);
        courseService.insert(courseBase);
    }

    @Test
    public void course_delete(){
        courseService.deleteById(21);
    }

    @Test
    public void course_query(){
        CourseParam courseParam=new CourseParam();
        List<CourseVO> list= (List<CourseVO>)courseService.selectCourseList(courseParam);
        System.out.println(list);
    }

}
